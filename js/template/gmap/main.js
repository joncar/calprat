// Google Map
$(function () {
	var map = new GMaps({
	el: "#map",
	lat: 41.7481267,
	lng: 1.3926781,
          zoom: 12, 
          zoomControl : true,
          zoomControlOpt: {
            style : "BIG",
            position: "TOP_LEFT"
          },
          panControl : true,
          streetViewControl : false,
          mapTypeControl: false,
          overviewMapControl: false
      });
        
      var styles = [
            {
              stylers: [
                { hue: "#00ffe6" },
                { saturation: -100 }
              ]
            }
      ];
        
      map.addStyle({
            styledMapName:"Styled Map",
            styles: styles,
            mapTypeId: "map_style"  
      });
        
      map.setStyle("map_style");

      map.addMarker({
        lat: 41.7481267,
        lng: 1.3926781,
        icon: "http://www.calprat.net/img/mapicon.png"
      });
});

