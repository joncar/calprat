$(function() {
    "use strict";
    // PAGE SCROLL
    $('body').on('click', '.page-scroll', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });

    // NUMBER INPUT
    $("input[type='number']").stepper();

    // FULLWIDTH SEARCH
    $(".ss-trigger").on('click', function() {
        $(".ss-content").addClass("ss-content-act");
        $(".bn-reserve").removeClass("bn-reserve-act");
    });
    $(".ss-close").on('click', function() {
        $(".ss-content").removeClass("ss-content-act");
    });

    $(".bn-trigger").on('click', function() {
        /*$(".bn-reserve").addClass("bn-reserve-act");
        $(".bn-overlay").addClass("active");*/
        document.location.href=URL+"iniciar-reserva";
    });

    $(".bn-close").on('click', function() {
        $(".bn-reserve").removeClass("bn-reserve-act");
        $(".bn-overlay").removeClass("active");
    });

    $(".fixed-bn").on('click', function() {
        $(".bn-reserve").addClass("bn-reserve-act");
        $(".bn-overlay").addClass("active");
    });

    // FLEXSLIDER - ROOM 1 COL
    $('.rb-slider').flexslider({
        animation: "fade",
        controlNav: true,
        directionNav: true,
        controlNav: false,
        smoothHeight: true
    });

    // DATE PICKER
    $('#datetimepicker1').datetimepicker({
        format: 'dd-mm-yyyy'
    });

    $('#datetimepicker2').datetimepicker({
        format: 'dd-mm-yyyy'
    });

    // FLEXSLIDER
    // FLEXSLIDER
    $('#gal-slider1').flexslider({
        animation: "fade",
        slideshow: false,
        //directionNav: true,
        //controlsContainer: ".dining-block",
        /*controlNav: true,
        manualControls: ".gal-nav1 li"*/
    });

    $('.flex-prev1').on('click', function() {
        $('#gal-slider1').flexslider('prev')
        return false;
    })

    $('.flex-next1').on('click', function() {
        $('#gal-slider1').flexslider('next')
        return false;
    })

    $('#gal-slider2').flexslider({
        animation: "fade",
        slideshow: false,
        //directionNav: true,
        /*controlsContainer: ".dining-block",
        controlNav: true,
        manualControls: ".gal-nav2 li"*/
    });

    $('.flex-prev2').on('click', function() {
        $('#gal-slider2').flexslider('prev')
        return false;
    })

    $('.flex-next2').on('click', function() {
        $('#gal-slider2').flexslider('next')
        return false;
    })

    $('#gal-slider3').flexslider({
        animation: "fade",
        slideshow: false,
        //directionNav: true,
        /*controlsContainer: ".dining-block",
        controlNav: true,
        manualControls: ".gal-nav3 li"*/
    });

    $('.flex-prev3').on('click', function() {
        $('#gal-slider3').flexslider('prev')
        return false;
    })

    $('.flex-next3').on('click', function() {
        $('#gal-slider3').flexslider('next')
        return false;
    })

    $('.spa-block #gal-slider').flexslider({
        animation: "fade",
        slideshow: false,
        //directionNav: true,
        controlsContainer: ".spa-block",
        //controlNav: true,
        //manualControls: ".spa-block .gal-nav li"
    });

    $('.spa-block .flex-prev').on('click', function() {
        $('.spa-block #gal-slider').flexslider('prev')
        return false;
    })

    $('.spa-block .flex-next').on('click', function() {
        $('.spa-block #gal-slider').flexslider('next')
        return false;
    })

    $('.entertain-block #gal-slider').flexslider({
        animation: "fade",
        slideshow: false,
        directionNav: true,
        controlsContainer: ".entertain-block",
        controlNav: true,
        manualControls: ".entertain-block .gal-nav li"
    });

    $('.entertain-block .flex-prev').on('click', function() {
        $('.entertain-block #gal-slider').flexslider('prev')
        return false;
    })

    $('.entertain-block .flex-next').on('click', function() {
        $('.entertain-block #gal-slider').flexslider('next')
        return false;
    })

    $('.expert-tip .slider-block #gal-slider').flexslider({
        animation: "fade",
        slideshow: false,
        directionNav: true,
        controlNav: true,
        manualControls: ".expert-tip .slider-block .gal-nav li",
        sync: ".expert-tip .slider-block #carousel",
        start: function(slider) {
            $(".slide-current-slide").text(slider.currentSlide + 1);
            $(".slide-total-slides").text("of" + slider.count)
        },
        before: function(slider) {
            $(".slide-current-slide").text(slider.animatingTo + 1)
        },
    });

    $('.expert-tip .slider-block .flex-prev').on('click', function() {
        $('.expert-tip .slider-block #gal-slider').flexslider('prev')
        return false;
    })

    $('.expert-tip .slider-block .flex-next').on('click', function() {
        $('.expert-tip .slider-block #gal-slider').flexslider('next')
        return false;
    })

    $('.expert-tip .slider-block #carousel').flexslider({
        animation: "slide",
        controlNav: true,
        animationLoop: false,
        slideshow: false,
        itemWidth: 210,
        itemMargin: 5,
        controlsContainer: $(".expert-tip .custom-controls-container"),
        customDirectionNav: $(".expert-tip .custom-navigation a"),
        asNavFor: '.expert-tip .slider-block #gal-slider',
    });

    // FLEXSLIDER
    $(".expert-tip .slider-block #gal-slider").flexslider({
        start: function(slider) {
            $(".slide-current-slide").text(slider.currentSlide + 1);
            $(".slide-total-slides").text("of" + slider.count)
        },
        before: function(slider) {
            $(".slide-current-slide").text(slider.animatingTo + 1)
        }
    });

    // OWL CAROUSEL
    var owlhome = $("#home-gallery");

    owlhome.owlCarousel({
        items: 4, //10 items above 1000px browser width
        itemsDesktop: [1000, 2], //5 items between 1000px and 901px
        itemsDesktopSmall: [900, 2], // betweem 900px and 601px
        itemsTablet: [767, 2], //2 items between 600 and 0
        itemsMobile: [600, 1],        
        autoPlay:true,   
        controlNav: true,     
        stopOnHover:false,        
        controlsContainer: $(".main-gal-slider .custom-controls-container"),
        customDirectionNav: $(".main-gal-slider .custom-navigation a"),
        asNavFor: '.main-gal-slider .slider-block #gal-slider',
    });

    $(".next").on('click', function() {        
        owlhome.trigger('owl.next');
    })
    $(".prev").on('click', function() {
        owlhome.trigger('owl.prev');
    })

    var owl = $("#gallery-villa");

    owl.owlCarousel({
        autoPlay: 3000, //Set AutoPlay to 3 seconds

        itemsCustom: [
            [0, 1],
            [450, 1],
            [600, 2],
            [700, 2],
            [1000, 3],
            [1200, 4],
            [1400, 4],
            [1600, 4]
        ]

    });
    $(".next").on('click', function() {
        owl.trigger('owl.next');
    })
    $(".prev").on('click', function() {
        owl.trigger('owl.prev');
    })

    var owl = $("#ab-carousel");
    owl.owlCarousel({

        itemsCustom: [
            [0, 1],
            [450, 1],
            [600, 1],
            [700, 1],
            [1000, 3],
            [1200, 3],
            [1400, 3],
            [1600, 3]
        ],
        navigation: false
    });
    $(".next").on('click', function() {
        owl.trigger('owl.next');
    })
    $(".prev").on('click', function() {
        owl.trigger('owl.prev');
    })

    // CUSTOM SELECT

    $('.main-grid .reservation #get_value').selectric();
    $('.select').selectric();
    $('#get_value').selectric();
    $('#get_value1').selectric();
    $('#get_value2').selectric();
    $('#get_value3').selectric();
    $('#get_value6').selectric();
    $('#get_value7').selectric();
    $('#sel_property').selectric();
    $('#basic').selectric();
    $('#basic1').selectric();

    // TOOLTIP


    $('.img-tooltip').popover({
        'trigger': 'hover',
        'html': true,
        'content': function() {
            return "<img src='" + $(this).data('imageUrl') + "'>";
        }
    });

    $('[data-toggle="tooltip"]').tooltip({
        'container': 'body'
    });

    // FLEXSLIDER

    $('#gal-slider').flexslider({
        
        directionNav: true,
        controlsContainer: ".bloggal-slider",
        controlNav: true,
        manualControls: ".bloggal-slider .gal-nav li",
    });

    $('.room-gallery .bloggal-slider .flex-prev').on('click', function() {
        $('.room-gallery .bloggal-slider #gal-slider').flexslider('prev')
        return false;
    })

    $('.room-gallery .bloggal-slider .flex-next').on('click', function() {
        $('.room-gallery .bloggal-slider #gal-slider').flexslider('next')
        return false;
    })

    // FLEXSLIDER

    $('#gal-slider1').flexslider({
        animation: "fade",
        slideshow: false,
        controlsContainer: $(".villadetail .ocean-breeze .bloggal-slider .custom-controls-container"),
        customDirectionNav: $(".villadetail .ocean-breeze .bloggal-slider .custom-navigation a"),
        directionNav: true,
        controlsContainer: ".bloggal-slider",
        controlNav: true,
        manualControls: ".villadetail .ocean-breeze .bloggal-slider .gal-nav li",
        startAt: 0,

    });

    $('.villadetail .ocean-breeze .bloggal-slider .flex-prev').on('click', function() {
        $('#gal-slider').flexslider('prev')
        return false;
    })

    $('.villadetail .ocean-breeze .bloggal-slider .flex-next').on('click', function() {
        $('#gal-slider').flexslider('next')
        return false;
    })

    // PRETTYPHOTO

    $("a[class^='prettyPhoto']").prettyPhoto({
        theme: 'pp_default'
    });

    // VIDEO
    
    $('.custom-th').on('click', function() {
        $('.custom-th').fadeOut('fast', function() {
            $("#thevideo").css("display", "block");
            player.playVideo();
        });
    });
    // TWEETS
    $('#tweetcool').tweecool({
        profile_image: false,
        username: 'envato',
        limit: 1
    });
});

// RESPONSIVE MENU
$(window).on('load', function() {
    $('#mobnav-btn').on('click', function() {
            $('.sf-menu').toggleClass("xactive");
    });

    $('.mobnav-subarrow').on('click', function() {
            $(this).parent().toggleClass("xpopdrop");
    });
});

$(window).load(function() {
    "use strict";

    // FLEXSLIDER
    $('#top-slider').flexslider({
        animation: "slide",
        controlNav: true,
        directionNav: true,
        smoothHeight: false,
        animationLoop: false
    });
    setTimeout(function(){$(".top-slider").show()},60);

    $('.b-slider').flexslider({
        animation: "slide",
        controlNav: true,
        directionNav: true,
        smoothHeight: true
    });

    // ISOTOPE - MASONRY BLOG
    var $container = $('#masonry-feed');
    $container.isotope({
        itemSelector: '.masonry-item'
    });
    var $optionSets = $('#portfolio-section .filter'),
        $optionLinks = $optionSets.find('a');
    $optionLinks.on('click', function() {
        var $this = $(this);
        if ($this.hasClass('selected')) {
            return false;
        }
        var $optionSet = $this.parents('.filter');
        $optionSet.find('.selected').removeClass('selected');
        $this.addClass('selected');
        var options = {},
            key = $optionSet.attr('data-option-key'),
            value = $this.attr('data-option-value');
        value = value === 'false' ? false : value;
        options[key] = value;
        if (key === 'layoutMode' && typeof changeLayoutMode === 'function') {
            changeLayoutMode($this, options);
        } else {
            $container.isotope(options);
        }
        return false;
    });

    // ISOTOPE - ROOMS 1 2 3
    var $container = $('#rooms-mason');
    $container.isotope({
        itemSelector: '.rm-item'
    });
    var $optionSets = $('.rm-filter'),
        $optionLinks = $optionSets.find('li a');
    $optionLinks.on('click', function() {
        var $this = $(this);
        if ($this.hasClass('selected')) {
            return false;
        }
        var $optionSet = $this.parents('.rm-filter');
        $optionSet.find('.selected').removeClass('selected');
        $this.addClass('selected');
        var options = {},
            key = $optionSet.attr('data-option-key'),
            value = $this.attr('data-option-value');
        value = value === 'false' ? false : value;
        options[key] = value;
        if (key === 'layoutMode' && typeof changeLayoutMode === 'function') {
            changeLayoutMode($this, options);
        } else {
            $container.isotope(options);
        }
        return false;
    });
});

// EXTRAS
$(".menu-trigger").on('click', function() {
    $(".navbar-default").addClass("nav-active");
    $(".menu-trigger").addClass("mt-hide");
    $(".header").css("z-index", "999999");
    $(".pre-header").addClass("ph-hide");
});

$(".menu-close").on('click', function() {
    $(".navbar-default").removeClass("nav-active");
    $(".menu-trigger").removeClass("mt-hide");
    $(".header").css("z-index", "999");
    $(".pre-header").removeClass("ph-hide");
    $(".bn-reserve").removeClass("bn-reserve-act");
});

$(".offers_1 .offer-expand").on('click', function() {
    $(this).parent().toggleClass("active");
    $(this).toggleClass("active");
    $(".offers_1 .offer-expand i").toggleClass("fa-caret-up fa-caret-down");
    $(".offers_2 , .offers_3 , .offers_4 ").removeClass("active");
    $(".offers_2 .offer-expand i, .offers_3 .offer-expand i, .offers_4 .offer-expand i").addClass("fa-caret-down");
    $(".offers_2 .offer-expand i, .offers_3 .offer-expand i, .offers_4 .offer-expand i").removeClass("fa-caret-up");
});

$(".offers_2 .offer-expand").on('click', function() {
    $(this).parent().toggleClass("active");
    $(this).toggleClass("active");
    $(".offers_2 .offer-expand i").toggleClass("fa-caret-up fa-caret-down");
    $(".offers_1 , .offers_3 , .offers_4 ").removeClass("active");
    $(".offers_1 .offer-expand i, .offers_3 .offer-expand i, .offers_4 .offer-expand i").addClass("fa-caret-down");
    $(".offers_1 .offer-expand i, .offers_3 .offer-expand i, .offers_4 .offer-expand i").removeClass("fa-caret-up");

});

$(".offers_3 .offer-expand").on('click', function() {
    $(this).parent().toggleClass("active");
    $(this).toggleClass("active");
    $(".offers_3 .offer-expand i").toggleClass("fa-caret-up fa-caret-down");
    $(".offers_1 , .offers_2 , .offers_4 ").removeClass("active");
    $(".offers_1 .offer-expand i, .offers_2 .offer-expand i, .offers_4 .offer-expand i").addClass("fa-caret-down");
    $(".offers_1 .offer-expand i, .offers_2 .offer-expand i, .offers_4 .offer-expand i").removeClass("fa-caret-up");
});

$(".offers_4 .offer-expand").on('click', function() {
    $(this).parent().toggleClass("active");
    $(this).toggleClass("active");
    $(".offers_4 .offer-expand i").toggleClass("fa-caret-up fa-caret-down");
    $(".offers_1 , .offers_2 , .offers_3").removeClass("active");
    $(".offers_2 .offer-expand i, .offers_3 .offer-expand i, .offers_1 .offer-expand i").addClass("fa-caret-down");
    $(".offers_2 .offer-expand i, .offers_3 .offer-expand i, .offers_1 .offer-expand i").removeClass("fa-caret-up");
});

$(".offers_1 .btn-active").on('click', function() {
    $(".offers_1").toggleClass("active");
    $("offers_1 .offer-expand").toggleClass("active");
    $(".offers_1 .offer-expand i").toggleClass("fa-caret-up fa-caret-down");
    $(".offers_2 , .offers_3 , .offers_4 ").removeClass("active");
    $(".offers_2 .offer-expand i, .offers_3 .offer-expand i, .offers_4 .offer-expand i").addClass("fa-caret-down");
    $(".offers_2 .offer-expand i, .offers_3 .offer-expand i, .offers_4 .offer-expand i").removeClass("fa-caret-up");
});

$(".offers_2 .btn-active").on('click', function() {
    $(".offers_2").toggleClass("active");
    $("offers_2 .offer-expand").toggleClass("active");
    $(".offers_2 .offer-expand i").toggleClass("fa-caret-up fa-caret-down");
    $(".offers_1 , .offers_3 , .offers_4 ").removeClass("active");
    $(".offers_1 .offer-expand i, .offers_3 .offer-expand i, .offers_4 .offer-expand i").addClass("fa-caret-down");
    $(".offers_1 .offer-expand i, .offers_3 .offer-expand i, .offers_4 .offer-expand i").removeClass("fa-caret-up");

});

$(".offers_3 .btn-active").on('click', function() {
    $(".offers_3").toggleClass("active");
    $("offers_3 .offer-expand").toggleClass("active");
    $(".offers_3 .offer-expand i").toggleClass("fa-caret-up fa-caret-down");
    $(".offers_1 , .offers_2 , .offers_4 ").removeClass("active");
    $(".offers_1 .offer-expand i, .offers_2 .offer-expand i, .offers_4 .offer-expand i").addClass("fa-caret-down");
    $(".offers_1 .offer-expand i, .offers_2 .offer-expand i, .offers_4 .offer-expand i").removeClass("fa-caret-up");
});

$(".offers_4 .btn-active").on('click', function() {
    $(".offers_4").toggleClass("active");
    $("offers_4 .offer-expand").toggleClass("active");
    $(".offers_4 .offer-expand i").toggleClass("fa-caret-up fa-caret-down");
    $(".offers_1 , .offers_2 , .offers_3").removeClass("active");
    $(".offers_2 .offer-expand i, .offers_3 .offer-expand i, .offers_1 .offer-expand i").addClass("fa-caret-down");
    $(".offers_2 .offer-expand i, .offers_3 .offer-expand i, .offers_1 .offer-expand i").removeClass("fa-caret-up");
});



function init_date_picker(bookingInlineDatepicker,mainBookingForm){
    bookingInlineDatepicker.datepicker('remove');
    var checkInDate             = null,
        checkOutDate            = null,
        oneDay                  = 24 * 60 * 60 * 1000;
    bookingInlineDatepicker.datepicker({
            format:    "dd-mm-yyyy",
            autoclose: true,
            startDate: new Date(),            
            weekStart: 1,
            inputs:    jQuery('#booking-date-range-inline .check-in, #booking-date-range-inline .check-out'),
            beforeShowDay: function(date){                
                if(typeof(reservas)!=='undefined'){
                    var da = date;
                    var dia = da.getDate()<10?'0'+da.getDate():da.getDate();
                    var m = (da.getMonth()+1);
                    m = m<10?'0'+m:m;
                    var d = da.getFullYear()+'-'+m+'-'+dia;                    
                    return reservas.indexOf(d)>=0?{enabled:false}:date;
                }
                return date;
            },
            beforeShowMonth:function(date){
                var months = [''];
                var month = date.getMonth()+1;
                return 'ENERO';
            }
    }).on('changeDate', function (e) {        
            var newDate    = new Date(e.date),
                    newDateStr = ("0" + newDate.getDate()).slice(-2) + '-' + ("0" + (newDate.getMonth() + 1)).slice(-2) + '-' + newDate.getFullYear();            
            if (e.target.className.search('check-in') < 0) {
                    checkInDate = newDate.getTime();
                    mainBookingForm.find('input[name="hasta"]').val(newDateStr);                    
            }
            else {
                    checkOutDate = newDate.getTime();
                    mainBookingForm.find('input[name="desde"]').val(newDateStr);                                        
            }
            var diffDays = Math.round(Math.abs((checkInDate - checkOutDate) / (oneDay)));
            //mainBookingForm.find('.duration').val((diffDays >= 1 ? diffDays + ' Nits' : diffDays + ' Nits'));
            mainBookingForm.find('.duration').val(diffDays);
            if(typeof(max_nits)!=='undefined' && diffDays>max_nits){
                $("#messagecontainer").html('<div class="alert alert-danger">Disculpe, pero la oferta solo es válida para '+max_nits+' Noches</div>');
                $("#realizarreserva").attr('disabled',true);
            }else if(diffDays==0){
                $("#messagecontainer").html('');
                $("#realizarreserva").attr('disabled',true);
            }else{
                $("#messagecontainer").html('');
                $("#realizarreserva").attr('disabled',false);
            }
            buscar_habitaciones();            
    });
}


var bookingInlineDatepicker = jQuery('#booking-date-range-inline');
var mainBookingForm         = jQuery('#room-information-form');


init_date_picker(bookingInlineDatepicker,mainBookingForm);

$(document).on('change','#get_value',function(){
    var max = parseInt($(this).find('option:selected').data('max'));
    var sel = '';    
    for(var i=1;i<=max;i++){    
        sel+='<option value="'+i+'">'+i+'</option>';
    }
    $("#get_value1").html(sel);
    $('#get_value1').selectric();
    calcular();
});
$(document).on('change',"#get_value1",function(){
    calcular();
});

function buscar_habitaciones(){    
    $.get(URL+'habitacion/reservaFrontend/get_habitaciones/',{desde:$('input[name="desde"]').val(),hasta:$('input[name="hasta"]').val()},function(data){        
        $("#habitacionContent .selectric-wrapper").remove();
        $("#habitacionContent").append(data);
        $('#get_value').selectric();
        onSearch = '';
    });
}

function calcular(){
    var precio = parseFloat($("#get_value").find('option:selected').data('price'));
    var abono = precio*0.25;
    /*var dias = parseInt($(".duration").val());
    var adultos = parseInt($("#get_value1").val());
    console.log(precio);
    console.log(dias);
    console.log(adultos);
    if(!isNaN(precio) && !isNaN(dias) && !isNaN(adultos)){
        $("#precio").val(precio*dias*adultos);
    }*/  
    $("#precio").val(precio);
    $("#abono").val(abono);
    $("#precioLabel").html(precio+'€');
    $("#abonoLabel").html(abono+'€');
    fillFields();
}

if($("input[name='dias']").val()!=='0'){
    calcular();  
}


function fillFields(){
    var nombre = $("#get_value3").val()+'. '+$("input[name='nombre']").val()+' '+$("input[name='apellido']").val();
    $("#nombre-label").html(nombre);
    $("#email-label").html($("input[name='email']").val());
    $("#direccion-label").html($("input[name='direccion']").val());
    $("#telefono-label").html($("input[name='telefono']").val());
    $("#whatsapp-label").html($("input[name='whatsapp']").val());
    $("#habitacion-label").html($("#get_value option:selected").html());
    $("#personas-label").html($("#get_value1").val()+'/'+$("#get_value2").val());
    $("#fecha-label").html($("input[name='desde']").val()+'/'+$("input[name='hasta']").val());
    var historia = $("#inlineRadio5").prop('checked')?'SI':'NO';
    $("#historia-label").html(historia);
    var extras = $("#inlineRadio1").prop('checked')?$("#inlineRadio1").val():'';
    extras+= extras!=='' && $("#inlineRadio2").prop('checked')?', ':'';
    extras += $("#inlineRadio2").prop('checked')?$("#inlineRadio2").val():'';
    $("#extras-label").html(extras);
    $("#extras").val(extras);
    $("#observacion-label").html($("textarea[name='observacion']").val());
} 

$("input,textarea,select").on('change',fillFields);

$('.input-daterange').datepicker({
    format:    "dd-mm-yyyy",
    autoclose: true,
    startDate: new Date(),

            weekStart: 1,
            beforeShowDay: function(date){

                if(typeof(reservas)!=='undefined'){
                    var da = date;
                    var dia = da.getDate()<10?'0'+da.getDate():da.getDate();
                    var m = (da.getMonth()+1);
                    m = m<10?'0'+m:m;
                    var d = da.getFullYear()+'-'+m+'-'+dia;                    
                    return reservas.indexOf(d)>=0?{enabled:false}:date;
                }
                return date;
            },
            beforeShowMonth:function(date){
                var months = [''];
                var month = date.getMonth()+1;
                return 'ENERO';
            }
});


//Calendarios de disponiblidad

var year = new Date().getFullYear();
var month = new Date().getMonth();
$("#datetimepicker11-inline").datepicker({
            format:    "dd-mm-yyyy",
            startDate: new Date(year,month,1),
            weekStart: 1,
            beforeShowDay: function(date){

                if(typeof(reservas)!=='undefined'){
                    var da = date;
                    var dia = da.getDate()<10?'0'+da.getDate():da.getDate();
                    var m = (da.getMonth()+1);
                    m = m<10?'0'+m:m;
                    var d = da.getFullYear()+'-'+m+'-'+dia;                    
                    return reservas.indexOf(d)>=0?{enabled:false}:date;
                }
                return date;
            }  
});

month+=1;
if(month>12){
    month = 1;
    year++;
}

$("#datetimepicker12-inline").datepicker({
            format:    "dd-mm-yyyy",
            startDate: new Date(year,month,1),
            weekStart: 1,
            beforeShowDay: function(date){

                if(typeof(reservas)!=='undefined'){
                    var da = date;
                    var dia = da.getDate()<10?'0'+da.getDate():da.getDate();
                    var m = (da.getMonth()+1);
                    m = m<10?'0'+m:m;
                    var d = da.getFullYear()+'-'+m+'-'+dia;                    
                    return reservas.indexOf(d)>=0?{enabled:false}:date;
                }
                return date;
            }  
});

month+=1;
if(month>12){
    month = 1;
    year++;
}

$("#datetimepicker13-inline").datepicker({
            format:    "dd-mm-yyyy",
            startDate: new Date(year,month,1),      
            weekStart: 1,
            beforeShowDay: function(date){

                if(typeof(reservas)!=='undefined'){
                    var da = date;
                    var dia = da.getDate()<10?'0'+da.getDate():da.getDate();
                    var m = (da.getMonth()+1);
                    m = m<10?'0'+m:m;
                    var d = da.getFullYear()+'-'+m+'-'+dia;                    
                    return reservas.indexOf(d)>=0?{enabled:false}:date;
                }
                return date;
            }  
});

month+=1;
if(month>12){
    month = 1;
    year++;
}

$("#datetimepicker14-inline").datepicker({
            format:    "dd-mm-yyyy",
            startDate: new Date(year,month,1),  
            weekStart: 1,
            beforeShowDay: function(date){

                if(typeof(reservas)!=='undefined'){
                    var da = date;
                    var dia = da.getDate()<10?'0'+da.getDate():da.getDate();
                    var m = (da.getMonth()+1);
                    m = m<10?'0'+m:m;
                    var d = da.getFullYear()+'-'+m+'-'+dia;                    
                    return reservas.indexOf(d)>=0?{enabled:false}:date;
                }
                return date;
            }  
});

month+=1;
if(month>12){
    month = 1;
    year++;
}

$("#datetimepicker15-inline").datepicker({
            format:    "dd-mm-yyyy",
            startDate: new Date(year,month,1),      
            weekStart: 1,
            beforeShowDay: function(date){

                if(typeof(reservas)!=='undefined'){
                    var da = date;
                    var dia = da.getDate()<10?'0'+da.getDate():da.getDate();
                    var m = (da.getMonth()+1);
                    m = m<10?'0'+m:m;
                    var d = da.getFullYear()+'-'+m+'-'+dia;                    
                    return reservas.indexOf(d)>=0?{enabled:false}:date;
                }
                return date;
            }  
});

month+=1;
if(month>12){
    month = 1;
    year++;
}

$("#datetimepicker16-inline").datepicker({
            format:    "dd-mm-yyyy",
            startDate: new Date(year,month,1),      
            weekStart: 1,
            beforeShowDay: function(date){

                if(typeof(reservas)!=='undefined'){
                    var da = date;
                    var dia = da.getDate()<10?'0'+da.getDate():da.getDate();
                    var m = (da.getMonth()+1);
                    m = m<10?'0'+m:m;
                    var d = da.getFullYear()+'-'+m+'-'+dia;                    
                    return reservas.indexOf(d)>=0?{enabled:false}:date;
                }
                return date;
            }  
});

month+=1;
if(month>12){
    month = 1;
    year++;
}

$("#datetimepicker17-inline").datepicker({
            format:    "dd-mm-yyyy",
            startDate: new Date(year,month,1),      
            weekStart: 1,
            beforeShowDay: function(date){

                if(typeof(reservas)!=='undefined'){
                    var da = date;
                    var dia = da.getDate()<10?'0'+da.getDate():da.getDate();
                    var m = (da.getMonth()+1);
                    m = m<10?'0'+m:m;
                    var d = da.getFullYear()+'-'+m+'-'+dia;                    
                    return reservas.indexOf(d)>=0?{enabled:false}:date;
                }
                return date;
            }  
});

month+=1;
if(month>12){
    month = 1;
    year++;
}

$("#datetimepicker18-inline").datepicker({
            format:    "dd-mm-yyyy",
            startDate: new Date(year,month,1),          
            weekStart: 1,
            beforeShowDay: function(date){

                if(typeof(reservas)!=='undefined'){
                    var da = date;
                    var dia = da.getDate()<10?'0'+da.getDate():da.getDate();
                    var m = (da.getMonth()+1);
                    m = m<10?'0'+m:m;
                    var d = da.getFullYear()+'-'+m+'-'+dia;                    
                    return reservas.indexOf(d)>=0?{enabled:false}:date;
                }
                return date;
            }  
});

month+=1;
if(month>12){
    month = 1;
    year++;
}

$("#datetimepicker19-inline").datepicker({
            format:    "dd-mm-yyyy",
            startDate: new Date(year,month,1),      
            weekStart: 1,
            beforeShowDay: function(date){

                if(typeof(reservas)!=='undefined'){
                    var da = date;
                    var dia = da.getDate()<10?'0'+da.getDate():da.getDate();
                    var m = (da.getMonth()+1);
                    m = m<10?'0'+m:m;
                    var d = da.getFullYear()+'-'+m+'-'+dia;                    
                    return reservas.indexOf(d)>=0?{enabled:false}:date;
                }
                return date;
            }  
});

month+=1;
if(month>12){
    month = 1;
    year++;
}

$("#datetimepicker110-inline").datepicker({
            startDate: new Date(year,month,1),
            format:    "dd-mm-yyyy",
            weekStart: 1,
            beforeShowDay: function(date){

                if(typeof(reservas)!=='undefined'){
                    var da = date;
                    var dia = da.getDate()<10?'0'+da.getDate():da.getDate();
                    var m = (da.getMonth()+1);
                    m = m<10?'0'+m:m;
                    var d = da.getFullYear()+'-'+m+'-'+dia;                    
                    return reservas.indexOf(d)>=0?{enabled:false}:date;
                }
                return date;
            }  
});

month+=1;
if(month>12){
    month = 1;
    year++;
}

$("#datetimepicker111-inline").datepicker({
            format:    "dd-mm-yyyy",
            startDate: new Date(year,month,1),      
            weekStart: 1,
            beforeShowDay: function(date){

                if(typeof(reservas)!=='undefined'){
                    var da = date;
                    var dia = da.getDate()<10?'0'+da.getDate():da.getDate();
                    var m = (da.getMonth()+1);
                    m = m<10?'0'+m:m;
                    var d = da.getFullYear()+'-'+m+'-'+dia;                    
                    return reservas.indexOf(d)>=0?{enabled:false}:date;
                }
                return date;
            }  
});

month+=1;
if(month>12){
    month = 1;
    year++;
}

$("#datetimepicker112-inline").datepicker({
            format:    "dd-mm-yyyy",
            startDate: new Date(year,month,1),       
            weekStart: 1,

            beforeShowDay: function(date){

                if(typeof(reservas)!=='undefined'){
                    var da = date;
                    var dia = da.getDate()<10?'0'+da.getDate():da.getDate();
                    var m = (da.getMonth()+1);
                    m = m<10?'0'+m:m;
                    var d = da.getFullYear()+'-'+m+'-'+dia;                    
                    return reservas.indexOf(d)>=0?{enabled:false}:date;
                }
                return date;
            }  
});


$("#datetimepickeragenda").datepicker({
            format:    "dd-mm-yyyy",
            startDate: new Date(year,month,1),       
            weekStart: 1,
            content:'asd',
            showOnFocus:function(){
                alert("");
            },
            beforeShowDay: function(date){               
                return{
                    enabled:false,
                }
            }  
});


window.onload = function () {    
    // delete main-preloader
    var mainPreloader = document.querySelector('.main-preloader');
    if (mainPreloader) {
        mainPreloader.classList.add('window-is-loaded');
        setTimeout(function () {
            if(mainPreloader.parentNode!==null){
                mainPreloader.parentNode.removeChild(mainPreloader);
            }
        }, 200);
    }
    // /delete main-preloader
}