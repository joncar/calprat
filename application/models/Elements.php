<?php 
class Elements extends CI_Model{
	function __construct(){
		parent::__construct();
	}

	function agenda(){
		$this->db->where('idioma',$_SESSION['lang']);
		$this->db->order_by('fecha','ASC');
		//$this->db->where('fecha >=',date("Y-m-d"));
		$agenda = $this->db->get('agenda');
		foreach($agenda->result() as $n=>$g){
			$agenda->row($n)->foto = base_url('img/agenda/'.$g->foto);
			$agenda->row($n)->fecha = strftime('%d %B %Y ',strtotime($g->fecha));
			$agenda->row($n)->explicacion = strip_tags($g->explicacion);
		}
		return $agenda;
	}

	function blog(){
		$this->db->where('idioma',$_SESSION['lang']);
		$this->db->order_by('fecha','DESC');		
		$galeria = $this->db->get_where('blog',array('status'=>1));
		foreach($galeria->result() as $n=>$g){
			$galeria->row($n)->foto = base_url('img/blog/'.$g->foto);
			$galeria->row($n)->link = base_url('blog/'.toUrl($g->id.'-'.$g->titulo));
			$galeria->row($n)->comentarios = $this->db->get_where('comentarios',array('blog_id'=>$g->id))->num_rows();
			$galeria->row($n)->texto = cortar_palabras(strip_tags($g->texto),70).'...';
			$galeria->row($n)->fecha = date("d/m/Y",strtotime($g->fecha));
		}
		return $galeria;
	}

	function banner(){
		$this->db->where('idioma',$_SESSION['lang']);
		$this->db->order_by('orden','ASC');		
		$galeria = $this->db->get('banner');		
		foreach($galeria->result() as $n=>$g){
			$galeria->row($n)->foto = base_url('img/Homepage_Resort_INDEX/'.$g->foto);
		}
		return $galeria;
	}

	function galeria(){
		$this->db->where('idioma',$_SESSION['lang']);
		$this->db->order_by('orden','ASC');		
		$this->db->where('categoria_galeria_id','1');
		$galeria = $this->db->get('galeria');
		foreach($galeria->result() as $n=>$g){
			$galeria->row($n)->foto = base_url('img/galeria/'.$g->foto);
		}
		return $galeria;
	}

	function galeria_patio(){
		$this->db->where('idioma',$_SESSION['lang']);
		$this->db->order_by('orden','ASC');		
		$this->db->where('categoria_galeria_id','2');
		$galeria = $this->db->get('galeria');
		foreach($galeria->result() as $n=>$g){
			$galeria->row($n)->foto = base_url('img/galeria/'.$g->foto);
		}
		return $galeria;
	}

	function galeria_joc(){
		$this->db->where('idioma',$_SESSION['lang']);
		$this->db->order_by('orden','ASC');		
		$this->db->where('categoria_galeria_id','3');
		$galeria = $this->db->get('galeria');
		foreach($galeria->result() as $n=>$g){
			$galeria->row($n)->foto = base_url('img/galeria/'.$g->foto);
		}
		return $galeria;
	}

	function galeria_piscina(){
		$this->db->where('idioma',$_SESSION['lang']);
		$this->db->order_by('orden','ASC');		
		$this->db->where('categoria_galeria_id','4');
		$galeria = $this->db->get('galeria');
		foreach($galeria->result() as $n=>$g){
			$galeria->row($n)->foto = base_url('img/galeria/'.$g->foto);
		}
		return $galeria;
	}

	
}