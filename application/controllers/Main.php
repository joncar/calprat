<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
ob_start();
session_name('calfusters');
session_start();

class Main extends CI_Controller {
    //public $theme = 'themes/bristol/';
    public $theme = '';
    protected $types_lang = array(
        'es'=>'spanish',
        'en'=>'english',
        'fr'=>'french',
        'ru'=>'russian',
        'it'=>'italian',
        'ca'=>'catalan',
        'ch'=>'zh_cn',        
        'al'=>'german',        
    );
    public $class_lang = array(
        'es'=>array('Español','spain'),
        'en'=>array('English','ingles'),
        'fr'=>array('Frances','frances'),
        'ru'=>array('Pусский','ruso'),
        'it'=>array('Italiano','italiano'),
        'ca'=>array('Catalá','catalunya'),
        'ch'=>array('中国','chino'),        
        'al'=>array('Deutsch','aleman'),        
    );
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->helper('form');
        $this->load->helper('h');
        $this->load->database();
        $this->load->model('user');
        $this->load->model('bdsource');
        $this->load->library('redsysAPI');
        $this->load->library('traduccion');
        date_default_timezone_set('Europe/Madrid');
        
        setlocale(LC_ALL, 'ca_ES', 'Catalan_Spain', 'Catalan');    
        if(empty($_SESSION['user']) && empty($_SESSION['tmpsesion'])){
            $_SESSION['tmpsesion'] = '1'.date("his");
        }
        
        if(!isset($_SESSION['lang'])){
            setlocale(LC_ALL, 'ca_ES', 'Catalan_Spain', 'Catalan');    
            $_SESSION['lang'] = 'ca';            
        }
        switch($_SESSION['lang']){
            case 'ca':
                $_SESSION['langLabel'] = 'CAT';
            break;
            case 'es':
                $_SESSION['langLabel'] = 'ESP';
            break;
            case 'en':
                $_SESSION['langLabel'] = 'ENG';
            break;
            case 'fr':
                $_SESSION['langLabel'] = 'FRA';
            break;
        }
        $this->theme = $_SESSION['lang'].'/';
        if(file_exists(APPPATH.'language/'.$this->types_lang[$_SESSION['lang']].'/web_lang.php')){
            $this->lang->load('web',$this->types_lang[$_SESSION['lang']]);
            $_SESSION['default_lang'] = $this->types_lang[$_SESSION['lang']];
        }
    }
    
    protected function crud_function($x,$y,$controller = ''){
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
            $crud = new ajax_grocery_CRUD($controller);
            $crud->set_theme('bootstrap2');
            $table = !empty($this->as[$this->router->fetch_method()])?$this->as[$this->router->fetch_method()]:$this->router->fetch_method();
            $crud->set_table($table);
            $crud->set_subject(ucfirst($this->router->fetch_method()));
            if(!empty($this->norequireds)){
                $crud->norequireds = $this->norequireds;
            }
            $crud->required_fields_array();
            
            if(method_exists('main',$this->router->fetch_method()))
             $crud = call_user_func(array('main',$this->router->fetch_method()),$crud,$x,$y);
            return $crud;
        } 
    
    
    
    function get_entries(){
        $blog = new Bdsource();
        $blog->limit = array('3','0');
        $blog->order_by = array('fecha','DESC');        
        $blog->init('blog');
        foreach($this->blog->result() as $n=>$b){
            $this->blog->row($n)->link = site_url('blog/'.toURL($b->id.'-'.$b->titulo));
            $this->blog->row($n)->foto = base_url('img/blog/'.$b->foto);
            $this->blog->row($n)->comentarios = $this->db->get_where('comentarios',array('blog_id'=>$b->id))->num_rows();                
            $this->blog->row($n)->categorias = $this->db->get_where('blog_categorias',array('id'=>$b->blog_categorias_id));
        }
        if($this->blog->num_rows()>0){
            $this->blog->tags = $this->blog->row()->tags;
        }
    }

    public function index() {
        $this->get_entries();
        $this->loadView(array('view'=>'main', 'blog'=>$this->blog));
    }

    public function success($msj) {
        return '<div class="alert alert-success">' . $msj . '</div>';
    }

    public function error($msj) {
        return '<div class="alert alert-danger">' . $msj . '</div>';
    }
    
    public function traduccion($idioma = 'ca'){        
        $_SESSION['lang'] = $idioma;        
        if(empty($_GET['url'])){
            redirect($_SERVER['HTTP_REFERER']);
        }else{
            redirect($_GET['url']);
        }
    }

    public function login() {
        if (!$this->user->log) {
            if (!empty($_POST['email']) && !empty($_POST['pass'])) {
                $this->db->where('email', $this->input->post('email'));
                $r = $this->db->get('user');
                if ($this->user->login($this->input->post('email', TRUE), $this->input->post('pass', TRUE))) {
                    if ($r->num_rows() > 0 && $r->row()->status == 1) {
                        if (!empty($_POST['remember']))
                            $_SESSION['remember'] = 1;
                        if (empty($_POST['redirect']))
                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="' . site_url('panel') . '"</script>');
                        else
                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="' . $_POST['redirect'] . '"</script>');
                    } else
                        $_SESSION['msj'] = $this->error('El usuario se encuentra bloqueado, comuniquese con un administrador para solucionar su problema');
                } else
                    $_SESSION['msj'] = $this->error('Usuario o contrasena incorrecta, intente de nuevo.');
            } else
                $_SESSION['msj'] = $this->error('Debe completar todos los campos antes de continuar');

            if (!empty($_SESSION['msj']))
                header("Location:" . base_url('panel'));
        } else
            header("Location:" . base_url('panel'));
    }

    public function unlog() {
        $this->user->unlog();
        header("Location:" . site_url());
    }

    function getHead($page){
        $ajustes = $this->db->get('ajustes')->row();
        $stocookie = '<link href="'.base_url('js/stocookie/stoCookie.css').'">';        
        $page = str_replace('</head>',$stocookie.'</head>',$page);
        return $page;
    }

    function getBody($page){
        $ajustes = $this->db->get('ajustes')->row();      
        
        $stocookie = html_entity_decode($ajustes->analytics);
        $page = str_replace('</body>',$stocookie.'</body>',$page);

        $stocookie = '<script src="'.base_url('js/stocookie/stoCookie.min.js').'"></script>';
        $stocookie.= html_entity_decode($ajustes->cookies);        
        $page = str_replace('</body>',$stocookie.'</body>',$page);

        
        return $page;
    }

    public function loadView($param = array('view' => 'main')) {        
        if (is_string($param))
            $param = array('view' => $param);
        $ajustes = $this->db->get('ajustes')->row();
        if(is_object($param)){
            $param = (array)$param;
        }
        if(empty($param['title'])){
            $param['title'] = $ajustes->titulo;
        }

        $param['title'] = ucfirst(str_replace('_',' ',$param['title']));
        $param['favicon'] = $ajustes->favicon;
        $param['keywords'] = $ajustes->keywords;
        $param['description'] = $ajustes->description;
        $page = $this->load->view('template', $param,true);
        $page = $this->getHead($page);
        $page = $this->getBody($page);
        $page = $this->traduccion->traducir($page,$_SESSION['lang']);

        $apikey = @file_get_contents('https://api.openweathermap.org/data/2.5/weather?lat=41.5814136&lon=1.6155993&units=metric&appid=a2106f98aee176f14e90c55757ab0409');   
        $apikey = json_decode($apikey);
        $temperatura = (int)$apikey->main->temp;
        $icon = base_url('img/clima/'.$apikey->weather[0]->icon.'.png');
        

        $page = str_replace('[temp]',$temperatura,$page);               
        $page = str_replace('[iconotemp]',$icon,$page); 
        echo $page;
    }

    public function loadViewAjax($view, $data = null) {
        $view = $this->valid_rules($view);
        $this->load->view($view, $data);
    }

    function error404() {
        $this->loadView(array('view' => 'errors/403'));
    }
    
    function enviarcorreo($usuario,$idnotificacion,$destinatario = ''){
            $mensaje = $this->db->get_where('notificaciones',array('id'=>$idnotificacion))->row();
            
            foreach($usuario as $n=>$v){             
             $mensaje->texto = str_replace('{'.$n.'}',$v,$mensaje->texto);
             $mensaje->titulo = str_replace('{'.$n.'}',$v,$mensaje->titulo);   
            }            
            if(empty($destinatario)){
                correo($usuario->email,$mensaje->titulo,$mensaje->texto);
            }
            else{
                correo($destinatario,$mensaje->titulo,$mensaje->texto);
            }
        }
        
        

}
