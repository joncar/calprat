<!--Breadcrumb Section-->
<section id="breadcrumb-section" data-bg-img="../assets/img/breadcrumb.jpg">
    <div class="inner-container container">
        <div class="ravis-title">
            <div class="inner-box">
                <div class="title">Reserva realitzada</div>
                <div class="sub-title">la seva reserva s'ha realitzat amb èxit</div>
            </div>
        </div>

        <div class="breadcrumb">
            <ul class="list-inline">
                <li><a href="<?= site_url() ?>">Inici</a></li>
                <li class="current"><a href="#">Reserva</a></li>
            </ul>
        </div>
    </div>
</section>
<!--End of Breadcrumb Section-->

<section id="booking-section" class="step-3">
    <div class="inner-container container">
        <div class="col-md-4 l-sec">
            <div class="ravis-title-t-2">
                <div class="title"><span>Informació de la Reserva</span></div>
            </div>
            <div class="check-in-out-container">
                <div class="check-in-out-box">
                    <div class="title">Entrada :</div>
                    <div class="value"><?= strftime("%d-%m-%Y",strtotime($reserva->desde)) ?></div>
                </div>
                <div class="check-in-out-box">
                    <div class="title">Sortida :</div>
                    <div class="value"><?= strftime("%d-%m-%Y",strtotime($reserva->hasta)) ?></div>
                </div>
            </div>

            <div class="selected-room-container">
                <?php $total = 0; ?>
                <?php foreach($reserva->habitaciones->result() as $n=>$r): ?> 
                    
                    <?php                                                 
                        $habitacion = $r->habitacion; 
                        $dias = $r->dias; 
                        $total+= $r->precio;
                        //echo $r['desde'].' '.$r['hasta'];
                    ?>
                    <div class="selected-room-box">
                        <div class="room-title">
                            <div class="title">Habitació:</div>
                            <div class="value"><?= $habitacion->habitacion_nombre ?></div>
                        </div>
                        <div class="room-title">
                            <div class="title">Duració :</div>
                            <div class="value"><?= $dias+1 ?> Dies y <?= $dias ?> Nits</div>
                        </div>
                        <div class="adult-count">
                            <div class="title">Adults :</div>
                            <div class="value"><?= $r->adultos; ?></div>
                        </div>
                        <div class="child-count">
                            <div class="title">Nens :</div>
                            <div class="value"><?= $r->infantes; ?></div>
                        </div>
                        <div class="price">
                            <?php                             
                                echo str_replace('.00','',$r->precio);
                            ?>€
                        </div>
                    </div>
                <?php endforeach ?>
            </div>

            <div class="price-details-container">
                <div class="price-detail-box">
                    <div class="title">Habitacions i Serveis:</div>
                    <div class="value"><?= $total ?>€</div>
                </div>
                <!--<div class="price-detail-box">
                    <div class="title">Vat 8% :</div>
                    <div class="value">$117.00</div>
                </div>-->
                <div class="price-detail-box total">
                    <div class="title">Preu total :</div>
                    <div class="value"><?= $total ?>€</div>
                </div>
            </div>
        </div>
        <div class="col-md-8 r-sec">
            <div class="inner-box">
                <div class="steps">
                    <ul class="list-inline">
                        <li>Tria data</li>
                        <li>Tria habitació</li>
                        <li>Fer la reserva</li>
                        <li class="active">Confirmació</li>
                    </ul>
                </div>

                <div id="confirmation-message">
                    <div class="ravis-title-t-2">
                        <div class="title"><span>Reservation Complete</span></div>
                        <div class="sub-title">Details of your reservation request was sent to your email </div>
                    </div>
                    <div class="desc">
                        For more information you can contact us via <a href="<?= site_url('p/contacte') ?>contact form</a> of website
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
