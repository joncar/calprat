<?php 
    //Borrar cuando se tenga TPV
    //$html = $this->load->view('proximamente',array(),TRUE,'paginas');
    $html = $this->load->view($this->theme.'reserva',array(),TRUE,'paginas');
    $html = str_replace('[startlabel]','DATA D\'ARRIBADA',$html);
    $html = str_replace('[endlabel]','DATA SORTIDA',$html);
    $html = str_replace('[oferta]','',$html);
    $habitacion = '<select name="habitacion" id="get_value"><option value="">Si us plau selecciona una habitació</option>';
    foreach($this->db->get('habitaciones')->result() as $h){
        $sel = !empty($_GET['habitacion']) && $_GET['habitacion']==$h->id?'selected':'';
        $habitacion.= '<option value="'.$h->id.'" data-max="'.(int)$h->max_personas.'" data-price="'.$h->precio_desde.'" '.$sel.'>'.$h->habitacion_nombre.'</option>';
    }
    $habitacion.= '</select>';
    $html = str_replace('[habitaciones]',$habitacion,$html);
    
    $adultos = empty($_GET['adultos'])?'':$_GET['adultos'];
    $infantes = empty($_GET['infantes'])?'':$_GET['infantes'];

    $html = str_replace('[adultos]',form_dropdown('adultos',array(1=>1,2=>2,3=>3,4=>4),$adultos,'id="get_value1"'),$html);
    //$html = str_replace('[nens]',form_dropdown('infantes',array(0=>0,1=>1,2=>2,3=>3,4=>4),$infantes,'id="get_value2"'),$html);
    $html = str_replace('[nens]','<input type="hidden" name="infantes" value="0">',$html);
    if(!empty($_SESSION['msj'])){
        $html = str_replace('[msg_response]','<div class="alert alert-danger">'.$_SESSION['msj'].'</div>',$html);
        unset($_SESSION['msj']);
    }else{
        $html = str_replace('[msg_response]','',$html);
    }
    $desde = empty($_GET['desde'])?'':'value="'.$_GET['desde'].'"';
    $hasta = empty($_GET['hasta'])?'':'value="'.$_GET['hasta'].'"';    
    $html = str_replace('[desde]',$desde,$html);
    $html = str_replace('[hasta]',$hasta,$html);

    $dias = 0;
    if(!empty($_GET['desde']) && !empty($_GET['hasta'])){
        $datetime1 = new DateTime(date("Y-m-d",strtotime($_GET['desde'])));
        $datetime2 = new DateTime(date("Y-m-d",strtotime($_GET['hasta'])));
        $interval = $datetime1->diff($datetime2);
        $dias = $interval->format('%a');
    }

    $html = str_replace('[dias]',$dias,$html);

    echo $html;
?>


<script>
    var reservas = <?php 
        $re = array();
        foreach($reservas->result() as $r){
            $datetime1 = new DateTime($r->desde);
            $datetime2 = new DateTime($r->hasta);
            $interval = $datetime1->diff($datetime2);
            $dias = $interval->format('%a días');
            for($i=0;$i<$dias;$i++){
                $fecha = date("Y-m-d",strtotime("+$i days ".$r->desde));
                if(!in_array($fecha, $re)){
                    $re[] = $fecha;
                }
            }
        }
        
        //Concatenamos las reservas locales
        if(!empty($_SESSION['reserva'])){
            foreach($_SESSION['reserva'] as $r){
                $datetime1 = new DateTime($r['desde']);
                $datetime2 = new DateTime($r['hasta']);
                $interval = $datetime1->diff($datetime2);
                $dias = $interval->format('%a días');
                for($i=0;$i<$dias;$i++){
                    $fecha = date("Y-m-d",strtotime("+$i days ".$r['desde']));
                    if(!in_array($fecha, $re)){
                        $re[] = $fecha;
                    }
                }
            }
        }
        echo json_encode($re);
    ?>;

    function goFotos(){        
        var body = $("html, body");
        body.stop().animate({scrollTop:$("#galeria").offset().top}, '500', 'swing');
    }
</script>