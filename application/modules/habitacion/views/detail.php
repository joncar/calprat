<?php 
    $page = $this->load->view($this->theme.'habitacion',array(),TRUE,'paginas');    

    $detail->slider = '';
    $detail->previews = '';
    foreach($detail->fotos->result() as $f){
        $detail->slider.= '<li><img src="'.base_url('img/habitaciones/'.$f->foto).'" alt=""></li>';
        $detail->previews.= '<li><img src="'.base_url('img/habitaciones/'.$f->foto).'" alt=""></li>';
    }

    $detail->fotos22 = '<div class="row" style="margin-bottom:40px;" uk-lightbox>';
    foreach($detail->fotos2->result() as $n=>$f){        
        if($n>3){
            $detail->fotos22.= '<div class="col-xs-12 col-sm-3" style="margin-top:29px"><a href="'.base_url('img/habitaciones/'.$f->foto).'"><img src="'.base_url('img/habitaciones/'.$f->foto).'" alt="" style="width:100%"></a></div>';
        }
    }
    $detail->fotos22.= '</div>';

    $this->db->select('ofertas.*,habitaciones.habitacion_nombre');
    $this->db->join('habitaciones','habitaciones.id = ofertas.habitaciones_id');
    $ofertas = $this->db->get_where('ofertas',array('habitaciones_id'=>$detail->id));
    if($ofertas->num_rows()>0){
        foreach($ofertas->result() as $n=>$f){        
            $ofertas->row($n)->fondo = base_url('img/entorno/'.$f->fondo);
            $ofertas->row($n)->in = $n==0?'in':'';
        }
        $detail->habitacion_oferta = $this->load->view('habitacion_oferta',array(),TRUE,'paginas');
        $detail->habitacion_oferta = $this->querys->fillFields($detail->habitacion_oferta,array('ofertas'=>$ofertas));
    }else{
        $detail->habitacion_oferta = '';
    }
    $detail->historia = strip_tags($detail->historia);
    $detail->precio_desde = number_format($detail->precio_desde,2,',','.');
    $detail->precio_desde = str_replace(',00','',$detail->precio_desde);    
    foreach($detail as $n=>$d){
        if(is_string($d)){
            $page = str_replace('['.$n.']',$d,$page);
        }        
    }

    $page = str_replace('[habitacion_id]','<input type="hidden" name="habitacion" value="'.$detail->id.'">',$page);
    $page = $this->load->view('read',array('page'=>$page),TRUE,'paginas');
    echo $page;
?>

<script>
    var reservas = <?php 
        $re = array();
        foreach($reservas->result() as $r){
            $datetime1 = new DateTime($r->desde);
            $datetime2 = new DateTime($r->hasta);
            $interval = $datetime1->diff($datetime2);
            $dias = $interval->format('%a días');
            for($i=0;$i<$dias;$i++){
                $fecha = date("Y-m-d",strtotime("+$i days ".$r->desde));
                if(!in_array($fecha, $re)){
                    $re[] = $fecha;
                }
            }
        }
        
        //Concatenamos las reservas locales
        if(!empty($_SESSION['reserva'])){
            foreach($_SESSION['reserva'] as $r){
                $datetime1 = new DateTime($r['desde']);
                $datetime2 = new DateTime($r['hasta']);
                $interval = $datetime1->diff($datetime2);
                $dias = $interval->format('%a días');
                for($i=0;$i<$dias;$i++){
                    $fecha = date("Y-m-d",strtotime("+$i days ".$r['desde']));
                    if(!in_array($fecha, $re)){
                        $re[] = $fecha;
                    }
                }
            }
        }
        echo json_encode($re);
    ?>;

    function goFotos(){        
        var body = $("html, body");
        body.stop().animate({scrollTop:$("#galeria").offset().top}, '500', 'swing');
    }
</script>