<?= $output ?>
<script>
    var habitacion = undefined;
    var ajustes = <?= json_encode((array)$this->db->get('ajustes')->row()) ?>;
    var reservas = [];
    $("#field-habitaciones_id").on("change",function(){
        //Consultar precio
        $.post('<?= base_url('habitacion/habitaciones/json_list') ?>',{"search_field[]":"habitaciones.id","search_text[]":$(this).val()},function(data){
            habitacion = JSON.parse(data);
            habitacion = habitacion[0];
            $("#field-desde").val('<?= date("d/m/Y") ?>');
            $("#field-hasta").val('<?= date("d/m/Y",strtotime(date("Y-m-d").' +1 days')) ?>');
            $("#field-precio").val(habitacion.precio);
            $("#field-adultos").val(habitacion.max_personas);
            $("#field-infantes").val(habitacion.max_infantes);
            $("#field-bebes").val(habitacion.max_bebes);
            $("#field-dias").val(1);
            var precio = parseFloat(habitacion.precio_desde);
            console.log(precio);
            precio+= parseInt($("#field-infantes").val())*ajustes.precio_infante;
            console.log(precio);
            precio+= parseInt($("#field-bebes").val())*ajustes.precio_bebe;
            console.log(precio);
            
            $("#field-precio").val(precio);
        });
        $.get('<?= base_url('habitacion/reservaFrontend/iniciar_reserva/') ?>/'+$(this).val(),{},function(data){
            reservas = JSON.parse(data);
            init_calendar();
        });
    });
    
    $("#field-desde").on("change",function(){
        getDays();
    });
    $("#field-hasta").on("change",function(){
        getDays();
    });
    
    function getDays(){
        var desde = $("#field-desde").val();
        var hasta = $("#field-hasta").val();
        if(desde!=='' && hasta!==''){
            desde = desde.split('/');
            hasta = hasta.split('/');
            desde = new Date(desde[2]+'-'+desde[1]+'-'+desde[0]);
            hasta = new Date(hasta[2]+'-'+hasta[1]+'-'+hasta[0]);
            var dias = parseInt((hasta.getTime()-desde.getTime())/(24*3600*1000));
            $("#field-dias").val(dias);
            var precio = dias*habitacion.precio_desde;
            precio+= parseInt($("#field-infantes").val())*ajustes.precio_infante;
            precio+= parseInt($("#field-bebes").val())*ajustes.precio_bebe;
            $("#field-precio").val(precio);
        }
    }
    
    function init_calendar(){
        var bookingInlineDatepicker = $(".datepicker-input");
        bookingInlineDatepicker.datepicker('remove');
        var checkInDate             = null,
            checkOutDate            = null,
            oneDay                  = 24 * 60 * 60 * 1000;
        bookingInlineDatepicker.datepicker({
            format:    "dd/mm/yyyy",
            autoclose: true,
            startDate: new Date(),            
            weekStart: 1,            
            beforeShowDay: function(date){
                if(typeof(reservas)!=='undefined'){
                    var da = date;
                    var dia = da.getDate()<10?'0'+da.getDate():da.getDate();
                    var d = da.getFullYear()+'-'+(da.getMonth()+1)+'-'+dia;
                    return reservas.indexOf(d)>=0?{enabled:false}:date;
                }
                return date;
            },
            beforeShowMonth:function(date){
                var months = [''];
                var month = date.getMonth()+1;
                return 'ENERO';
            }
    }).on('changeDate', function (e) {
            /*var newDate    = new Date(e.date),
                    newDateStr = ("0" + newDate.getDate()).slice(-2) + '-' + ("0" + (newDate.getMonth() + 1)).slice(-2) + '-' + newDate.getFullYear();
            if (e.target.className.search('check-in') < 0) {
                    checkInDate = newDate.getTime();
                    $('#field-hasta').val(newDateStr);                    
            }
            else {
                    checkOutDate = newDate.getTime();
                    mainBookingForm.find('.check-in').children('input').val(newDateStr);
                    mainBookingForm.find('.check-in').find('.value').text(newDateStr);
            }
            var diffDays = Math.round(Math.abs((checkInDate - checkOutDate) / (oneDay)));
            mainBookingForm.find('.duration').find('.value').text((diffDays >= 1 ? diffDays + ' Nits' : diffDays + ' Nits'));*/
    });
    }
</script>
