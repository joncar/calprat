<!-- 404 -->
    <section id="content" class="blog">
        <div class="container">
            <div class="error-page">
                <div class="error-bg">
                    <div class="error-img"></div>
                    <div class="col-md-6 no-padding">
                    </div>
                    <div class="col-md-6 no-padding">
                        <div class="error-detial">
                            
                            <div class="col-xs-12 col-sm-12 col-md-12 col-centered center-block">
                                <!-- Row Start -->
                                <div class="row">
                                    <div class="title">
                                        <h1>Reserva finalitzada</h1>                                        
                                        <div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
                                            <span><img class="img-responsive" src="http://hipo.tv/calprat/img/cap.png" alt=""></span>
                                        </div>
                                    </div>
                                </div>
                                <!-- Row End -->
                            </div>                            
                            <p>                                
                                Els detalls de la reserva s'han enviat al teu email
                            </p>
                            <p>
                                Per més informació omple el <a href="<?= base_url('p/contacte') ?>">formulari de contacte</a> de la web
                            </p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>