<div>
  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="<?= $accion!='edit'?'active':'' ?>"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Ficha</a></li>
    <li role="presentation" class="<?= $accion=='edit'?'active':'' ?>"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Habitaciones</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane <?= $accion!='edit'?'active':'' ?>" id="home"><?= $output ?></div>
    <div role="tabpanel" class="tab-pane <?= $accion=='edit'?'active':'' ?>" id="profile"><?= $habitaciones ?></div>
  </div>

</div>
