<?php 
    // Se crea Objeto
    $this->load->library('redsysAPI');
    $miObj = new RedsysAPI;    
    
    //$merchantCode 	="185013521"; //Testing
    $merchantCode = '185013521'; //Produccion

    //$key = 'iQB6NQj1x7CTF9vk6RXHv0Fw2c4IyWDJ'; //Test
    $key = 'T40fJZxWyyg++ILz6sOFOANBCEmV+FJq'; //Produccion
    
    //$url = "https://sis-t.redsys.es:25443/sis/realizarPago"; //Test
    $url = "https://sis.redsys.es/sis/realizarPago"; //Produccion
    
    //$url = "https://sis.redsys.es/sis/realizarPago";    
    $terminal 		="001";
    $amount 		=round($reserva->abonado,0)*100;
    $currency 		="978";
    $transactionType    ="0";
    $merchantURL 	=base_url('reservar/procesarPago/'.$reserva->id);
    $urlOK 		=base_url('reserva_realizada/'.$reserva->id);
    $urlKO 		=base_url('reservar/pagoKo');
    $order      =date("is").$reserva->id;   
    $miObj->setParameter("DS_MERCHANT_AMOUNT",$amount);
    $miObj->setParameter("DS_MERCHANT_ORDER",strval($order));
    $miObj->setParameter("DS_MERCHANT_MERCHANTCODE",$merchantCode);
    $miObj->setParameter("DS_MERCHANT_CURRENCY",$currency);
    $miObj->setParameter("DS_MERCHANT_TRANSACTIONTYPE",$transactionType);
    $miObj->setParameter("DS_MERCHANT_TERMINAL",$terminal);
    $miObj->setParameter("DS_MERCHANT_MERCHANTURL",$merchantURL);
    $miObj->setParameter("DS_MERCHANT_URLOK",$urlOK);		
    $miObj->setParameter("DS_MERCHANT_URLKO",$urlKO);
    $version="HMAC_SHA256_V1";   
    $request = "";
    $params = @$miObj->createMerchantParameters();
    $signature = @$miObj->createMerchantSignature($key);
    //echo $reserva->abonado;
    //echo ' '.$amount;
    /*
        Tarjeta de pruebas: 4548 8120 4940 0004
     *  Venc: 12/20
     *  CVV2: 285
     *  Auth Bancaria: 123456
    */
?>
<form id="myForm" action="<?= $url ?>" method="post">
                                           
    <input type="hidden" name="Ds_SignatureVersion" value="<?php echo $version; ?>"/>
    <input type="hidden" name="Ds_MerchantParameters" value="<?php echo $params; ?>"/>
    <input type="hidden" name="Ds_Signature" value="<?php echo $signature; ?>" />
    <div class="form-row place-order" align="center">
        <?= img('img/pay.jpg') ?><br/>
        Redireccionando a el banco por favor espere...        
    </div>
    <!--<button type="submit">Enviar</button>-->
</form>
<script>
    document.getElementById('myForm').submit();
</script>
