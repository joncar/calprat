<?php
$u = $user->row();
$porcentaje = $u->abonado != $u->total ? '50%' : '100%';
$pendiente = $u->total - $u->abonado;
?>
<table width="540" height="35" style="border:1px solid gray">
    <tbody>
        <tr style="border:1px solid gray">
            <td style="width: 480px; text-align: center;"><strong>Habitaci&oacute;n</strong></td>
            <td style="width: 60px; text-align: center;"><strong>Precio</strong></td>
        </tr>
        <?php
        foreach ($user->result() as $u):
            ?>
            <tr>
                <td style="border-right:1px solid gray">
                    <?= img('img/habitaciones/' . $u->foto, 'vertical-align:middle; width:80px') ?>
                    <span style="vertical-align:middle">
                        <?= $u->habitacion_nombre . ' por ' . $u->dias . ' noches ' ?>
                        <?= date("d/m/Y", strtotime($u->desde)) . ' - ' . date("d/m/Y", strtotime($u->hasta)) ?>
                    </span>
                </td>
                <td style="text-align:right"><?= str_replace('.00', '', $u->total) ?> €</td>
            </tr>
        <?php endforeach; ?>
        
        <tr>
            <td style="border-top:1px solid gray; width: 480px; text-align: left;">Cantidad adultos</td>
            <td style="border-top:1px solid gray; width: 60px; text-align: right;"><?= $u->adultos ?></td>
        </tr>
        <tr>
            <td style="border-top:1px solid gray; width: 480px; text-align: left;">Cantidad niños</td>
            <td style="border-top:1px solid gray; width: 60px; text-align: right;"><?= $u->infantes ?></td>
        </tr>
        <tr>
            <td style="border-top:1px solid gray; width: 480px; text-align: left;">Cantidad bebés</td>
            <td style="border-top:1px solid gray; width: 60px; text-align: right;"><?= $u->bebes ?></td>
        </tr>
        <!--<tr>
            <td style="border-top:1px solid gray; width: 480px; text-align: left;">Precio por desayuno</td>
            <td style="border-top:1px solid gray; width: 60px; text-align: right;"><?= str_replace('.00', '', ($u->adultos * 5)) ?>€</td>
        </tr>-->
        <tr>
            <td style="border-top:1px solid gray; width: 480px; text-align: left;">Abono (<?= $porcentaje ?>)</td>
            <td style="border-top:1px solid gray; width: 60px; text-align: right;"><?= str_replace('.00', '', $u->abonado) ?>€</td>
        </tr>
        <tr style="border:1px solid gray">
            <td style="width: 480px; text-align: left;">Pendiente por pagar</td>
            <td style="width: 60px; text-align: right;"><?= str_replace('.00', '', $pendiente) ?>€</td>
        </tr>
        <tr>
            <td style="border-top:1px solid gray; width: 480px; text-align: left;"><strong>Total Reserva</strong></td>
            <td style="border-top:1px solid gray; width: 60px; text-align: right;"><strong><?= str_replace('.00', '', $u->total) ?>€</strong></td>
        </tr>
    </tbody>
</table>
<p></p>