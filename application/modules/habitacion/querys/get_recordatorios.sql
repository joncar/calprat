DROP VIEW IF EXISTS get_recordatorios;
CREATE VIEW get_recordatorios as 
SELECT 
DATE(NOW()) as hoy,
reservas.desde,
reservas.hasta,
reservas.abonado,
reservas.precio,
(reservas.precio-reservas.abonado) as importe,
resta_dias.vigencia,
ajustes_recordatorio.limite,
notificaciones_recordatorios.id as IDNotif,
user.nombre,
user.apellido,
user.email,
reservas.idioma,
reservas.id as reservaid
FROM reservas_habitaciones as rh
INNER JOIN reservas ON rh.reservas_id = reservas.id
INNER JOIN resta_dias ON resta_dias.id = reservas.id
INNER JOIN user ON user.id = reservas.user_id
INNER JOIN ajustes_recordatorio
INNER JOIN notificaciones_recordatorios ON notificaciones_recordatorios.idioma = reservas.idioma
WHERE 
reservas.desde >= DATE(NOW()) AND
resta_dias.vigencia >= ajustes_recordatorio.limite AND
reservas.pagado = 1 AND
reservas.precio <> reservas.abonado