<?php 
    require_once APPPATH.'controllers/Main.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();
        }
        
        public function listado(){
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
            $crud = new ajax_grocery_CRUD();
            $crud->set_table("habitaciones");
            $crud->set_subject("Habitaciones");
            $crud->set_theme("crud");
            $crud->callback_column('historia',function($val,$row){
                $idioma = json_decode($row->idiomas);
                return $_SESSION['lang']!='ca'?$idioma->{$_SESSION['lang']}->historia:$val;
            });
            $crud = $crud->render('','application/modules/habitacion/views');
            $crud->view = 'habitaciones';
            $this->loadView($crud);
        }

        public function ofertas(){
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
            $crud = new ajax_grocery_CRUD();
            $crud->set_table("ofertas");
            $crud->set_subject("ofertas");
            $crud->set_theme("ofertas");            
            $crud = $crud->render('','application/modules/habitacion/views');
            $crud->view = 'habitaciones';
            $this->loadView($crud);
        }

        
        public function read($id,$hash = ''){
            $id = explode('-',$id);
            $id = $id[0];
            if(is_numeric($id)){
                $habitaciones = new Bdsource();
                $habitaciones->where('id',$id);
                $habitaciones->init('habitaciones',TRUE);
                if(isset($this->habitaciones)){
                    $this->habitaciones->link = site_url('habitacion/'.toURL($this->habitaciones->id.'-'.$this->habitaciones->habitacion_nombre));
                    $this->habitaciones->foto = base_url('img/habitaciones/'.$this->habitaciones->foto);
                    //$this->db->limit('5');
                    $this->db->order_by('priority','ASC');
                    $this->db->limit(4);
                    $this->habitaciones->fotos = $this->db->get_where('habitaciones_fotos',array('habitaciones_id'=>$this->habitaciones->id));

                    $this->db->order_by('priority','ASC');                    
                    $this->habitaciones->fotos2 = $this->db->get_where('habitaciones_fotos',array('habitaciones_id'=>$this->habitaciones->id));
                    

                    if($habitaciones->num_rows()>0){
                        $habitaciones->visitas++;
                        $habitaciones->save();
                    }else{
                        throw new Exception("La habitació que desitja mirar, ha estat eliminada o no existeix",404);
                        die();
                    }

                    $relacionados = new Bdsource();
                    $relacionados->limit = array(3);                 
                    $relacionados->where('id !=',$this->habitaciones->id);
                    $relacionados->where('id !=',5);
                    $relacionados->order_by = array('id','desc');
                    $relacionados->init('habitaciones',FALSE,'relacionados');
                    foreach($this->relacionados->result() as $n=>$b){
                        $this->relacionados->row($n)->link = site_url('habitacion/'.toURL($b->id.'-'.$b->habitacion_nombre));
                        $this->relacionados->row($n)->foto = base_url('img/habitaciones/'.$b->foto);
                    }
                    $this->load->model('querys');
                    $reservas = $this->querys->get_reservas($this->habitaciones->id);
                    $idioma = $this->habitaciones->idiomas;
                    $idioma = json_decode($idioma);
                    $this->habitaciones->historia = $_SESSION['lang']!='ca'?$idioma->{$_SESSION['lang']}->historia:$this->habitaciones->historia;
                    $this->loadView(
                        array(
                            'view'=>'detail',
                            'detail'=>$this->habitaciones,
                            'title'=>$this->habitaciones->habitacion_nombre,
                            'relacionados'=>$this->relacionados,
                            'bodyclass'=>'room-detials',
                            'reservas'=>$reservas
                        ));
                }
                else{
                    throw new Exception('La habitació que desitja mirar, ha estat eliminada o no existeix',404);
                }
            }else{
                throw new Exception('La habitació que desitja mirar, ha estat eliminada o no existeix',404);
            }
        }

        function precios(){
            $this->loadView(array('view'=>'precios','title'=>'Preus'));
        }
    }
?>
