<?php 
    require_once APPPATH.'controllers/Main.php';    
    class Recordatorios extends Main{
        function __construct() {
            parent::__construct();
            $this->load->model('querys');
        }
        
        function index(){
            $recordatorios = $this->db->get('get_recordatorios');
            foreach($recordatorios->result() as $r){
            	$this->db->select('reservas_habitaciones.*, habitaciones.*, reservas.email, reservas.nombre, reservas.idioma,  reservas.apellido, reservas.abonado, reservas.precio as total');
                $this->db->join('habitaciones','habitaciones.id = reservas_habitaciones.habitaciones_id');
                $this->db->join('reservas','reservas.id = reservas_habitaciones.reservas_id');                    
                $reserva = $this->db->get_where('reservas_habitaciones',array('reservas_id'=>$r->reservaid));
        		$r->reserva = $this->load->view('emails/'.$r->idioma,array('user'=>$reserva),TRUE);
        		$link = base_url('habitacion/reservaFrontend/abonar/'.$r->reservaid);
        		$r->enlace = '<a href="'.$link.'">'.$link.'</a>';
            	$this->enviarcorreo($r,$r->IDNotif);
            	$this->enviarcorreo($r,$r->IDNotif,'info@calfusterdetous.com');
            }
        }
        
    }
?>
