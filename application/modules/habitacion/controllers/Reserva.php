<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Reserva extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        public function reservas($x = '',$y = ''){            
            $crud = $this->crud_function('','');                   
            $crud->display_as('id','#Reserva')
                 ->display_as('user_id','Huesped');
            $crud->field_type('user_id','hidden')
                 ->field_type('pais','hidden',1);
            $crud->field_type('idioma','dropdown',array('ca'=>'Catalan','es'=>'Español','en'=>'Ingles'));
            $crud->columns('id','nombre','apellido','email','telefono','desde','hasta','fecha_reserva','abonado','precio','pagado');            
            $crud->callback_column('se8701ad4',function($val,$row){
                return '<a href="'.base_url('seguridad/user/edit/'.$row->user_id).'"><i class="fa fa-search"></i> '.$val.'</a>';
            });
            if($crud->getParameters()=='add'){                
                $crud->field_type('desde','hidden')
                     ->field_type('hasta','hidden')
                     ->field_type('fecha_reserva','hidden',date("Y-m-d"))
                     ->field_type('precio','hidden')
                     ->field_type('abonado','hidden');
            }            
            $crud->callback_before_insert(array($this,'ainsert'));
            $crud->callback_before_update(array($this,'ainsert'));
            if($crud->getParameters()!='list'){
                $crud->set_lang_string('insert_success_message','Su reserva se ha almacenado <script>document.location.href="'.base_url('habitacion/reserva/reservas_habitaciones/{id}/add').'";</script>');
            }
            $action = $crud->getParameters();
            $crud = $crud->render();
            if($action!='list'){
                if($action=='edit' && is_numeric($y)){
                    $this->as['reservas'] = 'reservas_habitaciones';
                    $habitaciones = $this->reservas_habitaciones($y,'get_header');
                    $habitaciones->unset_read()->unset_print()->unset_export();
                    $habitaciones->set_url('habitacion/reserva/reservas_habitaciones/'.$y.'/');
                    $habitaciones = $habitaciones->render(1);   
                    $crud->js_files = array_merge($crud->js_files,$habitaciones->js_files);
                    $habitaciones = $habitaciones->output;
                }else{
                    $habitaciones = '<div class="alert alert-warning">Primero ingrese la ficha para poder asignar las habitaciones</div>';
                }
                $crud->output = $this->load->view('crud_habitaciones',array('output'=>$crud->output,'accion'=>$action,'habitaciones'=>$habitaciones),TRUE);
            }else{
                $crud->output = $this->load->view('listado_reservas',array('output'=>$crud->output),TRUE);
            }
            $this->loadView($crud);
        }

        function modReservas(){            
            $crud = new ajax_grocery_crud();
            $crud->set_table('reservas_habitaciones');
            $crud->set_theme('bootstrap2');
            $crud->set_subject('reserva');
            $crud->callback_after_update(array($this,'ainserth'));
            $crud->unset_add()
                 ->unset_delete()
                 ->unset_print()
                 ->unset_export()
                 ->unset_read()
                 ->unset_list();
            $crud = $crud->render();
            $this->loadView($crud);            
        }
        
        public function ainsert($post){
            $this->db = get_instance()->db;
            $user = $this->db->get_where('user',array('email'=>$post['email']));
            if($user->num_rows()==0){
                $this->db->insert('user',
                        array(
                            'nombre'=>$post['nombre'],
                            'apellido'=>$post['apellido'],
                            'telefono'=>$post['telefono'],
                            'email'=>$post['email'],
                            'password'=>md5('12345678'),
                            'fecha_registro'=>date("Y-m-d"),
                            'fecha_actualizacion'=>date("Y-m-d"),
                            'status'=>1,
                            'admin'=>0
                        ));
                $userid = $this->db->insert_id();
            }else{
                $userid = $user->row()->id;
                $this->db->update('user',
                        array(
                            'nombre'=>$post['nombre'],
                            'apellido'=>$post['apellido'],
                            'telefono'=>$post['telefono'],                            
                            'fecha_actualizacion'=>date("Y-m-d"),
                        ),array('id'=>$userid));
            }
            $post['user_id'] = $userid;
            return $post;
        }
        
        function reservas_habitaciones($x = "",$y = ''){
            if(is_numeric($x)){                
                $crud = $this->crud_function('','');                   
                $crud->field_type('reservas_id','hidden',$x);
                $crud->where('reservas_id',$x);
                $crud->set_subject('habitaciones');
                $crud->set_relation('habitaciones_id','habitaciones','habitacion_nombre');
                $crud->display_as('habitaciones_id','Habitacion');                        
                $crud->display_as('reservas_id','#Reserva');    
                $crud->set_rules('desde','DESDE','required|callback_not_has_reserved');
                $crud->set_lang_string('insert_success_message','Su habitacion se ha almacenado <script>document.location.href="'.base_url('habitacion/reserva/reservas/edit/'.$x).'";</script>');
                $crud->callback_after_insert(array($this,'ainserth'));
                //$crud->callback_after_update(array($this,'ainserth'));
                $crud->unset_edit()->unset_read()->unset_print()->unset_export();
                if($y!='get_header'){
                    $crud = $crud->render();
                    $crud->output = $this->load->view('crud_detalles',array('output'=>$crud->output),TRUE);
                    $this->loadView($crud);
                }else{
                    return $crud;
                }
            }
        }
        
        function not_has_reserved(){
            $this->load->model('querys');         
            $tienereserva = $this->querys->hasReserva($_POST['habitaciones_id'],$_POST['desde'],$_POST['hasta']);            
            if($tienereserva){
                $this->form_validation->set_message('not_has_reserved','Habitación no disponible para la fecha solicitada ');
                return true;
            }            
            $this->form_validation->set_message('not_has_reserved','Habitación no disponible para la fecha solicitada');
            return false;
        }
        
        function ainserth($post,$primary){
            $this->db = get_instance()->db;            
            $this->db->select('reservas_habitaciones.*, habitaciones.precio_desde');
            $this->db->join('reservas','reservas.id = reservas_habitaciones.reservas_id');
            $this->db->join('habitaciones','habitaciones.id = reservas_habitaciones.habitaciones_id');
            $total = 0;
            $reserva = $this->db->get_where('reservas_habitaciones',array('reservas.id'=>$post['reservas_id']));
            foreach($reserva->result() as $r){
                $precio = $r->precio_desde;
                $precio += !empty($r->desayuno)?$r->adultos*5:0;
                $precio += !empty($r->desayuno)?$r->infantes*5:0;
                $precio+= $precio*$r->dias;
                $total+= $precio;
            }
            
            $this->db->update('reservas',array(
                'precio'=>$total,
                'desde'=>$this->db->query('select MAX(desde) as desde from reservas_habitaciones where reservas_id = '.$post['reservas_id'])->row()->desde,
                'hasta'=>$this->db->query('select MAX(hasta) as hasta from reservas_habitaciones where reservas_id = '.$post['reservas_id'])->row()->hasta,
            ),array('id'=>$post['reservas_id']));
            //Ver si tiene replica
            $habitacion = $this->db->get_where('habitaciones',array('id'=>$post['habitaciones_id']))->row();
            if(!empty($habitacion->comparte_con)){
                $data = $post;
                $data['precio'] = 0;
                $data['habitaciones_id'] = $habitacion->comparte_con;
                $this->db->delete('reservas_habitaciones',array('habitaciones_id'=>$habitacion->comparte_con,'precio'=>0,'reservas_id'=>$post['reservas_id']));
                $this->db->insert('reservas_habitaciones',$data);
            }
        }
        
        public function pagos(){
            $crud = $this->crud_function('','');   
            $crud->set_relation('reservas_id','reservas','id');
            $crud->display_as('reservas_id','#Reserva');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function recordatorios(){
            $crud = $this->crud_function('','');            
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function ofertas(){
            $crud = $this->crud_function('','');      
            $crud->set_relation('habitaciones_id','habitaciones','habitacion_nombre');
            $crud->field_type('detalle','tags');
            $crud->set_field_upload('fondo','img/entorno');
            $crud->columns('titulo','subtitulo','precio');
            $crud = $crud->render();
            $this->loadView($crud);
        }

        public function bloqueos_fechas(){
            $crud = $this->crud_function('','');            
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        
        
    }
?>
