<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Habitacion extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        public function habitaciones($x = '',$y = ''){
            $crud = $this->crud_function('','');
            $crud->field_type('servicios','tags');
            $crud->field_type('vistas','tags');
            $crud->field_type('desayuno','checkbox');
            if($x=='json_list'){
                $crud->display_as('precio_desde','precio_desde')
                     ->display_as('max_personas','max_personas')
                     ->display_as('max_infantes','max_infantes')
                     ->display_as('max_bebes','max_bebes');
            }else{
                $crud->display_as('precio_desde','Precio/Noche')
                     ->display_as('max_personas','Adults')
                     ->display_as('max_infantes','Nens')
                     ->display_as('max_bebes','Nadons');
                 $crud->columns('foto','habitacion_nombre','precio_desde');
            }
            $crud->set_field_upload('foto','img/habitaciones');
            $crud->set_field_upload('foto_main','img/habitaciones');                        
            $crud->add_action('<i class="fa fa-image"></i> Banner','',base_url().'habitacion/habitaciones_fotos/');
            $crud->add_action('<i class="fa fa-globe"></i> Traducir','',base_url('habitacion/habitaciones/traducir').'/');
            $crud->set_traducir();
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function habitaciones_fotos(){
            $this->load->library('image_crud');
            
            $crud = new image_CRUD();
            $crud->set_table('habitaciones_fotos')
                 ->set_url_field('foto')
                 ->set_relation_field('habitaciones_id')
                 ->set_ordering_field('priority')
                 ->set_image_path('img/habitaciones');
            $crud->module = 'habitacion';
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
    }
?>
