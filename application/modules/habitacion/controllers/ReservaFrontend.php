<?php 
    require_once APPPATH.'controllers/Main.php';    
    class ReservaFrontend extends Main{
        function __construct() {
            parent::__construct();
            $this->load->model('querys');
        }
        
        
        function iniciar_oferta($id){
            if(is_numeric($id)){
                $oferta = $this->db->get_where('ofertas',array('id'=>$id));
                if($oferta->num_rows()>0){
                    $oferta = $oferta->row();
                    $_GET = array(
                        'room-type'=>$oferta->cantidad_personas,
                        'guest'=>$oferta->cantidad_infantes,
                        'bebes'=>$oferta->cantidad_bebes
                    );
                    $this->loadView(array('title'=>'Iniciar Reservas','view'=>'iniciar-oferta','bodyclass'=>'booking','reservas'=>$this->querys->get_reservas(1),'oferta'=>$oferta));
                }else{
                    redirect(base_url());
                }
            }else{
                redirect(base_url());
            }
        }
        
        function iniciar_reserva($ajax = ''){            
            if(!empty($ajax)){
                echo json_encode($this->querys->get_reservas($ajax,true));
            }else{
                $this->loadView(array('title'=>'Iniciar Reservas','view'=>'iniciar-reserva','bodyclass'=>'booking','reservas'=>$this->querys->get_reservas(1)));
            } 
        }
        
        function habitacion($ajax = ''){
            if(isset($_GET['desayuno']) && isset($_GET['bebes']) && isset($_GET['room-type']) && isset($_GET['guest']) && is_numeric($_GET['room-type']) && is_numeric($_GET['guest']) && !empty($_GET['start']) && !empty($_GET['end'])){
                if($_GET['start'] != $_GET['end']){
                    $datetime1 = new DateTime(date("Y-m-d",strtotime($_GET['start'])));
                    $datetime2 = new DateTime(date("Y-m-d",strtotime($_GET['end'])));
                    $interval = $datetime1->diff($datetime2);
                    $dias = $interval->format('%a días');
                    if($dias>=2){
                        $this->load->model('querys');
                        $this->loadView(array('title'=>'Seleccio de habitacio','view'=>'selHabitacion','bodyclass'=>'booking','reservas'=>$this->querys->get_reservas(1)));
                    }else{
                        $_SESSION['msj'] = "només està permès 2 nits com a mínim per reservar";
                        redirect('iniciar-reserva');
                    }
                }else{
                    $_SESSION['msj'] = "El dia de entrada no pot ser igual al del dia de sortida";
                    redirect('iniciar-reserva');
                }
            }else{
                $_SESSION['msj'] = "sis plau completi les dades solicitades per realitzar la seva solicitut";
                redirect('iniciar-reserva');
            }
        }
        
        public function confirmar(){
            if(isset($_GET['desayuno']) && empty($_GET['oferta']) && isset($_GET['room-type']) && isset($_GET['guest']) && is_numeric($_GET['room-type']) && is_numeric($_GET['guest']) && !empty($_GET['start']) && !empty($_GET['end']) && !empty($_GET['habitacion']) && is_numeric($_GET['habitacion'])){
                $habitacion = $this->db->get_where('habitaciones',array('id'=>$_GET['habitacion']));
                if($habitacion->num_rows()>0){
                    $datetime1 = new DateTime(date("Y-m-d",strtotime($_GET['start'])));
                    $datetime2 = new DateTime(date("Y-m-d",strtotime($_GET['end'])));
                    $interval = $datetime1->diff($datetime2);
                    $dias = $interval->format('%a días');
                    if($dias>=2){
                        if(empty($_SESSION['reserva'])){
                            $_SESSION['reserva'] = array();
                        }
                         
                        $_SESSION['reserva'][] = array('desayuno'=>$_GET['desayuno'],'bebes'=>$_GET['bebes'],'habitacion'=>$habitacion->row(),'desde'=>$_GET['start'],'hasta'=>$_GET['end'],'dias'=>$dias,'adultos'=>$_GET['room-type'],'infantes'=>$_GET['guest']);
                        redirect('reservar/confirmar');         
                    }else{
                        $_SESSION['msj'] = "sis plau completi les dades solicitades per realitzar la seva solicitut";
                        $data = '';
                        foreach($_GET as $n=>$v){
                            $data.= $n.'='.$v.'&';
                        }   
                        redirect('iniciar-reserva?'.$data);
                    }
                }else{
                    throw new Exception("La seva reserva no s'ha pogut fer amb éxit");
                }
            }elseif(!empty($_GET['oferta']) && is_numeric($_GET['oferta'])){
                $oferta = $this->db->get_where('ofertas',array('id'=>$_GET['oferta']));
                if($oferta->num_rows()>0){
                    $oferta = $oferta->row();
                    $habitacion = $this->db->get_where('habitaciones',array('id'=>$oferta->habitaciones_id));
                    $dias = $oferta->cantidad_noches;
                    //Cambiamos el precio
                    $habitacion->precio_desde = $oferta->precio;                    
                    $_SESSION['reserva'][] = array('oferta'=>$oferta,'bebes'=>$_GET['bebes'],'habitacion'=>$habitacion->row(),'desde'=>$_GET['start'],'hasta'=>$_GET['end'],'dias'=>$dias,'adultos'=>$_GET['room-type'],'infantes'=>$_GET['guest']);
                    redirect('reservar/confirmar');
                }else{
                    redirect('iniciar-reserva');
                }
            }elseif(!empty($_SESSION['reserva'])){
                $desde = date("Y-m-d");
                $hasta = '1969-'.date("m-d");
                $n = 0;
                foreach($_SESSION['reserva'] as $r){ 
                    if($n==0){
                        $desde = $r['desde'];
                        $n = 1;
                    }                    
                    if(strtotime($desde)>=strtotime($r['desde'])){
                        $desde = date("d-m-Y",strtotime($r['desde']));
                    }
                }
                foreach($_SESSION['reserva'] as $r){
                    if(strtotime($hasta)<strtotime($r['hasta'])){
                        $hasta = date("d-m-Y",strtotime($r['hasta']));
                    }
                }
                $this->loadView(array('title'=>'Confirmar la teu Reserva','view'=>'confirmar_reserva','reserva'=>$_SESSION['reserva'],'desde'=>$desde,'hasta'=>$hasta));                
            }else{                
                redirect('iniciar-reserva');
            }
        }
        
        
        function remove($id){
            if(is_numeric($id)){
                if(isset($_SESSION['reserva'][$id])){
                    unset($_SESSION['reserva'][$id]);
                }
            }
            redirect('reservar/confirmar');
        }

        function calcular(){
            /*var precio = parseFloat($("#get_value").find('option:selected').data('price'));
            var abono = precio*0.25;
            /*var dias = parseInt($(".duration").val());
            var adultos = parseInt($("#get_value1").val());
            console.log(precio);
            console.log(dias);
            console.log(adultos);
            if(!isNaN(precio) && !isNaN(dias) && !isNaN(adultos)){
                $("#precio").val(precio*dias*adultos);
            }*/  
            /*$("#precio").val(precio);
            $("#abono").val(abono);
            $("#precioLabel").html(precio+'€');
            $("#abonoLabel").html(abono+'€');
            fillFields();*/
            //echo print_r($_POST,TRUE);
            //echo '<hr>';
            $habitaciones = $this->querys->get_precios($_POST['habitacion'],$_POST['desde'],$_POST['hasta']);            
            $_POST['total'] = $habitaciones[0]->precio_desde;
            $_POST['abono'] = $habitaciones[0]->precio_desde*0.25;
            $_POST['total'] = str_replace(',','.',$_POST['total']);
            $_POST['abono'] = str_replace(',','.',$_POST['abono']);
            //echo print_r($_POST,TRUE);
            //die();
        }

        function canReservar($desde,$hasta){
            $habitaciones = array();
            $habs = $this->db->get('habitaciones');
            foreach($habs->result() as $h){
                if($this->querys->hasReserva($h->id,$desde,$hasta)){
                    $habitaciones[] = $h;
                }
            }


            //Si hay una habitacion deshabilitar toda la casa
            $idhabinarray = -1;
            foreach($habitaciones as $n=>$h){
                if($h->id==1){
                    $idhabinarray = $n;
                }
            }

            //Si estan bloqueadas para las fechas se sacan
            foreach($habitaciones as $n=>$h){
                if(!$this->querys->hasReserva($h->id,$desde,$hasta,array())){
                    unset($habitaciones[$n]);
                    $idhabinarray = -1;
                }
            }




            //Si habs disponibles es diferente de la cantidad total de habitaciones y entre ellas esta toda la casa se saca del array
            if($habs->num_rows()!=count($habitaciones) && $idhabinarray>=0){
                unset($habitaciones[$idhabinarray]);
            }

            //Trae precios reales
            return count($habitaciones>0)?true:false;

        }
        
        function reservar(){
            $this->load->library('form_validation');
            $this->form_validation->set_rules('habitacion','Habitación','required');
            $this->form_validation->set_rules('nombre','Nombre','required');
            $this->form_validation->set_rules('apellido','Apellido','required');
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('telefono','Telefono','required');            
            $this->form_validation->set_rules('condiciones','Condicions de reserva','required');
            $this->form_validation->set_rules('politicas','Politicas de privacitat','required');
            $this->form_validation->set_rules('abono','Abono','required');
            $this->form_validation->set_rules('total','Total','required|numeric'); 
            $this->form_validation->set_rules('idioma','Idioma','required'); 
            if($this->form_validation->run()){                
                $this->load->library('recaptcha');
                /*if(!$this->recaptcha->recaptcha_check_answer($_POST['g-recaptcha-response'])){
                    $_SESSION['msj'] = $this->error('Captcha introduit incorrectament');
                    //redirect('reservar/confirmar');
                }else{*/
                    $datetime1 = new DateTime(date("Y-m-d",strtotime($_POST['desde'])));
                    $datetime2 = new DateTime(date("Y-m-d",strtotime($_POST['hasta'])));
                    $interval = $datetime1->diff($datetime2);
                    $dias = $interval->format('%a');
                    if($dias>=2){
                        if(!$this->canReservar($_POST['desde'],$_POST['hasta'])){
                            $_SESSION['msj'] = 'La reserva no pudo completarse';
                            redirect('reservar/confirmar');
                        }else{                            
                            $precio = $this->calcular();
                            //$_POST['abono'] = str_replace(',','.',$_POST['abono']);
                            $ajustes = $this->db->get('ajustes')->row();
                            //Sacamos el id del usuario
                            $user = $this->db->get_where('user',array('email'=>$_POST['email']));
                            if($user->num_rows()==0){
                                $this->db->insert('user',
                                        array(
                                            'nombre'=>$_POST['nombre'],
                                            'apellido'=>$_POST['apellido'],                                    
                                            'email'=>$_POST['email'],                                    
                                            'password'=>md5('12345678'),
                                            'fecha_registro'=>date("Y-m-d"),
                                            'fecha_actualizacion'=>date("Y-m-d"),
                                            'status'=>1,
                                            'admin'=>0
                                        ));
                                $userid = $this->db->insert_id();
                            }else{
                                $userid = $user->row()->id;
                                $this->db->update('user',
                                        array(
                                            'nombre'=>$_POST['nombre'],
                                            'apellido'=>$_POST['apellido'],                                    
                                            'fecha_actualizacion'=>date("Y-m-d"),
                                        ),array('id'=>$userid));
                            }
                            //echo $userid;
                            //Añadimos la reserva
                            $this->db->insert('reservas',
                                    array(
                                        'user_id'=>$userid,
                                        'fecha_reserva'=>date("Y-m-d"),
                                        'desde'=>date("Y-m-d",strtotime($_POST['desde'])),
                                        'hasta'=>date("Y-m-d",strtotime($_POST['hasta'])),
                                        'nombre'=>$_POST['nombre'],
                                        'apellido'=>$_POST['apellido'],
                                        'telefono'=>$_POST['telefono'],
                                        'email'=>$_POST['email'],
                                        'ciudad'=>$_POST['ciudad'],
                                        'cp'=>$_POST['cp'],
                                        'provincia'=>$_POST['provincia'],
                                        'pais'=>$_POST['pais'],
                                        'whatsapp'=>$_POST['whatsapp'],
                                        'extras'=>$_POST['extras'],
                                        'historia'=>$_POST['historia'],
                                        'precio'=>$_POST['total'],
                                        'abonado'=>$_POST['abono'],
                                        //'abonado'=>$_POST['total'],
                                        'requerimiento'=>$_POST['requerimiento'],
                                        'pagado'=>0,
                                        'idioma'=>$_POST['idioma']
                                    ));
                            $reservaid = $this->db->insert_id();
                            //Añadimos detalle                    
                            //foreach($_SESSION['reserva'] as $r){
                                if(!empty($_POST['desayuno'])){
                                    $desayuno = $_POST['adultos']*5;
                                    $desayuno+= $_POST['infantes']*5;
                                    $desayuno*= $_POST['dias'];
                                }
                                $_POST['habitacion'] = $this->db->get_where('habitaciones',array('id'=>$_POST['habitacion']))->row();
                                $insertdata = array(
                                    'reservas_id'=>$reservaid,
                                    'habitaciones_id'=>$_POST['habitacion']->id,
                                    'desde'=>date("Y-m-d",strtotime($_POST['desde'])),
                                    'hasta'=>date("Y-m-d",strtotime($_POST['hasta'])),
                                    'precio'=>$_POST['total'],
                                    'adultos'=>$_POST['adultos'],
                                    //'bebes'=>$r['bebes'],
                                    'dias'=>$_POST['dias'],
                                    'infantes'=>$_POST['infantes'],
                                    /*'desayuno'=>$r['desayuno'],
                                    'desayuno_importe'=>$desayuno*/
                                    'desayuno'=>0,
                                    'desayuno_importe'=>0
                                );
                                $this->db->insert('reservas_habitaciones',$insertdata);
                                if(!empty($r['habitacion']->comparte_con)){
                                    $insertreplica = $insertdata;
                                    $insertreplica['habitaciones_id'] = $r['habitacion']->comparte_con;
                                    $insertreplica['precio'] = 0;
                                    $this->db->insert('reservas_habitaciones',$insertreplica);
                                }
                            //}
                            //unset($_SESSION['reserva']);
                            //redirect('reserva_realizada/'.$reservaid);
                            /*if(!empty($_POST['cond']) && $this->db->get_where('subscritos',array('email'=>$_POST['email']))->num_rows()==0){
                                $this->db->insert('subscritos',array('email'=>$_POST['email']));
                            }*/
                            redirect('habitacion/reservaFrontend/gotpv/'.$reservaid);
                        }
                    }else{
                        switch($_SESSION['lang']){
                            case 'es':
                                $_SESSION['msj'] = 'Se deben reservar mínimo 2 días';
                            break;
                            case 'ca':
                                $_SESSION['msj'] = 'S\'ha de reservar mínim 2 dies';
                            break;
                            case 'en':
                                $_SESSION['msj'] = 'The minimal days of the reserve must be 2';
                            break;
                            default:
                                $_SESSION['msj'] = 'The minimal days of the reserve must be 2';
                            break;
                        }

                        redirect('reservar/confirmar');
                    }
                //}
            }else{
                $_SESSION['msj'] = $this->form_validation->error_string();
                redirect('reservar/confirmar');
            }
        }
        
        function gotpv($reservaid){
            if(is_numeric($reservaid)){
                $reserva = $this->db->get_where('reservas',array('id'=>$reservaid,'pagado'=>0));
                if($reserva->num_rows()>0){
                    $this->load->view('tpv',array('reserva'=>$reserva->row()));    
                }else{
                    //redirect('iniciar-reserva');
                }
            }else{  
                //redirect('iniciar-reserva');
            }
        }

        function abonar($reservaid){
            if(is_numeric($reservaid)){
                $reserva = $this->db->get_where('reservas',array('id'=>$reservaid));
                if($reserva->num_rows()>0){
                    $reserva->row()->abonado = $reserva->row()->precio - $reserva->row()->abonado;
                    $this->load->view('tpv',array('reserva'=>$reserva->row()));    
                }else{
                    //redirect('iniciar-reserva');
                }
            }else{  
                //redirect('iniciar-reserva');
            }
        }
        
        function reserva_realizada($reserva = ''){
            if(is_numeric($reserva)){
                $reserva = $this->db->get_where('reservas',array('id'=>$reserva));
                if($reserva->num_rows()>0){
                    $reserva = $reserva->row();                    
                    $reserva->habitaciones = $this->db->get_where('reservas_habitaciones',array('reservas_id'=>$reserva->id));
                    foreach($reserva->habitaciones->result() as $n=>$h){
                        $reserva->habitaciones->row($n)->habitacion = $this->db->get_where('habitaciones',array('id'=>$h->habitaciones_id))->row();
                    }
                    $this->loadView(array('title'=>'Reserva realitzada','view'=>'reserva_realizada','reserva'=>$reserva));
                }else{
                    redirect('iniciar-reserva');
                }
                
            }else{  
                redirect('iniciar-reserva');
            }
        }
        
        function pagoKo(){
            $this->loadView(array('title'=>'Error en la teu Reserva','view'=>'reserva_no_realizada'));
        }
        
        function procesar($reserva){
            //correo('joncar.c@gmail.com','TPV',json_encode($_POST));   
            /*$reserva = $this->db->get_where('reservas',array('id'=>$reserva));  
            $this->db->select('reservas_habitaciones.*, reservas.pagado, habitaciones.*, reservas.email, reservas.nombre, reservas.idioma,  reservas.apellido, reservas.abonado, reservas.precio as total');
            $this->db->join('habitaciones','habitaciones.id = reservas_habitaciones.habitaciones_id');
            $this->db->join('reservas','reservas.id = reservas_habitaciones.reservas_id');
            $_user = $this->db->get_where('reservas_habitaciones',array('reservas_id'=>$reserva->row()->id));
            $user = $_user->row();
            $user->reserva = $this->get_reserva_message($_user);
            $_user = $this->db->get_where('reservas_habitaciones',array('reservas_id'=>$reserva->row()->id));
            $this->enviarcorreo($user, 7,'info@calprat.net');      */
            if(is_numeric($reserva)){
                $reserva = $this->db->get_where('reservas',array('id'=>$reserva));
                if($reserva->num_rows()>0){
                    $this->load->library('redsysapi');
                    $miObj = new RedsysAPI; 
                    $this->db->select('reservas_habitaciones.*, reservas.pagado, habitaciones.*, reservas.email, reservas.nombre, reservas.idioma,  reservas.apellido, reservas.abonado, reservas.precio as total');
                    $this->db->join('habitaciones','habitaciones.id = reservas_habitaciones.habitaciones_id');
                    $this->db->join('reservas','reservas.id = reservas_habitaciones.reservas_id');                    
                    $_user = $this->db->get_where('reservas_habitaciones',array('reservas_id'=>$reserva->row()->id));
                    if($_user->row()->pagado==1){
                        //Se actualiza el pago en totalidad
                        $_user->row()->abonado = $_user->row()->total;
                    }
                    $user = $_user->row();
                    $user->reserva = $this->get_reserva_message($_user);
                    //$_POST = (array)json_decode('{"Ds_SignatureVersion":"HMAC_SHA256_V1","Ds_Signature":"ZhI5gMAEzClAEaoKhRqpjBm4bsUlj_8cD4_br1homao=","Ds_MerchantParameters":"eyJEc19EYXRlIjoiMzFcLzAxXC8yMDE4IiwiRHNfSG91ciI6IjE1OjQ5IiwiRHNfU2VjdXJlUGF5bWVudCI6IjEiLCJEc19DYXJkX0NvdW50cnkiOiI3MjQiLCJEc19BbW91bnQiOiIyNTAwIiwiRHNfQ3VycmVuY3kiOiI5NzgiLCJEc19PcmRlciI6IjMxMDEwMzQ4MDczIiwiRHNfTWVyY2hhbnRDb2RlIjoiMzQ0NTExOTg1IiwiRHNfVGVybWluYWwiOiIwMDEiLCJEc19SZXNwb25zZSI6IjAwMDAiLCJEc19NZXJjaGFudERhdGEiOiIiLCJEc19UcmFuc2FjdGlvblR5cGUiOiIwIiwiRHNfQ29uc3VtZXJMYW5ndWFnZSI6IjEiLCJEc19BdXRob3Jpc2F0aW9uQ29kZSI6IjI4ODIzMyIsIkRzX0NhcmRfQnJhbmQiOiIxIn0="}');
                    if (!empty($_POST)){
                        //correo('joncar.c@gmail.com','POST',json_encode($_POST));
                        $datos = $_POST["Ds_MerchantParameters"];
                        $decodec = json_decode($miObj->decodeMerchantParameters($datos));         
                        $success = false;
                        if(!empty($decodec) && ($decodec->Ds_Response=='0000' || $decodec->Ds_Response=='0001' || $decodec->Ds_Response=='0002' || $decodec->Ds_Response=='0099') && empty($decodec->Ds_ErrorCode)){                    
                            //Aprobado
                            $abonado = $reserva->row()->abonado;
                            if($reserva->row()->pagado==1){
                                $abonado =$reserva->row()->precio;                                
                            }//Pago la totalidad
                            $this->db->update('reservas',array('pagado'=>1,'abonado'=>$abonado),array('id'=>$reserva->row()->id));
                            $success = true;
                        }else{
                            if($reserva->row()->pagado==0){
                                $this->db->delete('reservas',array('id'=>$reserva->row()->id));
                                $this->db->delete('reservas_habitaciones',array('habitaciones_id'=>$reserva->row()->id));
                            }

                            $this->load->helper('redsys_responses');
                            $user->tpv = (INT)$decodec->Ds_Response;
                            $user->response = getMessageRedsys((INT)$decodec->Ds_Response);
                            $this->enviarcorreo($user, 15,'info@calprat.net');
                            
                        }
                        
                       $notif1 = 11;
                       $notif2 = 12;
                       $notife = 13;
                       switch($user->idioma){
                           case 'ca':
                                $notif1 = 5;
                                $notif2 = 7;
                                $notife = 6;
                           break;
                           case 'en':
                               $notif1 = 8;
                               $notif2 = 9;
                               $notife = 10;
                           break;
                       }
                       if($success){
                           $this->enviarcorreo($user, $notif1,'info@calprat.net');
                            $this->enviarcorreo($user, $notif2);
                       }else{
                           $this->enviarcorreo($user, $notife,'info@calprat.net');
                           $this->enviarcorreo($user, $notife);
                       }
                       
                        
                    }else{
                        $this->enviarcorreo($user, 4,'info@calprat.net');
                        $this->enviarcorreo($user, 4);
                    }
                }   
            }
        }
        
        function get_reserva_message($user){
            return $this->load->view('emails/'.$user->row()->idioma,array('user'=>$user),TRUE);
        }

        function get_habitaciones(){
            $habitacion = '<select name="habitacion" id="get_value"><option value="">Si us plau selecciona una habitació</option>';            

            if(!empty($_GET['desde']) && !empty($_GET['hasta'])){
                $habitaciones = array();
                $habs = $this->db->get('habitaciones');
                foreach($habs->result() as $h){
                    if($this->querys->hasReserva($h->id,$_GET['desde'],$_GET['hasta'])){
                        $habitaciones[] = $h;
                    }
                }


                //Si hay una habitacion deshabilitar toda la casa
                $idhabinarray = -1;
                foreach($habitaciones as $n=>$h){
                    if($h->id==1){
                        $idhabinarray = $n;
                    }
                }

                //Si estan bloqueadas para las fechas se sacan
                foreach($habitaciones as $n=>$h){
                    if(!$this->querys->hasReserva($h->id,$_GET['desde'],$_GET['hasta'],array())){
                        unset($habitaciones[$n]);
                        $idhabinarray = -1;
                    }
                }




                //Si habs disponibles es diferente de la cantidad total de habitaciones y entre ellas esta toda la casa se saca del array
                if($habs->num_rows()!=count($habitaciones) && $idhabinarray>=0){
                    unset($habitaciones[$idhabinarray]);
                }

                //Trae precios reales
                $habitaciones = $this->querys->get_precios($habitaciones,$_GET['desde'],$_GET['hasta']);


                foreach($habitaciones as $h){
                    $habitacion.= '<option value="'.$h->id.'" data-max="'.(int)$h->max_personas.'" data-price="'.$h->precio_desde.'">'.$h->habitacion_nombre.'</option>';
                }
            }
            
            $habitacion.= '</select>';
            echo $habitacion;
        }    
        
    }
?>
