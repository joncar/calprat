
<!-- WELCOME -->
<section id="content" style=" background: url(<?= base_url() ?>img/pattern-bg.png);">
    <section id="content" class="blog"><div class="container" style="position: relative;">
            <div class="row">
                <div class="blogpost-single">

                    <!-- PAGE HEADER -->
                    <div class="title-bar">
                        <h2 id="mce_91" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Notícies & Històries</h2>
                        <ol class="breadcrumb">
                            <li><a href="<?= base_urL() ?>" id="mce_92" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Inici</a></li>
                            <li><a href="<?= base_url('blog') ?>" id="mce_93" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Notícies</a></li>
<!--                            <li class="active">Video Post</li>-->
                        </ol>
                    </div>
				
				<!-- BLOG CONTENT -->
				<div class="top-blog-post">
					<div class="col-xs-12 col-sm-6 col-md-6">

						<div class="row">
							<div class="img">
								<img class="img-responsive" src="[base_url]img/blog-page/top-post-img1.jpg" alt=""><div class="col-xs-12 col-xs-10 col-md-10 text">
                                    <a href="#"><h2>Notícies i esdeveniments esportius</h2></a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12">
								<div class="row">
									<div class="img">
										<img class="img-responsive" src="[base_url]img/blog-page/top-post-img2.jpg" alt=""><div class="col-xs-12 col-xs-4 col-md-4 text">
											<h2>Activitats rurals</h2>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-6">
								<div class="row">
									<div class="img">
										<img class="img-responsive" src="[base_url]img/blog-page/top-post-img3.jpg" alt=""><div class="col-xs-12 col-xs-12 col-md-12 text">
											<h2>Tallers</h2>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-6">
								<div class="row">
									<div class="img">
										<img class="img-responsive" src="[base_url]img/blog-page/top-post-img4.jpg" alt=""><div class="col-xs-12 col-xs-12 col-md-12 text">
											<h2>Les millors rutes</h2>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="main-grid main-blog-mesonry">
					<div class="col-xs-12 col-sm-12 col-md-12">
						<div class="row">
							<div class="blog-mesonry">
								<ul class="masonry-feed row" id="masonry-feed">
									
									[foreach:blog]
									<li class="masonry-item">
										<img class="img-responsive" src="[foto]" alt=""><div class="blog-detail">
											<h3>[titulo]</h3>
											<div class="col-xs-6 col-sm-6 col-md-6 cap col-centered center-block">
												<span><img class="img-responsive" src="[base_url]img/cap.png" alt=""></span>
											</div>
											<p>[texto]</p>
											<a href="[link]">Continuar llegint</a>
										</div>
										<div class="visitor">
											<span class="calender"><i class="fa fa-calendar-o"></i>
											[fecha]</span>
											<!--<span class="comment"><i class="fa fa-comment"></i>[comentarios] Comentaris</span>-->
											<span class="views"><i class="fa fa-eye"></i>[vistas] Visites</span>
										</div>
									</li>
									[/foreach]
									
								</ul>
</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>    </section>