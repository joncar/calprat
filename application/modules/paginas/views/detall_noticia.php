
            
            
            
            <!-- WELCOME -->
<section id="content" style=" background: url(<?= base_url() ?>img/pattern-bg.png);">
            <section id="content" class="blog"><div class="container">
			<div class="row">
				<div class="blogpost-single">
				
					<!-- PAGE HEADER -->
					<div class="title-bar">
						<h2 id="mce_91" class="mce-content-body">Noticies &amp; Històries</h2>
						<ol class="breadcrumb">
<li><a href="<?= base_url() ?>" id="mce_92" class="mce-content-body">Inici</a></li>
							<li><a href="<?= base_url('blog') ?>" id="mce_93" class="mce-content-body">Noticies</a></li>
							<li class="active">[titulo]</li>
						</ol>
</div>
					
					<!-- BLOG SINGLE -->
					<div class="blogsingle-cont">
						<div class="row">
							<div class="col-xs-12 col-sm-10 col-md-10 col-centered">
								<div class="image-block">
									<img class="img-responsive" alt="" src="[foto]"><div class="visitor">
										<div class="row">
											<div class="col-xs-12 col-sm-4 col-md-6 col-lg-8">
												<span class="calender mce-content-body mce-edit-focus" id="mce_94"><i class="fa fa-calendar-o"></i>[fecha]</span>
											</div>
											<div class="col-xs-12 col-sm-4 col-md-3 col-lg-2">
												<span class="comment mce-content-body" id="mce_95"><i class="fa fa-comment"></i>[comentarios]</span>
											</div>
											<div class="col-xs-12 col-sm-4 col-md-3 col-lg-2">
												<span class="views mce-content-body" id="mce_96"><i class="fa fa-eye"></i>[vistas]</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-10 col-lg-8 col-centered">
								<div class="title">
									<h6 id="mce_97" class="mce-content-body">Historia de la Segarra</h6>
									<h1 id="mce_98" class="mce-content-body mce-edit-focus">[titulo]</h1>
									<div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
										<span id="mce_99" class="mce-content-body"><img class="img-responsive" src="[base_url]img/cap.png" alt=""></span>
									</div>
									
								</div>
								[texto]
								
								
								
								
								
								
							</div>
						</div>
						<div class="post-footer">
							<div class="row">
								<div class="col-xs-12 col-md-8 col-sm-12 col-centered">
									<div class="social">
										<ul>
<li><a href="https://www.twitter.com/username" id="mce_108" class="mce-content-body"><i class="fa fa-twitter-square"></i></a></li>
											<li><a href="https://www.facebook.com/username" id="mce_109" class="mce-content-body"><i class="fa fa-facebook-square"></i></a></li>
											<li><a href="https://www.pinterest.com" id="mce_110" class="mce-content-body"><i class="fa fa-pinterest-square"></i></a></li>
											<li><a href="https://plus.google.com" id="mce_111" class="mce-content-body"><i class="fa fa-google-plus-square"></i></a></li>
										</ul>
</div>
									
									
								</div>
							</div>

							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-centered">
                                    <div class="might-like">
                                        <h3 class="title mce-content-body mce-edit-focus" id="mce_48">You Might Also Like</h3>
                                        <ul>[foreach:relacionados]
                                            <li class="col-xs-12 col-sm-4 col-md-4">
                                                <img alt="" class="img-responsive" src="[foto]">
                                                <div class="detail">
                                                    <a id="mce_49" class="mce-content-body" href="[link]">[titulo]<span id="mce_50" class="mce-content-body">+</span></a>
                                                </div>
                                            </li>[/foreach]
                                            
                                            
                                        </ul>
                                    </div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><div></div><div></div><div></div><div></div><div></div><div></div>
	</section>    </section>                