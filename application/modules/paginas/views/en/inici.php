
            
            <!-- WELCOME -->
<section id="content">
            <section id="content"><div class="welcome" style="position: relative;">
			<div class="container">
				<div class="col-xs-12 col-sm-12 col-md-6 col-centered center-block">
					<div class="row">
						<div class="title">
							<h6 id="mce_91" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">welcome</h6><input type="hidden" name="mce_91"><input type="hidden" name="mce_91"><input type="hidden" name="mce_91">
							<h3 id="mce_92" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">The Hotel at a Glance</h3><input type="hidden" name="mce_92"><input type="hidden" name="mce_92"><input type="hidden" name="mce_92">
							<div class="col-xs-6 col-sm-4 col-md-4 cap col-centered center-block">
								<span id="mce_93" class="mce-content-body" contenteditable="true" spellcheck="false"><img alt="" src="[base_url]img/cap.png" class="img-responsive"></span><input type="hidden" name="mce_93"><input type="hidden" name="mce_93"><input type="hidden" name="mce_93">
							</div>
							<p id="mce_94" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi versa accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit</p><input type="hidden" name="mce_94"><input type="hidden" name="mce_94"><input type="hidden" name="mce_94">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-6">
						<div class="left-welcome">
							<h2 id="mce_95" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">The Perfect Place to Escape</h2><input type="hidden" name="mce_95"><input type="hidden" name="mce_95"><input type="hidden" name="mce_95">
							<p id="mce_96" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi versa accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti eu sociosqu ad litora torquent conubia per inceptos.</p><input type="hidden" name="mce_96"><input type="hidden" name="mce_96"><input type="hidden" name="mce_96">
							<p id="mce_97" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Mauris in erat justo nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum auctor fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.</p><input type="hidden" name="mce_97"><input type="hidden" name="mce_97"><input type="hidden" name="mce_97">
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6">
						<div class="right-welcome">
							<div id="promovid">
								<div class="custom-th">
									<img src="[base_url]img/Homepage_Resort_INDEX/video.png" class="img-responsive" alt="">
</div>
								<div id="thevideo" style="display:none; padding: 0px;">
									<video preload="auto"><source src="http://iurevych.github.com/Flat-UI-videos/big_buck_bunny.mp4"></video>
</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<input type="hidden" name="mce_98"><input type="hidden" name="mce_99"><input type="hidden" name="mce_100"><input type="hidden" name="mce_353"><input type="hidden" name="mce_354"><input type="hidden" name="mce_355"><input type="hidden" name="mce_386"><input type="hidden" name="mce_387"><input type="hidden" name="mce_388"></div>
		
		<!-- ACCOMODATION -->
		<div class="accomodation" style="position: relative;">
			<div class="container">
				<div class="col-xs-12 col-sm-12 col-md-6 col-centered center-block">
					<div class="row">
						<div class="title">
							<h6 id="mce_101" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">accommodations</h6><input type="hidden" name="mce_101"><input type="hidden" name="mce_101"><input type="hidden" name="mce_101">
							<h3 id="mce_102" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Our Rooms &amp; Suites</h3><input type="hidden" name="mce_102"><input type="hidden" name="mce_102"><input type="hidden" name="mce_102">
							<div class="col-xs-6 col-sm-4 col-md-4 cap col-centered center-block">
								<span id="mce_103" class="mce-content-body" contenteditable="true" spellcheck="false"><img alt="" src="[base_url]img/cap.png" class="img-responsive"></span><input type="hidden" name="mce_103"><input type="hidden" name="mce_103"><input type="hidden" name="mce_103">
							</div>
							<p id="mce_104" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi versa accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit</p><input type="hidden" name="mce_104"><input type="hidden" name="mce_104"><input type="hidden" name="mce_104">
						</div>
					</div>
				</div>
			</div>
			<div class="main-room">
				<ul>
<li class="col-xs-12 col-sm-6 col-md-3">
						<div class="mr-inner">
							<img src="[base_url]img/Homepage_Resort_INDEX/room-img1.jpg" class="img-responsive" alt=""><div class="mr-overlay">
								<h2 id="mce_105" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Luxury level<br>room</h2><input type="hidden" name="mce_105"><input type="hidden" name="mce_105"><input type="hidden" name="mce_105">
								<p id="mce_106" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Morbi versa accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio.</p><input type="hidden" name="mce_106"><input type="hidden" name="mce_106"><input type="hidden" name="mce_106">
								<a class="room-detail mce-content-body" href="./roomDetail-image.html" id="mce_107" contenteditable="true" spellcheck="false">room details<i class="fa fa-angle-right"></i></a><input type="hidden" name="mce_107"><input type="hidden" name="mce_107"><input type="hidden" name="mce_107">
							</div>
						</div>
					</li>
					<li class="col-xs-12 col-sm-6 col-md-3">
						<div class="mr-inner">
							<img src="[base_url]img/Homepage_Resort_INDEX/room-img2.jpg" class="img-responsive" alt=""><div class="mr-overlay">
								<h2 id="mce_108" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Luxury level<br>room</h2><input type="hidden" name="mce_108"><input type="hidden" name="mce_108"><input type="hidden" name="mce_108">
								<p id="mce_109" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Morbi versa accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio.</p><input type="hidden" name="mce_109"><input type="hidden" name="mce_109"><input type="hidden" name="mce_109">
								<a class="room-detail mce-content-body" href="./roomDetail-image.html" id="mce_110" contenteditable="true" spellcheck="false">room details<i class="fa fa-angle-right"></i></a><input type="hidden" name="mce_110"><input type="hidden" name="mce_110"><input type="hidden" name="mce_110">
							</div>
						</div>
					</li>
					<li class="col-xs-12 col-sm-6 col-md-3">
						<div class="mr-inner">
							<img src="[base_url]img/Homepage_Resort_INDEX/room-img3.jpg" class="img-responsive" alt=""><div class="mr-overlay">
								<h2 id="mce_111" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">ambassador level<br>suite</h2><input type="hidden" name="mce_111"><input type="hidden" name="mce_111"><input type="hidden" name="mce_111">
								<p id="mce_112" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Morbi versa accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio.</p><input type="hidden" name="mce_112"><input type="hidden" name="mce_112"><input type="hidden" name="mce_112">
								<a class="room-detail mce-content-body" href="./roomDetail-image.html" id="mce_113" contenteditable="true" spellcheck="false">room details<i class="fa fa-angle-right"></i></a><input type="hidden" name="mce_113"><input type="hidden" name="mce_113"><input type="hidden" name="mce_113">
							</div>
						</div>
					</li>
					<li class="col-xs-12 col-sm-6 col-md-3">
						<div class="mr-inner">
							<img src="[base_url]img/Homepage_Resort_INDEX/room-img4.jpg" class="img-responsive" alt=""><div class="mr-overlay">
								<h2 id="mce_114" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">ambassador level<br>suite</h2><input type="hidden" name="mce_114"><input type="hidden" name="mce_114"><input type="hidden" name="mce_114">
								<p id="mce_115" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Morbi versa accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio.</p><input type="hidden" name="mce_115"><input type="hidden" name="mce_115"><input type="hidden" name="mce_115">
								<a class="room-detail mce-content-body" href="./roomDetail-image.html" id="mce_116" contenteditable="true" spellcheck="false">room details<i class="fa fa-angle-right"></i></a><input type="hidden" name="mce_116"><input type="hidden" name="mce_116"><input type="hidden" name="mce_116">
							</div>
						</div>
					</li>
				</ul>
</div>
		<input type="hidden" name="mce_117"><input type="hidden" name="mce_118"><input type="hidden" name="mce_119"><input type="hidden" name="mce_356"><input type="hidden" name="mce_357"><input type="hidden" name="mce_358"><input type="hidden" name="mce_389"><input type="hidden" name="mce_390"><input type="hidden" name="mce_391"></div>
		
		<!-- ROOM OFFER -->
		<div class="room-offer" style="position: relative;">
			<div class="container">
				<h3 id="mce_120" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">all of our rooms offer the following amenities</h3><input type="hidden" name="mce_120"><input type="hidden" name="mce_120"><input type="hidden" name="mce_120">
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1">
						<ul class="specific">
<li>Sed odio sit amet nibh vulputate cursus</li>
							<li>Morbi versa accumsan ipsum velit</li>
							<li>Nam nec tellus a odio tincidunt auctor a ornare</li>
							<li>Sed non  mauris vitae erat consequat</li>
							<li>Class aptent taciti sociosqu ad litora torquent</li>
						</ul>
</div>
					<div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1">
						<ul class="specific">
<li>Class aptent taciti sociosqu ad litora torquent</li>
							<li>Mauris in erat justo</li>
							<li>Nullam ac urna eu felis dapibus condimentum</li>
							<li>Proin condimentum fermentum nun</li>
							<li>Etiam pharetra, erat sed fermentum feugiat</li>
						</ul>
</div>
				</div>
			</div>
		<input type="hidden" name="mce_121"><input type="hidden" name="mce_122"><input type="hidden" name="mce_123"><input type="hidden" name="mce_359"><input type="hidden" name="mce_360"><input type="hidden" name="mce_361"><input type="hidden" name="mce_392"><input type="hidden" name="mce_393"><input type="hidden" name="mce_394"><input type="hidden" name="mce_98"><input type="hidden" name="mce_99"></div>
		<div class="home-gal" style="position: relative;">
			<div class="dining-block dual-slider row">
				<div class="col-md-7 padding-right-none">
					<div id="gal-slider1" class="flexslider">
						<ul class="slides">
<li><img class="img-responsive" src="[base_url]img/Homepage_Resort_INDEX/home-gal-img1.png" alt=""></li>
							<li><img class="img-responsive" src="[base_url]img/Homepage_Resort_INDEX/home-gal-img2.png" alt=""></li>
							<li><img class="img-responsive" src="[base_url]img/Homepage_Resort_INDEX/home-gal-img3.png" alt=""></li>
							<li><img class="img-responsive" src="[base_url]img/Homepage_Resort_INDEX/home-gal-img1.png" alt=""></li>
						</ul>
<a class="flex-prev1 mce-content-body" id="mce_124" contenteditable="true" spellcheck="false"><i class="fa fa-chevron-left"></i></a><input type="hidden" name="mce_124"><input type="hidden" name="mce_124"><input type="hidden" name="mce_124">
						<a class="flex-next1 mce-content-body" id="mce_125" contenteditable="true" spellcheck="false"><i class="fa fa-chevron-right"></i></a><input type="hidden" name="mce_125"><input type="hidden" name="mce_125"><input type="hidden" name="mce_125">
					</div>
				</div>
				<div class="col-md-5 padding-left-none">
					<ul class="gal-nav1">
<li class="flex-active">
							<div>
								<h2 id="mce_126" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Fine Dining Choices</h2><input type="hidden" name="mce_126"><input type="hidden" name="mce_126"><input type="hidden" name="mce_126">
								<p id="mce_127" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi versa accumsan ipsum velit nam nec tellus elit tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent conubia per inceptos.</p><input type="hidden" name="mce_127"><input type="hidden" name="mce_127"><input type="hidden" name="mce_127">
								<p id="mce_128" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Mauris in erat justo nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque.</p><input type="hidden" name="mce_128"><input type="hidden" name="mce_128"><input type="hidden" name="mce_128">
								<a href="./dining.html" class="btn btn-default mce-content-body" id="mce_129" contenteditable="true" spellcheck="false" style="position: relative;">details</a><input type="hidden" name="mce_129"><input type="hidden" name="mce_129"><input type="hidden" name="mce_129">
							</div>
						</li>
						<li class="flex-active">
							<div>
								<h2 id="mce_130" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Fine Dining Choices</h2><input type="hidden" name="mce_130"><input type="hidden" name="mce_130"><input type="hidden" name="mce_130">
								<p id="mce_131" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi versa accumsan ipsum velit nam nec tellus elit tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent conubia per inceptos.</p><input type="hidden" name="mce_131"><input type="hidden" name="mce_131"><input type="hidden" name="mce_131">
								<p id="mce_132" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Mauris in erat justo nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque.</p><input type="hidden" name="mce_132"><input type="hidden" name="mce_132"><input type="hidden" name="mce_132">
								<a href="./dining.html" class="btn btn-default mce-content-body" id="mce_133" contenteditable="true" spellcheck="false" style="position: relative;">details</a><input type="hidden" name="mce_133"><input type="hidden" name="mce_133"><input type="hidden" name="mce_133">
							</div>
						</li>
						<li class="">
							<div>
								<h2 id="mce_134" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Fine Dining Choices</h2><input type="hidden" name="mce_134"><input type="hidden" name="mce_134"><input type="hidden" name="mce_134">
								<p id="mce_135" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi versa accumsan ipsum velit nam nec tellus elit tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent conubia per inceptos.</p><input type="hidden" name="mce_135"><input type="hidden" name="mce_135"><input type="hidden" name="mce_135">
								<p id="mce_136" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Mauris in erat justo nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque.</p><input type="hidden" name="mce_136"><input type="hidden" name="mce_136"><input type="hidden" name="mce_136">
								<a href="./dining.html" class="btn btn-default mce-content-body" id="mce_137" contenteditable="true" spellcheck="false" style="position: relative;">details</a><input type="hidden" name="mce_137"><input type="hidden" name="mce_137"><input type="hidden" name="mce_137">
							</div>
						</li>
						<li class="">
							<div>
								<h2 id="mce_138" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Fine Dining Choices</h2><input type="hidden" name="mce_138"><input type="hidden" name="mce_138"><input type="hidden" name="mce_138">
								<p id="mce_139" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi versa accumsan ipsum velit nam nec tellus elit tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent conubia per inceptos.</p><input type="hidden" name="mce_139"><input type="hidden" name="mce_139"><input type="hidden" name="mce_139">
								<p id="mce_140" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Mauris in erat justo nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque.</p><input type="hidden" name="mce_140"><input type="hidden" name="mce_140"><input type="hidden" name="mce_140">
								<a href="./dining.html" class="btn btn-default mce-content-body" id="mce_141" contenteditable="true" spellcheck="false" style="position: relative;">details</a><input type="hidden" name="mce_141"><input type="hidden" name="mce_141"><input type="hidden" name="mce_141">
							</div>
						</li>
					</ul>
</div>
			</div>
			
			<!-- SPA -->
			<div class="spa-block dual-slider row">
				<div class="col-md-5 padding-right-none">
					<ul class="gal-nav2">
<li class="flex-active">
							<div>
								<h2 id="mce_142" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Pamper Yourself in the Spa</h2><input type="hidden" name="mce_142"><input type="hidden" name="mce_142"><input type="hidden" name="mce_142">
								<p id="mce_143" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi versa accumsan ipsum velit nam nec tellus elit tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent conubia per inceptos.</p><input type="hidden" name="mce_143"><input type="hidden" name="mce_143"><input type="hidden" name="mce_143">
								<p id="mce_144" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Mauris in erat justo nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque.</p><input type="hidden" name="mce_144"><input type="hidden" name="mce_144"><input type="hidden" name="mce_144">
								<a href="./dining.html" class="btn btn-default mce-content-body" id="mce_145" contenteditable="true" spellcheck="false" style="position: relative;">details</a><input type="hidden" name="mce_145"><input type="hidden" name="mce_145"><input type="hidden" name="mce_145">
							</div>
						</li>
						<li class="flex-active">
							<div>
								<h2 id="mce_146" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Pamper Yourself in the Spa</h2><input type="hidden" name="mce_146"><input type="hidden" name="mce_146"><input type="hidden" name="mce_146">
								<p id="mce_147" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi versa accumsan ipsum velit nam nec tellus elit tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent conubia per inceptos.</p><input type="hidden" name="mce_147"><input type="hidden" name="mce_147"><input type="hidden" name="mce_147">
								<p id="mce_148" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Mauris in erat justo nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque.</p><input type="hidden" name="mce_148"><input type="hidden" name="mce_148"><input type="hidden" name="mce_148">
								<a href="./dining.html" class="btn btn-default mce-content-body" id="mce_149" contenteditable="true" spellcheck="false" style="position: relative;">details</a><input type="hidden" name="mce_149"><input type="hidden" name="mce_149"><input type="hidden" name="mce_149">
							</div>
						</li>
						<li class="">
							<div>
								<h2 id="mce_150" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Pamper Yourself in the Spa</h2><input type="hidden" name="mce_150"><input type="hidden" name="mce_150"><input type="hidden" name="mce_150">
								<p id="mce_151" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi versa accumsan ipsum velit nam nec tellus elit tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent conubia per inceptos.</p><input type="hidden" name="mce_151"><input type="hidden" name="mce_151"><input type="hidden" name="mce_151">
								<p id="mce_152" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Mauris in erat justo nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque.</p><input type="hidden" name="mce_152"><input type="hidden" name="mce_152"><input type="hidden" name="mce_152">
								<a href="./dining.html" class="btn btn-default mce-content-body" id="mce_153" contenteditable="true" spellcheck="false" style="position: relative;">details</a><input type="hidden" name="mce_153"><input type="hidden" name="mce_153"><input type="hidden" name="mce_153">
							</div>
						</li>
						<li class="">
							<div>
								<h2 id="mce_154" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Pamper Yourself in the Spa</h2><input type="hidden" name="mce_154"><input type="hidden" name="mce_154"><input type="hidden" name="mce_154">
								<p id="mce_155" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi versa accumsan ipsum velit nam nec tellus elit tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent conubia per inceptos.</p><input type="hidden" name="mce_155"><input type="hidden" name="mce_155"><input type="hidden" name="mce_155">
								<p id="mce_156" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Mauris in erat justo nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque.</p><input type="hidden" name="mce_156"><input type="hidden" name="mce_156"><input type="hidden" name="mce_156">
								<a href="./dining.html" class="btn btn-default mce-content-body" id="mce_157" contenteditable="true" spellcheck="false" style="position: relative;">details</a><input type="hidden" name="mce_157"><input type="hidden" name="mce_157"><input type="hidden" name="mce_157">
							</div>
						</li>
					</ul>
</div>
				<div class="col-md-7 padding-left-none">
					<div id="gal-slider2" class="flexslider">
						<ul class="slides">
<li><img class="img-responsive" src="[base_url]img/Homepage_Resort_INDEX/home-gal-img2.png" alt=""></li>
							<li><img class="img-responsive" src="[base_url]img/Homepage_Resort_INDEX/home-gal-img3.png" alt=""></li>
							<li><img class="img-responsive" src="[base_url]img/Homepage_Resort_INDEX/home-gal-img1.png" alt=""></li>
							<li><img class="img-responsive" src="[base_url]img/Homepage_Resort_INDEX/home-gal-img2.png" alt=""></li>
						</ul>
<a class="flex-prev2 mce-content-body" id="mce_158" contenteditable="true" spellcheck="false"><i class="fa fa-chevron-left"></i></a><input type="hidden" name="mce_158"><input type="hidden" name="mce_158"><input type="hidden" name="mce_158">
						<a class="flex-next2 mce-content-body" id="mce_159" contenteditable="true" spellcheck="false"><i class="fa fa-chevron-right"></i></a><input type="hidden" name="mce_159"><input type="hidden" name="mce_159"><input type="hidden" name="mce_159">
					</div>
				</div>
			</div>
			<div class="entertain-block dual-slider row">
				<div class="col-md-7 padding-right-none">
					<div id="gal-slider3" class="flexslider">
						<ul class="slides">
<li><img class="img-responsive" src="[base_url]img/Homepage_Resort_INDEX/home-gal-img3.png" alt=""></li>
							<li><img class="img-responsive" src="[base_url]img/Homepage_Resort_INDEX/home-gal-img1.png" alt=""></li>
							<li><img class="img-responsive" src="[base_url]img/Homepage_Resort_INDEX/home-gal-img2.png" alt=""></li>
							<li><img class="img-responsive" src="[base_url]img/Homepage_Resort_INDEX/home-gal-img3.png" alt=""></li>
						</ul>
<a class="flex-prev3 mce-content-body" id="mce_160" contenteditable="true" spellcheck="false"><i class="fa fa-chevron-left"></i></a><input type="hidden" name="mce_160"><input type="hidden" name="mce_160"><input type="hidden" name="mce_160">
						<a class="flex-next3 mce-content-body" id="mce_161" contenteditable="true" spellcheck="false"><i class="fa fa-chevron-right"></i></a><input type="hidden" name="mce_161"><input type="hidden" name="mce_161"><input type="hidden" name="mce_161">
					</div>
				</div>
				<div class="col-md-5 padding-left-none">
					<ul class="gal-nav3">
<li class="flex-active">
							<div>
								<h2 id="mce_162" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Let Us Entertain You</h2><input type="hidden" name="mce_162"><input type="hidden" name="mce_162"><input type="hidden" name="mce_162">
								<p id="mce_163" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi versa accumsan ipsum velit nam nec tellus elit tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent conubia per inceptos.</p><input type="hidden" name="mce_163"><input type="hidden" name="mce_163"><input type="hidden" name="mce_163">
								<p id="mce_164" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Mauris in erat justo nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque.</p><input type="hidden" name="mce_164"><input type="hidden" name="mce_164"><input type="hidden" name="mce_164">
								<a href="./dining.html" class="btn btn-default mce-content-body" id="mce_165" contenteditable="true" spellcheck="false" style="position: relative;">details</a><input type="hidden" name="mce_165"><input type="hidden" name="mce_165"><input type="hidden" name="mce_165">
							</div>
						</li>
						<li class="flex-active">
							<div>
								<h2 id="mce_166" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Let Us Entertain You</h2><input type="hidden" name="mce_166"><input type="hidden" name="mce_166"><input type="hidden" name="mce_166">
								<p id="mce_167" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi versa accumsan ipsum velit nam nec tellus elit tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent conubia per inceptos.</p><input type="hidden" name="mce_167"><input type="hidden" name="mce_167"><input type="hidden" name="mce_167">
								<p id="mce_168" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Mauris in erat justo nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque.</p><input type="hidden" name="mce_168"><input type="hidden" name="mce_168"><input type="hidden" name="mce_168">
								<a href="./dining.html" class="btn btn-default mce-content-body" id="mce_169" contenteditable="true" spellcheck="false" style="position: relative;">details</a><input type="hidden" name="mce_169"><input type="hidden" name="mce_169">
							</div>
						</li>
						<li class="">
							<div>
								<h2 id="mce_170" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Let Us Entertain You</h2><input type="hidden" name="mce_170"><input type="hidden" name="mce_170">
								<p id="mce_171" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi versa accumsan ipsum velit nam nec tellus elit tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent conubia per inceptos.</p><input type="hidden" name="mce_171"><input type="hidden" name="mce_171">
								<p id="mce_172" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Mauris in erat justo nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque.</p><input type="hidden" name="mce_172"><input type="hidden" name="mce_172">
								<a href="./dining.html" class="btn btn-default mce-content-body" id="mce_173" contenteditable="true" spellcheck="false" style="position: relative;">details</a><input type="hidden" name="mce_173"><input type="hidden" name="mce_173">
							</div>
						</li>
						<li class="">
							<div>
								<h2 id="mce_174" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Let Us Entertain Youlocation</h2><input type="hidden" name="mce_174"><input type="hidden" name="mce_174">
								<p id="mce_175" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi versa accumsan ipsum velit nam nec tellus elit tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent conubia per inceptos.</p><input type="hidden" name="mce_175"><input type="hidden" name="mce_175">
								<p id="mce_176" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Mauris in erat justo nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque.</p><input type="hidden" name="mce_176"><input type="hidden" name="mce_176">
								<a href="./dining.html" class="btn btn-default mce-content-body" id="mce_177" contenteditable="true" spellcheck="false" style="position: relative;">details</a><input type="hidden" name="mce_177"><input type="hidden" name="mce_177">
							</div>
						</li>
					</ul>
</div>
			</div>
		<input type="hidden" name="mce_178"><input type="hidden" name="mce_179"><input type="hidden" name="mce_180"><input type="hidden" name="mce_362"><input type="hidden" name="mce_363"><input type="hidden" name="mce_364"><input type="hidden" name="mce_395"><input type="hidden" name="mce_396"><input type="hidden" name="mce_397"><input type="hidden" name="mce_100"></div>
		<div class="activ-eve" style="position: relative;">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-6">
						<h3 id="mce_181" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Activities</h3><input type="hidden" name="mce_181"><input type="hidden" name="mce_181">
						<p id="mce_182" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi versa accumsan ipsum velit nam nec tellus elit tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent conubia per inceptos.</p><input type="hidden" name="mce_182"><input type="hidden" name="mce_182">
						<ul class="specific">
<li class="col-xs-12 col-sm-6 col-md-6">Golf</li>
							<li class="col-xs-12 col-sm-6 col-md-6">Racquetball</li>
							<li class="col-xs-12 col-sm-6 col-md-6">Tennis</li>
							<li class="col-xs-12 col-sm-6 col-md-6">Swimming Lessons</li>
							<li class="col-xs-12 col-sm-6 col-md-6">Volleyball</li>
							<li class="col-xs-12 col-sm-6 col-md-6">Planned Excursions</li>
						</ul>
</div>
					<div class="col-xs-12 col-sm-6 col-md-6">
						<h3 id="mce_183" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Events</h3><input type="hidden" name="mce_183"><input type="hidden" name="mce_183">
						<p id="mce_184" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi versa accumsan ipsum velit nam nec tellus elit tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent conubia per inceptos.</p><input type="hidden" name="mce_184"><input type="hidden" name="mce_184">
						<ul class="specific">
<li class="col-xs-12 col-sm-6 col-md-6">Weddings</li>
							<li class="col-xs-12 col-sm-6 col-md-6">Catered Dinners</li>
							<li class="col-xs-12 col-sm-6 col-md-6">Receptions</li>
							<li class="col-xs-12 col-sm-6 col-md-6">Business Conferences</li>
							<li class="col-xs-12 col-sm-6 col-md-6">Luncheons</li>
							<li class="col-xs-12 col-sm-6 col-md-6">Museum Tours</li>
						</ul>
</div>
				</div>
			</div>
			<div class="learn-more">
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="row">
						<img class="img-responsive" src="[base_url]img/Homepage_Resort_INDEX/learn-img1.png" alt="">
</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="row">
						<img class="img-responsive" src="[base_url]img/Homepage_Resort_INDEX/learn-img2.png" alt="">
</div>
				</div>
				<a href="./activities.html" class="btn btn-default mce-content-body" id="mce_185" contenteditable="true" spellcheck="false">learn more</a><input type="hidden" name="mce_185"><input type="hidden" name="mce_185">
			</div>
		<input type="hidden" name="mce_186"><input type="hidden" name="mce_187"><input type="hidden" name="mce_188"><input type="hidden" name="mce_365"><input type="hidden" name="mce_366"><input type="hidden" name="mce_367"><input type="hidden" name="mce_398"><input type="hidden" name="mce_399"><input type="hidden" name="mce_400"></div>
		
		<!-- SERVICES -->
		<div class="main-service" style="position: relative;">
			<div class="container">
				<div class="col-xs-12 col-sm-12 col-md-6 col-centered center-block">
					<div class="row">
						<div class="title">
							<h6 id="mce_189" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">services</h6><input type="hidden" name="mce_189"><input type="hidden" name="mce_189">
							<h3 id="mce_190" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">We've Got You Covered</h3><input type="hidden" name="mce_190"><input type="hidden" name="mce_190">
							<div class="col-xs-6 col-sm-4 col-md-4 cap col-centered center-block">
								<span id="mce_191" class="mce-content-body" contenteditable="true" spellcheck="false"><img class="img-responsive" src="[base_url]img/cap.png" alt=""></span><input type="hidden" name="mce_191"><input type="hidden" name="mce_191">
							</div>
							<p id="mce_192" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi versa accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit</p><input type="hidden" name="mce_192"><input type="hidden" name="mce_192">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1">
						<ul class="specific">
<li>Sed odio sit amet nibh vulputate cursus</li>
							<li>Morbi versa accumsan ipsum velit</li>
							<li>Nam nec tellus a odio tincidunt auctor a ornare</li>
							<li>Sed non  mauris vitae erat consequat</li>
							<li>Class aptent taciti sociosqu ad litora torquent</li>
							<li>Mauris in erat justo</li>
							<li>Nullam ac urna eu felis dapibus condimentum</li>
						</ul>
</div>
					<div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1">
						<ul class="specific">
<li>Nam nec tellus a odio tincidunt auctor a ornare</li>
							<li>Class aptent taciti sociosqu ad litora torquent</li>
							<li>Mauris in erat justo</li>
							<li>Nullam ac urna eu felis dapibus condimentum</li>
							<li>Proin condimentum fermentum nun</li>
							<li>Etiam pharetra, erat sed fermentum feugiat</li>
							<li>Velit mauris egestas quam ut aliquam</li>
						</ul>
</div>
				</div>
				<a href="./about.html" class="btn btn-default mce-content-body" id="mce_193" contenteditable="true" spellcheck="false" style="position: relative;">view all services</a><input type="hidden" name="mce_193"><input type="hidden" name="mce_193">
			</div>
		<input type="hidden" name="mce_194"><input type="hidden" name="mce_195"><input type="hidden" name="mce_196"><input type="hidden" name="mce_368"><input type="hidden" name="mce_369"><input type="hidden" name="mce_370"><input type="hidden" name="mce_401"><input type="hidden" name="mce_402"><input type="hidden" name="mce_403"></div>
		<div class="home-img-panel parallax-bg" data-stellar-ratio="2" data-stellar-background-ratio="0.5" style="position: relative;">
			<div class="panel-img-detail">
				<h2 id="mce_197" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">We Make it Easy <br>to Plan Your Stay</h2><input type="hidden" name="mce_197"><input type="hidden" name="mce_197">
				<div class="col-xs-7 col-sm-7 col-md-7 cap col-centered center-block">
					<span id="mce_198" class="mce-content-body" contenteditable="true" spellcheck="false"><img alt="" src="[base_url]img/cap.png" class="img-responsive"></span><input type="hidden" name="mce_198"><input type="hidden" name="mce_198">
				</div>
				<p id="mce_199" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Duis sed odio sit amet nibh vulputate <br>cursus a sit amet mauris. Morbi versa accumsan ipsum velit nam nec tellus.</p><input type="hidden" name="mce_199"><input type="hidden" name="mce_199">
				<a href="./rooms-1col.html" class="btn btn-default mce-content-body" id="mce_200" contenteditable="true" spellcheck="false" style="position: relative;">details</a><input type="hidden" name="mce_200"><input type="hidden" name="mce_200">
			</div>
		<input type="hidden" name="mce_201"><input type="hidden" name="mce_202"><input type="hidden" name="mce_203"><input type="hidden" name="mce_371"><input type="hidden" name="mce_372"><input type="hidden" name="mce_373"><input type="hidden" name="mce_404"><input type="hidden" name="mce_405"><input type="hidden" name="mce_406"></div>
		<div class="main-gallery" style="position: relative;">
			<div class="container">
				<div class="col-xs-12 col-sm-12 col-md-6 col-centered center-block">
					<div class="row">
						<div class="title">
							<h3 class="light-title mce-content-body" id="mce_204" contenteditable="true" spellcheck="false" style="position: relative;">Gallery</h3><input type="hidden" name="mce_204"><input type="hidden" name="mce_204">
							<div class="col-xs-6 col-sm-4 col-md-4 cap col-centered center-block">
								<span id="mce_205" class="mce-content-body" contenteditable="true" spellcheck="false"><img alt="" src="[base_url]img/cap.png" class="img-responsive"></span><input type="hidden" name="mce_205"><input type="hidden" name="mce_205">
							</div>
							<p id="mce_206" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi versa accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit</p><input type="hidden" name="mce_206"><input type="hidden" name="mce_206">
						</div>
					</div>
				</div>
			</div>
			<div class="main-gal-slider">
				<div id="home-gallery" class="owl-carousel owl-theme">
					<div class="item">
						<img src="[base_url]img/Homepage_Resort_INDEX/slider-img2.jpg" alt="Owl Image"><div class="figcaption">
							<h3 id="mce_207" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">ocean view balcony room</h3><input type="hidden" name="mce_207"><input type="hidden" name="mce_207">
							<div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
								<span id="mce_208" class="mce-content-body" contenteditable="true" spellcheck="false"><img class="img-responsive" src="[base_url]img/cap.png" alt=""></span><input type="hidden" name="mce_208"><input type="hidden" name="mce_208">
							</div>
							<ul>
<li>
									<span class="des mce-content-body" id="mce_209" contenteditable="true" spellcheck="false" style="position: relative;">beds</span><input type="hidden" name="mce_209"><input type="hidden" name="mce_209">
									<span class="detail mce-content-body" id="mce_210" contenteditable="true" spellcheck="false" style="position: relative;">King or 2 Queen</span><input type="hidden" name="mce_210"><input type="hidden" name="mce_210">
								</li>
								<li>
									<span class="des mce-content-body" id="mce_211" contenteditable="true" spellcheck="false" style="position: relative;">occupancy</span><input type="hidden" name="mce_211"><input type="hidden" name="mce_211">
									<span class="detail mce-content-body" id="mce_212" contenteditable="true" spellcheck="false" style="position: relative;">2 People</span><input type="hidden" name="mce_212"><input type="hidden" name="mce_212">
								</li>
								<li>
									<span class="des mce-content-body" id="mce_213" contenteditable="true" spellcheck="false" style="position: relative;">size</span><input type="hidden" name="mce_213"><input type="hidden" name="mce_213">
									<span class="detail mce-content-body" id="mce_214" contenteditable="true" spellcheck="false" style="position: relative;">60 sqm / 650 sqf</span><input type="hidden" name="mce_214"><input type="hidden" name="mce_214">
								</li>
								<li>
									<span class="des mce-content-body" id="mce_215" contenteditable="true" spellcheck="false" style="position: relative;">view</span><input type="hidden" name="mce_215"><input type="hidden" name="mce_215">
									<span class="detail mce-content-body" id="mce_216" contenteditable="true" spellcheck="false" style="position: relative;">Ocean / City</span><input type="hidden" name="mce_216"><input type="hidden" name="mce_216">
								</li>
								<li>
									<span class="des mce-content-body" id="mce_217" contenteditable="true" spellcheck="false" style="position: relative;">rates from</span><input type="hidden" name="mce_217"><input type="hidden" name="mce_217">
									<span class="detail mce-content-body" id="mce_218" contenteditable="true" spellcheck="false" style="position: relative;">215 USD</span><input type="hidden" name="mce_218"><input type="hidden" name="mce_218">
								</li>
							</ul>
<div>
								<a href="./roomDetail-gallery.html" class="btn btn-default pull-left mce-content-body" id="mce_219" contenteditable="true" spellcheck="false" style="position: relative;">room details</a><input type="hidden" name="mce_219"><input type="hidden" name="mce_219">
								<a href="./reservation.html" class="btn btn-default pull-right mce-content-body" id="mce_220" contenteditable="true" spellcheck="false" style="position: relative;">book now</a><input type="hidden" name="mce_220"><input type="hidden" name="mce_220">
							</div>
						</div>
					</div>
					<div class="item">
						<img src="[base_url]img/Homepage_Resort_INDEX/slider-img3.jpg" alt="Owl Image"><div class="figcaption">
							<h3 id="mce_221" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">ocean view balcony room</h3><input type="hidden" name="mce_221"><input type="hidden" name="mce_221">
							<div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
								<span id="mce_222" class="mce-content-body" contenteditable="true" spellcheck="false"><img class="img-responsive" src="[base_url]img/cap.png" alt=""></span><input type="hidden" name="mce_222"><input type="hidden" name="mce_222">
							</div>
							<ul>
<li>
									<span class="des mce-content-body" id="mce_223" contenteditable="true" spellcheck="false" style="position: relative;">beds</span><input type="hidden" name="mce_223"><input type="hidden" name="mce_223">
									<span class="detail mce-content-body" id="mce_224" contenteditable="true" spellcheck="false" style="position: relative;">King or 2 Queen</span><input type="hidden" name="mce_224"><input type="hidden" name="mce_224">
								</li>
								<li>
									<span class="des mce-content-body" id="mce_225" contenteditable="true" spellcheck="false" style="position: relative;">occupancy</span><input type="hidden" name="mce_225"><input type="hidden" name="mce_225">
									<span class="detail mce-content-body" id="mce_226" contenteditable="true" spellcheck="false" style="position: relative;">2 People</span><input type="hidden" name="mce_226"><input type="hidden" name="mce_226">
								</li>
								<li>
									<span class="des mce-content-body" id="mce_227" contenteditable="true" spellcheck="false" style="position: relative;">size</span><input type="hidden" name="mce_227"><input type="hidden" name="mce_227">
									<span class="detail mce-content-body" id="mce_228" contenteditable="true" spellcheck="false" style="position: relative;">60 sqm / 650 sqf</span><input type="hidden" name="mce_228"><input type="hidden" name="mce_228">
								</li>
								<li>
									<span class="des mce-content-body" id="mce_229" contenteditable="true" spellcheck="false" style="position: relative;">view</span><input type="hidden" name="mce_229"><input type="hidden" name="mce_229">
									<span class="detail mce-content-body" id="mce_230" contenteditable="true" spellcheck="false" style="position: relative;">Ocean / City</span><input type="hidden" name="mce_230"><input type="hidden" name="mce_230">
								</li>
								<li>
									<span class="des mce-content-body" id="mce_231" contenteditable="true" spellcheck="false" style="position: relative;">rates from</span><input type="hidden" name="mce_231"><input type="hidden" name="mce_231">
									<span class="detail mce-content-body" id="mce_232" contenteditable="true" spellcheck="false" style="position: relative;">215 USD</span><input type="hidden" name="mce_232"><input type="hidden" name="mce_232">
								</li>
							</ul>
<div>
								<a href="./roomDetail-gallery.html" class="btn btn-default pull-left mce-content-body" id="mce_233" contenteditable="true" spellcheck="false" style="position: relative;">room details</a><input type="hidden" name="mce_233"><input type="hidden" name="mce_233">
								<a href="./reservation.html" class="btn btn-default pull-right mce-content-body" id="mce_234" contenteditable="true" spellcheck="false" style="position: relative;">book now</a><input type="hidden" name="mce_234"><input type="hidden" name="mce_234">
							</div>
						</div>
					</div>
					<div class="item">
						<img src="[base_url]img/Homepage_Resort_INDEX/slider-img2.jpg" alt="Owl Image"><div class="figcaption">
							<h3 id="mce_235" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">ocean view balcony room</h3><input type="hidden" name="mce_235"><input type="hidden" name="mce_235">
							<div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
								<span id="mce_236" class="mce-content-body" contenteditable="true" spellcheck="false"><img class="img-responsive" src="[base_url]img/cap.png" alt=""></span><input type="hidden" name="mce_236"><input type="hidden" name="mce_236">
							</div>
							<ul>
<li>
									<span class="des mce-content-body" id="mce_237" contenteditable="true" spellcheck="false" style="position: relative;">beds</span><input type="hidden" name="mce_237"><input type="hidden" name="mce_237">
									<span class="detail mce-content-body" id="mce_238" contenteditable="true" spellcheck="false" style="position: relative;">King or 2 Queen</span><input type="hidden" name="mce_238"><input type="hidden" name="mce_238">
								</li>
								<li>
									<span class="des mce-content-body" id="mce_239" contenteditable="true" spellcheck="false" style="position: relative;">occupancy</span><input type="hidden" name="mce_239"><input type="hidden" name="mce_239">
									<span class="detail mce-content-body" id="mce_240" contenteditable="true" spellcheck="false" style="position: relative;">2 People</span><input type="hidden" name="mce_240"><input type="hidden" name="mce_240">
								</li>
								<li>
									<span class="des mce-content-body" id="mce_241" contenteditable="true" spellcheck="false" style="position: relative;">size</span><input type="hidden" name="mce_241"><input type="hidden" name="mce_241">
									<span class="detail mce-content-body" id="mce_242" contenteditable="true" spellcheck="false" style="position: relative;">60 sqm / 650 sqf</span><input type="hidden" name="mce_242"><input type="hidden" name="mce_242">
								</li>
								<li>
									<span class="des mce-content-body" id="mce_243" contenteditable="true" spellcheck="false" style="position: relative;">view</span><input type="hidden" name="mce_243"><input type="hidden" name="mce_243">
									<span class="detail mce-content-body" id="mce_244" contenteditable="true" spellcheck="false" style="position: relative;">Ocean / City</span><input type="hidden" name="mce_244"><input type="hidden" name="mce_244">
								</li>
								<li>
									<span class="des mce-content-body" id="mce_245" contenteditable="true" spellcheck="false" style="position: relative;">rates from</span><input type="hidden" name="mce_245"><input type="hidden" name="mce_245">
									<span class="detail mce-content-body" id="mce_246" contenteditable="true" spellcheck="false" style="position: relative;">215 USD</span><input type="hidden" name="mce_246"><input type="hidden" name="mce_246">
								</li>
							</ul>
<div>
								<a href="./roomDetail-gallery.html" class="btn btn-default pull-left mce-content-body" id="mce_247" contenteditable="true" spellcheck="false" style="position: relative;">room details</a><input type="hidden" name="mce_247"><input type="hidden" name="mce_247">
								<a href="./reservation.html" class="btn btn-default pull-right mce-content-body" id="mce_248" contenteditable="true" spellcheck="false" style="position: relative;">book now</a><input type="hidden" name="mce_248"><input type="hidden" name="mce_248">
							</div>
						</div>
					</div>
					<div class="item">
						<img src="[base_url]img/Homepage_Resort_INDEX/slider-img3.jpg" alt="Owl Image"><div class="figcaption">
							<h3 id="mce_249" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">ocean view balcony room</h3><input type="hidden" name="mce_249"><input type="hidden" name="mce_249">
							<div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
								<span id="mce_250" class="mce-content-body" contenteditable="true" spellcheck="false"><img class="img-responsive" src="[base_url]img/cap.png" alt=""></span><input type="hidden" name="mce_250"><input type="hidden" name="mce_250">
							</div>
							<ul>
<li>
									<span class="des mce-content-body" id="mce_251" contenteditable="true" spellcheck="false" style="position: relative;">beds</span><input type="hidden" name="mce_251"><input type="hidden" name="mce_251">
									<span class="detail mce-content-body" id="mce_252" contenteditable="true" spellcheck="false" style="position: relative;">King or 2 Queen</span><input type="hidden" name="mce_252"><input type="hidden" name="mce_252">
								</li>
								<li>
									<span class="des mce-content-body" id="mce_253" contenteditable="true" spellcheck="false" style="position: relative;">occupancy</span><input type="hidden" name="mce_253"><input type="hidden" name="mce_253">
									<span class="detail mce-content-body" id="mce_254" contenteditable="true" spellcheck="false" style="position: relative;">2 People</span><input type="hidden" name="mce_254"><input type="hidden" name="mce_254">
								</li>
								<li>
									<span class="des mce-content-body" id="mce_255" contenteditable="true" spellcheck="false" style="position: relative;">size</span><input type="hidden" name="mce_255"><input type="hidden" name="mce_255">
									<span class="detail mce-content-body" id="mce_256" contenteditable="true" spellcheck="false" style="position: relative;">60 sqm / 650 sqf</span><input type="hidden" name="mce_256"><input type="hidden" name="mce_256">
								</li>
								<li>
									<span class="des mce-content-body" id="mce_257" contenteditable="true" spellcheck="false" style="position: relative;">view</span><input type="hidden" name="mce_257"><input type="hidden" name="mce_257">
									<span class="detail mce-content-body" id="mce_258" contenteditable="true" spellcheck="false" style="position: relative;">Ocean / City</span><input type="hidden" name="mce_258"><input type="hidden" name="mce_258">
								</li>
								<li>
									<span class="des mce-content-body" id="mce_259" contenteditable="true" spellcheck="false" style="position: relative;">rates from</span><input type="hidden" name="mce_259"><input type="hidden" name="mce_259">
									<span class="detail mce-content-body" id="mce_260" contenteditable="true" spellcheck="false" style="position: relative;">215 USD</span><input type="hidden" name="mce_260"><input type="hidden" name="mce_260">
								</li>
							</ul>
<div>
								<a href="./roomDetail-gallery.html" class="btn btn-default pull-left mce-content-body" id="mce_261" contenteditable="true" spellcheck="false" style="position: relative;">room details</a><input type="hidden" name="mce_261"><input type="hidden" name="mce_261">
								<a href="./reservation.html" class="btn btn-default pull-right mce-content-body" id="mce_262" contenteditable="true" spellcheck="false" style="position: relative;">book now</a><input type="hidden" name="mce_262"><input type="hidden" name="mce_262">
							</div>
						</div>
					</div>
					<div class="item">
						<img src="[base_url]img/Homepage_Resort_INDEX/slider-img2.jpg" alt="Owl Image"><div class="figcaption">
							<h3 id="mce_263" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">ocean view balcony room</h3><input type="hidden" name="mce_263"><input type="hidden" name="mce_263">
							<div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
								<span id="mce_264" class="mce-content-body" contenteditable="true" spellcheck="false"><img class="img-responsive" src="[base_url]img/cap.png" alt=""></span><input type="hidden" name="mce_264"><input type="hidden" name="mce_264">
							</div>
							<ul>
<li>
									<span class="des mce-content-body" id="mce_265" contenteditable="true" spellcheck="false" style="position: relative;">beds</span><input type="hidden" name="mce_265"><input type="hidden" name="mce_265">
									<span class="detail mce-content-body" id="mce_266" contenteditable="true" spellcheck="false" style="position: relative;">King or 2 Queen</span><input type="hidden" name="mce_266"><input type="hidden" name="mce_266">
								</li>
								<li>
									<span class="des mce-content-body" id="mce_267" contenteditable="true" spellcheck="false" style="position: relative;">occupancy</span><input type="hidden" name="mce_267"><input type="hidden" name="mce_267">
									<span class="detail mce-content-body" id="mce_268" contenteditable="true" spellcheck="false" style="position: relative;">2 People</span><input type="hidden" name="mce_268"><input type="hidden" name="mce_268">
								</li>
								<li>
									<span class="des mce-content-body" id="mce_269" contenteditable="true" spellcheck="false" style="position: relative;">size</span><input type="hidden" name="mce_269"><input type="hidden" name="mce_269">
									<span class="detail mce-content-body" id="mce_270" contenteditable="true" spellcheck="false" style="position: relative;">60 sqm / 650 sqf</span><input type="hidden" name="mce_270"><input type="hidden" name="mce_270">
								</li>
								<li>
									<span class="des mce-content-body" id="mce_271" contenteditable="true" spellcheck="false" style="position: relative;">view</span><input type="hidden" name="mce_271"><input type="hidden" name="mce_271">
									<span class="detail mce-content-body" id="mce_272" contenteditable="true" spellcheck="false" style="position: relative;">Ocean / City</span><input type="hidden" name="mce_272"><input type="hidden" name="mce_272">
								</li>
								<li>
									<span class="des mce-content-body" id="mce_273" contenteditable="true" spellcheck="false" style="position: relative;">rates from</span><input type="hidden" name="mce_273"><input type="hidden" name="mce_273">
									<span class="detail mce-content-body" id="mce_274" contenteditable="true" spellcheck="false" style="position: relative;">215 USD</span><input type="hidden" name="mce_274"><input type="hidden" name="mce_274">
								</li>
							</ul>
<div>
								<a href="./roomDetail-gallery.html" class="btn btn-default pull-left mce-content-body" id="mce_275" contenteditable="true" spellcheck="false" style="position: relative;">room details</a><input type="hidden" name="mce_275"><input type="hidden" name="mce_275">
								<a href="./reservation.html" class="btn btn-default pull-right mce-content-body" id="mce_276" contenteditable="true" spellcheck="false" style="position: relative;">book now</a><input type="hidden" name="mce_276"><input type="hidden" name="mce_276">
							</div>
						</div>
					</div>
				</div>
				<div class="customNavigation">
					<a class="prev mce-content-body" id="mce_277" contenteditable="true" spellcheck="false"><i class="fa fa-angle-left"></i></a><input type="hidden" name="mce_277"><input type="hidden" name="mce_277">
					<a class="next mce-content-body" id="mce_278" contenteditable="true" spellcheck="false"><i class="fa fa-angle-right"></i></a><input type="hidden" name="mce_278"><input type="hidden" name="mce_278">
				</div>
			</div>
			<div class="container">
				<a href="./rooms-1col.html" class="btn btn-default mce-content-body" id="mce_279" contenteditable="true" spellcheck="false" style="position: relative;">view all rooms</a><input type="hidden" name="mce_279"><input type="hidden" name="mce_279">
			</div>
		<input type="hidden" name="mce_280"><input type="hidden" name="mce_281"><input type="hidden" name="mce_282"><input type="hidden" name="mce_374"><input type="hidden" name="mce_375"><input type="hidden" name="mce_376"><input type="hidden" name="mce_407"><input type="hidden" name="mce_408"><input type="hidden" name="mce_409"></div>
		
		<!-- TESTIMONIAL -->
		<div class="testimonial" style="position: relative;">
			<div class="container">
				<div class="col-xs-12 col-sm-12 col-md-6 col-centered center-block">
					<div class="row">
						<div class="title">
							<h6 id="mce_283" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">testimonials</h6><input type="hidden" name="mce_283"><input type="hidden" name="mce_283">
							<h3 id="mce_284" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">What Our Guests Say</h3><input type="hidden" name="mce_284"><input type="hidden" name="mce_284">
							<div class="col-xs-6 col-sm-4 col-md-4 cap col-centered center-block">
								<span id="mce_285" class="mce-content-body" contenteditable="true" spellcheck="false"><img class="img-responsive" src="[base_url]img/cap.png" alt=""></span><input type="hidden" name="mce_285"><input type="hidden" name="mce_285">
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-6">
						<div class="testi-block">
							<div class="row">
								<div class="col-xs-12 col-sm-2 col-md-2">
									<img src="[base_url]img/Homepage_Resort_INDEX/testimonial-img1.png" alt="">
</div>
								<div class="col-xs-12 col-sm-10 col-md-10">
									<div class="testi-detail">
										<p id="mce_286" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Morbi versa accumsan ipsum velit nam nec tellus elit tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent conubia per inceptos.</p><input type="hidden" name="mce_286"><input type="hidden" name="mce_286">
										<h6 id="mce_287" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">– T. Robertson</h6><input type="hidden" name="mce_287"><input type="hidden" name="mce_287">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6">
						<div class="testi-block">
							<div class="row">
								<div class="col-xs-12 col-sm-2 col-md-2">
									<img src="[base_url]img/Homepage_Resort_INDEX/testimonial-img1.png" alt="">
</div>
								<div class="col-xs-12 col-sm-10 col-md-10">
									<div class="testi-detail">
										<p id="mce_288" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Morbi versa accumsan ipsum velit nam nec tellus elit tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent conubia per inceptos.</p><input type="hidden" name="mce_288"><input type="hidden" name="mce_288">
										<h6 id="mce_289" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">– B. Silva</h6><input type="hidden" name="mce_289"><input type="hidden" name="mce_289">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="prom-offers">
				<div class="dark-gold"></div>
				<div class="light-gold"></div>
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-5 col-md-5">
							<div class="prom-block">
								<h3 id="mce_290" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Promotions</h3><input type="hidden" name="mce_290"><input type="hidden" name="mce_290">
								<p id="mce_291" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Morbi versa accumsan ipsum velit nam nec tellus elit tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent conubia per inceptos.</p><input type="hidden" name="mce_291"><input type="hidden" name="mce_291">
							</div>
						</div>
						<div class="col-xs-12 col-sm-2 col-md-2">
							<img class="img-responsive" src="[base_url]img/Homepage_Resort_INDEX/testimonial-img2.png" alt="">
</div>
						<div class="col-xs-12 col-sm-5 col-md-5">
							<div class="offer-block">
								<h3 id="mce_292" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Special Offers</h3><input type="hidden" name="mce_292"><input type="hidden" name="mce_292">
								<p id="mce_293" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Morbi versa accumsan ipsum velit nam nec tellus elit tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent conubia per inceptos.</p><input type="hidden" name="mce_293"><input type="hidden" name="mce_293">
							</div>
						</div>
					</div>
					<a href="./promotions.html" class="btn btn-default mce-content-body" id="mce_294" contenteditable="true" spellcheck="false">view all offers</a><input type="hidden" name="mce_294"><input type="hidden" name="mce_294">
				</div>
			</div>
		<input type="hidden" name="mce_295"><input type="hidden" name="mce_296"><input type="hidden" name="mce_297"><input type="hidden" name="mce_377"><input type="hidden" name="mce_378"><input type="hidden" name="mce_379"><input type="hidden" name="mce_410"><input type="hidden" name="mce_411"><input type="hidden" name="mce_412"><input type="hidden" name="mce_117"></div>
		
		<!-- LOCATION -->
		<div class="location" style="position: relative;">
			<div class="container">
				<div class="col-xs-12 col-sm-12 col-md-6 col-centered center-block">
					<div class="row">
						<div class="title">
							<h6 id="mce_298" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">location</h6><input type="hidden" name="mce_298"><input type="hidden" name="mce_298">
							<h3 id="mce_299" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Where to Find Us</h3><input type="hidden" name="mce_299"><input type="hidden" name="mce_299">
							<div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
								<span id="mce_300" class="mce-content-body" contenteditable="true" spellcheck="false"><img class="img-responsive" src="[base_url]img/cap.png" alt=""></span><input type="hidden" name="mce_300"><input type="hidden" name="mce_300">
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-8">
						<div class="map">
							<div id="map">
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-4">
						<ul>
<li>
								<div class="row">
									<div class="col-xs-12 col-sm-4 col-md-6">
										<span class="specific mce-content-body" id="mce_301" contenteditable="true" spellcheck="false" style="position: relative;">address</span><input type="hidden" name="mce_301"><input type="hidden" name="mce_301">
									</div>
									<div class="col-xs-12 col-sm-8 col-md-6">
										<p id="mce_302" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Hotel Fontainbleu 4332 Lombard Street San Francisco, CA 10023 USA</p><input type="hidden" name="mce_302"><input type="hidden" name="mce_302">
									</div>
								</div>
							</li>
							<li>
								<div class="row">
									<div class="col-xs-12 col-sm-4 col-md-6">
										<span class="specific mce-content-body" id="mce_303" contenteditable="true" spellcheck="false" style="position: relative;">telephone</span><input type="hidden" name="mce_303"><input type="hidden" name="mce_303">
									</div>
									<div class="col-xs-12 col-sm-8 col-md-6">
										<p id="mce_304" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">(415) 899-3456</p><input type="hidden" name="mce_304"><input type="hidden" name="mce_304">
									</div>
								</div>
							</li>
							<li>
								<div class="row">
									<div class="col-xs-12 col-sm-4 col-md-6">
										<span class="specific mce-content-body" id="mce_305" contenteditable="true" spellcheck="false" style="position: relative;">email</span><input type="hidden" name="mce_305"><input type="hidden" name="mce_305">
									</div>
									<div class="col-xs-12 col-sm-8 col-md-6">
										<a href="mailto:info@fontainbleu.com" id="mce_306" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">info@fontainbleu.com</a><input type="hidden" name="mce_306"><input type="hidden" name="mce_306">
									</div>
								</div>
							</li>
							<li>
								<div class="row">
									<div class="col-xs-12 col-sm-4 col-md-6">
										<span class="specific mce-content-body" id="mce_307" contenteditable="true" spellcheck="false" style="position: relative;">airport distance</span><input type="hidden" name="mce_307"><input type="hidden" name="mce_307">
									</div>
									<div class="col-xs-12 col-sm-8 col-md-6">
										<p id="mce_308" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">39km (24 miles) from San Francisco International Airport</p><input type="hidden" name="mce_308"><input type="hidden" name="mce_308">
									</div>
								</div>
							</li>
							<li>
								<div class="row">
									<div class="col-xs-12 col-sm-4 col-md-6">
										<span class="specific mce-content-body" id="mce_309" contenteditable="true" spellcheck="false" style="position: relative;">nearby</span><input type="hidden" name="mce_309"><input type="hidden" name="mce_309">
									</div>
									<div class="col-xs-12 col-sm-8 col-md-6">
										<p id="mce_310" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">39km (24 miles) from San Francisco International Airport</p><input type="hidden" name="mce_310"><input type="hidden" name="mce_310">
									</div>
								</div>
							</li>
						</ul>
</div>
				</div>
			</div>
		<input type="hidden" name="mce_311"><input type="hidden" name="mce_312"><input type="hidden" name="mce_313"><input type="hidden" name="mce_380"><input type="hidden" name="mce_381"><input type="hidden" name="mce_382"><input type="hidden" name="mce_413"><input type="hidden" name="mce_414"><input type="hidden" name="mce_415"><input type="hidden" name="mce_118"><input type="hidden" name="mce_119"></div><div style="text-align: center; position: relative;"><input type="hidden" name="mce_383"><input type="hidden" name="mce_384"><input type="hidden" name="mce_385"><input type="hidden" name="mce_416"><input type="hidden" name="mce_417"><input type="hidden" name="mce_418"><input type="hidden" name="mce_121"><input type="hidden" name="mce_122"><input type="hidden" name="mce_123"></div><div style="text-align: center; position: relative;"><input type="hidden" name="mce_419"><input type="hidden" name="mce_420"><input type="hidden" name="mce_421"></div><div style="text-align: center; position: relative;"></div><div style="text-align:center;"></div>
	</section>    </section>        