
    <section id="content" class="spa-main"><!-- PAGE HEADER --><div class="title-color" style="position: relative;">
            <h2 id="mce_40" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">The pool</h2><input type="hidden" name="mce_40">
            <ol class="breadcrumb">
                <li><a href="./index.html" id="mce_41" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Home</a><input type="hidden" name="mce_41"></li>
                <li class="active">The pool</li>
            </ol>
            <input type="hidden" name="mce_43"><input type="hidden" name="mce_44"><input type="hidden" name="mce_45"></div>

        <!-- SPA -->
        <div class="spa-block" style="position: relative;">
            <div class="db-image">
                <div class="dbi-inner" style="height:100%; background:url(<?= base_url() ?>img/spa/1.jpg) no-repeat center center">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="spa-right">
                    <h1 id="mce_46" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">THE BEST WAY OF NURTURING YOURSELF</h1><input type="hidden" name="mce_46">
                    <h6 id="mce_47" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Enjoy a Spa experience in Cal Prat</h6><input type="hidden" name="mce_47">
                    <p id="mce_48" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">During your stay, you could find everything you were looking for: silence, calm, relax…</p><input type="hidden" name="mce_48">
                    <p id="mce_49" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Take pleasure in our heated swimming pool with solarium, where you can have a bath, sunbathe or just read a book.The pool is provided with a system for swimming against the current, a relaxing waterfall, lighting, solarium and underfloor heating.

The pool works with salt chlorination system, that not only is respectful with the environment but also is protecting your skin.</p><input type="hidden" name="mce_49">
                    <h3 id="mce_50" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;"></h3><input type="hidden" name="mce_50">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <ul class="specific">
                                <li>Solarium area for sunbathing</li>
                                <li>Sunbed</li>
                                
                            </ul>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <ul class="specific">
                                <li>Sofa in the pool area</li>
                                <li>Pool towels</li>
                                                            </ul>


                        </div>
                    </div>
                    <h3 id="mce_50" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;"></h3>
                    <div class="row" uk-lightbox>
                        [foreach:galeria_piscina]
                            <div class="col-xs-12 col-sm-3" style="margin-top: 29px;"><a href="[foto]"><img src="[foto]" alt="" style="width:100%"></a></div>
                        [/foreach]
                    </div>
                </div>
            </div>
        </div>

        <div class="spa-img-bot" style="position: relative;">
            <div class="spa-natural">
                <h2 id="mce_55" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">We only use natural products</h2><input type="hidden" name="mce_55">
                <div class="col-xs-12 col-sm-6 col-md-6 cap col-centered center-block">
                    <span id="mce_56" class="mce-content-body" contenteditable="true" spellcheck="false"><img class="img-responsive" src="[base_url]img/cap.png" alt=""></span><input type="hidden" name="mce_56">
                </div>
                <p id="mce_57" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">The pool works with salt chlorination system, that not only is respectful with the environment but also is protecting your skin.</p><input type="hidden" name="mce_57">
                <p id="mce_58" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;"></p><input type="hidden" name="mce_58">
            </div>
        </div>
        <div style="text-align:center;"></div>
    </section>