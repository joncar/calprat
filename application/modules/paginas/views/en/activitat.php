
    <section id="content" class="gallery-main">
    
        <div class="title-color">
        <h2>ACTIVITIES & TOURS</h2>
        <ol class="breadcrumb">
    <li><a href="<?= base_url() ?>">Inici</a></li>
    <li><a href="#">Activities & Tours</a></li>
<!--            <li><a href="--><?//= base_url() ?><!--">Activitats i Excursions</a></li>-->
            <!--                <li class="active"><a>Activities &amp; Excursions</a></li>-->
        </ol>
    </div>

    <!-- ACTIVITIES CONTENT -->
    <div class="activitie-block">
        <div class="container">
            <div class="col-xs-12 col-sm-12 col-md-8 col-centered center-block">
                <div class="row">
                    <div class="title">
                        <h6>Do not know what to do?</h6>
                        <h1>Activities &amp; Tours</h1>
                        <div class="col-xs-6 col-sm-4 col-md-4 cap col-centered center-block">
                            <span><img class="img-responsive" src="[base_url]img/cap.png" alt=""></span>
                        </div>
                        <p>Here you have a brief overview of events near Cal Prat. A thousand places to go, a thousand hikes to enjoy. </p>
                    </div>
                </div>
            </div>

            <div class="" style=" padding: 0px 0 0 0;">

                




                





            </div>
        </div>
        <div class="gallery-block">
            <div class="main-room">
                <ul>
                    <li class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                        <div class="img">
                            <img src="<?= base_url() ?>theme/images/Gallery/1.jpg" class="img-responsive" alt="">
                            <div class="mr-head">
                                <h3>Chose the one you prefer, enjoy the surroundings </h3>
                                <div class="col-md-6 cap col-centered center-block">
                                    <span><img class="img-responsive" src="<?= base_url() ?>theme/images/cap.png" alt=""></span>
                                </div>
                                <p>Cal Prat is more than silence and relax.</p>
                                <p>Here you find a small recap of activities but there are much more to do, don’t hesitate of contact us for more information.</p>
                            </div>
                        </div>
                    </li>
                    [foreach:agenda]
                        <li class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <div class="mr-inner">
                                <img src="[foto]" class="img-responsive" alt="">
                                <div class="mr-overlay">
                                    <h2>[titulo]</h2>
                                    
                                    <p>[explicacion]</p>
                                    <a class="room-detail" href="[enlace]" target="_new">+ Info</a>
                                </div>
                            </div>
                        </li>
                    [/foreach]
                </ul>
            </div>
        </div>
    </section>