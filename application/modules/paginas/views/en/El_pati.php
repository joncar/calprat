
    <section id="content" class="spa-main">
        <!-- PAGE HEADER -->
        <div class="title-color" style="position: relative;">
            <h2 id="mce_40" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">El pati, BBQ</h2>
            <input type="hidden" name="mce_40">
            <ol class="breadcrumb">
                <li><a href="./index.html" id="mce_41" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Inici</a><input type="hidden" name="mce_41"></li>
                
                <li class="active">El pati</li>
            </ol>
            <input type="hidden" name="mce_43"><input type="hidden" name="mce_44"><input type="hidden" name="mce_45">
        </div>
        <!-- SPA -->
        <div class="spa-block" style="position: relative;">
            <div class="db-image">
                <div class="dbi-inner" style="height:100%; background:url(<?= base_url() ?>img/spa/9.jpg) no-repeat center center">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="spa-right">
                    <h1 id="mce_46" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">THE PERFECT PLACE FOR MEETING YOUR FRIENDS OR FAMILY</h1>
                    <input type="hidden" name="mce_46">
                    <h6 id="mce_47" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">The yard is biggest enough for sunbathing, eat out or just relaxing.</h6>
                    <input type="hidden" name="mce_47">
                    <p id="mce_48" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">
                        Thanks to the barbecue you can eat out in the porch (table and chairs), protected from the sun by a pergola. BBQ material is provided.
                    </p>
                    <input type="hidden" name="mce_48">
                    <p id="mce_49" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">
                        
                    </p>
                    <input type="hidden" name="mce_49">
                    <h3 id="mce_50" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">What you can find in the terrace</h3>
                    <input type="hidden" name="mce_50">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <ul class="specific">
                                <li>Barbecue with coals, lighter, tongs, kitchen cloths, dustpan for ashes…</li>
                                <li>Table, chairs and benches</li>
                                <li>Pergola for sun protection</li>
                                
                            </ul>
                        </div>
                        <!-- 
<div class="col-xs-12 col-sm-6 col-md-6">
                            <ul class="specific">
                                <li>Zona ajardinada</li>
                                <li>fanals per la nit</li>
                                <li>Nevera aprop</li>
                                <li>Lavabo</li>
                                <li>Accés directe a la sala de jocs</li>
                            </ul>
                        </div>
 -->
                    </div>
                    <h3 id="mce_50" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Terrace photos</h3>
                    <div class="row" uk-lightbox>
                        [foreach:galeria_patio]
                            <div class="col-xs-12 col-sm-3" style="margin-top: 29px;"><a href="[foto]"><img src="[foto]" alt="" style="width:100%"></a></div>
                        [/foreach]
                    </div>
                </div>
            </div>
    </section>