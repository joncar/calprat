<section id="content" class="business-main">
	
	<!-- PAGE HEADER -->
	<div class="title-color">
		<h2>Political of reservation and cancellations</h2>
		<ol class="breadcrumb">
			<li><a href="<?= site_url() ?>">Home</a></li>
			<li class="active"><a href="#">Political of reservation and cancellations</a></li>
		</ol>
	</div>
	
	<!-- MAIN CONTENT -->
	<div class="container">
		<div class="col-xs-12 col-sm-9 col-md-9 col-centered center-block">
			<div class="row">
				<div class="title">
					<h6>&nbsp;</h6>
					<h1>Political of reservation and cancellations</h1>
					<div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
						<span><img class="img-responsive" src="<?= base_url() ?>img/cap.png" alt=""></span>
					</div>
					<p>It is necessary Meadow concerns for the security of the data that our customers provide us and for like this follows the new legislation of protection of data.</p>
				</div>
			</div>
		</div>
	</div>
	<div class="business-detail" style="margin-bottom:39px">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
                    <h6 style="font-weight: 700;"> PAY AND SIGNAL FOR CHILDBIRTH OF THE PEOPLE USERS</h6>
					<p>The people headlines of the establishments of accommodation can demand to the people users that effect a reservation, an overtaking of the price that understands on account of the resultant amount of the services loaned.<br/>
In this sense to formalise the reservation (minimum two nights) to our establishment, It Is necessary Meadow will request an income of the 25€ of the total amount indicating your name, D.N.I. And day to begin with.<br/>
The payment of the rest will effect the same day of exit in cash or for banking transfer.<br/></p>
					<br><br>
					<h6 style="font-weight: 700;">CANCELLATION OF THE RESERVATION</h6>
					<p>1. The establishment of tourist accommodation is forced to inform to the person user, before the agreement, on the clauses of cancellation.<br/>
2. The cancellation of the reservation within of the 15 previous days to the date of employment, will give place to the loss of the quantity rid on account.<br/>
a) It reserves for 2 or fewer days, 50% of the total price of the stay.<br/>
b) Reserves for more than 2 days until 7 days, 35% of the total price of stay.<br/>
c) Reserves for more than 7 days, 25% of the total price of stay.<br/>
The previous penalties are not of application when the cancellation produces owing to force majeure, properly accredited.<br/>
3. The person user has right to cancel the reservation confirmed, without any penalty, whenever it do before the 15 previous days to the date of arrival, yeast contrary pact. <br><br></p>
					<h6 style="font-weight: 700;">REWARD FOR RENUNCIATION OF THE STAY</h6>
					<p>The person title of the establishment of tourist accommodation is forced to inform the person or people users, before the realisation of the agreement, on the applicable rule in case of renunciation of the stay.<br/>
When the customer or customer of an establishment of accommodation abandon the unit reserved before the date until which had it reserved, the person title of the establishment will not return any amount of the reservation effected.<br/>
					<br><br></p>
					
				</div>
			</div>
			
		</div>
	</div>
</section>