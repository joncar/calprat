<section id="content" class="spa-main"><!-- PAGE HEADER --><div class="title-color" style="position: relative;">
            <h2 id="mce_40" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Sala de jocs</h2><input type="hidden" name="mce_40">
            <ol class="breadcrumb">
                <li><a href="./index.html" id="mce_41" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Inici</a><input type="hidden" name="mce_41"></li>
                <li class="active">Room of games</li>
            </ol>
            <input type="hidden" name="mce_43"><input type="hidden" name="mce_44"><input type="hidden" name="mce_45"></div>

        <!-- SPA -->
        <div class="spa-block" style="position: relative;">
            <div class="db-image">
                <div class="dbi-inner" style="height:100%; background:url(<?= base_url() ?>img/spa/22.jpg) no-repeat center center">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="spa-right">
                    <h1 id="mce_46" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">The children's corner</h1><input type="hidden" name="mce_46">
                    <h6 id="mce_47" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">The children would love playing here.</h6><input type="hidden" name="mce_47">
                    <p id="mce_48" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Beside the terrace there is the games room, a room plenty of all ages playthings.</p><input type="hidden" name="mce_48">
                    <p id="mce_49" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Adults will feel relaxed while eating out with the kids playing safely just near them.</p><input type="hidden" name="mce_49">
                    <h3 id="mce_50" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Playthings</h3><input type="hidden" name="mce_50">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <ul class="specific">
                                <li>Table football</li>
                                <li>Dolls and doll accessories (carriage, cradle)</li>
                                <li>Play kitchen with its tools</li>
                                <li>Car parking</li>
                                <li>2 skateboards and 3 helmets</li>
                                <li>3 Tricycle</li>
                                <li>2 Molto bikes</li>
                                <li>2 Roller skates</li>
                                <li>Storybooks</li>
                            </ul>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <ul class="specific">
                                <li>Teddies</li>
                                <li>Magnetized board with letters</li>
                                <li>Chess</li>
                                <li>“Parxís” game</li>
                                <li>Badminton set</li>
                                <li>Bowling</li>
                            </ul>
                        </div>
                    </div>

                    <h3 id="mce_50" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Game Room photos</h3>
                    <div class="row" uk-lightbox>
                        [foreach:galeria_joc]
                            <div class="col-xs-12 col-sm-3" style="margin-top: 29px;"><a href="[foto]"><img src="[foto]" alt="" style="width:100%"></a></div>
                        [/foreach]
                    </div>
                </div>
                
            </div>
        </div>
</section>  