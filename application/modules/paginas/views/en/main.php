<div class="pre-header">
    <ul>
        <li class="ph-tel">
            <div class="dropdown">
              <a style="color: #AF9A5F;" id="dLabel" data-target="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-flag-o fa-2x" style="vertical-align: top; font-size:1.3em"></i> ENG
                <span class="caret"></span>
              </a>

              <ul class="dropdown-menu" aria-labelledby="dLabel">
                <li><a href="<?= base_url('main/traduccion/ca') ?>">CAT</a></li>
                <li><a href="<?= base_url('main/traduccion/es') ?>">ESP</a></li>
                <li><a href="<?= base_url('main/traduccion/en') ?>">ENG</a></li>
                <li><a href="<?= base_url('main/traduccion/fr') ?>">FRA</a></li>
              </ul>
            </div>
        </li>
        <li class="ph-tel"><a href="tel:+34650936400" style="color:inherit;"> <i class="fa fa-mobile fa-2x" style="vertical-align: top;color: #AF9A5F;"></i> <span>+34 650 93 64 00</span></a></li>
        <li class="ph-weather"><img src="[iconotemp]" alt=""/> <span>[temp]  &deg;</span></li>
        <li class="ph-social">
            <div>
                <a href="https://www.facebook.com/Cal-Prat-TurismeRural-143945365955474/" class="fa fa-facebook"></a>
                <a href="https://www.pinterest.com/username" class="fa fa-pinterest"></a>
                <a href="https://www.youtube.com/channel/UCrMyhl_KCzMONdNgmyJJmAA" class="fa fa-youtube"></a>
                <a href="https://www.instagram.com/calprat_turismerural/?hl=es" class="fa fa-instagram"></a>
            </div>
        </li>
        <li class="ph-search">
            <span class="ss-trigger fa fa-search"></span>
            <div class="ss-content">
                <span class="ss-close fa fa-times"></span>
                <div class="ssc-inner">
                    <form action="<?= base_url('paginas/frontend/buscar') ?>">
                        <input type="text" name="q" placeholder="Què busques?">
                        <button type="submit"><i class="fa fa-search"></i></button>
                    </form>
                </div>
            </div>
        </li>
    </ul>
</div>
<div class="top-slider" style="display: none">
    <div id="top-slider">
        <ul class="slides">
            [foreach:banner]
            <li>
                <img src="[foto]" alt="">
                <div class="flex-caption">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 id="mce_91" class="mce-content-body mce-edit-focus" >[titulo]</h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <p id="mce_92">[subtitulo]</p>
                        </div>
                    </div>
                </div>
            </li>
            [/foreach]
        </ul>
        <ul class="flex-direction-nav">
            <li><a class="flex-prev mce-content-body" id="mce_99" ><i class="fa fa-angle-left"></i></a></li>
            <li><a class="flex-next mce-content-body" id="mce_100" ><i class="fa fa-angle-right"></i></a></li>
        </ul>
    </div>
</div>
<!-- WELCOME -->
<section id="content">
    <div class="welcome" style="position: relative;">
        <div class="container">
            <div class="col-xs-12 col-sm-12 col-md-6 col-centered center-block">
                <div class="row">
                    <div class="title">
                        <h6 id="mce_101">Welcome to</h6>
                        <h3 id="mce_102">The country house Cal Prat</h3>
                        <div class="col-xs-6 col-sm-4 col-md-4 cap col-centered center-block">
                            <span id="mce_103"><img alt="" src="[base_url]img/cap.png" class="img-responsive"></span>
                        </div>
                        <p id="mce_104">Cal Prat is an ancient country house of the eighteenth century situated in the small village of El Soler, in the municipal area of Calonge de Segarra. The cottage is placed in Alta Anoia, that is surrounded by the neighbouring areas of La Segarra and El Solsonès.
</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="left-welcome">
                        <h2 id="mce_105">The perfect place for a getaway</h2>
                        <p id="mce_106">La vostra estada a Cal Prat us permetrà gaudir-ne tot imaginant-vos com ha estat la vida de la nostra pagesia en els darrers segles, i podreu fruir del paisatge i la tranquil·litat del món rural d’avui.</p>
                        <p id="mce_107">The house (originally built in the XVIII century) recently restored with respect of its historical characteristics keeping the farmer style in the cellar, el cup del vi, the barn, the wooden oven or the fireplace.</p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="right-welcome">
                        <div id="promovid">
                            <div class="custom-th">
                                <img src="[base_url]img/Homepage_Resort_INDEX/video.png" class="img-responsive" alt="">
                            </div>
                            <div id="thevideo" style="display:none; padding: 0px;">                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ACCOMODATION -->
    <div class="accomodation" style="position: relative;">
        <div class="container">
            <div class="col-xs-12 col-sm-12 col-md-6 col-centered center-block">
                <div class="row">
                    <div class="title">
                        <h6 id="mce_111">Facilities</h6>
                        <h3 id="mce_112">Accommodations & Areas</h3>
                        <div class="col-xs-6 col-sm-4 col-md-4 cap col-centered center-block">
                            <span id="mce_113"><img alt="" src="[base_url]img/cap.png" class="img-responsive"></span>
                        </div>
                        <p id="mce_114">El Forn and El Celler are the names of Cal Prat accommodations, with capacity for 8 and 5 people respectively. The house can be rented entirely, as both accommodations could be connected. 
</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-room">
            <ul>
                <li class="col-xs-12 col-sm-6 col-md-3">
                    <div class="mr-inner">
                        <img src="[base_url]img/Homepage_Resort_INDEX/room-img1.jpg" class="img-responsive" alt="">
                        <div class="mr-overlay">
                            <h2 id="mce_115">Your home<br>13 people</h2>
                            <p id="mce_116">Rent the entire house for more privacity. Ideal place for families and group of friends.</p>
                            <a class="room-detail mce-content-body" href="[base_url]habitacion/1-tota-la-casa" id="mce_117" >Read more<i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </li>
                <li class="col-xs-12 col-sm-6 col-md-3">
                    <div class="mr-inner">
                        <img src="[base_url]img/Homepage_Resort_INDEX/room-img2.jpg" class="img-responsive" alt="">
                        <div class="mr-overlay">
                            <h2 id="mce_118">El forn<br>8 people</h2>
                            <p id="mce_119">El Forn, in the northeast part of the house, has a capacity for 8 people.<br><br>
                                </p>
                            <a class="room-detail mce-content-body" href="[base_url]habitacion/2-el-form" id="mce_120" >Read more<i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </li>
                <li class="col-xs-12 col-sm-6 col-md-3">
                    <div class="mr-inner">
                        <img src="[base_url]img/Homepage_Resort_INDEX/room-img3.jpg" class="img-responsive" alt="">
                        <div class="mr-overlay">
                            <h2 id="mce_121">El celler<br>5 people</h2>
                            <p id="mce_122">El Celler, in the southeast part of the house, has a capacity for 5 people.<br><br>
                               </p>
                            <a class="room-detail mce-content-body" href="[base_url]habitacion/3-el-celler" id="mce_123" >Read more<i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </li>
                <li class="col-xs-12 col-sm-6 col-md-3">
                    <div class="mr-inner">
                        <img src="[base_url]img/Homepage_Resort_INDEX/room-img4.jpg" class="img-responsive" alt="">
                        <div class="mr-overlay">
                            <h2 id="mce_124">The private<br>pool</h2>
                            <p id="mce_125">Find what you are looking for in your stay in Cal Prat: silence, peace, relax…<br><br>
                                </p>
                            <a class="room-detail mce-content-body" href="[base_url]p/la_piscina.html" id="mce_126" >Read more<i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <input name="mce_127" type="hidden"><input name="mce_128" type="hidden"></div>
        <!-- ROOM OFFER -->
        <div class="room-offer" style="position: relative;">
            <div class="container">
                <h3 id="mce_130">ALL ACCOMMODATIONS ARE PROVIDED WITH</h3><input name="mce_130" type="hidden">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1">
                        <ul class="specific">
                            <li>Heating system</li>
                            <li>Fireplace (wood included)</li>
                            <li>TV</li>
                            <li>WiFi</li>
                            <li>Bed linen</li>
                            <li>Towels and pool towels</li>
                            <li>Microwaves / Fridge</li>
                            <li>Dishwasher / Washing machine</li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1">
                        <ul class="specific">
                            <li>Nespresso coffee maker / Italian coffee maker</li>
                            <li>Baby cot / Highchair / Bed rails (90cm)</li>
                            <li>First-aid kit</li>
                            <li>Board games</li>
                            <li>DVD / CD player</li>
                            <li>Cleaning products for kitchen</li>
                            <li>Tablecloth</li>
                        </ul>
                    </div>
                </div>
            </div>
            <input name="mce_131" type="hidden"><input name="mce_132" type="hidden"><input name="mce_133" type="hidden"></div>
            <div class="home-gal" style="position: relative;">
                <div class="dining-block dual-slider row">
                    <div class="col-md-7 padding-right-none">
                        <div id="gal-slider1" class="flexslider">
                            <ul class="slides">
                                <li><img class="img-responsive" src="[base_url]img/Homepage_Resort_INDEX/home-gal-img1.png" alt=""></li>
                            </ul>
                            <!--<a class="flex-prev1 mce-content-body" id="mce_134" ><i class="fa fa-chevron-left"></i></a><input name="mce_134" type="hidden">
                            <a class="flex-next1 mce-content-body" id="mce_135" ><i class="fa fa-chevron-right"></i></a><input name="mce_135" type="hidden">-->
                        </div>
                    </div>
                    <div class="col-md-5 padding-left-none">
                        <ul class="gal-nav1">
                            <li class="flex-active">
                                <div>
                                    <h2 id="mce_136">Enjoy with your loved ones</h2><input name="mce_136" type="hidden">
                                    <p id="mce_137">Cal Prat is characterized by its peacefulness, surrounded by nature, the perfect place for a stay with family or friends. </p><input name="mce_137" type="hidden">
                                    <p id="mce_138">However, this area has many interesting places which are well worth visiting.</p><input name="mce_138" type="hidden">
                                    <a href="[base_url]p/activitat.html" class="btn btn-default mce-content-body" id="mce_139" >More details</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- SPA -->
                <div class="spa-block dual-slider row">
                    <div class="hidden-xs col-md-5 padding-right-none">
                        <ul class="gal-nav2">
                            <li class="flex-active">
                                <div>
                                    <h2 id="mce_152">LET YOURSELF BE PAMPERED</h2><input name="mce_152" type="hidden">
                                    <p id="mce_153"></p><input name="mce_153" type="hidden">
                                    <p id="mce_154">Cal Prat is specialized in Rural Holidays for families and couples. Our main aim is to offer our guests relaxation and tranquility away from the frenetic pace of today’s society.  The country house is placed in a bucolic retreat with a private Spa, the Cal Prat gem.</p><input name="mce_154" type="hidden">
                                    <a href="[base_url]p/la_piscina.html" class="btn btn-default mce-content-body" id="mce_155" >More details</a><input name="mce_155" type="hidden">
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-7 padding-left-none">
                        <div id="gal-slider2" class="flexslider">
                            <ul class="slides">
                                <li><img class="img-responsive" src="[base_url]img/Homepage_Resort_INDEX/home-gal-img2.jpg" alt=""></li>
                            </ul>
                            <!--<a class="flex-prev2 mce-content-body" ><i class="fa fa-chevron-left"></i></a><input name="mce_168" type="hidden">
                            <a class="flex-next2 mce-content-body" ><i class="fa fa-chevron-right"></i></a><input name="mce_169" type="hidden">-->
                        </div>
                    </div>
                    <div class="visible-xs col-md-5 padding-right-none">
                        <ul class="gal-nav2">
                            <li class="flex-active">
                                <div>
                                    <h2 id="mce_152">LET YOURSELF BE PAMPERED</h2><input name="mce_152" type="hidden">
                                    <p id="mce_153"></p><input name="mce_153" type="hidden">
                                    <p id="mce_154">Cal Prat is specialized in Rural Holidays for families and couples. Our main aim is to offer our guests relaxation and tranquility away from the frenetic pace of today’s society.  The country house is placed in a bucolic retreat with a private Spa, the Cal Prat gem.</p><input name="mce_154" type="hidden">
                                    <a href="[base_url]p/la_piscina.html" class="btn btn-default mce-content-body" id="mce_155" >More details</a><input name="mce_155" type="hidden">
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- 
<div class="entertain-block dual-slider row">
                    <div class="col-md-7 padding-right-none">
                        <div id="gal-slider3" class="flexslider">
                            <ul class="slides">
                                <li><img class="img-responsive" src="[base_url]img/Homepage_Resort_INDEX/home-gal-img3.png" alt=""></li>
                            </ul>
                            <!~~<a class="flex-prev3 mce-content-body" id="mce_170" ><i class="fa fa-chevron-left"></i></a><input name="mce_170" type="hidden">
                            <a class="flex-next3 mce-content-body" id="mce_171" ><i class="fa fa-chevron-right"></i></a><input name="mce_171" type="hidden">~~>
                        </div>
                    </div>
                    <div class="col-md-5 padding-left-none">
                        <ul class="gal-nav3">
                            <li class="flex-active">
                                <div>
                                    <h2 id="mce_172">Discover the place</h2><input name="mce_172" type="hidden">
                                    <p id="mce_173">You can come to Cal Prat for a relaxing stay, wander around, cycling the outskirts or just chilling out in the swimming pool.</p><input name="mce_173" type="hidden">
                                    <p id="mce_174">Guided visits, Hikes, Horse rides, Mountain bike routes, Guided cultural visits, Balloon rides…</p><input name="mce_174" type="hidden">
                                    <a href="[base_url]p/la_piscina.html" class="btn btn-default mce-content-body" id="mce_175" >More details</a><input name="mce_175" type="hidden">
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
 -->
                <input name="mce_188" type="hidden"><input name="mce_189" type="hidden"><input name="mce_190" type="hidden"></div>
                <div class="activ-eve" style="position: relative;">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <h3 id="mce_191">Activities</h3><input name="mce_191" type="hidden">
                                <p id="mce_192">Here there is plenty to do. Ask for more information, we are delighted to recommend you activities and routes. We also have some discounts. </p><input name="mce_192" type="hidden">
                                <ul class="specific">
                                    <li class="col-xs-12 col-sm-6 col-md-6">Guided visits</li>
                                    <li class="col-xs-12 col-sm-6 col-md-6">Astronomical observations</li>
                                    <li class="col-xs-12 col-sm-6 col-md-6">Mountain bike routes</li>
                                    <li class="col-xs-12 col-sm-6 col-md-6">Adventure activities</li>
                                    <li class="col-xs-12 col-sm-6 col-md-6">Horse rides</li>
                                    <li class="col-xs-12 col-sm-6 col-md-6">Geocaching</li>
                                </ul>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <h3 id="mce_193">Events</h3><input name="mce_193" type="hidden">
                                <p id="mce_194">Here there is plenty to do. Ask for more information, we are delighted to recommend you the events around. We also have some discounts. </p><input name="mce_194" type="hidden">
                                <ul class="specific">
                                    <li class="col-xs-12 col-sm-6 col-md-6">Music festivals</li>
                                    <li class="col-xs-12 col-sm-6 col-md-6">Fairs and markets</li>
                                    <li class="col-xs-12 col-sm-6 col-md-6">Medieval castle routes</li>
                                    <li class="col-xs-12 col-sm-6 col-md-6">Trails and walks</li>
                                    <li class="col-xs-12 col-sm-6 col-md-6">Pastorets de Calaf (Shepherds)</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="learn-more">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="row">
                                <img class="img-responsive" src="[base_url]img/Homepage_Resort_INDEX/learn-img1.jpg" alt="">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="row">
                                <img class="img-responsive" src="[base_url]img/Homepage_Resort_INDEX/learn-img2.jpg" alt="">
                            </div>
                        </div>
                        <a href="[base_url]agenda" class="btn btn-default mce-content-body" id="mce_195" spellcheck="false">Read more</a>
                    </div>
                </div>
                <!-- SERVICES -->
                <div class="activ-eve" style="position: relative;">
                    <div class="container">
                        <div class="col-xs-12 col-sm-12 col-md-6 col-centered center-block">
                            <div class="title" style="padding-bottom: 0px">
                                <h6 id="mce_199">Certificate</h6><input name="mce_199" type="hidden">
                                <h3 id="mce_200">Accommodation with Biosphere Certification</h3><input name="mce_200" type="hidden">
                                <div class="col-xs-6 col-sm-4 col-md-4 cap col-centered center-block">
                                    <span id="mce_201"><img class="img-responsive" src="[base_url]img/cap.png" alt=""></span><input name="mce_201" type="hidden">
                                </div>
                                <p id="mce_202">Cal Prat accommodation has been recognized within the Biosphere Responsible Tourism Certification. We are committed to the environment, supporting a responsible and sustainable tourism.</p><input name="mce_202" type="hidden">
                            </div>                            
                        </div>
                        
                        <div class="col-xs-12 col-sm-12 col-md-12" style="padding-bottom: 30px">
                            <p style="text-align:center; margin-top:41px"><img src="[base_url]img/bio.jpg"></p>
                            <p id="mce_192" style="text-align:center">
                                This certification is given by the Institute of Responsible Tourism, an organization associated with the UNESCO and partner of the Word Tourism Organization and the Global Sustainable Tourism Council. This is a pioneering certification, based on the 17 Sustainable Development goals of the United Nations embedded in Events 2030.<br>
                                The quality label confirms that the three tourism brands - “Costa Barcelona”, “Paisatges Barcelona” and “Pirineus Barcelona” led by the Diputació de Barcelona meet the requirements of “Biosphere Destionation” standard and apply successfully 6 specific criteria in their tourism policies: Sustainable Management, Economic and Social development, Conservation and Improvement of Cultural heritage, Environmental conservation, Quality and Safety and Visitor involvement. 
                            </p>
                        </div>
                            
                    </div>
                </div>
            </div>
                        <div class="home-img-panel parallax-bg" data-stellar-ratio="2" data-stellar-background-ratio="0.5" style="position: relative;">
                            <div class="panel-img-detail">
                                <h2 id="mce_207">We make it easier for planning your stay</h2><input name="mce_207" type="hidden">
                                <div class="col-xs-7 col-sm-7 col-md-7 cap col-centered center-block">
                                    <span id="mce_208"><img alt="" src="[base_url]img/cap.png" class="img-responsive"></span><input name="mce_208" type="hidden">
                                </div>
                                <p id="mce_209">Are you thinking of surprising your partner? Are you planning a family meeting? Do you want to do a birthday celebration for your children? Or a celebration for your parents silver/golden wedding anniversary? Or maybe an informal working meeting? 
Whatever you are thinking of, contact us and we will guide you in the planification of the event.
                                    </p><input name="mce_209" type="hidden">
                                    <a href="[base_url]iniciar-reserva" class="btn btn-default mce-content-body" id="mce_210" >More details</a><input name="mce_210" type="hidden">
                                </div>
                                <input name="mce_211" type="hidden"><input name="mce_212" type="hidden"><input name="mce_213" type="hidden"></div>
                                <div class="main-gallery" style="position: relative;">
                                    <div class="container">
                                        <div class="col-xs-12 col-sm-12 col-md-6 col-centered center-block">
                                            <div class="row">
                                                <div class="title">
                                                    <h3 class="light-title mce-content-body" id="mce_214" >Gallery</h3><input name="mce_214" type="hidden">
                                                    <div class="col-xs-6 col-sm-4 col-md-4 cap col-centered center-block">
                                                        <span id="mce_215"><img alt="" src="[base_url]img/cap.png" class="img-responsive"></span><input name="mce_215" type="hidden">
                                                    </div>
                                                    <p id="mce_216">Meet our country house through the photo gallery. Every corner is special and different.</p><input name="mce_216" type="hidden">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="main-gal-slider">
                                        <div id="home-gallery" class="owl-carousel owl-theme">
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img3.jpg" alt="Owl Image">
                                                <!--<div class="figcaption">
                                                    <h3>Sala d'estar Forn</h3>
                                                    <div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
                                                        <span><img class="img-responsive" src="<?= base_url() ?>img/cap.png" alt=""></span>
                                                    </div>
                                                    <ul>
                                                        <li>
                                                            <span class="des">beds</span>
                                                            <span class="detail">King or 2 Queen</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">occupancy</span>
                                                            <span class="detail">2 People</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">size</span>
                                                            <span class="detail">60 sqm / 650 sqf</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">view</span>
                                                            <span class="detail">Ocean / City</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">rates from</span>
                                                            <span class="detail">215 USD</span>
                                                        </li>
                                                    </ul>
                                                    <div>
                                                        <a href="./roomDetail-gallery.html" class="btn btn-default pull-left">Informació sala</a>
                                                        <a href="./reservation.html" class="btn btn-default pull-right">Reservar</a>
                                                    </div>
                                                </div>-->
                                            </div>
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img2.jpg" alt="Owl Image">
                                                <!--<div class="figcaption">
                                                    <h3>Dormitori Forn 1</h3>
                                                    <div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
                                                        <span><img class="img-responsive" src="<?= base_url() ?>img/cap.png" alt=""></span>
                                                    </div>
                                                    <ul>
                                                        <li>
                                                            <span class="des">Llit doble de matrimoni</span>
                                                            <span class="detail">King or 2 Queen</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">occupancy</span>
                                                            <span class="detail">2 People</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">size</span>
                                                            <span class="detail">60 sqm / 650 sqf</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">view</span>
                                                            <span class="detail">Ocean / City</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">rates from</span>
                                                            <span class="detail">215 USD</span>
                                                        </li>
                                                    </ul>
                                                    <div>
                                                        <a href="./roomDetail-gallery.html" class="btn btn-default pull-left">room details</a>
                                                        <a href="./reservation.html" class="btn btn-default pull-right">book now</a>
                                                    </div>
                                                </div>-->
                                            </div>
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img4.jpg" alt="Owl Image">
                                                <!--<div class="figcaption">
                                                    <h3>Cuina</h3>
                                                    <div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
                                                        <span><img class="img-responsive" src="<?= base_url() ?>img/cap.png" alt=""></span>
                                                    </div>
                                                    <ul>
                                                        <li>
                                                            <span class="des">beds</span>
                                                            <span class="detail">King or 2 Queen</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">occupancy</span>
                                                            <span class="detail">2 People</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">size</span>
                                                            <span class="detail">60 sqm / 650 sqf</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">view</span>
                                                            <span class="detail">Ocean / City</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">rates from</span>
                                                            <span class="detail">215 USD</span>
                                                        </li>
                                                    </ul>
                                                    <div>
                                                        <a href="./roomDetail-gallery.html" class="btn btn-default pull-left">room details</a>
                                                        <a href="./reservation.html" class="btn btn-default pull-right">book now</a>
                                                    </div>
                                                </div>-->
                                            </div>
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img5.jpg" alt="Owl Image">
                                                <!--<div class="figcaption">
                                                    <h3>ocean view balcony room</h3>
                                                    <div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
                                                        <span><img class="img-responsive" src="<?= base_url() ?>img/cap.png" alt=""></span>
                                                    </div>
                                                    <ul>
                                                        <li>
                                                            <span class="des">beds</span>
                                                            <span class="detail">King or 2 Queen</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">occupancy</span>
                                                            <span class="detail">2 People</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">size</span>
                                                            <span class="detail">60 sqm / 650 sqf</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">view</span>
                                                            <span class="detail">Ocean / City</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">rates from</span>
                                                            <span class="detail">215 USD</span>
                                                        </li>
                                                    </ul>
                                                    <div>
                                                        <a href="./roomDetail-gallery.html" class="btn btn-default pull-left">room details</a>
                                                        <a href="./reservation.html" class="btn btn-default pull-right">book now</a>
                                                    </div>
                                                </div>-->
                                            </div>
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img7.jpg" alt="Owl Image">
                                            </div>
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img8.jpg" alt="Owl Image">
                                            </div>   
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img9.jpg" alt="Owl Image">
                                            </div>   
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img10.jpg" alt="Owl Image">
                                            </div>   
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img11.jpg" alt="Owl Image">
                                            </div>   
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img12.jpg" alt="Owl Image">
                                            </div>   
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img13.jpg" alt="Owl Image">
                                            </div>   
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img14.jpg" alt="Owl Image">
                                            </div>                                                   
                                        </div>
                                        <!--<div class="customNavigation">
                                            <a class="prev"><i class="fa fa-angle-left"></i></a>
                                            <a class="next"><i class="fa fa-angle-right"></i></a>
                                        </div>-->
                                    </div>
                                    <div class="container">
                                        <a href="<?= base_url('p/galeria') ?>" class="btn btn-default mce-content-body" id="mce_289" >Full gallery</a><input name="mce_289" type="hidden">
                                    </div>
                                </div>
                            </div>
                            <!-- TESTIMONIAL -->
                            <div class="testimonial" style="position: relative;">
                                <div class="container">
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-centered center-block">
                                        <div class="row">
                                            <div class="title">
                                                <h6 id="mce_293">Reviews</h6><input name="mce_293" type="hidden">
                                                <h3 id="mce_294">What our guests say about us</h3><input name="mce_294" type="hidden">
                                                <div class="col-xs-6 col-sm-4 col-md-4 cap col-centered center-block">
                                                    <span id="mce_295"><img class="img-responsive" src="[base_url]img/cap.png" alt=""></span><input name="mce_295" type="hidden">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6 col-md-6">
                                            <div class="testi-block">
                                                <div class="row">
                                                    <!--<div class="hidden-xs col-xs-12 col-sm-2 col-md-2">
                                                        <img src="[base_url]img/Homepage_Resort_INDEX/testimonial-img5.png" alt="">
                                                    </div>-->
                                                    <div class="col-xs-12 col-sm-offset-2 col-sm-10 col-md-10">
                                                        <div class="testi-detail">
                                                            <p id="mce_296">"We were looking for a remote country house for enjoying a family getaway. Cal Prat met our expectations! The kids had so much fun and the landlady was so kind."</p><input name="mce_296" type="hidden">
                                                            <h6 id="mce_297">– J. Magaña</h6><input name="mce_297" type="hidden">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6">
                                            <div class="testi-block">
                                                <div class="row">
                                                    <!--<div class="hidden-xs col-xs-12 col-sm-2 col-md-2">
                                                        <img src="[base_url]img/Homepage_Resort_INDEX/testimonial-img6.png" alt="">
                                                    </div>-->
                                                    <div class="col-xs-12 col-sm-offset-2 col-sm-10 col-md-10">
                                                        <div class="testi-detail">
                                                            <p id="mce_298">"We rented the house with a group of friends and everything went perfect! We enjoyed the cold weather in a rustic cottage plenty of details. For sure we will come back again!"</p><input name="mce_298" type="hidden">
                                                            <h6 id="mce_299">– B. Tusell</h6><input name="mce_299" type="hidden">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="prom-offers">
                                    <div class="dark-gold"></div>
                                    <div class="light-gold"></div>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-5 col-md-5">
                                                <div class="prom-block">
                                                    <h3 id="mce_300">Promotions</h3><input name="mce_300" type="hidden">
                                                    <p id="mce_301">We have special promotions for some weekdays and weekends along the year.</p><input name="mce_301" type="hidden">
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-2 col-md-2">
                                                <img class="img-responsive" src="[base_url]img/Homepage_Resort_INDEX/testimonial-img2.png" alt="">
                                            </div>
                                            <div class="col-xs-12 col-sm-5 col-md-5">
                                                <div class="offer-block">
                                                    <h3 id="mce_302">Special offers</h3><input name="mce_302" type="hidden">
                                                    <p id="mce_303">Getting married, a huge group of people...contact us for special offers. We will offer you the best treatment for your special occasion.</p><input name="mce_303" type="hidden">
                                                </div>
                                            </div>
                                        </div>
                                        <a href="[base_url]ofertas" class="btn btn-default mce-content-body" id="mce_304" >Check all offers</a><input name="mce_304" type="hidden">
                                    </div>
                                </div>
                                <input name="mce_305" type="hidden"><input name="mce_306" type="hidden"><input name="mce_307" type="hidden"></div>
                                <!-- LOCATION -->
                                <div class="location" style="position: relative;">
                                    <div class="container">
                                        <div class="col-xs-12 col-sm-12 col-md-6 col-centered center-block">
                                            <div class="row">
                                                <div class="title">
                                                    <h6 id="mce_308">LOCATION</h6><input name="mce_308" type="hidden">
                                                    <h3 id="mce_309">WHERE ARE WE</h3><input name="mce_309" type="hidden">
                                                    <div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
                                                        <span id="mce_310"><img class="img-responsive" src="[base_url]img/cap.png" alt=""></span><input name="mce_310" type="hidden">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-8">
                                                <div class="map">
                                                    <div id="map">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-4">
                                                <ul>
                                                    <li>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4 col-md-6">
                                                                <span class="specific mce-content-body" id="mce_311" >ADDRESS</span><input name="mce_311" type="hidden">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8 col-md-6">
                                                                <p id="mce_312">El Soler, s/n Calonge de Segarra, Barcelona</p><input name="mce_312" type="hidden">
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4 col-md-6">
                                                                <span class="specific mce-content-body" id="mce_313" >phone</span><input name="mce_313" type="hidden">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8 col-md-6">
                                                                <p id="mce_314">+34 650 93 64 00</p><input name="mce_314" type="hidden">
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4 col-md-6">
                                                                <span class="specific mce-content-body" id="mce_315" >email</span><input name="mce_315" type="hidden">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8 col-md-6">
                                                                <a href="mailto:info@fontainbleu.com" id="mce_316"> <input name="mce_316" type="hidden"><a href="mailto:info@calprat.net">info@calprat.net</a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4 col-md-6">
                                                                <span class="specific mce-content-body" id="mce_317" >distance</span><input name="mce_317" type="hidden">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8 col-md-6">
                                                                <p id="mce_318">80km from barcelona <br> 80km from LLeida <br> 100km from Tarragona <br> 150km from Girona</p><input name="mce_318" type="hidden">
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4 col-md-6">
                                                                <span class="specific mce-content-body" id="mce_319" >nearby</span><input name="mce_319" type="hidden">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8 col-md-6">
                                                                <p id="mce_320">20 Km. to Igualada<br> 20 km. to Cervera<br> 30 km. to Manresa <br>40 km. to Tàrrega</p><input name="mce_320" type="hidden">
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <input name="mce_321" type="hidden"><input name="mce_322" type="hidden"><input name="mce_323" type="hidden"></div><div style="text-align:center;"></div>
                                </section>