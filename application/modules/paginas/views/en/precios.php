<!-- WELCOME -->
<section id="content">
	<section id="content" class="room-gallery"><!-- PAGE HEADER --><div class="title-color">
	<div class="container">
		<h2 id="mce_91" class="mce-content-body mce-edit-focus">Our prices</h2>
		<ol class="breadcrumb">
			<li><a href="<?= base_url() ?>" id="mce_92" class="mce-content-body">Home</a></li>
			
			<li class="active">Prices</li>
		</ol>
	</div>
</div>
<!-- ROOM DETAIL-->
<div class="container">
	<div class="blog">		
		<div class="row">
			<div class="main-grid main-blog-grid">
					<div class="col-xs-12 col-sm-12 col-md-12">
						<div class="row">
							<div class="blog-grid">
								<ul>

									<?php foreach($this->db->get_where('habitaciones')->result() as $h): ?>
										<li class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
											<img class="img-responsive" src="<?= base_url('img/habitaciones/'.$h->foto) ?>" alt="">
											<div class="blog-detail">
												<h3 style="height:38px"><?= $h->habitacion_nombre ?></h3>
												<div class="col-xs-6 col-sm-6 col-md-6 cap col-centered center-block">
													<span><img class="img-responsive" src="[base_url]img/cap.png" alt=""></span>
												</div>
												
													<table class="table table-bordered tablepreus tb-table ">
														<thead>
															<tr>
																<th colspan="2">Winter</th>																
															</tr>
														</thead>
														<tbody>
															<tr>
																<td style="text-align:left">Weekend</td>
																<td class="price" style="padding:0 20px; text-align:right"><?= str_replace(',00','',number_format(($h->precio_fin_tbaja*2),2,',','.')); ?>€</td>
															</tr>
															<tr>
																<td style="text-align:left">Day</td>
																<td class="price" style="padding:0 20px; text-align:right"><?= str_replace(',00','',number_format($h->precio_dia_tbaja,2,',','.')); ?>€</td>
															</tr>
															<tr>
																<td style="text-align:left">All week</td>
																<td class="price" style="padding:0 20px; text-align:right"><?= str_replace(',00','',number_format($h->precio_sem_tbaja,2,',','.')); ?>€</td>
															</tr>															
														</tbody>
														<thead>
															<tr>
																<th colspan="2">Summer</th>																
															</tr>
														</thead>
														<tbody>
															<tr>
																<td style="text-align:left">Weekend</td>
																<td class="price" style="padding:0 20px; text-align:right"><?= str_replace(',00','',number_format(($h->precio_fin_talta*2),2,',','.')); ?>€</td>
															</tr>
															<tr>
																<td style="text-align:left">Day</td>
																<td class="price" style="padding:0 20px; text-align:right"><?= str_replace(',00','',number_format($h->precio_dia_talta,2,',','.')); ?>€</td>
															</tr>
															<tr>
																<td style="text-align:left">All week</td>
																<td class="price" style="padding:0 20px; text-align:right"><?= str_replace(',00','',number_format($h->precio_sem_talta,2,',','.')); ?>€</td>
															</tr>															
														</tbody>
													</table>
													
												<div>
													<a href="<?= base_url('iniciar-reserva') ?>" class="btn btn-default btn-preus">Reservar</a>													
												</div>
											</div>
											<div class="visitor">
												<span class="calender">
													<b style="font-weight:bold;">Winter</b> 01/10 - 30/06
												</span>												
												<span class="views">
													<b style="font-weight:bold;">Summer</b> 01/07 - 30/09
												</span>
											</div>
										</li>
									<?php endforeach ?>
								</ul>
							</div>
							<span class="views">
													<b style="font-weight:bold;">*Christmas, New Year's Eve and Easter:</b> Available only rent of the whole house.
													<br><b style="font-weight:bold;">*For take a long week end:</b> consult us.
												</span>
						</div>
					</div>
				</div>
		</div>
	</div>
</div>

</section>