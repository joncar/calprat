<!-- WELCOME -->
<section id="content" class="activitie-main"><!-- PAGE HEADER --><div class="title-color">
<h2>Activitats & Excursions</h2>
<ol class="breadcrumb">
	<li><a href="<?= base_url() ?>">Inici</a></li>
	<li><a href="<?= base_url() ?>">Agenda</a></li>
	<li><a href="<?= base_url() ?>">Agenda</a></li>
	<!--				<li class="active"><a>Activities &amp; Excursions</a></li>-->
</ol>
</div>

<!-- ACTIVITIES CONTENT -->
<div class="activitie-block">
<div class="container">
	<div class="col-xs-12 col-sm-12 col-md-8 col-centered center-block">
		<div class="row">
			<div class="title">
				<h6>no saps què fer?</h6>
				<h1>Agenda</h1>
				<div class="col-xs-6 col-sm-4 col-md-4 cap col-centered center-block">
					<span><img class="img-responsive" src="[base_url]img/cap.png" alt=""></span>
				</div>
				<p>Aquí tens un petit recull de les moltes activitats que podràs fer a Cal Prat i el seu entorn. Hi ha mil llocs, mil excursions que t'encantaran!</p>
			</div>
		</div>
	</div>
	[foreach:eventos]
	<div class="activities">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-6 no-padding">
				<a href="<?= base_url() ?>">
					<div class="img">
						<img class="img-responsive" src="[foto_grande]" alt="" style="min-height:508px;"><span class="btn btn-default">detalls</span>
					</div>
				</a>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6 no-padding">
				<div class="right-activ">
					<h2>[titulo]</h2>
					
					<p>[descripcion]</p>
					<div class="include">
						<h4>Què veuràs</h4>
						[tags]
					</div>
					<div class="activ-botpan">
						<div class="active-left">
							<ul>
								<li><a href="<?= base_url() ?>"><i class="fa fa-cutlery"></i></a></li>
								<li><a href="<?= base_url() ?>"><i class="fa fa-glass"></i></a></li>
								<li><a href="<?= base_url() ?>"><i class="fa fa-wheelchair"></i></a></li>
							</ul>
						</div>
						<div class="active-right">
							<img src="[base_url]img/activities/6.png" alt="">
						</div>
					</div>
				</div>
				<div class="visitor">
					<span class="calender"><i class="fa fa-calendar-o"></i>[fecha]</span>
					<span class="comment"><i class="fa fa-clock-o"></i>[hora]</span>
					<span class="views"><i class="fa fa-bookmark"></i>[precio]</span>
				</div>
			</div>
		</div>
	</div>
	[/foreach]
	
	
	
	<div class="activitie-cal">
		<div class="col-xs-12 col-sm-12 col-md-8 col-centered center-block">
			<div class="row">
				<div class="title">
					<h6>calendari d'activitats</h6>
					<h1>Proximament...</h1>
					<div class="col-xs-6 col-sm-4 col-md-4 cap col-centered center-block">
						<span><img alt="" src="[base_url]img/cap.png" class="img-responsive"></span>
					</div>
					<p>Troba aquí els esdeveniments que es celebraran en els propers dies a les rodalies i proximitats de Cal Prat</p>
				</div>
			</div>
		</div>
		
	</div>

	<div class="gallery-block">
            <div class="main-room">
                <ul>
                    <li class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                        <div class="img">
                            <img src="<?= base_url() ?>theme/images/Gallery/1.jpg" class="img-responsive" alt="">
                            <div class="mr-head">
                                <h3>Calendari<br>d'activitats</h3>
                                <div class="col-md-6 cap col-centered center-block">
                                    <span><img class="img-responsive" src="<?= base_url() ?>theme/images/cap.png" alt=""></span>
                                </div>
                                <p>Disfruta-les, aprofita-les....</p>
                                <p></p>
                            </div>
                        </div>
                    </li>
                    [foreach:activitats]
                        <li class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <div class="mr-inner">
                                <img src="[foto]" class="img-responsive" alt="" style="min-height:327px;">
                                <div class="mr-overlay">
                                    <h2>[titulo]</h2>
                                    <p>[fecha]</p>
                                    <p>[descripcion_corta]</p>                                    
                                </div>
                            </div>
                        </li>
                    [/foreach]
                </ul>
            </div>
        </div>
</div>
</div>
</section>