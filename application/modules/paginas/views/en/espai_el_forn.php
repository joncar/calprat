
            <!-- WELCOME -->
<section id="content">
            <section id="content" class="room-gallery"><!-- PAGE HEADER --><div class="title-color" style="position: relative;">
			<div class="container">
				<h2 id="mce_91" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">El forn</h2><input type="hidden" name="mce_91">
				<ol class="breadcrumb">
<li><a href="./index.html" id="mce_92" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Inici</a><input type="hidden" name="mce_92"></li>
					<li><a href="./accommodations.html" id="mce_93" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Allotjaments</a><input type="hidden" name="mce_93"></li>
					<li class="active">El forn</li>
				</ol>
</div>
		<input type="hidden" name="mce_94"><input type="hidden" name="mce_95"><input type="hidden" name="mce_96"></div>

                <!-- ROOM DETAIL-->
                <div class="container" style="position: relative;">
                    <div class="delux-city-view">
                        <div class="bloggal-slider">
                            <div id="gal-slider" class="flexslider">
                                <ul class="slides">
                                    <li><img src="[base_url]img/blog-page/blogpost-gallery/slider-img1.jpg" alt=""></li>
                                    <li><img src="[base_url]img/blog-page/blogpost-gallery/slider-img1.jpg" alt=""></li>
                                    <li><img src="[base_url]img/blog-page/blogpost-gallery/slider-img1.jpg" alt=""></li>
                                    <li><img src="[base_url]img/blog-page/blogpost-gallery/slider-img1.jpg" alt=""></li>
                                </ul>
                                <ul class="flex-direction-nav">
                                    <li><a class="flex-prev mce-content-body" id="mce_97" contenteditable="true" spellcheck="false"><i class="fa fa-angle-left"></i></a><input type="hidden" name="mce_97"></li>
                                    <li><a class="flex-next mce-content-body" id="mce_98" contenteditable="true" spellcheck="false"><i class="fa fa-angle-right"></i></a><input type="hidden" name="mce_98"></li>
                                </ul>
                            </div>
                            <ul class="gal-nav">
                                <li>
                                    <div>
                                        <img src="[base_url]img/blog-page/blogpost-gallery/slider-img2.jpg" alt="">
                                    </div>
                                </li>
                                <li>
                                    <div>
                                        <img src="[base_url]img/blog-page/blogpost-gallery/slider-img3.jpg" alt="">
                                    </div>
                                </li>
                                <li>
                                    <div>
                                        <img src="[base_url]img/blog-page/blogpost-gallery/slider-img4.jpg" alt="">
                                    </div>
                                </li>
                                <li>
                                    <div>
                                        <img src="[base_url]img/blog-page/blogpost-gallery/slider-img5.jpg" alt="">
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-7 col-md-7">
                                <h1 id="mce_99" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Tota la casa per a vosaltres</h1><input type="hidden" name="mce_99">
                                <h6 id="mce_99" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;font-family: 'TrajanProBold';font-weight: 400;color: #af9a5f;text-transform: uppercase;font-size: 22px;line-height: 22px;">13 persones</h6>
                                <p id="mce_100" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent conubia per inceptos.</p><input type="hidden" name="mce_100">
                                <p id="mce_101" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit duis sed odio sit amet nibh vulputate.</p><input type="hidden" name="mce_101">
                                <p id="mce_102" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p><input type="hidden" name="mce_102">
                            </div>
                            <div class="col-xs-12 col-sm-5 col-md-4 col-md-offset-1 pull-right">
                                <div class="reservation">
                                    <h2 id="mce_103" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Fer una reserva</h2><input type="hidden" name="mce_103">
                                    <div class="rate-from">
                                        <h5 id="mce_104" class="mce-content-body" contenteditable="true" spellcheck="false">des de</h5><input type="hidden" name="mce_104">
                                        <h1 id="mce_105" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;"><span id="mce_106"><sup>€</sup></span><input type="hidden" name="mce_106">120</h1><input type="hidden" name="mce_105">
                                        <h6 id="mce_107" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">per nit</h6><input type="hidden" name="mce_107">
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6 cap col-centered center-block">
                                        <span id="mce_108" class="mce-content-body" contenteditable="true" spellcheck="false"><img class="img-responsive" src="[base_url]img/cap.png" alt=""></span><input type="hidden" name="mce_108">
                                    </div>
                                    <div class="row date-picker">
                                        <div class="col-xs-12 col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <div id="datetimepicker1" class="input-group date">
                                                    <input type="text" placeholder="entrada" class="form-control"><span class="input-group-addon mce-content-body" id="mce_109" contenteditable="true" spellcheck="false"><i class="fa fa-calendar"></i></span><input type="hidden" name="mce_109">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <div id="datetimepicker2" class="input-group date">
                                                    <input type="text" placeholder="sortida" class="form-control"><span class="input-group-addon mce-content-body" id="mce_110" contenteditable="true" spellcheck="false"><i class="fa fa-calendar"></i></span><input type="hidden" name="mce_110">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row date-picker">
                                        <div class="col-xs-12 col-sm-6 col-md-6">
                                            <select id="get_value" tabindex="0"><option value="">Adults</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option></select>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6">
                                            <select id="get_value2" tabindex="0"><option value="">Nens</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option></select>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-default btn-availble">fer reserva</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="spa-glan-tab">
                        <div role="tabpanel">
                            <ul role="tablist" class="nav nav-tabs">
                                <li class="active" role="presentation">
                                    <a data-toggle="tab" role="tab" href="#overview" id="mce_111" class="mce-content-body" contenteditable="true" spellcheck="false"><i class="fa fa-globe"></i>general</a><input type="hidden" name="mce_111">
                                </li>
                                <li role="presentation">
                                    <a data-toggle="tab" role="tab" href="#amenities" id="mce_112" class="mce-content-body" contenteditable="true" spellcheck="false"><i class="fa fa-trophy"></i>caracteristiques</a><input type="hidden" name="mce_112">
                                </li>
                                <li role="presentation">
                                    <a data-toggle="tab" role="tab" href="#available" id="mce_113" class="mce-content-body" contenteditable="true" spellcheck="false"><i class="fa fa-heart"></i>disponibilitat</a><input type="hidden" name="mce_113">
                                </li>
                                <li role="presentation">
                                    <a data-toggle="tab" role="tab" href="#reviews" id="mce_114" class="mce-content-body" contenteditable="true" spellcheck="false"><i class="fa fa-pagelines"></i>opinions</a><input type="hidden" name="mce_114">
                                </li>
                            </ul>
                            <!-- Tabs --><div class="tab-content">
                                <div id="overview" class="tab-pane fade in active" role="tabpanel">
                                    <div class="style-comfort">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-6 col-md-6">
                                                <img src="[base_url]img/RoomDetail-gallery/1.jpg" alt="">
                                            </div>
                                            <div class="col-xs-12 col-sm-6 col-md-6">
                                                <h2 id="mce_115" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Una casa rústica per a la vostra comoditat</h2><input type="hidden" name="mce_115">
                                                <p id="mce_116" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p><input type="hidden" name="mce_116">
                                                <ul>
                                                    <li>
                                                        <span class="des mce-content-body" id="mce_117" contenteditable="true" spellcheck="false" style="position: relative;">llits</span><input type="hidden" name="mce_117">
                                                        <span class="detail mce-content-body" id="mce_118" contenteditable="true" spellcheck="false" style="position: relative;">2 Queen</span><input type="hidden" name="mce_118">
                                                    </li>
                                                    <li>
                                                        <span class="des mce-content-body" id="mce_119" contenteditable="true" spellcheck="false" style="position: relative;">capacitat</span><input type="hidden" name="mce_119">
                                                        <span class="detail mce-content-body" id="mce_120" contenteditable="true" spellcheck="false" style="position: relative;">4 People</span><input type="hidden" name="mce_120">
                                                    </li>
                                                    <li>
                                                        <span class="des mce-content-body" id="mce_121" contenteditable="true" spellcheck="false" style="position: relative;">tamany</span><input type="hidden" name="mce_121">
                                                        <span class="detail mce-content-body" id="mce_122" contenteditable="true" spellcheck="false" style="position: relative;">79 sqm / 855 sqf</span><input type="hidden" name="mce_122">
                                                    </li>
                                                    <li>
                                                        <span class="des mce-content-body" id="mce_123" contenteditable="true" spellcheck="false" style="position: relative;">vistes</span><input type="hidden" name="mce_123">
                                                        <span class="detail mce-content-body" id="mce_124" contenteditable="true" spellcheck="false" style="position: relative;">City</span><input type="hidden" name="mce_124">
                                                    </li>
                                                    <li>
                                                        <span class="des mce-content-body" id="mce_125" contenteditable="true" spellcheck="false" style="position: relative;">Des de</span><input type="hidden" name="mce_125">
                                                        <span class="detail mce-content-body" id="mce_126" contenteditable="true" spellcheck="false" style="position: relative;">175 €</span><input type="hidden" name="mce_126">
                                                    </li>
                                                    <li>
                                                        <span class="des mce-content-body" id="mce_127" contenteditable="true" spellcheck="false" style="position: relative;">veure video</span><input type="hidden" name="mce_127">
                                                        <span class="detail mce-content-body" id="mce_128" contenteditable="true" spellcheck="false" style="position: relative;"><a href="https://vimeo.com/25598691" class="prettyPhoto" id="mce_129"><i class="fa fa-video-camera"></i></a><input type="hidden" name="mce_129"></span><input type="hidden" name="mce_128">
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="amenities" class="tab-pane fade" role="tabpanel">
                                    <div class="aminities">
                                        <div class="col-xs-12 col-sm-8 col-md-8 col-centered center-block">
                                            <div class="row">
                                                <div class="title">
                                                    <h1 id="mce_130" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Una àmplia gamma d'equipaments</h1><input type="hidden" name="mce_130">
                                                    <div class="col-xs-3 col-sm-3 col-md-3 cap col-centered center-block">
                                                        <span id="mce_131" class="mce-content-body" contenteditable="true" spellcheck="false"><img class="img-responsive" src="[base_url]img/cap.png" alt=""></span><input type="hidden" name="mce_131">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <p id="mce_132" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life. One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar. The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn’t listen. Separated they live in Bookmarksgrove.</p><input type="hidden" name="mce_132">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-5 col-md-5">
                                                <ul class="specific">
                                                    <li>Sed odio sit amet nibh vulputate cursus</li>
                                                    <li>Morbi versa accumsan ipsum velit</li>
                                                    <li>Nam nec tellus a odio tincidunt auctor a ornare</li>
                                                    <li>Sed non  mauris vitae erat consequat</li>
                                                    <li>Class aptent taciti sociosqu ad litora torquent</li>
                                                </ul>
                                            </div>
                                            <div class="col-xs-12 col-sm-2 col-md-2">
                                            </div>
                                            <div class="col-xs-12 col-sm-5 col-md-5">
                                                <ul class="specific">
                                                    <li>Class aptent taciti sociosqu ad litora torquent</li>
                                                    <li>Mauris in erat justo</li>
                                                    <li>Nullam ac urna eu felis dapibus condimentum</li>
                                                    <li>Proin condimentum fermentum nun</li>
                                                    <li>Etiam pharetra, erat sed fermentum feugiat</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="available" class="tab-pane fade" role="tabpanel">
                                    <div class="availability">
                                        <p id="mce_133" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p><input type="hidden" name="mce_133">
                                        <div id="ab-carousel" class="owl-carousel owl-theme">
                                            <div class="item">
                                                <div class="main-cal">
                                                    <table class="cal">
                                                        <caption>
                                                            abril 2018
                                                        </caption>
                                                        <thead><tr>

                                                            <th>dil</th>
                                                            <th>dim</th>
                                                            <th>dmc</th>
                                                            <th>dij</th>
                                                            <th>div</th>
                                                            <th>dis</th>
                                                            <th>diu</th>
                                                        </tr></thead>
                                                        <tbody>
                                                        <tr>
                                                            <td class="off"><a href="index.html" id="mce_134" class="mce-content-body" contenteditable="true" spellcheck="false">26</a><input type="hidden" name="mce_134"></td>
                                                            <td class="off"><a href="index.html" id="mce_135" class="mce-content-body" contenteditable="true" spellcheck="false">27</a><input type="hidden" name="mce_135"></td>
                                                            <td class="off"><a href="index.html" id="mce_136" class="mce-content-body" contenteditable="true" spellcheck="false">28</a><input type="hidden" name="mce_136"></td>
                                                            <td class="off"><a href="index.html" id="mce_137" class="mce-content-body" contenteditable="true" spellcheck="false">29</a><input type="hidden" name="mce_137"></td>
                                                            <td><a href="index.html" id="mce_138" class="mce-content-body" contenteditable="true" spellcheck="false">1</a><input type="hidden" name="mce_138"></td>
                                                            <td><a href="index.html" id="mce_139" class="mce-content-body" contenteditable="true" spellcheck="false">2</a><input type="hidden" name="mce_139"></td>
                                                            <td><a href="index.html" id="mce_140" class="mce-content-body" contenteditable="true" spellcheck="false">3</a><input type="hidden" name="mce_140"></td>
                                                        </tr>
                                                        <tr>
                                                            <td><a href="index.html" id="mce_141" class="mce-content-body" contenteditable="true" spellcheck="false">4</a><input type="hidden" name="mce_141"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_142" class="mce-content-body" contenteditable="true" spellcheck="false">5</a><input type="hidden" name="mce_142"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_143" class="mce-content-body" contenteditable="true" spellcheck="false">6</a><input type="hidden" name="mce_143"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_144" class="mce-content-body" contenteditable="true" spellcheck="false">7</a><input type="hidden" name="mce_144"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_145" class="mce-content-body" contenteditable="true" spellcheck="false">8</a><input type="hidden" name="mce_145"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_146" class="mce-content-body" contenteditable="true" spellcheck="false">9</a><input type="hidden" name="mce_146"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_147" class="mce-content-body" contenteditable="true" spellcheck="false">10</a><input type="hidden" name="mce_147"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="unavailable"><a href="index.html" id="mce_148" class="mce-content-body" contenteditable="true" spellcheck="false">11</a><input type="hidden" name="mce_148"></td>
                                                            <td><a href="index.html" id="mce_149" class="mce-content-body" contenteditable="true" spellcheck="false">12</a><input type="hidden" name="mce_149"></td>
                                                            <td><a href="index.html" id="mce_150" class="mce-content-body" contenteditable="true" spellcheck="false">13</a><input type="hidden" name="mce_150"></td>
                                                            <td><a href="index.html" id="mce_151" class="mce-content-body" contenteditable="true" spellcheck="false">14</a><input type="hidden" name="mce_151"></td>
                                                            <td><a href="index.html" id="mce_152" class="mce-content-body" contenteditable="true" spellcheck="false">15</a><input type="hidden" name="mce_152"></td>
                                                            <td><a href="index.html" id="mce_153" class="mce-content-body" contenteditable="true" spellcheck="false">16</a><input type="hidden" name="mce_153"></td>
                                                            <td><a href="index.html" id="mce_154" class="mce-content-body" contenteditable="true" spellcheck="false">17</a><input type="hidden" name="mce_154"></td>
                                                        </tr>
                                                        <tr>
                                                            <td><a href="index.html" id="mce_155" class="mce-content-body" contenteditable="true" spellcheck="false">18</a><input type="hidden" name="mce_155"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_156" class="mce-content-body" contenteditable="true" spellcheck="false">19</a><input type="hidden" name="mce_156"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_157" class="mce-content-body" contenteditable="true" spellcheck="false">20</a><input type="hidden" name="mce_157"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_158" class="mce-content-body" contenteditable="true" spellcheck="false">21</a><input type="hidden" name="mce_158"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_159" class="mce-content-body" contenteditable="true" spellcheck="false">22</a><input type="hidden" name="mce_159"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_160" class="mce-content-body" contenteditable="true" spellcheck="false">23</a><input type="hidden" name="mce_160"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_161" class="mce-content-body" contenteditable="true" spellcheck="false">24</a><input type="hidden" name="mce_161"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="unavailable"><a href="index.html" id="mce_162" class="mce-content-body" contenteditable="true" spellcheck="false">25</a><input type="hidden" name="mce_162"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_163" class="mce-content-body" contenteditable="true" spellcheck="false">26</a><input type="hidden" name="mce_163"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_164" class="mce-content-body" contenteditable="true" spellcheck="false">27</a><input type="hidden" name="mce_164"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_165" class="mce-content-body" contenteditable="true" spellcheck="false">28</a><input type="hidden" name="mce_165"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_166" class="mce-content-body" contenteditable="true" spellcheck="false">29</a><input type="hidden" name="mce_166"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_167" class="mce-content-body" contenteditable="true" spellcheck="false">30</a><input type="hidden" name="mce_167"></td>
                                                            <td class="off"><a href="index.html" id="mce_168" class="mce-content-body" contenteditable="true" spellcheck="false">31</a><input type="hidden" name="mce_168"></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="main-cal">
                                                    <table class="cal">
                                                        <caption>
                                                            maig 2018
                                                        </caption>
                                                        <thead><tr>
                                                            <th>dil</th>
                                                            <th>dim</th>
                                                            <th>dmc</th>
                                                            <th>dij</th>
                                                            <th>div</th>
                                                            <th>dis</th>
                                                            <th>diu</th>
                                                        </tr></thead>
                                                        <tbody>
                                                        <tr>
                                                            <td class="unavailable"><a href="index.html" id="mce_169" class="mce-content-body" contenteditable="true" spellcheck="false">31</a><input type="hidden" name="mce_169"></td>
                                                            <td class="off"><a href="index.html" id="mce_170" class="mce-content-body" contenteditable="true" spellcheck="false">26</a><input type="hidden" name="mce_170"></td>
                                                            <td class="off"><a href="index.html" id="mce_171" class="mce-content-body" contenteditable="true" spellcheck="false">27</a><input type="hidden" name="mce_171"></td>
                                                            <td class="off"><a href="index.html" id="mce_172" class="mce-content-body" contenteditable="true" spellcheck="false">28</a><input type="hidden" name="mce_172"></td>
                                                            <td class="off"><a href="index.html" id="mce_173" class="mce-content-body" contenteditable="true" spellcheck="false">29</a><input type="hidden" name="mce_173"></td>
                                                            <td><a href="index.html" id="mce_174" class="mce-content-body" contenteditable="true" spellcheck="false">1</a><input type="hidden" name="mce_174"></td>
                                                            <td><a href="index.html" id="mce_175" class="mce-content-body" contenteditable="true" spellcheck="false">2</a><input type="hidden" name="mce_175"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="unavailable"><a href="index.html" id="mce_176" class="mce-content-body" contenteditable="true" spellcheck="false">3</a><input type="hidden" name="mce_176"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_177" class="mce-content-body" contenteditable="true" spellcheck="false">4</a><input type="hidden" name="mce_177"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_178" class="mce-content-body" contenteditable="true" spellcheck="false">5</a><input type="hidden" name="mce_178"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_179" class="mce-content-body" contenteditable="true" spellcheck="false">6</a><input type="hidden" name="mce_179"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_180" class="mce-content-body" contenteditable="true" spellcheck="false">7</a><input type="hidden" name="mce_180"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_181" class="mce-content-body" contenteditable="true" spellcheck="false">8</a><input type="hidden" name="mce_181"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_182" class="mce-content-body" contenteditable="true" spellcheck="false">9</a><input type="hidden" name="mce_182"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="unavailable"><a href="index.html" id="mce_183" class="mce-content-body" contenteditable="true" spellcheck="false">10</a><input type="hidden" name="mce_183"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_184" class="mce-content-body" contenteditable="true" spellcheck="false">11</a><input type="hidden" name="mce_184"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_185" class="mce-content-body" contenteditable="true" spellcheck="false">12</a><input type="hidden" name="mce_185"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_186" class="mce-content-body" contenteditable="true" spellcheck="false">13</a><input type="hidden" name="mce_186"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_187" class="mce-content-body" contenteditable="true" spellcheck="false">14</a><input type="hidden" name="mce_187"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_188" class="mce-content-body" contenteditable="true" spellcheck="false">15</a><input type="hidden" name="mce_188"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_189" class="mce-content-body" contenteditable="true" spellcheck="false">16</a><input type="hidden" name="mce_189"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="unavailable"><a href="index.html" id="mce_190" class="mce-content-body" contenteditable="true" spellcheck="false">17</a><input type="hidden" name="mce_190"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_191" class="mce-content-body" contenteditable="true" spellcheck="false">18</a><input type="hidden" name="mce_191"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_192" class="mce-content-body" contenteditable="true" spellcheck="false">19</a><input type="hidden" name="mce_192"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_193" class="mce-content-body" contenteditable="true" spellcheck="false">20</a><input type="hidden" name="mce_193"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_194" class="mce-content-body" contenteditable="true" spellcheck="false">21</a><input type="hidden" name="mce_194"></td>
                                                            <td><a href="index.html" id="mce_195" class="mce-content-body" contenteditable="true" spellcheck="false">22</a><input type="hidden" name="mce_195"></td>
                                                            <td><a href="index.html" id="mce_196" class="mce-content-body" contenteditable="true" spellcheck="false">23</a><input type="hidden" name="mce_196"></td>
                                                        </tr>
                                                        <tr>
                                                            <td><a href="index.html" id="mce_197" class="mce-content-body" contenteditable="true" spellcheck="false">24</a><input type="hidden" name="mce_197"></td>
                                                            <td><a href="index.html" id="mce_198" class="mce-content-body" contenteditable="true" spellcheck="false">25</a><input type="hidden" name="mce_198"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_199" class="mce-content-body" contenteditable="true" spellcheck="false">26</a><input type="hidden" name="mce_199"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_200" class="mce-content-body" contenteditable="true" spellcheck="false">27</a><input type="hidden" name="mce_200"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_201" class="mce-content-body" contenteditable="true" spellcheck="false">28</a><input type="hidden" name="mce_201"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_202" class="mce-content-body" contenteditable="true" spellcheck="false">29</a><input type="hidden" name="mce_202"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_203" class="mce-content-body" contenteditable="true" spellcheck="false">30</a><input type="hidden" name="mce_203"></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="main-cal">
                                                    <table class="cal">
                                                        <caption>
                                                            juny 2018
                                                        </caption>
                                                        <thead><tr>
                                                            <th>dil</th>
                                                            <th>dim</th>
                                                            <th>dmc</th>
                                                            <th>dij</th>
                                                            <th>div</th>
                                                            <th>dis</th>
                                                            <th>diu</th>													</tr></thead>
                                                        <tbody>
                                                        <tr>
                                                            <td class="off"><a href="index.html" id="mce_204" class="mce-content-body" contenteditable="true" spellcheck="false">26</a><input type="hidden" name="mce_204"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_205" class="mce-content-body" contenteditable="true" spellcheck="false">1</a><input type="hidden" name="mce_205"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_206" class="mce-content-body" contenteditable="true" spellcheck="false">2</a><input type="hidden" name="mce_206"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_207" class="mce-content-body" contenteditable="true" spellcheck="false">3</a><input type="hidden" name="mce_207"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_208" class="mce-content-body" contenteditable="true" spellcheck="false">4</a><input type="hidden" name="mce_208"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_209" class="mce-content-body" contenteditable="true" spellcheck="false">5</a><input type="hidden" name="mce_209"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_210" class="mce-content-body" contenteditable="true" spellcheck="false">6</a><input type="hidden" name="mce_210"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="unavailable"><a href="index.html" id="mce_211" class="mce-content-body" contenteditable="true" spellcheck="false">7</a><input type="hidden" name="mce_211"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_212" class="mce-content-body" contenteditable="true" spellcheck="false">8</a><input type="hidden" name="mce_212"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_213" class="mce-content-body" contenteditable="true" spellcheck="false">9</a><input type="hidden" name="mce_213"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_214" class="mce-content-body" contenteditable="true" spellcheck="false">10</a><input type="hidden" name="mce_214"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_215" class="mce-content-body" contenteditable="true" spellcheck="false">11</a><input type="hidden" name="mce_215"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_216" class="mce-content-body" contenteditable="true" spellcheck="false">12</a><input type="hidden" name="mce_216"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_217" class="mce-content-body" contenteditable="true" spellcheck="false">13</a><input type="hidden" name="mce_217"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="unavailable"><a href="index.html" id="mce_218" class="mce-content-body" contenteditable="true" spellcheck="false">14</a><input type="hidden" name="mce_218"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_219" class="mce-content-body" contenteditable="true" spellcheck="false">15</a><input type="hidden" name="mce_219"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_220" class="mce-content-body" contenteditable="true" spellcheck="false">16</a><input type="hidden" name="mce_220"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_221" class="mce-content-body" contenteditable="true" spellcheck="false">17</a><input type="hidden" name="mce_221"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_222" class="mce-content-body" contenteditable="true" spellcheck="false">18</a><input type="hidden" name="mce_222"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_223" class="mce-content-body" contenteditable="true" spellcheck="false">19</a><input type="hidden" name="mce_223"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_224" class="mce-content-body" contenteditable="true" spellcheck="false">20</a><input type="hidden" name="mce_224"></td>
                                                        </tr>
                                                        <tr>
                                                            <td><a href="index.html" id="mce_225" class="mce-content-body" contenteditable="true" spellcheck="false">21</a><input type="hidden" name="mce_225"></td>
                                                            <td><a href="index.html" id="mce_226" class="mce-content-body" contenteditable="true" spellcheck="false">22</a><input type="hidden" name="mce_226"></td>
                                                            <td><a href="index.html" id="mce_227" class="mce-content-body" contenteditable="true" spellcheck="false">23</a><input type="hidden" name="mce_227"></td>
                                                            <td><a href="index.html" id="mce_228" class="mce-content-body" contenteditable="true" spellcheck="false">24</a><input type="hidden" name="mce_228"></td>
                                                            <td><a href="index.html" id="mce_229" class="mce-content-body" contenteditable="true" spellcheck="false">25</a><input type="hidden" name="mce_229"></td>
                                                            <td><a href="index.html" id="mce_230" class="mce-content-body" contenteditable="true" spellcheck="false">26</a><input type="hidden" name="mce_230"></td>
                                                            <td><a href="index.html" id="mce_231" class="mce-content-body" contenteditable="true" spellcheck="false">27</a><input type="hidden" name="mce_231"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="unavailable"><a href="index.html" id="mce_232" class="mce-content-body" contenteditable="true" spellcheck="false">28</a><input type="hidden" name="mce_232"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_233" class="mce-content-body" contenteditable="true" spellcheck="false">29</a><input type="hidden" name="mce_233"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_234" class="mce-content-body" contenteditable="true" spellcheck="false">30</a><input type="hidden" name="mce_234"></td>
                                                            <td class="off"><a href="index.html" id="mce_235" class="mce-content-body" contenteditable="true" spellcheck="false">31</a><input type="hidden" name="mce_235"></td>
                                                            <td class="off"><a href="index.html" id="mce_236" class="mce-content-body" contenteditable="true" spellcheck="false">29</a><input type="hidden" name="mce_236"></td>
                                                            <td class="off"><a href="index.html" id="mce_237" class="mce-content-body" contenteditable="true" spellcheck="false">30</a><input type="hidden" name="mce_237"></td>
                                                            <td class="off"><a href="index.html" id="mce_238" class="mce-content-body" contenteditable="true" spellcheck="false">31</a><input type="hidden" name="mce_238"></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="main-cal">
                                                    <table class="cal">
                                                        <caption>
                                                            juliol 2015
                                                        </caption>
                                                        <thead><tr>
                                                            <th>dil</th>
                                                            <th>dim</th>
                                                            <th>dmc</th>
                                                            <th>dij</th>
                                                            <th>div</th>
                                                            <th>dis</th>
                                                            <th>diu</th>
                                                        </tr></thead>
                                                        <tbody>
                                                        <tr>
                                                            <td class="unavailable"><a href="index.html" id="mce_239" class="mce-content-body" contenteditable="true" spellcheck="false">31</a><input type="hidden" name="mce_239"></td>
                                                            <td class="off"><a href="index.html" id="mce_240" class="mce-content-body" contenteditable="true" spellcheck="false">26</a><input type="hidden" name="mce_240"></td>
                                                            <td class="off"><a href="index.html" id="mce_241" class="mce-content-body" contenteditable="true" spellcheck="false">27</a><input type="hidden" name="mce_241"></td>
                                                            <td class="off"><a href="index.html" id="mce_242" class="mce-content-body" contenteditable="true" spellcheck="false">28</a><input type="hidden" name="mce_242"></td>
                                                            <td class="off"><a href="index.html" id="mce_243" class="mce-content-body" contenteditable="true" spellcheck="false">29</a><input type="hidden" name="mce_243"></td>
                                                            <td><a href="index.html" id="mce_244" class="mce-content-body" contenteditable="true" spellcheck="false">1</a><input type="hidden" name="mce_244"></td>
                                                            <td><a href="index.html" id="mce_245" class="mce-content-body" contenteditable="true" spellcheck="false">2</a><input type="hidden" name="mce_245"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="unavailable"><a href="index.html" id="mce_246" class="mce-content-body" contenteditable="true" spellcheck="false">3</a><input type="hidden" name="mce_246"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_247" class="mce-content-body" contenteditable="true" spellcheck="false">4</a><input type="hidden" name="mce_247"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_248" class="mce-content-body" contenteditable="true" spellcheck="false">5</a><input type="hidden" name="mce_248"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_249" class="mce-content-body" contenteditable="true" spellcheck="false">6</a><input type="hidden" name="mce_249"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_250" class="mce-content-body" contenteditable="true" spellcheck="false">7</a><input type="hidden" name="mce_250"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_251" class="mce-content-body" contenteditable="true" spellcheck="false">8</a><input type="hidden" name="mce_251"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_252" class="mce-content-body" contenteditable="true" spellcheck="false">9</a><input type="hidden" name="mce_252"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="unavailable"><a href="index.html" id="mce_253" class="mce-content-body" contenteditable="true" spellcheck="false">10</a><input type="hidden" name="mce_253"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_254" class="mce-content-body" contenteditable="true" spellcheck="false">11</a><input type="hidden" name="mce_254"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_255" class="mce-content-body" contenteditable="true" spellcheck="false">12</a><input type="hidden" name="mce_255"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_256" class="mce-content-body" contenteditable="true" spellcheck="false">13</a><input type="hidden" name="mce_256"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_257" class="mce-content-body" contenteditable="true" spellcheck="false">14</a><input type="hidden" name="mce_257"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_258" class="mce-content-body" contenteditable="true" spellcheck="false">15</a><input type="hidden" name="mce_258"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_259" class="mce-content-body" contenteditable="true" spellcheck="false">16</a><input type="hidden" name="mce_259"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="unavailable"><a href="index.html" id="mce_260" class="mce-content-body" contenteditable="true" spellcheck="false">17</a><input type="hidden" name="mce_260"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_261" class="mce-content-body" contenteditable="true" spellcheck="false">18</a><input type="hidden" name="mce_261"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_262" class="mce-content-body" contenteditable="true" spellcheck="false">19</a><input type="hidden" name="mce_262"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_263" class="mce-content-body" contenteditable="true" spellcheck="false">20</a><input type="hidden" name="mce_263"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_264" class="mce-content-body" contenteditable="true" spellcheck="false">21</a><input type="hidden" name="mce_264"></td>
                                                            <td><a href="index.html" id="mce_265" class="mce-content-body" contenteditable="true" spellcheck="false">22</a><input type="hidden" name="mce_265"></td>
                                                            <td><a href="index.html" id="mce_266" class="mce-content-body" contenteditable="true" spellcheck="false">23</a><input type="hidden" name="mce_266"></td>
                                                        </tr>
                                                        <tr>
                                                            <td><a href="index.html" id="mce_267" class="mce-content-body" contenteditable="true" spellcheck="false">24</a><input type="hidden" name="mce_267"></td>
                                                            <td><a href="index.html" id="mce_268" class="mce-content-body" contenteditable="true" spellcheck="false">25</a><input type="hidden" name="mce_268"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_269" class="mce-content-body" contenteditable="true" spellcheck="false">26</a><input type="hidden" name="mce_269"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_270" class="mce-content-body" contenteditable="true" spellcheck="false">27</a><input type="hidden" name="mce_270"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_271" class="mce-content-body" contenteditable="true" spellcheck="false">28</a><input type="hidden" name="mce_271"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_272" class="mce-content-body" contenteditable="true" spellcheck="false">29</a><input type="hidden" name="mce_272"></td>
                                                            <td class="unavailable"><a href="index.html" id="mce_273" class="mce-content-body" contenteditable="true" spellcheck="false">30</a><input type="hidden" name="mce_273"></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="main-cal">
                                                    <table class="cal">
                                                        <caption>
                                                            agost 2018
                                                        </caption>
                                                        <thead><tr>
                                                            <th>dil</th>
                                                            <th>dim</th>
                                                            <th>dmc</th>
                                                            <th>dij</th>
                                                            <th>div</th>
                                                            <th>dis</th>
                                                            <th>diu</th>													</tr></thead>
                                                        <tbody>
                                                        <tr>
                                                            <td class="off"><a href="index.html" id="mce_274" class="mce-content-body" contenteditable="true" spellcheck="false">26</a><input type="hidden" name="mce_274"></td>
                                                            <td class="off"><a href="index.html" id="mce_275" class="mce-content-body" contenteditable="true" spellcheck="false">27</a><input type="hidden" name="mce_275"></td>
                                                            <td class="off"><a href="index.html" id="mce_276" class="mce-content-body" contenteditable="true" spellcheck="false">28</a><input type="hidden" name="mce_276"></td>
                                                            <td class="off"><a href="index.html" id="mce_277" class="mce-content-body" contenteditable="true" spellcheck="false">29</a><input type="hidden" name="mce_277"></td>
                                                            <td><a href="index.html" id="mce_278" class="mce-content-body" contenteditable="true" spellcheck="false">1</a><input type="hidden" name="mce_278"></td>
                                                            <td><a href="index.html" id="mce_279" class="mce-content-body" contenteditable="true" spellcheck="false">2</a><input type="hidden" name="mce_279"></td>
                                                            <td><a href="index.html" id="mce_280" class="mce-content-body" contenteditable="true" spellcheck="false">3</a><input type="hidden" name="mce_280"></td>
                                                        </tr>
                                                        <tr>
                                                            <td><a href="index.html" id="mce_281" class="mce-content-body" contenteditable="true" spellcheck="false">4</a><input type="hidden" name="mce_281"></td>
                                                            <td><a href="index.html" id="mce_282" class="mce-content-body" contenteditable="true" spellcheck="false">5</a><input type="hidden" name="mce_282"></td>
                                                            <td><a href="index.html" id="mce_283" class="mce-content-body" contenteditable="true" spellcheck="false">6</a><input type="hidden" name="mce_283"></td>
                                                            <td><a href="index.html" id="mce_284" class="mce-content-body" contenteditable="true" spellcheck="false">7</a><input type="hidden" name="mce_284"></td>
                                                            <td><a href="index.html" id="mce_285" class="mce-content-body" contenteditable="true" spellcheck="false">8</a><input type="hidden" name="mce_285"></td>
                                                            <td><a href="index.html" id="mce_286" class="mce-content-body" contenteditable="true" spellcheck="false">9</a><input type="hidden" name="mce_286"></td>
                                                            <td><a href="index.html" id="mce_287" class="mce-content-body" contenteditable="true" spellcheck="false">10</a><input type="hidden" name="mce_287"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="active"><a href="index.html" id="mce_288" class="mce-content-body" contenteditable="true" spellcheck="false">11</a><input type="hidden" name="mce_288"></td>
                                                            <td><a href="index.html" id="mce_289" class="mce-content-body" contenteditable="true" spellcheck="false">12</a><input type="hidden" name="mce_289"></td>
                                                            <td><a href="index.html" id="mce_290" class="mce-content-body" contenteditable="true" spellcheck="false">13</a><input type="hidden" name="mce_290"></td>
                                                            <td><a href="index.html" id="mce_291" class="mce-content-body" contenteditable="true" spellcheck="false">14</a><input type="hidden" name="mce_291"></td>
                                                            <td><a href="index.html" id="mce_292" class="mce-content-body" contenteditable="true" spellcheck="false">15</a><input type="hidden" name="mce_292"></td>
                                                            <td><a href="index.html" id="mce_293" class="mce-content-body" contenteditable="true" spellcheck="false">16</a><input type="hidden" name="mce_293"></td>
                                                            <td><a href="index.html" id="mce_294" class="mce-content-body" contenteditable="true" spellcheck="false">17</a><input type="hidden" name="mce_294"></td>
                                                        </tr>
                                                        <tr>
                                                            <td><a href="index.html" id="mce_295" class="mce-content-body" contenteditable="true" spellcheck="false">18</a><input type="hidden" name="mce_295"></td>
                                                            <td><a href="index.html" id="mce_296" class="mce-content-body" contenteditable="true" spellcheck="false">19</a><input type="hidden" name="mce_296"></td>
                                                            <td><a href="index.html" id="mce_297" class="mce-content-body" contenteditable="true" spellcheck="false">20</a><input type="hidden" name="mce_297"></td>
                                                            <td><a href="index.html" id="mce_298" class="mce-content-body" contenteditable="true" spellcheck="false">21</a><input type="hidden" name="mce_298"></td>
                                                            <td><a href="index.html" id="mce_299" class="mce-content-body" contenteditable="true" spellcheck="false">22</a><input type="hidden" name="mce_299"></td>
                                                            <td><a href="index.html" id="mce_300" class="mce-content-body" contenteditable="true" spellcheck="false">23</a><input type="hidden" name="mce_300"></td>
                                                            <td><a href="index.html" id="mce_301" class="mce-content-body" contenteditable="true" spellcheck="false">24</a><input type="hidden" name="mce_301"></td>
                                                        </tr>
                                                        <tr>
                                                            <td><a href="index.html" id="mce_302" class="mce-content-body" contenteditable="true" spellcheck="false">25</a><input type="hidden" name="mce_302"></td>
                                                            <td><a href="index.html" id="mce_303" class="mce-content-body" contenteditable="true" spellcheck="false">26</a><input type="hidden" name="mce_303"></td>
                                                            <td><a href="index.html" id="mce_304" class="mce-content-body" contenteditable="true" spellcheck="false">27</a><input type="hidden" name="mce_304"></td>
                                                            <td><a href="index.html" id="mce_305" class="mce-content-body" contenteditable="true" spellcheck="false">28</a><input type="hidden" name="mce_305"></td>
                                                            <td><a href="index.html" id="mce_306" class="mce-content-body" contenteditable="true" spellcheck="false">29</a><input type="hidden" name="mce_306"></td>
                                                            <td><a href="index.html" id="mce_307" class="mce-content-body" contenteditable="true" spellcheck="false">30</a><input type="hidden" name="mce_307"></td>
                                                            <td class="off"><a href="index.html" id="mce_308" class="mce-content-body" contenteditable="true" spellcheck="false">31</a><input type="hidden" name="mce_308"></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="main-cal">
                                                    <table class="cal">
                                                        <caption>
                                                            april 2018
                                                        </caption>
                                                        <thead><tr>
                                                            <th>dil</th>
                                                            <th>dim</th>
                                                            <th>dmc</th>
                                                            <th>dij</th>
                                                            <th>div</th>
                                                            <th>dis</th>
                                                            <th>diu</th>													</tr></thead>
                                                        <tbody>
                                                        <tr>
                                                            <td class="off"><a href="index.html" id="mce_309" class="mce-content-body" contenteditable="true" spellcheck="false">26</a><input type="hidden" name="mce_309"></td>
                                                            <td class="off"><a href="index.html" id="mce_310" class="mce-content-body" contenteditable="true" spellcheck="false">27</a><input type="hidden" name="mce_310"></td>
                                                            <td class="off"><a href="index.html" id="mce_311" class="mce-content-body" contenteditable="true" spellcheck="false">28</a><input type="hidden" name="mce_311"></td>
                                                            <td class="off"><a href="index.html" id="mce_312" class="mce-content-body" contenteditable="true" spellcheck="false">29</a><input type="hidden" name="mce_312"></td>
                                                            <td><a href="index.html" id="mce_313" class="mce-content-body" contenteditable="true" spellcheck="false">1</a><input type="hidden" name="mce_313"></td>
                                                            <td><a href="index.html" id="mce_314" class="mce-content-body" contenteditable="true" spellcheck="false">2</a><input type="hidden" name="mce_314"></td>
                                                            <td><a href="index.html" id="mce_315" class="mce-content-body" contenteditable="true" spellcheck="false">3</a><input type="hidden" name="mce_315"></td>
                                                        </tr>
                                                        <tr>
                                                            <td><a href="index.html" id="mce_316" class="mce-content-body" contenteditable="true" spellcheck="false">4</a><input type="hidden" name="mce_316"></td>
                                                            <td><a href="index.html" id="mce_317" class="mce-content-body" contenteditable="true" spellcheck="false">5</a><input type="hidden" name="mce_317"></td>
                                                            <td><a href="index.html" id="mce_318" class="mce-content-body" contenteditable="true" spellcheck="false">6</a><input type="hidden" name="mce_318"></td>
                                                            <td><a href="index.html" id="mce_319" class="mce-content-body" contenteditable="true" spellcheck="false">7</a><input type="hidden" name="mce_319"></td>
                                                            <td><a href="index.html" id="mce_320" class="mce-content-body" contenteditable="true" spellcheck="false">8</a><input type="hidden" name="mce_320"></td>
                                                            <td><a href="index.html" id="mce_321" class="mce-content-body" contenteditable="true" spellcheck="false">9</a><input type="hidden" name="mce_321"></td>
                                                            <td><a href="index.html" id="mce_322" class="mce-content-body" contenteditable="true" spellcheck="false">10</a><input type="hidden" name="mce_322"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="active"><a href="index.html" id="mce_323" class="mce-content-body" contenteditable="true" spellcheck="false">11</a><input type="hidden" name="mce_323"></td>
                                                            <td><a href="index.html" id="mce_324" class="mce-content-body" contenteditable="true" spellcheck="false">12</a><input type="hidden" name="mce_324"></td>
                                                            <td><a href="index.html" id="mce_325" class="mce-content-body" contenteditable="true" spellcheck="false">13</a><input type="hidden" name="mce_325"></td>
                                                            <td><a href="index.html" id="mce_326" class="mce-content-body" contenteditable="true" spellcheck="false">14</a><input type="hidden" name="mce_326"></td>
                                                            <td><a href="index.html" id="mce_327" class="mce-content-body" contenteditable="true" spellcheck="false">15</a><input type="hidden" name="mce_327"></td>
                                                            <td><a href="index.html" id="mce_328" class="mce-content-body" contenteditable="true" spellcheck="false">16</a><input type="hidden" name="mce_328"></td>
                                                            <td><a href="index.html" id="mce_329" class="mce-content-body" contenteditable="true" spellcheck="false">17</a><input type="hidden" name="mce_329"></td>
                                                        </tr>
                                                        <tr>
                                                            <td><a href="index.html" id="mce_330" class="mce-content-body" contenteditable="true" spellcheck="false">18</a><input type="hidden" name="mce_330"></td>
                                                            <td><a href="index.html" id="mce_331" class="mce-content-body" contenteditable="true" spellcheck="false">19</a><input type="hidden" name="mce_331"></td>
                                                            <td><a href="index.html" id="mce_332" class="mce-content-body" contenteditable="true" spellcheck="false">20</a><input type="hidden" name="mce_332"></td>
                                                            <td><a href="index.html" id="mce_333" class="mce-content-body" contenteditable="true" spellcheck="false">21</a><input type="hidden" name="mce_333"></td>
                                                            <td><a href="index.html" id="mce_334" class="mce-content-body" contenteditable="true" spellcheck="false">22</a><input type="hidden" name="mce_334"></td>
                                                            <td><a href="index.html" id="mce_335" class="mce-content-body" contenteditable="true" spellcheck="false">23</a><input type="hidden" name="mce_335"></td>
                                                            <td><a href="index.html" id="mce_336" class="mce-content-body" contenteditable="true" spellcheck="false">24</a><input type="hidden" name="mce_336"></td>
                                                        </tr>
                                                        <tr>
                                                            <td><a href="index.html" id="mce_337" class="mce-content-body" contenteditable="true" spellcheck="false">25</a><input type="hidden" name="mce_337"></td>
                                                            <td><a href="index.html" id="mce_338" class="mce-content-body" contenteditable="true" spellcheck="false">26</a><input type="hidden" name="mce_338"></td>
                                                            <td><a href="index.html" id="mce_339" class="mce-content-body" contenteditable="true" spellcheck="false">27</a><input type="hidden" name="mce_339"></td>
                                                            <td><a href="index.html" id="mce_340" class="mce-content-body" contenteditable="true" spellcheck="false">28</a><input type="hidden" name="mce_340"></td>
                                                            <td><a href="index.html" id="mce_341" class="mce-content-body" contenteditable="true" spellcheck="false">29</a><input type="hidden" name="mce_341"></td>
                                                            <td><a href="index.html" id="mce_342" class="mce-content-body" contenteditable="true" spellcheck="false">30</a><input type="hidden" name="mce_342"></td>
                                                            <td class="off"><a href="index.html" id="mce_343" class="mce-content-body" contenteditable="true" spellcheck="false">31</a><input type="hidden" name="mce_343"></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="customNavigation">
                                            <a class="prev mce-content-body" id="mce_344" contenteditable="true" spellcheck="false" style="position: relative;"><i class="fa fa-angle-left"></i></a><input type="hidden" name="mce_344">
                                            <a class="next mce-content-body" id="mce_345" contenteditable="true" spellcheck="false" style="position: relative;"><i class="fa fa-angle-right"></i></a><input type="hidden" name="mce_345">
                                        </div>
                                        <ul>
                                            <li>
                                                <span class="avail mce-content-body" id="mce_346" contenteditable="true" spellcheck="false"><br></span><input type="hidden" name="mce_346">lliure</li>
                                            <li>
                                                <span class="unavail mce-content-body" id="mce_347" contenteditable="true" spellcheck="false"><br></span><input type="hidden" name="mce_347">ocupat</li>
                                        </ul>
                                    </div>
                                </div>
                                <div id="reviews" class="tab-pane fade" role="tabpanel">
                                    <div class="main-review">
                                        <div class="review-block">
                                            <div class="title">
                                                <h4 id="mce_348" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">travelzoo</h4><input type="hidden" name="mce_348">
                                                <ul>
                                                    <li><a id="mce_349" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;"><i class="fa fa-star"></i></a><input type="hidden" name="mce_349"></li>
                                                    <li><a id="mce_350" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;"><i class="fa fa-star"></i></a><input type="hidden" name="mce_350"></li>
                                                    <li><a id="mce_351" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;"><i class="fa fa-star"></i></a><input type="hidden" name="mce_351"></li>
                                                    <li><a id="mce_352" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;"><i class="fa fa-star"></i></a><input type="hidden" name="mce_352"></li>
                                                    <li class="active"><a id="mce_353" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;"><i class="fa fa-star-o"></i></a><input type="hidden" name="mce_353"></li>
                                                </ul>
                                                <span class="days mce-content-body" id="mce_354" contenteditable="true" spellcheck="false" style="position: relative;">2 days ago</span><input type="hidden" name="mce_354">
                                            </div>
                                            <h3 id="mce_355" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">The room was comfortable with a view to die for!</h3><input type="hidden" name="mce_355">
                                            <p id="mce_356" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Etiam pharetra, erat fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim. Proin gravida nibh vel velit auctor aliquet sollicitudin lorem quis bibendum auctor. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc.</p><input type="hidden" name="mce_356">
                                            <img src="[base_url]img/Homepage_Resort_INDEX/testimonial-img1.png" alt="">
                                        </div>
                                        <div class="review-block">
                                            <div class="title">
                                                <h4 id="mce_357" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">travelzoo</h4><input type="hidden" name="mce_357">
                                                <ul>
                                                    <li><a id="mce_358" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;"><i class="fa fa-star"></i></a><input type="hidden" name="mce_358"></li>
                                                    <li><a id="mce_359" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;"><i class="fa fa-star"></i></a><input type="hidden" name="mce_359"></li>
                                                    <li><a id="mce_360" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;"><i class="fa fa-star"></i></a><input type="hidden" name="mce_360"></li>
                                                    <li><a id="mce_361" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;"><i class="fa fa-star"></i></a><input type="hidden" name="mce_361"></li>
                                                    <li class="active"><a id="mce_362" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;"><i class="fa fa-star-o"></i></a><input type="hidden" name="mce_362"></li>
                                                </ul>
                                                <span class="days mce-content-body" id="mce_363" contenteditable="true" spellcheck="false" style="position: relative;">2 days ago</span><input type="hidden" name="mce_363">
                                            </div>
                                            <h3 id="mce_364" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">The room was comfortable with a view to die for!</h3><input type="hidden" name="mce_364">
                                            <p id="mce_365" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Etiam pharetra, erat fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim. Proin gravida nibh vel velit auctor aliquet sollicitudin lorem quis bibendum auctor. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc.</p><input type="hidden" name="mce_365">
                                            <img src="[base_url]img/Homepage_Resort_INDEX/testimonial-img1.png" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="mce_366"><input type="hidden" name="mce_367"><input type="hidden" name="mce_368"></div>

                <div class="special-offer" style="position: relative;">
                    <div class="container">
                        <div class="col-xs-12 col-sm-8 col-md-8 col-centered center-block">
                            <div class="row">
                                <div class="title">
                                    <h6 id="mce_369" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">oferta especial</h6><input type="hidden" name="mce_369">
                                    <h1 id="mce_370" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Ofertes exclusives per als nostres clients</h1><input type="hidden" name="mce_370">
                                    <div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
                                        <span id="mce_371" class="mce-content-body" contenteditable="true" spellcheck="false"><img class="img-responsive" src="[base_url]img/cap.png" alt=""></span><input type="hidden" name="mce_371">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div aria-multiselectable="true" role="tablist" id="accordion" class="panel-group">
                            <div class="panel panel-default">
                                <div id="headingOne" role="tab" class="panel-heading">
                                    <h4 class="panel-title mce-content-body" id="mce_372" contenteditable="true" spellcheck="false"><a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" id="mce_373"> Oferta exclusiva "La Primavera està aquí" </a><input type="hidden" name="mce_373"> <span class="viewdetail1" id="mce_374">veure detall +</span><input type="hidden" name="mce_374"></h4><input type="hidden" name="mce_372">
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-7 col-md-7">
                                                <img class="img-responsive" src="[base_url]img/RoomDetail-gallery/img1.png" alt="">
                                            </div>
                                            <div class="col-xs-12 col-sm-5 col-md-5">
                                                <p id="mce_375" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi versa accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent conubia.</p><input type="hidden" name="mce_375">
                                                <p id="mce_376" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl neque.</p><input type="hidden" name="mce_376">
                                                <h3 id="mce_377" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Avantatges</h3><input type="hidden" name="mce_377">
                                                <ul>
                                                    <li>Complimentary room upgrade for most room categories</li>
                                                    <li>Complimentary airport transfers</li>
                                                    <li>1,000 points “Stay Credit” per room (redeemable at spa and hotel restaurants</li>
                                                </ul>
                                                <a href="./reservation.html" class="btn btn-default mce-content-body" id="mce_378" contenteditable="true" spellcheck="false" style="position: relative;">reserva ara</a><input type="hidden" name="mce_378">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div id="headingTwo" role="tab" class="panel-heading">
                                    <h4 class="panel-title mce-content-body" id="mce_379" contenteditable="true" spellcheck="false"><a aria-controls="collapseTwo" aria-expanded="false" href="#collapseTwo" data-parent="#accordion" data-toggle="collapse" class="collapsed" id="mce_380"> Oferta exclusiva "La Primavera està aquí" </a><input type="hidden" name="mce_380"> <span class="viewdetail1" id="mce_381">veure detall +</span><input type="hidden" name="mce_381"></h4><input type="hidden" name="mce_379">
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-7 col-md-7">
                                                <img class="img-responsive" src="[base_url]img/RoomDetail-gallery/img1.png" alt="">
                                            </div>
                                            <div class="col-xs-12 col-sm-5 col-md-5">
                                                <p id="mce_382" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi versa accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent conubia.</p><input type="hidden" name="mce_382">
                                                <p id="mce_383" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl neque.</p><input type="hidden" name="mce_383">
                                                <h3 id="mce_384" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">benefits</h3><input type="hidden" name="mce_384">
                                                <ul>
                                                    <li>Complimentary room upgrade for most room categories</li>
                                                    <li>Complimentary airport transfers</li>
                                                    <li>1,000 points “Stay Credit” per room (redeemable at spa and hotel restaurants</li>
                                                </ul>
                                                <a href="./reservation.html" class="btn btn-default mce-content-body" id="mce_385" contenteditable="true" spellcheck="false" style="position: relative;">book now</a><input type="hidden" name="mce_385">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div id="headingThree" role="tab" class="panel-heading">
                                    <h4 class="panel-title mce-content-body" id="mce_386" contenteditable="true" spellcheck="false"><a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree" id="mce_387"> Oferta exclusiva "La Primavera està aquí" </a><input type="hidden" name="mce_387"> <span class="viewdetail1" id="mce_388">veure detall +</span><input type="hidden" name="mce_388"></h4><input type="hidden" name="mce_386">
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-7 col-md-7">
                                                <img class="img-responsive" src="[base_url]img/RoomDetail-gallery/img1.png" alt="">
                                            </div>
                                            <div class="col-xs-12 col-sm-5 col-md-5">
                                                <p id="mce_389" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi versa accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent conubia.</p><input type="hidden" name="mce_389">
                                                <p id="mce_390" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl neque.</p><input type="hidden" name="mce_390">
                                                <h3 id="mce_391" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">benefits</h3><input type="hidden" name="mce_391">
                                                <ul>
                                                    <li>Complimentary room upgrade for most room categories</li>
                                                    <li>Complimentary airport transfers</li>
                                                    <li>1,000 points “Stay Credit” per room (redeemable at spa and hotel restaurants</li>
                                                </ul>
                                                <a href="./reservation.html" class="btn btn-default mce-content-body" id="mce_392" contenteditable="true" spellcheck="false" style="position: relative;">book now</a><input type="hidden" name="mce_392">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div id="headingFour" role="tab" class="panel-heading">
                                    <h4 class="panel-title mce-content-body" id="mce_393" contenteditable="true" spellcheck="false"><a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour" id="mce_394"> Oferta exclusiva "La Primavera està aquí" </a><input type="hidden" name="mce_394"> <span class="viewdetail1" id="mce_395">veure detall +</span><input type="hidden" name="mce_395"></h4><input type="hidden" name="mce_393">
                                </div>
                                <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-7 col-md-7">
                                                <img class="img-responsive" src="[base_url]img/RoomDetail-gallery/img1.png" alt="">
                                            </div>
                                            <div class="col-xs-12 col-sm-5 col-md-5">
                                                <p id="mce_396" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi versa accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent conubia.</p><input type="hidden" name="mce_396">
                                                <p id="mce_397" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl neque.</p><input type="hidden" name="mce_397">
                                                <h3 id="mce_398" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">benefits</h3><input type="hidden" name="mce_398">
                                                <ul>
                                                    <li>Complimentary room upgrade for most room categories</li>
                                                    <li>Complimentary airport transfers</li>
                                                    <li>1,000 points “Stay Credit” per room (redeemable at spa and hotel restaurants</li>
                                                </ul>
                                                <a href="./reservation.html" class="btn btn-default mce-content-body" id="mce_399" contenteditable="true" spellcheck="false" style="position: relative;">book now</a><input type="hidden" name="mce_399">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="mce_400"><input type="hidden" name="mce_401"><input type="hidden" name="mce_402"></div><div style="text-align:center;"></div>
            </section>    </section>