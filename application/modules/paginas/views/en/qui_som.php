<!-- WELCOME -->
<section id="content">
            <section id="content" class="about-main"><!-- PAGE HEADER --><div class="title-color">
			<h2>About Cal Prat</h2>
			<ol class="breadcrumb">
<li><a href="<?= base_url() ?>">Inici</a></li>
				<li class="active"><a href="<?= base_url() ?>p/qui_som.html">About us</a></li>
			</ol>
</div>
		
		<!-- ABOUT -->
		<div class="about-content">
			<div class="db-image quisom">
				<div class="dbi-inner" style="background:url([base_url]img/About/1.jpg) no-repeat center center">
					<img src="[base_url]img/About/1.jpg" style="width:100%" class="visible-xs">
				</div>
				<div class="dbi-inner" style="background:url([base_url]img/About/2.jpg) no-repeat center center">
					<img src="[base_url]img/About/2.jpg" style="width:100%" class="visible-xs">
				</div>
				<div class="dbi-inner" style="background:url([base_url]img/About/3.jpg) no-repeat center center">
					<img src="[base_url]img/About/3.jpg" style="width:100%" class="visible-xs">
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6">
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6">
				<div class="row">
					<div class="right-about">
						<h1>Cal Prat, country farmhouse</h1>
						<h6>Rural tourism, relax and calm</h6>
						<p>Cal Prat is an ancient country house of the eighteenth century situated in the small village of El Soler, in the municipal area of Calonge de Segarra. The cottage is placed in Alta Anoia, that is surrounded by the neighbouring areas of La Segarra and El Solsonès.</p>
						<p>The house has been recently restored with respect of its historical characteristics keeping the farmer style in the cellar, the barn, the wooden oven or the fireplace.</p>
						<p>By staying in Cal Prat you could imagine how our ancestors farmers lived in the last centuries, admiring the rural landscape and its unique tranquility.</p>
						<p>El Forn and Sol de Tous are the names of Cal Prat accommodations, with capacity for 8 and 5 people respectively. The house can be rented entirely, as both accommodations could be connected.</p>
						<div class="highlights">
							<h6>Highlighted</h6>
							<ul class="specific">
<li>Complete silence in the midst of nature</li>
								<li>Hikes in the surroundings and guided tours</li>
								<li>Children equipment</li>
								<li>Private indoor pool</li>
								<li>Private rooms with all commodities</li>
							</ul>
</div>
						<div class="clearfix"></div>
						<div class="img">
							<img src="[base_url]img/About/4.jpg" alt="">
</div>
						<div class="clearfix"></div>
						<h2>WE ARE NOT REASSURED UNTIL YOU FEEL AT HOME</h2>
						<p>Explain us what you need, what you are looking for, if you need something else or any question you might have. We want you to enjoy at 100% your stay.</p>
						<p>The house is equipped with highchair, baby cots, extra beds and other equipment for making your stay even better.</p>
					</div>
				</div>
			</div>
		</div>
	</section>    </section>