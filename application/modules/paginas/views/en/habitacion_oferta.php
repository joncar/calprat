
		<div class="special-offer">
			<div class="container">
				<div class="col-xs-12 col-sm-8 col-md-8 col-centered center-block">
					<div class="row">
						<div class="title">
							<h6>Ofertes especials</h6>
							<h1>Ofertes exclusives per a moments especials</h1>
							<div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
								<span><img class="img-responsive" src="<?= base_url() ?>img/cap.png" alt=""></span>
							</div>
						</div>
					</div>
				</div>
				<div aria-multiselectable="true" role="tablist" id="accordion" class="panel-group">

					[foreach:ofertas]

					<div class="panel panel-default">
						<div id="heading[id]" role="tab" class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion" href="#collapse[id]" aria-expanded="true" aria-controls="collapse[id]">
								[titulo]
								</a>
								<span class="viewdetail1">Veure detalls +</span>
							</h4>
						</div>

						<div id="collapse[id]" class="panel-collapse collapse [in]" role="tabpanel" aria-labelledby="heading[id]">
							<div class="panel-body">
								<div class="row">
									<div class="col-xs-12 col-sm-7 col-md-7">
										<img class="img-responsive" src="[fondo]" alt="">
									</div>
									<div class="col-xs-12 col-sm-5 col-md-5">
										<p>[descripcion_paquete]</p>
										<a href="[base_url]iniciar-oferta/[id]" class="btn btn-default">Reservar</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					[/foreach]

				</div>
			</div>
		</div>