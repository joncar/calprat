<section id="content" class="business-main">
	
		<!-- PAGE HEADER -->
		<div class="title-color">
			<h2>Avís legal</h2>
			<ol class="breadcrumb">
				<li><a href="<?= site_url() ?>">Inicio</a></li>
				<li class="active"><a href="#">Aviso legal</a></li>				
			</ol>
		</div>
		
		<!-- MAIN CONTENT -->
		<div class="container">
			<div class="col-xs-12 col-sm-9 col-md-9 col-centered center-block">
				<div class="row">
					<div class="title">
						<h6>Aviso legal</h6>
						<h1>Protegemos tus datos</h1>
						<div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
							<span><img class="img-responsive" src="<?= base_url() ?>img/cap.png" alt=""></span>
						</div>
						<p>Cal Prat se preocupa por la seguridad de los datos que nuestros clientes nos proporcionas y por eso sigue la nueva legislación de protección de datos.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="business-detail" style="margin-bottom:39px">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<h6>Quién es el responsable del tratamiento de sus datos personales?</h6> <br>
<p>M. Alba Escolà Montaner es el responsable del tratamiento de los datos personales que usted nos proporciona y se responsabiliza de los referidos datos personales de acuerdo con la normativa aplicable en materia de protección de datos.
<br>M. Alba Escolà Montaner con domicilio social en calle Soler, s/n, Calonge de Segarra, España; con CIF número 79276286-R e inscrita en el Registro de Fundaciones privadas de la Generalitat de Cataluña con lo numero PCC-000384 y PCC-000385.
<br>E-Mail de contacto: info@calprat.net
</p>
<h6>Donde almacenamos sus datos?</h6> <br>
<p>Los datos que recopilamos sobre usted se almacenan dentro del Espacio Económico Europeo («EEE»). Cualquier transferencia de sus datos personales será realizada en conformidad con las leyes aplicables.
</p>
<h6>¿A quien comunicamos sus datos?</h6> <br>
<p>Sus datos pueden ser compartidos por la empresa de Cal Prat. Nunca pasamos, vendemos ni intercambiamos sus datos personales con terceras partes. Los datos que se envíen a terceros, se utilizarán únicamente para ofrecerle a usted nuestros servicios. Le detallaremos las categorías de terceros que acceden a sus datos para cada actividad de tratamiento específica.
</p>
<h6>¿Cual es la base legal para el tratamiento de sus datos personales? </h6> <br>
<p>En cada tratamiento específico de datos personales recopilados sobre usted, le informaremos si la comunicación de datos personales es un requisito legal o contractual, o un requisito necesario para subscribir un contrato, y si está obligado a facilitar los datos personales, así como de las posibles consecuencias de no facilitar tales datos.
</p>
<h6>¿Cuales son sus derechos?</h6> <br>
<h6>Derecho de acceso: </h6> <br>
<p>Cualquier persona tiene derecho a obtener confirmación sobre si en Cal Prat estamos tratando datos personales que los conciernan, o no. Puede contactar en info@calprat.net que le remitirá los datos personales que tratamos sobre usted por email.
</p>
<h6>Derecho de portabilidad: </h6> <br>
<p>Siempre que Cal Prat procese sus datos personales a través de medios automatizados en base a su consentimiento o a un acuerdo, usted tiene derecho a obtener una copia de sus datos en un formato estructurado, de uso común y lectura mecánica transferida a su nombre o a un tercero. En ella se incluirán únicamente los datos personales que usted nos haya facilitado.
</p>
<h6>Derecho de rectificación:  </h6> <br>
<p>Usted tiene derecho a solicitar la rectificación de sus datos personales si son inexactos, incluyendo el derecho a completar datos que figuren incompletos.
</p>
<h6>Derecho de supresión:  </h6> <br>
<p>Usted tiene derecho a obtener sin más dilación indebida la supresión de cualquier dato personal suyo tratada por Cal Prat en cualquier momento, excepto en las siguientes situaciones: <br> *tiene una deuda pendiente con Cal Prat, independientemente del método de pago
<br>* se sospecha o está confirmado que ha utilizado incorrectamente nuestros servicios en los últimos cuatro años
<br>* ha contratado algún servicio por el que conservaremos sus datos personales en relación con la transacción por normativa contable.

</p>
<h6>Derecho de oposición al tratamiento de datos en base al interés legítimo: </h6> <br>
<p>Usted tiene derecho a oponerse al tratamiento de sus datos personales en base al interés legítimo de Cal Prat. Cal Prat no seguirá tratando los datos personales salvo que podamos acreditar motivos legítimos imperiosos para el tratamiento que prevalezcan sobre sus intereses, derechos y libertades, o bien para la formulación, el ejercicio o la defensa de reclamaciones.
</p>
</p>
<h6>Derecho de oposición al marketing directo: </h6> <br>
<p>Usted tiene derecho a oponerse al marketing directo, incluyendo la elaboración de perfiles realizada para este marketing directo.<br>
Puede desvincularse del marketing directo en cualquier momento de las siguientes maneras: *siguiendo las indicaciones facilitadas en cada correo de marketing

</p>
</p>
<h6>Derecho a presentar una reclamación ante una autoridad de control: </h6> <br>
<p>Si usted considera que Cal Prat trata sus datos de una manera incorrecta, puede contactar con nosotros. También tiene derecho a presentar una queja ante la autoridad de protección de datos competente.
</p>
<h6>Derecho a limitación en el tratamiento:  </h6> <br>
<p>Usted tiene derecho a solicitar que Cal Prat limite el tratamiento de sus datos personales en las siguientes circunstancias:
<br>* si usted se opone al tratamiento de sus datos en base al interés legítimo de Cal Prat. Cal Prat tendrá que limitar cualquier tratamiento de estos datos a la espera de la verificación del interés legítimo.
<br>* si usted reclama que sus datos personales son incorrectos, Cal Prat tiene que limitar cualquier tratamiento de estos datos hasta que se verifique la precisión de los mismos.
<br>* si el tratamiento es ilegal, usted podrá oponerse al hecho que se eliminen los datos personales y, en su lugar, solicitar la limitación de su uso.
<br>* si Cal Prat ya no necesita los datos personales, pero usted los necesita para la formulación, el ejercicio o la defensa de reclamaciones.

</p>
<h6>Ejercicio de derechos:  </h6> <br>
<p>Nos tomamos muy en serio la protección de datos y, por lo tanto, contamos con personal de servicio al cliente dedicado que se ocupa de sus solicitudes en relación con los derechos antes mencionados. Siempre puede ponerse en contacto con ellos a: info@calprat.net

</p>
						</div>					
				</div>
				
			</div>
		</div>
		
	</section>