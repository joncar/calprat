<!-- WELCOME -->
<section id="content">
            <section id="content" class="activitie-main"><!-- PAGE HEADER --><div class="title-color">
			<h2>Activities &amp; Excursions</h2>
			<ol class="breadcrumb">
<li><a href="./index.html">Home</a></li>
				<li><a href="./index.html">Features</a></li>
				<li><a href="./index.html">Pages</a></li>
				<li class="active"><a>Activities &amp; Excursions</a></li>
			</ol>
</div>
		
		<!-- ACTIVITIES CONTENT -->
		<div class="activitie-block">
			<div class="container">
				<div class="col-xs-12 col-sm-12 col-md-8 col-centered center-block">
					<div class="row">
						<div class="title">
							<h6>goings-on</h6>
							<h1>Activities &amp; Excursions</h1>
							<div class="col-xs-6 col-sm-4 col-md-4 cap col-centered center-block">
								<span><img class="img-responsive" src="[base_url]img/cap.png" alt=""></span>
							</div>
							<p>Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi versa accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit</p>
						</div>
					</div>
				</div>
				<div class="activities">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-6 no-padding">
							<a href="./index.html">
								<div class="img">
									<img class="img-responsive" src="[base_url]img/activities/1.jpg" alt=""><span class="btn btn-default">details</span>
								</div>
							</a>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-6 no-padding">
							<div class="right-activ">
								<h2>Wine Country Tour &amp; Tasting</h2>
								<div class="star">
									<ul>
<li><a><i class="fa fa-star"></i></a></li>
										<li><a><i class="fa fa-star"></i></a></li>
										<li><a><i class="fa fa-star"></i></a></li>
										<li><a><i class="fa fa-star"></i></a></li>
										<li><a><i class="fa fa-star-half-o"></i></a></li>
									</ul>
</div>
								<div class="review">
									<ul>
<li><a href="./index.html">Write a Review</a></li>
										<li><a href="./index.html">Read Reviews</a></li>
									</ul>
</div>
								<p>Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo nullam ac urna eu felis dapibus condimentum sit amet a augue.</p>
								<div class="include">
									<h4>includes</h4>
									<ul>
<li>Sed odio sit amet nibh vulputate cursus</li>
										<li>Morbi versa accumsan ipsum velit</li>
										<li>Nam nec tellus a odio tincidunt auctor a ornare</li>
										<li>Sed non  mauris vitae erat consequat</li>
									</ul>
</div>
								<div class="activ-botpan">
									<div class="active-left">
										<ul>
<li><a href="./index.html"><i class="fa fa-cutlery"></i></a></li>
											<li><a href="./index.html"><i class="fa fa-glass"></i></a></li>
											<li><a href="./index.html"><i class="fa fa-wheelchair"></i></a></li>
										</ul>
</div>
									<div class="active-right">
										<img src="[base_url]img/activities/6.png" alt="">
</div>
								</div>
							</div>
							<div class="visitor">
								<span class="calender"><i class="fa fa-calendar-o"></i>may 1, 2015</span>
								<span class="comment"><i class="fa fa-clock-o"></i>11:30 am - 3:30 PM</span>
								<span class="views"><i class="fa fa-bookmark"></i>$30 / person</span>
							</div>
						</div>
					</div>
				</div>
				<div class="activities">
					<div class="row">
						<div class="col-xs-12 col-sm-5 col-md-5 no-padding">
							<a href="./index.html">
								<div class="img">
									<img class="img-responsive" src="[base_url]img/activities/2.jpg" alt=""><span class="btn btn-default">details</span>
								</div>
							</a>
						</div>
						<div class="col-xs-12 col-sm-7 col-md-7 no-padding">
							<div class="right-activ">
								<h2>3rd Annual Bocce Ball Tournament</h2>
								<p>Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia.</p>
							</div>
							<div class="visitor">
								<span class="calender"><i class="fa fa-calendar-o"></i>may 2, 2015</span>
								<span class="comment"><i class="fa fa-clock-o"></i>2:00 - 4:00 pm</span>
								<span class="views"><i class="fa fa-bookmark"></i>free</span>
							</div>
						</div>
					</div>
				</div>
				<div class="activities">
					<div class="row">
						<div class="col-xs-12 col-sm-5 col-md-5 no-padding">
							<a href="./index.html">
								<div class="img">
									<img class="img-responsive" src="[base_url]img/activities/3.jpg" alt=""><span class="btn btn-default">details</span>
								</div>
							</a>
						</div>
						<div class="col-xs-12 col-sm-7 col-md-7 no-padding">
							<div class="right-activ">
								<h2>Chef’s Choice Wine Tasting Dinner</h2>
								<p>Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia.</p>
							</div>
							<div class="visitor">
								<span class="calender"><i class="fa fa-calendar-o"></i>may 9, 2015</span>
								<span class="comment"><i class="fa fa-clock-o"></i>7:00 - 9:30 pm</span>
								<span class="views"><i class="fa fa-bookmark"></i>$50 / person</span>
							</div>
						</div>
					</div>
				</div>
				<div class="activities">
					<div class="row">
						<div class="col-xs-12 col-sm-5 col-md-5 no-padding">
							<a href="./index.html">
								<div class="img">
									<img class="img-responsive" src="[base_url]img/activities/4.jpg" alt=""><span class="btn btn-default">details</span>
								</div>
							</a>
						</div>
						<div class="col-xs-12 col-sm-7 col-md-7 no-padding">
							<div class="right-activ">
								<h2>Scenes from Bali Slideshow</h2>
								<p>Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia.</p>
							</div>
							<div class="visitor">
								<span class="calender"><i class="fa fa-calendar-o"></i>may 20, 2015</span>
								<span class="comment"><i class="fa fa-clock-o"></i>7:00 - 9:30 pm</span>
								<span class="views"><i class="fa fa-bookmark"></i>free</span>
							</div>
						</div>
					</div>
				</div>
				<div class="activities">
					<div class="row">
						<div class="col-xs-12 col-sm-5 col-md-5 no-padding">
							<a href="./index.html">
								<div class="img">
									<img class="img-responsive" src="[base_url]img/activities/5.jpg" alt=""><span class="btn btn-default">details</span>
								</div>
							</a>
						</div>
						<div class="col-xs-12 col-sm-7 col-md-7 no-padding">
							<div class="right-activ">
								<h2>British Tea Time</h2>
								<p>Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia.</p>
							</div>
							<div class="visitor">
								<span class="calender"><i class="fa fa-calendar-o"></i>daily</span>
								<span class="comment"><i class="fa fa-clock-o"></i>4:00 - 5:00 pm</span>
								<span class="views"><i class="fa fa-bookmark"></i>free</span>
							</div>
						</div>
					</div>
				</div>
				<div class="button">
					<button class="btn btn-default" type="submit">more activities</button>
				</div>
				<div class="activitie-cal">
					<div class="col-xs-12 col-sm-12 col-md-8 col-centered center-block">
						<div class="row">
							<div class="title">
								<h6>activities calendar</h6>
								<h1>Look What’s on the Horizon</h1>
								<div class="col-xs-6 col-sm-4 col-md-4 cap col-centered center-block">
									<span><img alt="" src="[base_url]img/cap.png" class="img-responsive"></span>
								</div>
								<p>Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi versa accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit</p>
							</div>
						</div>
					</div>
					<div class="main-cal cal-big">
						<table class="cal">
<caption>
								<a class="prev"><i class="fa fa-angle-left"></i></a>
								<a class="next"><i class="fa fa-angle-right"></i></a>
								april 2015
							</caption>
							<thead><tr>
<th>sun</th>
									<th>mon</th>
									<th>tue</th>
									<th>wed</th>
									<th>thu</th>
									<th>fri</th>
									<th>sat</th>
								</tr></thead>
<tbody>
<tr>
<td><a href="index.html">31</a></td>
									<td class="off"><a href="index.html">27</a></td>
									<td class="off"><a href="index.html">28</a></td>
									<td class="off"><a href="index.html">29</a></td>
									<td class="off"><a href="index.html">1</a></td>
									<td class="index mc-tooltip">
										<div class="mc-popup mc-popup-right mc-popup-right-mob">
											<img src="[base_url]img/activities/7.png" class="img-responsive" alt=""><div class="mcp-inner">
												<h2>May 20</h2>
												<h4>Scenes from Bali Slideshow</h4>
												<span>7:30 pm</span>
												<p>Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit.</p>
											</div>
										</div>
										<a>
											1
											<div class="detail">
												<h6>wine country tour<span>11:30 am</span>
</h6>
											</div>
										</a>
									</td>
									<td class="index mc-tooltip">
										<div class="mc-popup mc-popup-right">
											<img src="[base_url]img/activities/7.png" class="img-responsive" alt=""><div class="mcp-inner">
												<h2>May 20</h2>
												<h4>Scenes from Bali Slideshow</h4>
												<span>7:30 pm</span>
												<p>Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit.</p>
											</div>
										</div>
										<a>
											2
											<div class="detail">
												<h6>bocce ball tournament<span>2:00 pm</span>
</h6>
											</div>
										</a>
									</td>
								</tr>
<tr>
<td><a href="index.html">3</a></td>
									<td><a href="index.html">4</a></td>
									<td><a href="index.html">5</a></td>
									<td><a href="index.html">6</a></td>
									<td><a href="index.html">7</a></td>
									<td><a href="index.html">8</a></td>
									<td class="index mc-tooltip">
										<div class="mc-popup mc-popup-right">
											<img src="[base_url]img/activities/7.png" class="img-responsive" alt=""><div class="mcp-inner">
												<h2>May 20</h2>
												<h4>Scenes from Bali Slideshow</h4>
												<span>7:30 pm</span>
												<p>Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit.</p>
											</div>
										</div>
										<a>
											9
											<div class="detail">
												<h6>Chef's Choice Wine Tasting Dinner<span>7:00 - 9:30 pm</span>
</h6>
											</div>
										</a>
									</td>
								</tr>
<tr>
<td><a href="index.html">10</a></td>
									<td><a href="index.html">11</a></td>
									<td><a href="index.html">12</a></td>
									<td><a href="index.html">13</a></td>
									<td><a href="index.html">14</a></td>
									<td><a href="index.html">15</a></td>
									<td><a href="index.html">16</a></td>
								</tr>
<tr>
<td><a href="index.html">17</a></td>
									<td><a href="index.html">18</a></td>
									<td><a href="index.html">19</a></td>
									<td class="reserved mc-tooltip">
										<div class="mc-popup">
											<img src="[base_url]img/activities/7.png" class="img-responsive" alt=""><div class="mcp-inner">
												<h2>May 20</h2>
												<h4>Scenes from Bali Slideshow</h4>
												<span>7:30 pm</span>
												<p>Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit.</p>
											</div>
										</div>
										<a>
											20
											<div class="detail">
												<h6>scenes from bali
													slideshow<span>7:30 pm</span>
												</h6>
											</div>
										</a>
									</td>
									<td><a href="index.html">21</a></td>
									<td><a href="index.html">22</a></td>
									<td><a href="index.html">23</a></td>
								</tr>
<tr>
<td><a href="index.html">24</a></td>
									<td><a href="index.html">25</a></td>
									<td><a href="index.html">26</a></td>
									<td><a href="index.html">27</a></td>
									<td><a href="index.html">28</a></td>
									<td><a href="index.html">29</a></td>
									<td><a href="index.html">30</a></td>
								</tr>
</tbody>
</table>
</div>
				</div>
			</div>
		</div>
	</section>    </section>