
            <!-- WELCOME -->
<section id="content">
            <section id="content" class="promotion-main"><!-- PAGE HEADER --><div class="title-color" style="position: relative;">
			<h2 id="mce_91" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Promotions &amp; Special Offers</h2><input type="hidden" name="mce_91">
			<ol class="breadcrumb">
<li><a href="./index.html" id="mce_92" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Home</a><input type="hidden" name="mce_92"></li>
				<li><a href="./index.html" id="mce_93" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Features</a><input type="hidden" name="mce_93"></li>
				<li><a href="./index.html" id="mce_94" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Pages</a><input type="hidden" name="mce_94"></li>
				<li class="active"><a href="./promotions.html" id="mce_95" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Promotions &amp; Special Offers</a><input type="hidden" name="mce_95"></li>
			</ol>
<input type="hidden" name="mce_96"><input type="hidden" name="mce_97"><input type="hidden" name="mce_98"></div>
		
		<!-- INNER CONTENT -->
		<div class="promotion-block" style="position: relative;">
			<div class="container">
				<div class="col-xs-12 col-sm-12 col-md-8 col-centered center-block">
					<div class="row">
						<div class="title">
							<h6 id="mce_99" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">offers</h6><input type="hidden" name="mce_99">
							<h1 id="mce_100" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Promotions &amp; Special Offers</h1><input type="hidden" name="mce_100">
							<div class="col-xs-6 col-sm-4 col-md-4 cap col-centered center-block">
								<span id="mce_101" class="mce-content-body" contenteditable="true" spellcheck="false"><img class="img-responsive" src="[base_url]img/cap.png" alt=""></span><input type="hidden" name="mce_101">
							</div>
							<p id="mce_102" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi versa accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit</p><input type="hidden" name="mce_102">
						</div>
					</div>
				</div>
				<div class="offers offers_1">
					<a class="toggle-btn offer-expand mce-content-body" id="mce_103" contenteditable="true" spellcheck="false"><i class="fa fa-caret-down"></i></a><input type="hidden" name="mce_103">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-5">
							<img class="on-img-active" src="[base_url]img/promotions/1.jpg" alt=""><img class="off-img-active" src="[base_url]img/promotions/2.jpg" alt="">
</div>
						<div class="col-xs-12 col-sm-12 col-md-7">
							<div class="offer-side">
								<div class="top-pan">
									<h2 id="mce_104" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Stay 7 Nights / Pay for 6</h2><input type="hidden" name="mce_104">
									<button class="btn btn-default btn-active" type="submit">show details</button>
									<a href="./reservation.html" class="btn btn-default btn-hidden mce-content-body" id="mce_105" contenteditable="true" spellcheck="false">Book Now</a><input type="hidden" name="mce_105">
								</div>
								<p id="mce_106" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa.</p><input type="hidden" name="mce_106">
								<div class="offer-content">
								<h4 id="mce_107" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Features &amp; Benefits</h4><input type="hidden" name="mce_107">
								<ul>
<li>Sed odio sit amet nibh vulputate cursus</li>
									<li>Morbi versa accumsan ipsum velit</li>
									<li>Nam nec tellus a odio tincidunt auctor a ornare</li>
									<li>Sed non  mauris vitae erat consequat</li>
									<li>Class aptent taciti sociosqu ad litora torquent</li>
								</ul>
<div class="valid-box">
									<div class="left-valid">
										<h5 id="mce_108" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">offer valid from</h5><input type="hidden" name="mce_108">
										<h3 id="mce_109" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">June 1st to July 15th, 2015.</h3><input type="hidden" name="mce_109">
									</div>
									<div class="right-valid">
										<h6 id="mce_110" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">rates vary</h6><input type="hidden" name="mce_110">
										<h3 id="mce_111" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">inquire</h3><input type="hidden" name="mce_111">
									</div>
								</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="offers offers_2 active">
					<a class="toggle-btn offer-expand active mce-content-body" id="mce_112" contenteditable="true" spellcheck="false"><i class="fa fa-caret-up"></i></a><input type="hidden" name="mce_112">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-5">
							<img class="on-img-active" src="[base_url]img/promotions/1.jpg" alt=""><img class="off-img-active" src="[base_url]img/promotions/2.jpg" alt="">
</div>
						<div class="col-xs-12 col-sm-12 col-md-7">
							<div class="offer-side">
								<div class="top-pan">
									<h2 id="mce_113" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Your Most Beautiful Wedding</h2><input type="hidden" name="mce_113">
									<button class="btn btn-default btn-active" type="submit">show details</button>
									<a href="./reservation.html" class="btn btn-default btn-hidden mce-content-body" id="mce_114" contenteditable="true" spellcheck="false">Book Now</a><input type="hidden" name="mce_114">
								</div>
								<p id="mce_115" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa.</p><input type="hidden" name="mce_115">
								<div class="offer-content">
								<h4 id="mce_116" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Features &amp; Benefits</h4><input type="hidden" name="mce_116">
								<ul>
<li>Sed odio sit amet nibh vulputate cursus</li>
									<li>Morbi versa accumsan ipsum velit</li>
									<li>Nam nec tellus a odio tincidunt auctor a ornare</li>
									<li>Sed non  mauris vitae erat consequat</li>
									<li>Class aptent taciti sociosqu ad litora torquent</li>
								</ul>
<div class="valid-box">
									<div class="left-valid">
										<h5 id="mce_117" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">offer valid from</h5><input type="hidden" name="mce_117">
										<h3 id="mce_118" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">June 1st to July 15th, 2015.</h3><input type="hidden" name="mce_118">
									</div>
									<div class="right-valid">
										<h6 id="mce_119" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">rates vary</h6><input type="hidden" name="mce_119">
										<h3 id="mce_120" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">inquire</h3><input type="hidden" name="mce_120">
									</div>
								</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="offers offers_3">
					<a class="toggle-btn offer-expand mce-content-body" id="mce_121" contenteditable="true" spellcheck="false"><i class="fa fa-caret-down"></i></a><input type="hidden" name="mce_121">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-5">
							<img class="on-img-active" src="[base_url]img/promotions/3.jpg" alt=""><img class="off-img-active" src="[base_url]img/promotions/2.jpg" alt="">
</div>
						<div class="col-xs-12 col-sm-12 col-md-7">
							<div class="offer-side">
								<div class="top-pan">
									<h2 id="mce_122" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Pure Indulgence Spa Package</h2><input type="hidden" name="mce_122">
									<button class="btn btn-default btn-active" type="submit">show details</button>
									<a href="./reservation.html" class="btn btn-default btn-hidden mce-content-body" id="mce_123" contenteditable="true" spellcheck="false">Book Now</a><input type="hidden" name="mce_123">
								</div>
								<p id="mce_124" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa.</p><input type="hidden" name="mce_124">
								<div class="offer-content">
								<h4 id="mce_125" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Features &amp; Benefits</h4><input type="hidden" name="mce_125">
								<ul>
<li>Sed odio sit amet nibh vulputate cursus</li>
									<li>Morbi versa accumsan ipsum velit</li>
									<li>Nam nec tellus a odio tincidunt auctor a ornare</li>
									<li>Sed non  mauris vitae erat consequat</li>
									<li>Class aptent taciti sociosqu ad litora torquent</li>
								</ul>
<div class="valid-box">
									<div class="left-valid">
										<h5 id="mce_126" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">offer valid from</h5><input type="hidden" name="mce_126">
										<h3 id="mce_127" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">June 1st to July 15th, 2015.</h3><input type="hidden" name="mce_127">
									</div>
									<div class="right-valid">
										<h6 id="mce_128" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">rates vary</h6><input type="hidden" name="mce_128">
										<h3 id="mce_129" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">inquire</h3><input type="hidden" name="mce_129">
									</div>
								</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="offers offers_4">
					<a class="toggle-btn offer-expand mce-content-body" id="mce_130" contenteditable="true" spellcheck="false"><i class="fa fa-caret-down"></i></a><input type="hidden" name="mce_130">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-5">
							<img class="on-img-active" src="[base_url]img/promotions/4.jpg" alt=""><img class="off-img-active" src="[base_url]img/promotions/2.jpg" alt="">
</div>
						<div class="col-xs-12 col-sm-12 col-md-7">
							<div class="offer-side">
								<div class="top-pan">
									<h2 id="mce_131" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Book 5 or More Rooms &amp; Save!</h2><input type="hidden" name="mce_131">
									<button class="btn btn-default btn-active" type="submit">show details</button>
									<a href="./reservation.html" class="btn btn-default btn-hidden mce-content-body" id="mce_132" contenteditable="true" spellcheck="false">Book Now</a><input type="hidden" name="mce_132">
								</div>
								<p id="mce_133" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa.</p><input type="hidden" name="mce_133">
								<div class="offer-content">
								<h4 id="mce_134" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Features &amp; Benefits</h4><input type="hidden" name="mce_134">
								<ul>
<li>Sed odio sit amet nibh vulputate cursus</li>
									<li>Morbi versa accumsan ipsum velit</li>
									<li>Nam nec tellus a odio tincidunt auctor a ornare</li>
									<li>Sed non  mauris vitae erat consequat</li>
									<li>Class aptent taciti sociosqu ad litora torquent</li>
								</ul>
<div class="valid-box">
									<div class="left-valid">
										<h5 id="mce_135" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">offer valid from</h5><input type="hidden" name="mce_135">
										<h3 id="mce_136" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">June 1st to July 15th, 2015.</h3><input type="hidden" name="mce_136">
									</div>
									<div class="right-valid">
										<h6 id="mce_137" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">rates vary</h6><input type="hidden" name="mce_137">
										<h3 id="mce_138" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">inquire</h3><input type="hidden" name="mce_138">
									</div>
								</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<input type="hidden" name="mce_139"><input type="hidden" name="mce_140"><input type="hidden" name="mce_141"></div>
		
		<div class="find-room" style="position: relative;">
			<div class="container">
				<h2 id="mce_142" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Find a Room</h2><input type="hidden" name="mce_142">
				<div class="col-md-12">
					<div class="form row">
						<div class="form-group col-xs-12 col-sm-6 col-md-2 col-lg-3">
							<label>arrival date</label>
							<div id="datetimepicker1" class="input-group date">
								<input type="text" placeholder="" class="form-control"><span class="input-group-addon mce-content-body" id="mce_143" contenteditable="true" spellcheck="false"><i class="fa fa-calendar"></i></span><input type="hidden" name="mce_143">
							</div>
						</div>
						<div class="form-group col-xs-12 col-sm-6 col-md-2 col-lg-3">
							<label>departure date</label>
							<div id="datetimepicker2" class="input-group date">
								<input type="text" placeholder="" class="form-control"><span class="input-group-addon mce-content-body" id="mce_144" contenteditable="true" spellcheck="false"><i class="fa fa-calendar"></i></span><input type="hidden" name="mce_144">
							</div>
						</div>
						<div class="form-group col-xs-12 col-sm-6 col-md-3 col-lg-2">
							<label># of rooms required</label>
							<input type="text" placeholder="" class="form-control">
</div>
						<div class="form-group col-xs-12 col-sm-6 col-md-2 col-lg-2">
							<label># of guests</label>
							<input type="text" placeholder="" class="form-control">
</div>
						<div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-2">
							<button class="btn btn-default btn-availble" type="submit">find rooms</button>
						</div>
					</div>
				</div>
			</div>
		<input type="hidden" name="mce_145"><input type="hidden" name="mce_146"><input type="hidden" name="mce_147"></div><div style="text-align:center;"></div>
	</section>    </section>    