<!-- WELCOME -->
<section id="content">
            <section id="content"><!-- PAGE HEADER --><div class="title-color">
			<div class="container">
				<h2>Rooms - 2 column</h2>
				<ol class="breadcrumb">
<li><a href="./index.html">Home</a></li>
					<li><a href="./accommodations.html">Accommodations</a></li>
					<li class="active">Rooms - 2 column</li>
				</ol>
</div>
		</div>
		
		<!-- ROOM INFO -->
		<div class="container room-col room-three">
			<div class="col-xs-12 col-sm-10 col-md-6 col-centered center-block">
				<div class="row">
					<div class="title">
						<h6>accommodations</h6>
						<h1>Our Rooms &amp; Suites</h1>
						<div class="col-xs-12 col-sm-4 col-md-4 cap col-centered center-block">
							<span><img class="img-responsive" src="[base_url]img/cap.png" alt=""></span>
						</div>
						<p>Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi versa accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit</p>
					</div>
				</div>
			</div>
			<div class="sort-select">
				<div class="sort-left">
					<h6>sort</h6>
					<select id="basic1"><option value="">relevance</option>
<option value="">price: low to high</option>
<option value="">price: medium</option></select>
</div>
				<div class="select-right ">
				<ul class="rm-filter" data-option-key="filter">
<li><a class="selected" href="#filter" data-option-value="*">All</a></li>
					<li><a href="#" data-option-value=".item-room">Rooms</a></li>
					<li><a href="#" data-option-value=".item-suite">Suites</a></li>
				</ul>
</div>
			</div>
			<div class="three-col">
				<div id="rooms-mason" class="isotope rm-mason-3col">
				<div>
					<div class="rm-item item-room">
						<div class="item">
							<img alt="Owl Image" src="[base_url]img/rooms-col2/1.jpg"><div class="figcaption">
								<h3>ocean view balcony room</h3>
								<div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
									<span><img alt="" src="[base_url]img/cap.png" class="img-responsive"></span>
								</div>
								<ul>
<li>
										<span class="des">beds</span>
										<span class="detail">[camas]</span>
									</li>
									<li>
										<span class="des">occupancy</span>
										<span class="detail">[max_personas]</span>
									</li>
									<li>
										<span class="des">size</span>
										<span class="detail">[tamano]</span>
									</li>
									<li>
										<span class="des">view</span>
										<span class="detail">[vistas]</span>
									</li>
									<li>
										<span class="des">rates from</span>
										<span class="detail">[precio_desde]</span>
									</li>
									<li>
										<span class="des">video</span>
										<span class="detail"><a href="https://vimeo.com/25598691" class="prettyPhoto"><i class="fa fa-video-camera"></i></a></span>
									</li>
								</ul>
<div class="dual-btns">
									<a href="./roomDetail-gallery.html" class="btn btn-default pull-left">room details</a>
									<a href="./reservation.html" class="btn btn-default pull-right">book now</a>
								</div>
							</div>
						</div>
						<div class="single-detail">
							<span class="plus">+</span>
							<h3>ocean view balcony room</h3>
						</div>
					</div>

					<div class="rm-item item-suite">
						<div class="item">
							<img alt="Owl Image" src="[base_url]img/rooms-col2/2.jpg"><div class="figcaption">
								<h3>ocean view balcony room</h3>
								<div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
									<span><img alt="" src="[base_url]img/cap.png" class="img-responsive"></span>
								</div>
								<ul>
<li>
										<span class="des">beds</span>
										<span class="detail">King or 2 Queen</span>
									</li>
									<li>
										<span class="des">occupancy</span>
										<span class="detail">2 People</span>
									</li>
									<li>
										<span class="des">size</span>
										<span class="detail">60 sqm / 650 sqf</span>
									</li>
									<li>
										<span class="des">view</span>
										<span class="detail">Ocean / City</span>
									</li>
									<li>
										<span class="des">rates from</span>
										<span class="detail">215 USD</span>
									</li>
									<li>
										<span class="des">video</span>
										<span class="detail"><a href="https://vimeo.com/25598691" class="prettyPhoto"><i class="fa fa-video-camera"></i></a></span>
									</li>
								</ul>
<div class="dual-btns">
									<a href="./roomDetail-gallery.html" class="btn btn-default pull-left">room details</a>
									<a href="./reservation.html" class="btn btn-default pull-right">book now</a>
								</div>
							</div>
						</div>
						<div class="single-detail">
							<span class="plus">+</span>
							<h3>ocean view balcony room</h3>
						</div>
					</div>

					<div class="rm-item item-room">
						<div class="item">
							<img alt="Owl Image" src="[base_url]img/rooms-col2/3.jpg"><div class="figcaption">
								<h3>deluxe ocean view room</h3>
								<div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
									<span><img alt="" src="[base_url]img/cap.png" class="img-responsive"></span>
								</div>
								<ul>
<li>
										<span class="des">beds</span>
										<span class="detail">King or 2 Queen</span>
									</li>
									<li>
										<span class="des">occupancy</span>
										<span class="detail">2 People</span>
									</li>
									<li>
										<span class="des">size</span>
										<span class="detail">60 sqm / 650 sqf</span>
									</li>
									<li>
										<span class="des">view</span>
										<span class="detail">Ocean / City</span>
									</li>
									<li>
										<span class="des">rates from</span>
										<span class="detail">215 USD</span>
									</li>
									<li>
										<span class="des">video</span>
										<span class="detail"><a href="https://vimeo.com/25598691" class="prettyPhoto"><i class="fa fa-video-camera"></i></a></span>
									</li>
								</ul>
<div class="dual-btns">
									<a href="./roomDetail-gallery.html" class="btn btn-default pull-left">room details</a>
									<a href="./reservation.html" class="btn btn-default pull-right">book now</a>
								</div>
							</div>
						</div>
						<div class="single-detail">
							<span class="plus">+</span>
							<h3>deluxe ocean view room</h3>
						</div>
					</div>
					<div class="rm-item item-room">
						<div class="item">
							<img alt="Owl Image" src="[base_url]img/rooms-col2/4.jpg"><div class="figcaption">
								<h3>luxury terrace suite</h3>
								<div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
									<span><img alt="" src="[base_url]img/cap.png" class="img-responsive"></span>
								</div>
								<ul>
<li>
										<span class="des">beds</span>
										<span class="detail">King or 2 Queen</span>
									</li>
									<li>
										<span class="des">occupancy</span>
										<span class="detail">2 People</span>
									</li>
									<li>
										<span class="des">size</span>
										<span class="detail">60 sqm / 650 sqf</span>
									</li>
									<li>
										<span class="des">view</span>
										<span class="detail">Ocean / City</span>
									</li>
									<li>
										<span class="des">rates from</span>
										<span class="detail">215 USD</span>
									</li>
									<li>
										<span class="des">video</span>
										<span class="detail"><a href="https://vimeo.com/25598691" class="prettyPhoto"><i class="fa fa-video-camera"></i></a></span>
									</li>
								</ul>
<div class="dual-btns">
									<a href="./roomDetail-gallery.html" class="btn btn-default pull-left">room details</a>
									<a href="./reservation.html" class="btn btn-default pull-right">book now</a>
								</div>
							</div>
						</div>
						<div class="single-detail">
							<span class="plus">+</span>
							<h3>luxury terrace suite</h3>
						</div>
					</div>
					<div class="rm-item item-suite">
						<div class="item">
							<img alt="Owl Image" src="[base_url]img/rooms-col2/5.jpg"><div class="figcaption">
								<h3>deluxe executive room</h3>
								<div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
									<span><img alt="" src="[base_url]img/cap.png" class="img-responsive"></span>
								</div>
								<ul>
<li>
										<span class="des">beds</span>
										<span class="detail">King or 2 Queen</span>
									</li>
									<li>
										<span class="des">occupancy</span>
										<span class="detail">2 People</span>
									</li>
									<li>
										<span class="des">size</span>
										<span class="detail">60 sqm / 650 sqf</span>
									</li>
									<li>
										<span class="des">view</span>
										<span class="detail">Ocean / City</span>
									</li>
									<li>
										<span class="des">rates from</span>
										<span class="detail">215 USD</span>
									</li>
									<li>
										<span class="des">video</span>
										<span class="detail"><a href="https://vimeo.com/25598691" class="prettyPhoto"><i class="fa fa-video-camera"></i></a></span>
									</li>
								</ul>
<div class="dual-btns">
									<a href="./roomDetail-gallery.html" class="btn btn-default pull-left">room details</a>
									<a href="./reservation.html" class="btn btn-default pull-right">book now</a>
								</div>
							</div>
						</div>
						<div class="single-detail">
							<span class="plus">+</span>
							<h3>luxury terrace suite</h3>
						</div>
					</div>
					<div class="rm-item item-suite">
						<div class="item">
							<img alt="Owl Image" src="[base_url]img/rooms-col2/6.jpg"><div class="figcaption">
								<h3>luxury city view suite</h3>
								<div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
									<span><img alt="" src="[base_url]img/cap.png" class="img-responsive"></span>
								</div>
								<ul>
<li>
										<span class="des">beds</span>
										<span class="detail">King or 2 Queen</span>
									</li>
									<li>
										<span class="des">occupancy</span>
										<span class="detail">2 People</span>
									</li>
									<li>
										<span class="des">size</span>
										<span class="detail">60 sqm / 650 sqf</span>
									</li>
									<li>
										<span class="des">view</span>
										<span class="detail">Ocean / City</span>
									</li>
									<li>
										<span class="des">rates from</span>
										<span class="detail">215 USD</span>
									</li>
									<li>
										<span class="des">video</span>
										<span class="detail"><a href="https://vimeo.com/25598691" class="prettyPhoto"><i class="fa fa-video-camera"></i></a></span>
									</li>
								</ul>
<div class="dual-btns">
									<a href="./roomDetail-gallery.html" class="btn btn-default pull-left">room details</a>
									<a href="./reservation.html" class="btn btn-default pull-right">book now</a>
								</div>
							</div>
						</div>
						<div class="single-detail">
							<span class="plus">+</span>
							<h3>luxury city view suite</h3>
						</div>
					</div>
					<div class="rm-item item-room">
						<div class="item">
							<img alt="Owl Image" src="[base_url]img/rooms-col2/7.jpg"><div class="figcaption">
								<h3>deluxe executive room</h3>
								<div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
									<span><img alt="" src="[base_url]img/cap.png" class="img-responsive"></span>
								</div>
								<ul>
<li>
										<span class="des">beds</span>
										<span class="detail">King or 2 Queen</span>
									</li>
									<li>
										<span class="des">occupancy</span>
										<span class="detail">2 People</span>
									</li>
									<li>
										<span class="des">size</span>
										<span class="detail">60 sqm / 650 sqf</span>
									</li>
									<li>
										<span class="des">view</span>
										<span class="detail">Ocean / City</span>
									</li>
									<li>
										<span class="des">rates from</span>
										<span class="detail">215 USD</span>
									</li>
									<li>
										<span class="des">video</span>
										<span class="detail"><a href="https://vimeo.com/25598691" class="prettyPhoto"><i class="fa fa-video-camera"></i></a></span>
									</li>
								</ul>
<div class="dual-btns">
									<a href="./roomDetail-gallery.html" class="btn btn-default pull-left">room details</a>
									<a href="./reservation.html" class="btn btn-default pull-right">book now</a>
								</div>
							</div>
						</div>
						<div class="single-detail">
							<span class="plus">+</span>
							<h3>deluxe executive room</h3>
						</div>
					</div>
					<div class="rm-item item-suite">
						<div class="item">
							<img alt="Owl Image" src="[base_url]img/rooms-col2/8.jpg"><div class="figcaption">
								<h3>luxury city view suite</h3>
								<div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
									<span><img alt="" src="[base_url]img/cap.png" class="img-responsive"></span>
								</div>
								<ul>
<li>
										<span class="des">beds</span>
										<span class="detail">King or 2 Queen</span>
									</li>
									<li>
										<span class="des">occupancy</span>
										<span class="detail">2 People</span>
									</li>
									<li>
										<span class="des">size</span>
										<span class="detail">60 sqm / 650 sqf</span>
									</li>
									<li>
										<span class="des">view</span>
										<span class="detail">Ocean / City</span>
									</li>
									<li>
										<span class="des">rates from</span>
										<span class="detail">215 USD</span>
									</li>
									<li>
										<span class="des">video</span>
										<span class="detail"><a href="https://vimeo.com/25598691" class="prettyPhoto"><i class="fa fa-video-camera"></i></a></span>
									</li>
								</ul>
<div class="dual-btns">
									<a href="./roomDetail-gallery.html" class="btn btn-default pull-left">room details</a>
									<a href="./reservation.html" class="btn btn-default pull-right">book now</a>
								</div>
							</div>
						</div>
						<div class="single-detail">
							<span class="plus">+</span>
							<h3>luxury city view suite</h3>
						</div>
					</div>
					<div class="rm-item item-room">
						<div class="item">
							<img alt="Owl Image" src="[base_url]img/rooms-col2/1.jpg"><div class="figcaption">
								<h3>ocean view balcony room</h3>
								<div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
									<span><img alt="" src="[base_url]img/cap.png" class="img-responsive"></span>
								</div>
								<ul>
<li>
										<span class="des">beds</span>
										<span class="detail">King or 2 Queen</span>
									</li>
									<li>
										<span class="des">occupancy</span>
										<span class="detail">2 People</span>
									</li>
									<li>
										<span class="des">size</span>
										<span class="detail">60 sqm / 650 sqf</span>
									</li>
									<li>
										<span class="des">view</span>
										<span class="detail">Ocean / City</span>
									</li>
									<li>
										<span class="des">rates from</span>
										<span class="detail">215 USD</span>
									</li>
									<li>
										<span class="des">video</span>
										<span class="detail"><a href="https://vimeo.com/25598691" class="prettyPhoto"><i class="fa fa-video-camera"></i></a></span>
									</li>
								</ul>
<div class="dual-btns">
									<a href="./roomDetail-gallery.html" class="btn btn-default pull-left">room details</a>
									<a href="./reservation.html" class="btn btn-default pull-right">book now</a>
								</div>
							</div>
						</div>
						<div class="single-detail">
							<span class="plus">+</span>
							<h3>ocean view balcony room</h3>
						</div>
					</div>
				</div>
			</div>
			</div>
		</div>
	</section>    </section>