<section id="content" class="business-main">
	
		<!-- PAGE HEADER -->
		<div class="title-color">
			<h2>Resultats de busqueda</h2>
			<ol class="breadcrumb">
				<li><a href="<?= site_url() ?>">Inicio</a></li>
				<li class="active"><a href="#">Resultados de la búsqueda</a></li>				
			</ol>
		</div>
		
		<!-- MAIN CONTENT -->
		<div class="container">
			<div class="col-xs-12 col-sm-9 col-md-9 col-centered center-block">
				<div class="row">
					<div class="title" style="padding-bottom: 29px;">						
						<h1>Resultados de la búsqueda</h1>
						<div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
							<span><img class="img-responsive" src="<?= base_url() ?>img/cap.png" alt=""></span>
						</div>						
					</div>
				</div>
			</div>
		</div>
		<div class="business-detail" style="margin-bottom:39px">
			<div class="container">
				<div class="row buscador">
					<?php foreach($resultados->result() as $r): ?>
						<div class="col-xs-12">
							<h4><a href="<?= base_url($r->url.'/'.toUrl($r->id.'-'.$r->texto1)) ?>"><?= $r->texto1 ?></a></h4>
							<p>
								<?= substr(strip_tags($r->texto2),0,255) ?>
							</p>
						</div>		
					<?php endforeach ?>
					<?php if($resultados->num_rows()==0): ?>
						<div class="col-xs-12">
							<p>No se han encontrado resultados</p>
						</div>
					<?php endif ?>			
				</div>
				
			</div>
		</div>
		
	</section>