<section id="content" class="business-main">
	
	<!-- PAGE HEADER -->
	<div class="title-color">
		<h2>Política de reserva y cancelaciones</h2>
		<ol class="breadcrumb">
			<li><a href="<?= site_url() ?>">Inicio</a></li>
			<li class="active"><a href="#">Política de reserva y cancelaciones</a></li>
		</ol>
	</div>
	
	<!-- MAIN CONTENT -->
	<div class="container">
		<div class="col-xs-12 col-sm-9 col-md-9 col-centered center-block">
			<div class="row">
				<div class="title">
					<h6>&nbsp;</h6>
					<h1>Política de reserva y cancelaciones</h1>
					<div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
						<span><img class="img-responsive" src="<?= base_url() ?>img/cap.png" alt=""></span>
					</div>
					<p><p>Cal Prat se preocupa por la seguridad de los datos que nuestros clientes nos proporcionan y para así seguir la nueva legislación de protección de datos.</p></p>
				</div>
			</div>
		</div>
	</div>
	<div class="business-detail" style="margin-bottom:39px">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
                    <h6 style="font-weight: 700;">PAGA Y SEÑAL POR PARTE DE LAS PERSONAS USUARIAS</h6>
					<p>Las personas titulares de los establecimientos de alojamiento pueden exigir a las personas usuarias que efectuen una reserva, un avance del precio que se entiende a cuenta del importe resultante de los servicios prestados.<br/>
					En este sentido para formalizar la reserva (mínimo dos noches)en nuestro establecimiento, Cal Prat solicitará un pago avanzando mínimo del 50% del precio total de la estancia y una semana antes se deberá pagar el resto de la reserva.<br/></p>
					<br><br>
                    <h6 style="font-weight: 700;">CANCELACIÓN DE LA RESERVA</h6>
					
					<p>1. Es establecimiento de alojaiento turístico está obligado a informar a la persona usuaria, antes de la formalización del contrato, soble las cláusulas de cancelación.<br/>
					2. La cancelación efectuada por la persona usuaria dentro de los 15 días anteriores a la fecha de llegada da lugar a las siguientes penalizaciones, salvo pacto contrario:<br/>
					a) Reserva para 2 o menos días, el 50% del precio total de la estancia.<br/>
					b) Reserva para más de 2 días hasta 7 días, el 35% del precio total de la estancia.<br/>
					c) Reserva para más de 7 días, el 25% del precio total de la estancia.<br/>
					Las anteriores penalizaciones no son de aplicación cuando la cancelación se produce por causa de fuerza mayor, debidamente acreditada.<br/>
					3. La persona usuaria tiene derecho a cancelar la reserva confirmada, sin ninguna penalización, siempre que se haga antes de los 15 días anteriores a la fecha de llegada, salvo pacto contrario.    <br><br></p>

                    <h6 style="font-weight: 700;">RESARCIMIENTO POR RENUNCIA DE LA ESTANCIA</h6>
					<p>La persona titular del establecimiento de alojamiento turístico está obligada a informar la persona o personas usuarias, antes de la realización del contrato, sobre la normativa aplicable en caso de renuncia de la estancia.<br/>
					Cuando el cliente o clienta de un establecimiento de alojamiento abandone la unidad reservada antes de la fecha hasta la cual la tenía reservada, la persona titular del establecimiento no devolverá ningún importe de la reserva efectuada.<br/>
					<br><br></p>
					
				</div>
			</div>
			
		</div>
	</div>
</section>