<!-- WELCOME -->
<section id="content">
    <section id="content" class="contact-main">
        <!-- PAGE HEADER -->
        <div class="title-color">
            <div class="container">
                <h2 id="mce_91">Contacta con nosotros</h2>
                <input name="mce_91" type="hidden"><input name="mce_91" type="hidden"><input name="mce_91" type="hidden">
                <ol class="breadcrumb">
                    <li><a href="./index.html" id="mce_92">Inicio</a><input name="mce_92" type="hidden"><input name="mce_92" type="hidden"><input name="mce_92" type="hidden"></li>
                    <li><a href="./accommodations.html" id="mce_93">Contacto</a><input name="mce_93" type="hidden"><input name="mce_93" type="hidden"><input name="mce_93" type="hidden"></li>
                    <!--					<li class="active">Contacte</li>-->
                </ol>
            </div>
            <input name="mce_94" type="hidden"><input name="mce_95" type="hidden"><input name="mce_96" type="hidden"><input name="mce_48" type="hidden"><input name="mce_49" type="hidden"><input name="mce_50" type="hidden">
        </div>
        <img class="visible-xs" src="[base_url]img/contact/contact-bg.jpg">
        <!-- CONTACT INFO -->
        <div class="main-contact">

            <div class="container">
                <div class="detail-cont">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-7 col-lg-7 left-detail">
                            <div class="direct-inq">
                                <form method="post" action="<?= base_url('paginas/frontend/contacto') ?>">
                                [response]
                                <h3 id="mce_97">Formulario de Contacto</h3>
                                <input name="mce_97" type="hidden"><input name="mce_97" type="hidden">
                                <div class="row detail-type">
                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                        <h6 id="mce_98">Nom *</h6>                                        
                                        <input name="nombre" id="senderfirstName" class="form-control"  type="text">
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                        <h6 id="mce_99">Apellidos *</h6>                                        
                                        <input name="apellido" id="senderlastName" class="form-control"  type="text">
                                    </div>
                                </div>
                                <div class="row detail-type">
                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                        <h6 id="mce_100">email *</h6>                                        
                                        <input name="email" id="senderEmail2" class="form-control"  type="email">
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                        <h6 id="mce_101">Teléfono</h6>                                        
                                        <input name="telefono" id="phone" class="form-control" type="text">
                                    </div>
                                </div>
                                <div class="row detail-type">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <h6 id="mce_102">Tema</h6>                                        
                                        <input name="tema" id="subject2" class="form-control"  type="text">
                                    </div>
                                </div>
                                <div class="row detail-type">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <h6 id="mce_103">Mensaje</h6>                                        
                                        <textarea name="message" id="message2" rows="3"></textarea>
                                        <span class="req mce-content-body" id="mce_104" spellcheck="false">* obligatorio</span>
                                    </div>
                                </div>
                                <div class="row detail-type">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <h6>
                                            <input type="checkbox" name="politicas" value="1"> He leido y acepto la <a href="<?= base_url('p/avis-legal') ?>" target="_new" style="color:#464646; text-decoration: underline !important;">política de privacidad* </a>
                                        </h6>
                                    </div>
                                </div>
                                <div class="row detail-type">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <div class="g-recaptcha" data-sitekey="6LdSNGAUAAAAAPFcFDPrON0NKwHLnN0GeL3ilX8s"></div>
                                    </div>
                                </div>
                                <div class="row detail-type">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <button class="btn btn-default" type="submit">ENVIAR</button>
                                    </div>
                                </div>   
                                </form>                            
                            </div>
                            <div class="map-direction">
                                <h3 id="mce_109" class="mce-content-body mce-edit-focus" spellcheck="false">Mapa / Ubicación</h3>
                                <input name="mce_109" type="hidden">
                                <div class="map">
                                    <div id="map">
                                    </div>
                                </div>
                                <ul>
                                    <li>
                                        <h4 id="mce_110"></h4>
                                        <input name="mce_110" type="hidden"><input name="mce_110" type="hidden">
                                        <p id="mce_111">
                                           COORDENADAS GPS:  E 1º27’44’’   N 41º44’53’’<br>
Cuando lleguéis a la C/1412, justo en el punto quilométrico núm.33 ,  torcer a mano izquierda dirección “El Soler”.<br>
Cal Prat se encuentra en el segundo grupo de casa.
</p>
                                        <input name="mce_111" type="hidden"><input name="mce_111" type="hidden">
                                    </li>
                                    <li>
                                        <h4 id="mce_112"></h4>
                                        <input name="mce_112" type="hidden"><input name="mce_112" type="hidden">
                                        <p id="mce_113">
                                            </p>
                                        <input name="mce_113" type="hidden"><input name="mce_113" type="hidden">
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5 right-detail">

                            <div class="blog">
                                <div class="main-sidebar">
                                    <form method="get" action="<?= base_url('iniciar-reserva') ?>">
                                    <div class="reservation">
                                        <h2 id="mce_114">Hacer una reserva</h2>                                        
                                        <div class="rate-from">
                                            <h5 id="mce_115">desde</h5>                                            
                                            <h1 id="mce_116"><span id="mce_117"><sup></sup></span>120€</h1>                                            
                                            <h6 id="mce_118">por noche</h6>                                            
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6 cap col-centered center-block">
                                            <span id="mce_119"><img class="img-responsive" src="[base_url]img/cap.png" alt=""></span>
                                        </div>
                                        <div class="row input-daterange">
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <div id="datetimepicker1" class="input-group date">
                                            <input placeholder="FECHA LLEGADA" name="desde" class="form-control" type="text" readonly="">
                                            <span class="input-group-addon mce-content-body" id="mce_109">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <div id="datetimepicker2" class="input-group date">
                                            <input placeholder="FECHA SALIDA" name="hasta" class="form-control" type="text" readonly="">
                                            <span class="input-group-addon mce-content-body" id="mce_110">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row date-picker">
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <select id="get_value" name="adultos" tabindex="0">
                                        <option value="">Adultos</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select>
</div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <select id="get_value2" name="infantes" tabindex="0">
                                        <option value="">Niños</option>
                                        <option value="0">0</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select>
</div>
</div>
                                        <button type="submit" class="btn btn-default btn-availble">Ver disponibilidad</button>
                                    </div></form>
                                    <div class="sidebar-contact">
                                        <div class="detail">
                                            <h3 class="title mce-content-body" id="mce_124" spellcheck="false">Como contactarnos?</h3>
                                            <input name="mce_124" type="hidden">
                                            <ul>
                                                <li>
                                                    <div class="img">
                                                        <i class="fa fa-phone"></i>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                                        <div class="row">
                                                            <span class="category mce-content-body" id="mce_125" spellcheck="false">teléfono</span><input name="mce_125" type="hidden"><input name="mce_125" type="hidden">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                                        <div class="row">
                                                            <p id="mce_126">
                                                                +34 650 93 64 00
                                                            </p>
                                                            
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="img">
                                                        <i class="fa fa-bell"></i>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                                        <div class="row">
                                                            <span class="category mce-content-body" id="mce_127" spellcheck="false">whats app</span><input name="mce_127" type="hidden"><input name="mce_127" type="hidden">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                                        <div class="row">
                                                            <p id="mce_128">
                                                                +34 650 93 64 00
                                                            </p>
                                                            
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="img">
                                                        <i class="fa fa-envelope"></i>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                                        <div class="row">
                                                            <span class="category mce-content-body" id="mce_129" spellcheck="false">email</span><input name="mce_129" type="hidden"><input name="mce_129" type="hidden">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                                        <div class="row">
                                                            <a href="mailto:info@fontainbleu.com" id="mce_130">info@calprat.net</a><input name="mce_130" type="hidden"><input name="mce_130" type="hidden">
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="img">
                                                        <i class="fa fa-phone"></i>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                                        <div class="row">
                                                            <span class="category mce-content-body" id="mce_131" spellcheck="false">adreça</span><input name="mce_131" type="hidden"><input name="mce_131" type="hidden">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                                        <div class="row">
                                                            <p id="mce_132">
                                                                El Soler, s/n <br>
                                                                CALONGE DE SEGARRA <br>
                                                                Barcelona

                                                            </p>
                                                            
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="subscribe" id="subscribir"
                                        <div class="title-block">
                                            <h3 class="title">Inscríbete en el boletín
                                            </h3>
                                        </div>
                                        <div class="sub-detail">
                                            <p>Te mantendremos informado de nuestras ofertas y novedades. No seremos pesados, únicamente queremos que estés a la última</p>
                                            [msg2]
                                            <form class="form-inline" method="post" action="<?= base_url('paginas/frontend/subscribir') ?>" onsubmit="if($('#recaptchaDivBolleti').css('display')=='none'){$('#recaptchaDivBolleti').css('display','block'); return false;}">
                                                <div class="form-group">
                                                    <input type="email" name="email" placeholder="escriu el teu email" id="exampleInputEmail2" class="form-control">
                                                </div>
                                                <button class="btn btn-default" type="submit">enviar</button>
                                                <div id="recaptchaDivBolleti" class="form-group" style="display: none">
                                                    <div class="g-recaptcha"  data-theme="dark" data-sitekey="6LdSNGAUAAAAAPFcFDPrON0NKwHLnN0GeL3ilX8s" data-callback='showCaptcha'></div>
                                                </div>
                                            </form>
                                            <span class="promise">Nos comprometemos a no mandar correo basura.</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input name="mce_136" type="hidden"><input name="mce_137" type="hidden"><input name="mce_138" type="hidden"><input name="mce_51" type="hidden"><input name="mce_52" type="hidden"><input name="mce_53" type="hidden">
        </div>
        <div style="text-align: center; position: relative;">
            <input name="mce_54" type="hidden"><input name="mce_55" type="hidden"><input name="mce_56" type="hidden">
        </div>
        <div style="text-align:center;">
        </div>
    </section></section>