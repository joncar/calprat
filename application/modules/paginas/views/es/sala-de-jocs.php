<section id="content" class="spa-main"><!-- PAGE HEADER --><div class="title-color" style="position: relative;">
            <h2 id="mce_40" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Sala de juegos</h2><input type="hidden" name="mce_40">
            <ol class="breadcrumb">
                <li><a href="./index.html" id="mce_41" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Inici</a><input type="hidden" name="mce_41"></li>
                <li class="active">Sala de juegos</li>
            </ol>
            <input type="hidden" name="mce_43"><input type="hidden" name="mce_44"><input type="hidden" name="mce_45"></div>

        <!-- SPA -->
        <div class="spa-block" style="position: relative;">
            <div class="db-image">
                <div class="dbi-inner" style="height:100%; background:url(<?= base_url() ?>img/spa/22.jpg) no-repeat center center">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="spa-right">
                    <h1 id="mce_46" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">El espacio de los más pequeños</h1><input type="hidden" name="mce_46">
                    <h6 id="mce_47" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Una sala donde divertirse jugando.</h6><input type="hidden" name="mce_47">
                    <p id="mce_48" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Cerca del patio encontraréis el acceso a la sala de juegos, una sala llena de juguetes para diferentes edades.</p><input type="hidden" name="mce_48">
                    <p id="mce_49" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Podréis estar tranquilos, mientras coméis y descansáis en la mesa, los niños podran estar al lado jugando.</p><input type="hidden" name="mce_49">
                    <h3 id="mce_50" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Juegos</h3><input type="hidden" name="mce_50">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <ul class="specific">
                                <li>Futbolín</li>
                                <li>Cochecito, camita y muñecas</li>
                                <li>Cocinita y utensilios para cocinar</li>
                                <li>Parking coches</li>
                                <li>2 patinetes y 3 cascos para los niños</li>
                                <li>3 triciclos</li>
                                <li>2 motos Molto</li>
                                <li>2 juegos de patines</li>
                                <li>Cuentos</li>
                            </ul>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <ul class="specific">
                                <li>Ositos de peluche</li>
                                <li>Pizarra de iman con letras</li>
                                <li>Juego de ajedrez</li>
                                <li>Juegos de parchís</li>
                                <li>Palas bádminton</li>
                                <li>Bolos</li>
                            </ul>
                        </div>
                    </div>

                    <h3 id="mce_50" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Imágenes de la Sala de Juegos</h3>
                    <div class="row" uk-lightbox>
                        [foreach:galeria_joc]
                            <div class="col-xs-12 col-sm-3" style="margin-top: 29px;"><a href="[foto]"><img src="[foto]" alt="" style="width:100%"></a></div>
                        [/foreach]
                    </div>
                </div>
                
            </div>
        </div>
</section>  