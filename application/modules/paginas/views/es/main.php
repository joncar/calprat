<div class="pre-header">
    <ul>
        <li class="ph-tel">
            <div class="dropdown">
              <a style="color: #AF9A5F;" id="dLabel" data-target="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-flag-o fa-2x" style="vertical-align: top; font-size:1.3em"></i> ESP
                <span class="caret"></span>
              </a>

              <ul class="dropdown-menu" aria-labelledby="dLabel">
                <li><a href="<?= base_url('main/traduccion/ca') ?>">CAT</a></li>
                <li><a href="<?= base_url('main/traduccion/es') ?>">ESP</a></li>
                <li><a href="<?= base_url('main/traduccion/en') ?>">ENG</a></li>
                <li><a href="<?= base_url('main/traduccion/fr') ?>">FRA</a></li>
              </ul>
            </div>
        </li>
        <li class="ph-tel"><a href="tel:+34650936400" style="color:inherit;"> <i class="fa fa-mobile fa-2x" style="vertical-align: top;color: #AF9A5F;"></i> <span>+34 650 93 64 00</span></a></li>
        <li class="ph-weather"><img src="[iconotemp]" alt=""/> <span>[temp]  &deg;</span></li>
        <li class="ph-social">
            <div>
                <a href="https://www.facebook.com/Cal-Prat-TurismeRural-143945365955474/" class="fa fa-facebook"></a>
                <a href="https://www.pinterest.com/username" class="fa fa-pinterest"></a>
                <a href="https://www.youtube.com/channel/UCrMyhl_KCzMONdNgmyJJmAA" class="fa fa-youtube"></a>
                <a href="https://www.instagram.com/calprat_turismerural/?hl=es" class="fa fa-instagram"></a>
            </div>
        </li>
        <li class="ph-search">
            <span class="ss-trigger fa fa-search"></span>
            <div class="ss-content">
                <span class="ss-close fa fa-times"></span>
                <div class="ssc-inner">
                    <form action="<?= base_url('paginas/frontend/buscar') ?>">
                        <input type="text" name="q" placeholder="Què busques?">
                        <button type="submit"><i class="fa fa-search"></i></button>
                    </form>
                </div>
            </div>
        </li>
    </ul>
</div>
<div class="top-slider">
    <div id="top-slider">
        <ul class="slides">
            [foreach:banner]
            <li>
                <img src="[foto]" alt="">
                <div class="flex-caption">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 id="mce_91" class="mce-content-body mce-edit-focus" >[titulo]</h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <p id="mce_92">[subtitulo]</p>
                        </div>
                    </div>
                </div>
            </li>
            [/foreach]
        </ul>
        <ul class="flex-direction-nav">
            <li><a class="flex-prev mce-content-body" id="mce_99" ><i class="fa fa-angle-left"></i></a></li>
            <li><a class="flex-next mce-content-body" id="mce_100" ><i class="fa fa-angle-right"></i></a></li>
        </ul>
    </div>
</div>
<!-- WELCOME -->
<section id="content">
    <div class="welcome" style="position: relative;">
        <div class="container">
            <div class="col-xs-12 col-sm-12 col-md-6 col-centered center-block">
                <div class="row">
                    <div class="title">
                        <h6 id="mce_101">Bienvenidos a</h6>
                        <h3 id="mce_102">la casa rural Cal Prat</h3>
                        <div class="col-xs-6 col-sm-4 col-md-4 cap col-centered center-block">
                            <span id="mce_103"><img alt="" src="[base_url]img/cap.png" class="img-responsive"></span>
                        </div>
                        <p id="mce_104">Cal Prat es una antigua casa de payés del siglo XVIII situada en el pequeño municipio de El Soler, dentro del término municipal de Calonge de Segarra en la Alta Anoia, justo a tocar de las comarcas de La Segarra y El Solsonès.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="left-welcome">
                        <h2 id="mce_105">El lugar perfecto para hacer una escapada</h2>
                        <p id="mce_106">Vuestra estancia a Hace falta Prat os permitirá disfrutar imaginándoos cómo ha sido la vida de nuestro campesinado en los últimos siglos, y podréis disfrutar del paisaje y la tranquilidad del mundo rural de hoy.</p>
                        <p id="mce_107">Una cuidadosa restauración nos ha permitido conservar todos los aposentos y elementos típicos de una antigua casa de labrador como son la bodega, el cup del vino, el establo, el horno de pan, el hogar de fuego, etc</p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="right-welcome">
                        <div id="promovid">
                            <div class="custom-th">
                                <img src="[base_url]img/Homepage_Resort_INDEX/video.png" class="img-responsive" alt="">
                            </div>
                            <div id="thevideo" style="display:none; padding: 0px;">
                                <div id="thevideo" style="display:none; padding: 0px;">                                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ACCOMODATION -->
    <div class="accomodation" style="position: relative;">
        <div class="container">
            <div class="col-xs-12 col-sm-12 col-md-6 col-centered center-block">
                <div class="row">
                    <div class="title">
                        <h6 id="mce_111">Instalaciones</h6>
                        <h3 id="mce_112">Alojamientos y espacios</h3>
                        <div class="col-xs-6 col-sm-4 col-md-4 cap col-centered center-block">
                            <span id="mce_113"><img alt="" src="[base_url]img/cap.png" class="img-responsive"></span>
                        </div>
                        <p id="mce_114">El Forn y El Celler de Cal Prat, así se denominan nuestros alojamientos por la particularidad que caracteriza a cada uno, tienen una capacidad de hasta 8 y 5 personas respectivamente. También se puede alquilar la casa entera, puesto que los dos alojamientos se pueden comunicar.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-room">
            <ul>
                <li class="col-xs-12 col-sm-6 col-md-3">
                    <div class="mr-inner">
                        <img src="[base_url]img/Homepage_Resort_INDEX/room-img1.jpg" class="img-responsive" alt="">
                        <div class="mr-overlay">
                            <h2 id="mce_115">Tu casa<br>13 personas</h2>
                            <p id="mce_116">Podéis disfrutar de total privacidad alquilando la casa entera. <br> Un espacio amplio para familias y grupos de amigos.</p>
                            <a class="room-detail mce-content-body" href="[base_url]habitacion/1-tota-la-casa" id="mce_117" >Más información<i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </li>
                <li class="col-xs-12 col-sm-6 col-md-3">
                    <div class="mr-inner">
                        <img src="[base_url]img/Homepage_Resort_INDEX/room-img2.jpg" class="img-responsive" alt="">
                        <div class="mr-overlay">
                            <h2 id="mce_118">El forn<br>8 personas</h2>
                            <p id="mce_119">El Forn con capacidad para 8 personas, ocupa la cara norte de la casa.<br><br>
                                </p>
                            <a class="room-detail mce-content-body" href="[base_url]habitacion/2-el-form" id="mce_120" >Más información<i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </li>
                <li class="col-xs-12 col-sm-6 col-md-3">
                    <div class="mr-inner">
                        <img src="[base_url]img/Homepage_Resort_INDEX/room-img3.jpg" class="img-responsive" alt="">
                        <div class="mr-overlay">
                            <h2 id="mce_121">El celler<br>5 personas</h2>
                            <p id="mce_122">El Celler, con capacidad para 5 personas, ocupa la cara sud de la casa.<br><br>
                                </p>
                            <a class="room-detail mce-content-body" href="[base_url]habitacion/3-el-celler" id="mce_123" >Más información<i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </li>
                <li class="col-xs-12 col-sm-6 col-md-3">
                    <div class="mr-inner">
                        <img src="[base_url]img/Homepage_Resort_INDEX/room-img4.jpg" class="img-responsive" alt="">
                        <div class="mr-overlay">
                            <h2 id="mce_124">La piscina<br>privada</h2>
                            <p id="mce_125">Durante vuestra estancia, queremos que podáis encontrara todo lo que buscáis: silencio, tranquilidad, relax…<br><br>
                                </p>
                            <a class="room-detail mce-content-body" href="[base_url]p/la_piscina.html" id="mce_126" >Más información<i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <input name="mce_127" type="hidden"><input name="mce_128" type="hidden"></div>
        <!-- ROOM OFFER -->
        <div class="room-offer" style="position: relative;">
            <div class="container">
                <h3 id="mce_130">TODAS LAS ESTANCIAS OFRECEN</h3><input name="mce_130" type="hidden">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1">
                        <ul class="specific">
                            <li>Calefacción</li>
                            <li>Chimenea (leña incluïda)</li>
                            <li>Televisión</li>
                            <li>Wifi</li>
                            <li>Sábanas y ropa de cama</li>
                            <li>Toallas y toallas para la piscina</li>
                            <li>Microondas/ Nevera</li>
                            <li>Lavaplatos / Lavadora</li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1">
                        <ul class="specific">
                            <li>Cafetera Nespresso / Italiana</li>
                            <li>Cuna / Trona / Barandilla cama 90 cm</li>
                            <li>Botiquín</li>
                            <li>Juegos de mesa</li>
                            <li>DVD / Reproductor CD</li>
                            <li>Material para la limpieza de la cocina</li>
                            <li>Utensilios y material para la mesa</li>
                        </ul>
                    </div>
                </div>
            </div>
            <input name="mce_131" type="hidden"><input name="mce_132" type="hidden"><input name="mce_133" type="hidden"></div>
            <div class="home-gal" style="position: relative;">
                <div class="dining-block dual-slider row">
                    <div class="col-md-7 padding-right-none">
                        <div id="gal-slider1" class="flexslider">
                            <ul class="slides">
                                <li><img class="img-responsive" src="[base_url]img/Homepage_Resort_INDEX/home-gal-img1.png" alt=""></li>
                            </ul>
                            <!--<a class="flex-prev1 mce-content-body" id="mce_134" ><i class="fa fa-chevron-left"></i></a><input name="mce_134" type="hidden">
                            <a class="flex-next1 mce-content-body" id="mce_135" ><i class="fa fa-chevron-right"></i></a><input name="mce_135" type="hidden">-->
                        </div>
                    </div>
                    <div class="col-md-5 padding-left-none">
                        <ul class="gal-nav1">
                            <li class="flex-active">
                                <div>
                                    <h2 id="mce_136">Disfruta con los tuyos</h2><input name="mce_136" type="hidden">
                                    <p id="mce_137">Cal Prat se caracteriza por la tranquilidad de su entorno, rodeada de naturaleza, ideal para disfrutar de una escapada con la familia o amigos.</p><input name="mce_137" type="hidden">
                                    <p id="mce_138">Sin embargo, hay muchos lugares interesantes para visitar en toda la zona, y que seguro no os dejarán indiferentes.</p><input name="mce_138" type="hidden">
                                    <a href="[base_url]p/activitat.html" class="btn btn-default mce-content-body" id="mce_139" >Detalles</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- SPA -->
                <div class="spa-block dual-slider row">
                    <div class="hidden-xs col-md-5 padding-right-none">
                        <ul class="gal-nav2">
                            <li class="flex-active">
                                <div>
                                    <h2 id="mce_152">Mimaros en la piscina</h2><input name="mce_152" type="hidden">
                                    <p id="mce_153"></p><input name="mce_153" type="hidden">
                                    <p id="mce_154">En Cal Prat estamos especializados en un Turismo Rural familiar y para parejas, y nuestra principal preocupación es la de ofrecer a nuestros clientes descanso y relajación lejos del ritmo y ruido de la ciudad. En un entorno natural muy tranquilo se encuentra nuestra Casa Rural con Spa de uso exclusivo, uno de los tesoros de Cal Prat.</p><input name="mce_154" type="hidden">
                                    <a href="[base_url]p/la_piscina.html" class="btn btn-default mce-content-body" id="mce_155" >Detalles</a><input name="mce_155" type="hidden">
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-7 padding-left-none">
                        <div id="gal-slider2" class="flexslider">
                            <ul class="slides">
                                <li><img class="img-responsive" src="[base_url]img/Homepage_Resort_INDEX/home-gal-img2.jpg" alt=""></li>
                            </ul>
                            <!--<a class="flex-prev2 mce-content-body" ><i class="fa fa-chevron-left"></i></a><input name="mce_168" type="hidden">
                            <a class="flex-next2 mce-content-body" ><i class="fa fa-chevron-right"></i></a><input name="mce_169" type="hidden">-->
                        </div>
                    </div>
                    <div class="visible-xs col-md-5 padding-right-none">
                        <ul class="gal-nav2">
                            <li class="flex-active">
                                <div>
                                    <h2 id="mce_152">Mimaros en la piscina</h2><input name="mce_152" type="hidden">
                                    <p id="mce_153"></p><input name="mce_153" type="hidden">
                                    <p id="mce_154">En Cal Prat estamos especializados en un Turismo Rural familiar y para parejas, y nuestra principal preocupación es la de ofrecer a nuestros clientes descanso y relajación lejos del ritmo y ruido de la ciudad. En un entorno natural muy tranquilo se encuentra nuestra Casa Rural con Spa de uso exclusivo, uno de los tesoros de Cal Prat.</p><input name="mce_154" type="hidden">
                                    <a href="[base_url]p/la_piscina.html" class="btn btn-default mce-content-body" id="mce_155" >Detalles</a><input name="mce_155" type="hidden">
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- 
<div class="entertain-block dual-slider row">
                    <div class="col-md-7 padding-right-none">
                        <div id="gal-slider3" class="flexslider">
                            <ul class="slides">
                                <li><img class="img-responsive" src="[base_url]img/Homepage_Resort_INDEX/home-gal-img3.png" alt=""></li>
                            </ul>
                            <!~~<a class="flex-prev3 mce-content-body" id="mce_170" ><i class="fa fa-chevron-left"></i></a><input name="mce_170" type="hidden">
                            <a class="flex-next3 mce-content-body" id="mce_171" ><i class="fa fa-chevron-right"></i></a><input name="mce_171" type="hidden">~~>
                        </div>
                    </div>
                    <div class="col-md-5 padding-left-none">
                        <ul class="gal-nav3">
                            <li class="flex-active">
                                <div>
                                    <h2 id="mce_172">Decubre el entorno</h2><input name="mce_172" type="hidden">
                                    <p id="mce_173">En Cal Prat podéis venir a descansar, pasear, hacer un tumbo en bicicleta por los alrededores, o bien bañaros en la piscina y desconectar.</p><input name="mce_173" type="hidden">
                                    <p id="mce_174">Visitas guidades, excursiones, paseos a caballo, rutas en BTT, rutas culturales, quieres en globos,...</p><input name="mce_174" type="hidden">
                                    <a href="[base_url]p/la_piscina.html" class="btn btn-default mce-content-body" id="mce_175" >Detalles</a><input name="mce_175" type="hidden">
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
 -->
                <input name="mce_188" type="hidden"><input name="mce_189" type="hidden"><input name="mce_190" type="hidden"></div>
                <div class="activ-eve" style="position: relative;">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <h3 id="mce_191">Actividades</h3><input name="mce_191" type="hidden">
                                <p id="mce_192">En nuestro entorno hay mucho que hacer. Pídenos información y estaremos encantados de recomendarte actividades y rutas para hacer. Disponemos de datos de contacto y descuentos.</p><input name="mce_192" type="hidden">
                                <ul class="specific">
                                    <li class="col-xs-12 col-sm-6 col-md-6">Visitas guiadas</li>
                                    <li class="col-xs-12 col-sm-6 col-md-6">Observaciones astronómicas</li>
                                    <li class="col-xs-12 col-sm-6 col-md-6">Excursiones con bicicleta</li>
                                    <li class="col-xs-12 col-sm-6 col-md-6">Actividades de aventura</li>
                                    <li class="col-xs-12 col-sm-6 col-md-6">Paseos en caballo</li>
                                    <li class="col-xs-12 col-sm-6 col-md-6">Geocaching</li>
                                </ul>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <h3 id="mce_193">Eventos</h3><input name="mce_193" type="hidden">
                                <p id="mce_194">En nuestro entorno hay mucho que hacer. Pídenos información y estaremos encantados de recomendarte actividades y rutas para hacer. Disponemos de datos de contacto y descuentos.</p><input name="mce_194" type="hidden">
                                <ul class="specific">
                                    <li class="col-xs-12 col-sm-6 col-md-6">Festivales de música</li>
                                    <li class="col-xs-12 col-sm-6 col-md-6">Ferias y mercados</li>
                                    <li class="col-xs-12 col-sm-6 col-md-6">Ruta de castillos</li>
                                    <li class="col-xs-12 col-sm-6 col-md-6">Caminatas populares</li>
                                    <li class="col-xs-12 col-sm-6 col-md-6">Pastorets de Calaf</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="learn-more">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="row">
                                <img class="img-responsive" src="[base_url]img/Homepage_Resort_INDEX/learn-img1.jpg" alt="">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="row">
                                <img class="img-responsive" src="[base_url]img/Homepage_Resort_INDEX/learn-img2.jpg" alt="">
                            </div>
                        </div>
                        <a href="[base_url]agenda" class="btn btn-default mce-content-body" id="mce_195" spellcheck="false">Leer más</a>
                    </div>
                </div>
                <!-- SERVICES -->
                <div class="activ-eve" style="position: relative;">
                    <div class="container">
                        <div class="col-xs-12 col-sm-12 col-md-6 col-centered center-block">
                            <div class="title" style="padding-bottom: 0px">
                                <h6 id="mce_199">Certificación</h6><input name="mce_199" type="hidden">
                                <h3 id="mce_200">Alojamiento con certificación Biosphere</h3><input name="mce_200" type="hidden">
                                <div class="col-xs-6 col-sm-4 col-md-4 cap col-centered center-block">
                                    <span id="mce_201"><img class="img-responsive" src="[base_url]img/cap.png" alt=""></span><input name="mce_201" type="hidden">
                                </div>
                                <p id="mce_202">Cal Prat es un alojamiento que ha obtenido la certificación Biosphere Responsible Tourism. De este modo apostamos por un turismo responsable y sostenible, comprometido con el medio ambiente. </p><input name="mce_202" type="hidden">
                            </div>                            
                        </div>
                        
                        <div class="col-xs-12 col-sm-12 col-md-12" style="padding-bottom: 30px">
                            <p style="text-align:center; margin-top:41px"><img src="[base_url]img/bio.jpg"></p>
                            <p id="mce_192" style="text-align:center">
                                La certificación la otorga el Instituto de Turismo Responsable, un organismo vinculado a la UNESCO y socio de la Organización Mundial del Turismo y del Global Sustainable Tourism Council. Se trata de una certificación pionera, basada en los 17 objetivos de Desarrollo Sostenible de Naciones Unidas integrados en la Agenda 2030.<br>
                                El sello acredita que las tres marcas turísticas de la demarcación –“Costa Barcelona”, “Paisajes Barcelona” y “Pirineo Barcelona”, impulsadas por la Diputación de Barcelona- cumplen los requisitos del estándar “Biosphere Destination” y que aplican con éxito 6 criterios concretos en sus políticas turísticas: Gestión sostenible, Desarrollo económico y social, Conservación y mejora del patrimonio cultural, Conservación Ambiental, Calidad y seguridad, e Implicación del visitante.
                            </p>
                        </div>
                            
                    </div>
                </div>
            </div>
                        <div class="home-img-panel parallax-bg" data-stellar-ratio="2" data-stellar-background-ratio="0.5" style="position: relative;">
                            <div class="panel-img-detail">
                                <h2 id="mce_207">Lo hacemos fácil para planificar vuestra estancia</h2><input name="mce_207" type="hidden">
                                <div class="col-xs-7 col-sm-7 col-md-7 cap col-centered center-block">
                                    <span id="mce_208"><img alt="" src="[base_url]img/cap.png" class="img-responsive"></span><input name="mce_208" type="hidden">
                                </div>
                                <p id="mce_209">Estás pensando en hacer una sorpresa a tu pareja, o realizar aquel encuentro familiar de que tanto habláis, o bien celebrar el aniversario de los niños o la boda de los padres, o aquella reunión de trabajo que quieres que sea más distendida...? Sea cual sea llámanos y te ayudaremos a planificarlo bien !!
                                    </p><input name="mce_209" type="hidden">
                                    <a href="[base_url]iniciar-reserva" class="btn btn-default mce-content-body" id="mce_210" >detalles</a><input name="mce_210" type="hidden">
                                </div>
                                <input name="mce_211" type="hidden"><input name="mce_212" type="hidden"><input name="mce_213" type="hidden"></div>
                                <div class="main-gallery" style="position: relative;">
                                    <div class="container">
                                        <div class="col-xs-12 col-sm-12 col-md-6 col-centered center-block">
                                            <div class="row">
                                                <div class="title">
                                                    <h3 class="light-title mce-content-body" id="mce_214" >Galería</h3><input name="mce_214" type="hidden">
                                                    <div class="col-xs-6 col-sm-4 col-md-4 cap col-centered center-block">
                                                        <span id="mce_215"><img alt="" src="[base_url]img/cap.png" class="img-responsive"></span><input name="mce_215" type="hidden">
                                                    </div>
                                                    <p id="mce_216">Conoce un poco nuestra casa a través de la galería de imágenes. Cada rincón es especial y diferente.</p><input name="mce_216" type="hidden">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="main-gal-slider">
                                        <div id="home-gallery" class="owl-carousel owl-theme">
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img3.jpg" alt="Owl Image">
                                                <!--<div class="figcaption">
                                                    <h3>Sala d'estar Forn</h3>
                                                    <div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
                                                        <span><img class="img-responsive" src="<?= base_url() ?>img/cap.png" alt=""></span>
                                                    </div>
                                                    <ul>
                                                        <li>
                                                            <span class="des">beds</span>
                                                            <span class="detail">King or 2 Queen</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">occupancy</span>
                                                            <span class="detail">2 People</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">size</span>
                                                            <span class="detail">60 sqm / 650 sqf</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">view</span>
                                                            <span class="detail">Ocean / City</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">rates from</span>
                                                            <span class="detail">215 USD</span>
                                                        </li>
                                                    </ul>
                                                    <div>
                                                        <a href="./roomDetail-gallery.html" class="btn btn-default pull-left">Informació sala</a>
                                                        <a href="./reservation.html" class="btn btn-default pull-right">Reservar</a>
                                                    </div>
                                                </div>-->
                                            </div>
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img2.jpg" alt="Owl Image">
                                                <!--<div class="figcaption">
                                                    <h3>Dormitori Forn 1</h3>
                                                    <div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
                                                        <span><img class="img-responsive" src="<?= base_url() ?>img/cap.png" alt=""></span>
                                                    </div>
                                                    <ul>
                                                        <li>
                                                            <span class="des">Llit doble de matrimoni</span>
                                                            <span class="detail">King or 2 Queen</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">occupancy</span>
                                                            <span class="detail">2 People</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">size</span>
                                                            <span class="detail">60 sqm / 650 sqf</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">view</span>
                                                            <span class="detail">Ocean / City</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">rates from</span>
                                                            <span class="detail">215 USD</span>
                                                        </li>
                                                    </ul>
                                                    <div>
                                                        <a href="./roomDetail-gallery.html" class="btn btn-default pull-left">room details</a>
                                                        <a href="./reservation.html" class="btn btn-default pull-right">book now</a>
                                                    </div>
                                                </div>-->
                                            </div>
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img4.jpg" alt="Owl Image">
                                                <!--<div class="figcaption">
                                                    <h3>Cuina</h3>
                                                    <div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
                                                        <span><img class="img-responsive" src="<?= base_url() ?>img/cap.png" alt=""></span>
                                                    </div>
                                                    <ul>
                                                        <li>
                                                            <span class="des">beds</span>
                                                            <span class="detail">King or 2 Queen</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">occupancy</span>
                                                            <span class="detail">2 People</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">size</span>
                                                            <span class="detail">60 sqm / 650 sqf</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">view</span>
                                                            <span class="detail">Ocean / City</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">rates from</span>
                                                            <span class="detail">215 USD</span>
                                                        </li>
                                                    </ul>
                                                    <div>
                                                        <a href="./roomDetail-gallery.html" class="btn btn-default pull-left">room details</a>
                                                        <a href="./reservation.html" class="btn btn-default pull-right">book now</a>
                                                    </div>
                                                </div>-->
                                            </div>
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img5.jpg" alt="Owl Image">
                                                <!--<div class="figcaption">
                                                    <h3>ocean view balcony room</h3>
                                                    <div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
                                                        <span><img class="img-responsive" src="<?= base_url() ?>img/cap.png" alt=""></span>
                                                    </div>
                                                    <ul>
                                                        <li>
                                                            <span class="des">beds</span>
                                                            <span class="detail">King or 2 Queen</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">occupancy</span>
                                                            <span class="detail">2 People</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">size</span>
                                                            <span class="detail">60 sqm / 650 sqf</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">view</span>
                                                            <span class="detail">Ocean / City</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">rates from</span>
                                                            <span class="detail">215 USD</span>
                                                        </li>
                                                    </ul>
                                                    <div>
                                                        <a href="./roomDetail-gallery.html" class="btn btn-default pull-left">room details</a>
                                                        <a href="./reservation.html" class="btn btn-default pull-right">book now</a>
                                                    </div>
                                                </div>-->
                                            </div>
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img6.jpg" alt="Owl Image">
                                                <!--<div class="figcaption">
                                                    <h3>ocean view balcony room</h3>
                                                    <div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
                                                        <span><img class="img-responsive" src="<?= base_url() ?>img/cap.png" alt=""></span>
                                                    </div>
                                                    <ul>
                                                        <li>
                                                            <span class="des">beds</span>
                                                            <span class="detail">King or 2 Queen</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">occupancy</span>
                                                            <span class="detail">2 People</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">size</span>
                                                            <span class="detail">60 sqm / 650 sqf</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">view</span>
                                                            <span class="detail">Ocean / City</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">rates from</span>
                                                            <span class="detail">215 USD</span>
                                                        </li>
                                                    </ul>
                                                    <div>
                                                        <a href="./roomDetail-gallery.html" class="btn btn-default pull-left">room details</a>
                                                        <a href="./reservation.html" class="btn btn-default pull-right">book now</a>
                                                    </div>
                                                </div>-->
                                            </div>
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img14.jpg" alt="Owl Image">
                                            </div>   
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img8.jpg" alt="Owl Image">
                                            </div>   
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img9.jpg" alt="Owl Image">
                                            </div>   
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img10.jpg" alt="Owl Image">
                                            </div>   
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img11.jpg" alt="Owl Image">
                                            </div>   
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img12.jpg" alt="Owl Image">
                                            </div>   
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img13.jpg" alt="Owl Image">
                                            </div> 
                                        </div>
                                        <!--<div class="customNavigation">
                                            <a class="prev"><i class="fa fa-angle-left"></i></a>
                                            <a class="next"><i class="fa fa-angle-right"></i></a>
                                        </div>-->
                                    </div>
                                    <div class="container">
                                        <a href="<?= base_url('p/galeria') ?>" class="btn btn-default mce-content-body" id="mce_289" >ir a la galería</a><input name="mce_289" type="hidden">
                                    </div>
                                </div>
                            </div>
                            <!-- TESTIMONIAL -->
                            <div class="testimonial" style="position: relative;">
                                <div class="container">
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-centered center-block">
                                        <div class="row">
                                            <div class="title">
                                                <h6 id="mce_293">testimonios</h6><input name="mce_293" type="hidden">
                                                <h3 id="mce_294">Qué dicen nuestros clientes</h3><input name="mce_294" type="hidden">
                                                <div class="col-xs-6 col-sm-4 col-md-4 cap col-centered center-block">
                                                    <span id="mce_295"><img class="img-responsive" src="[base_url]img/cap.png" alt=""></span><input name="mce_295" type="hidden">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6 col-md-6">
                                            <div class="testi-block">
                                                <div class="row">
                                                    <!--<div class="hidden-xs col-xs-12 col-sm-2 col-md-2">
                                                        <img src="[base_url]img/Homepage_Resort_INDEX/testimonial-img5.png" alt="">
                                                    </div>-->
                                                    <div class="col-xs-12 col-sm-offset-2 col-sm-10 col-md-10">
                                                        <div class="testi-detail">
                                                            <p id="mce_296">"Buscábamos una casa tranquila donde pasar unos días en familia y nos encantó Cal Prat. Los niños podían jugar y disfrutar del entorno y el trato por parte de la propietaria fue muy amable y atento"</p><input name="mce_296" type="hidden">
                                                            <h6 id="mce_297">– J. Magaña</h6><input name="mce_297" type="hidden">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6">
                                            <div class="testi-block">
                                                <div class="row">
                                                    <!--<div class="hidden-xs col-xs-12 col-sm-2 col-md-2">
                                                        <img src="[base_url]img/Homepage_Resort_INDEX/testimonial-img6.png" alt="">
                                                    </div>-->
                                                    <div class="col-xs-12 col-sm-offset-2 col-sm-10 col-md-10">
                                                        <div class="testi-detail">
                                                            <p id="mce_298">"Alquilamos con la pandilla de amigos Cal Prat y fue genial! Pudimos disfrutar del frío en una casa rústica y llena de detalles. Seguro que repetimos!</p><input name="mce_298" type="hidden">
                                                            <h6 id="mce_299">– B. Tusell</h6><input name="mce_299" type="hidden">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="prom-offers">
                                    <div class="dark-gold"></div>
                                    <div class="light-gold"></div>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-5 col-md-5">
                                                <div class="prom-block">
                                                    <h3 id="mce_300">Promociones</h3><input name="mce_300" type="hidden">
                                                    <p id="mce_301">Consultad nuestras promociones puntuales para días concretos y fines de semana concretos durante el año.</p><input name="mce_301" type="hidden">
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-2 col-md-2">
                                                <img class="img-responsive" src="[base_url]img/Homepage_Resort_INDEX/testimonial-img2.png" alt="">
                                            </div>
                                            <div class="col-xs-12 col-sm-5 col-md-5">
                                                <div class="offer-block">
                                                    <h3 id="mce_302">Ofertas Especiales</h3><input name="mce_302" type="hidden">
                                                    <p id="mce_303">Sois novios, sois grupos numerosos...contactáis con Hace falta Prat y podemos hacer precios especiales. Estamos siempre dispuestos a ofrecerte el mejor trato que os merecéis en momentos especiales.</p><input name="mce_303" type="hidden">
                                                </div>
                                            </div>
                                        </div>
                                        <a href="[base_url]ofertas" class="btn btn-default mce-content-body" id="mce_304" >Ver todas las ofertas</a><input name="mce_304" type="hidden">
                                    </div>
                                </div>
                                <input name="mce_305" type="hidden"><input name="mce_306" type="hidden"><input name="mce_307" type="hidden"></div>
                                <!-- LOCATION -->
                                <div class="location" style="position: relative;">
                                    <div class="container">
                                        <div class="col-xs-12 col-sm-12 col-md-6 col-centered center-block">
                                            <div class="row">
                                                <div class="title">
                                                    <h6 id="mce_308">LOCALIZACIÓN</h6><input name="mce_308" type="hidden">
                                                    <h3 id="mce_309">DÓNDE ENCONTRARNOS</h3><input name="mce_309" type="hidden">
                                                    <div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
                                                        <span id="mce_310"><img class="img-responsive" src="[base_url]img/cap.png" alt=""></span><input name="mce_310" type="hidden">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-8">
                                                <div class="map">
                                                    <div id="map">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-4">
                                                <ul>
                                                    <li>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4 col-md-6">
                                                                <span class="specific mce-content-body" id="mce_311" >DIRECCIÓN</span><input name="mce_311" type="hidden">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8 col-md-6">
                                                                <p id="mce_312">El Soler, s/n Calonge de Segarra, Barcelona</p><input name="mce_312" type="hidden">
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4 col-md-6">
                                                                <span class="specific mce-content-body" id="mce_313" >teléfono</span><input name="mce_313" type="hidden">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8 col-md-6">
                                                                <p id="mce_314">+34 650 93 64 00</p><input name="mce_314" type="hidden">
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4 col-md-6">
                                                                <span class="specific mce-content-body" id="mce_315" >email</span><input name="mce_315" type="hidden">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8 col-md-6">
                                                                <a href="mailto:info@fontainbleu.com" id="mce_316"> <input name="mce_316" type="hidden"><a href="mailto:info@calprat.net">info@calprat.net</a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4 col-md-6">
                                                                <span class="specific mce-content-body" id="mce_317" >distancias</span><input name="mce_317" type="hidden">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8 col-md-6">
                                                                <p id="mce_318">80km de barcelona <br> 80km de LLeida <br> 100km de Tarragona <br> 150km de Girona</p><input name="mce_318" type="hidden">
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4 col-md-6">
                                                                <span class="specific mce-content-body" id="mce_319" >Cerca de</span><input name="mce_319" type="hidden">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8 col-md-6">
                                                                <p id="mce_320">20 Km. de Igualada<br> 20 km. de Cervera<br> 30 km. de Manresa <br>40 km. de Tàrrega</p><input name="mce_320" type="hidden">
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <input name="mce_321" type="hidden"><input name="mce_322" type="hidden"><input name="mce_323" type="hidden"></div><div style="text-align:center;"></div>
                                </section>