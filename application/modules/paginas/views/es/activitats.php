<!-- WELCOME -->
<section id="content" class="activitie-main"><!-- PAGE HEADER --><div class="title-color">
<h2>Actividades & Excursiones</h2>
<ol class="breadcrumb">
	<li><a href="<?= base_url() ?>">Inicio</a></li>
	<li><a href="<?= base_url() ?>">Agenda</a></li>
	<li><a href="<?= base_url() ?>">Agenda</a></li>
	<!--				<li class="active"><a>Activities &amp; Excursions</a></li>-->
</ol>
</div>

<!-- ACTIVITIES CONTENT -->
<div class="activitie-block">
<div class="container">
	<div class="col-xs-12 col-sm-12 col-md-8 col-centered center-block">
		<div class="row">
			<div class="title">
				<h6>no sabes qué hacer?</h6>
				<h1>Agenda</h1>
				<div class="col-xs-6 col-sm-4 col-md-4 cap col-centered center-block">
					<span><img class="img-responsive" src="[base_url]img/cap.png" alt=""></span>
				</div>
				<p>Aquí tienes un pequeño ejemplo de las muchas actividades que podrás hacer en Cal Prat y su entorno. Hay mil sitios, mil excursiones que te encantarán!</p>
			</div>
		</div>
	</div>
	[foreach:eventos]
	<div class="activities">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-6 no-padding">
				<a href="<?= base_url() ?>">
					<div class="img">
						<img class="img-responsive" src="[foto_grande]" alt="" style="min-height:508px;"><span class="btn btn-default">detalles</span>
					</div>
				</a>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6 no-padding">
				<div class="right-activ">
					<h2>[titulo]</h2>
					
					<p>[descripcion]</p>
					<div class="include">
						<h4>Qué verás</h4>
						[tags]
					</div>
					<div class="activ-botpan">
						<div class="active-left">
							<ul>
								<li><a href="<?= base_url() ?>"><i class="fa fa-cutlery"></i></a></li>
								<li><a href="<?= base_url() ?>"><i class="fa fa-glass"></i></a></li>
								<li><a href="<?= base_url() ?>"><i class="fa fa-wheelchair"></i></a></li>
							</ul>
						</div>
						<div class="active-right">
							<img src="[base_url]img/activities/6.png" alt="">
						</div>
					</div>
				</div>
				<div class="visitor">
					<span class="calender"><i class="fa fa-calendar-o"></i>[fecha]</span>
					<span class="comment"><i class="fa fa-clock-o"></i>[hora]</span>
					<span class="views"><i class="fa fa-bookmark"></i>[precio]</span>
				</div>
			</div>
		</div>
	</div>
	[/foreach]
	
	
	
	<div class="activitie-cal">
		<div class="col-xs-12 col-sm-12 col-md-8 col-centered center-block">
			<div class="row">
				<div class="title">
					<h6>calendario de actividades</h6>
					<h1>Próximamente...</h1>
					<div class="col-xs-6 col-sm-4 col-md-4 cap col-centered center-block">
						<span><img alt="" src="[base_url]img/cap.png" class="img-responsive"></span>
					</div>
					<p>Encuentra aquí los acontecimientos que se celebrarán en los próximos días en las cercanías y proximidades de Cal Prat</p>
				</div>
			</div>
		</div>
		
	</div>

	<div class="gallery-block">
            <div class="main-room">
                <ul>
                    <li class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                        <div class="img">
                            <img src="<?= base_url() ?>theme/images/Gallery/1.jpg" class="img-responsive" alt="">
                            <div class="mr-head">
                                <h3>Calendario<br>de actividades</h3>
                                <div class="col-md-6 cap col-centered center-block">
                                    <span><img class="img-responsive" src="<?= base_url() ?>theme/images/cap.png" alt=""></span>
                                </div>
                                <p>Disfrútalas, aprovechalas....</p>
                                <p></p>
                            </div>
                        </div>
                    </li>
                    [foreach:activitats]
                        <li class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <div class="mr-inner">
                                <img src="[foto]" class="img-responsive" alt="" style="min-height:327px;">
                                <div class="mr-overlay">
                                    <h2>[titulo]</h2>
                                    <p>[fecha]</p>
                                    <p>[descripcion_corta]</p>                                    
                                </div>
                            </div>
                        </li>
                    [/foreach]
                </ul>
            </div>
        </div>
</div>
</div>
</section>