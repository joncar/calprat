
    <section id="content" class="spa-main"><!-- PAGE HEADER --><div class="title-color" style="position: relative;">
            <h2 id="mce_40" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">La piscina</h2><input type="hidden" name="mce_40">
            <ol class="breadcrumb">
                <li><a href="./index.html" id="mce_41" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Inici</a><input type="hidden" name="mce_41"></li>
                <li class="active">La piscina</li>
            </ol>
            <input type="hidden" name="mce_43"><input type="hidden" name="mce_44"><input type="hidden" name="mce_45"></div>

        <!-- SPA -->
        <div class="spa-block" style="position: relative;">
            <div class="db-image">
                <div class="dbi-inner" style="height:100%; background:url(<?= base_url() ?>img/spa/1.jpg) no-repeat center center">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="spa-right">
                    <h1 id="mce_46" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">La mejor forma de mimarse</h1><input type="hidden" name="mce_46">
                    <h6 id="mce_47" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Disfruta de una experiencia spa en Cal Prat.</h6><input type="hidden" name="mce_47">
                    <p id="mce_48" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Durante vuestra estancia, queremos que podáis encontrar aquello que buscáis: silencio, tranquilidad, relax…</p><input type="hidden" name="mce_48">
                    <p id="mce_49" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Por eso podréis disfrutar de una piscina climatizada con solarium, donde lógicamente os podréis bañar, tomar el sol, leer un libro…Esta dispone de un sistema de natación contracorriente, saltante de agua relajante, solarium y calefacción por tierra radiante.

Utilizamos un sistema de depuración por electrólisis salina. De este modo no sólo somos respetuosos con el medio ambiente sino que también queremos respetar vuestra piel.</p><input type="hidden" name="mce_49">
                    <h3 id="mce_50" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;"></h3><input type="hidden" name="mce_50">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <ul class="specific">
                                <li>Zona solarium para tomar el sol</li>
                                <li>Tumbonas</li>
                                
                            </ul>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <ul class="specific">
                                <li>Sofás en el interior de la piscina</li>
                                <li>Toallas de piscina</li>
                                                            </ul>


                        </div>
                    </div>
                    <h3 id="mce_50" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;"></h3>
                    <div class="row" uk-lightbox>
                        [foreach:galeria_piscina]
                            <div class="col-xs-12 col-sm-3" style="margin-top: 29px;"><a href="[foto]"><img src="[foto]" alt="" style="width:100%"></a></div>
                        [/foreach]
                    </div>
                </div>
            </div>
        </div>

        <div class="spa-img-bot" style="position: relative;">
            <div class="spa-natural">
                <h2 id="mce_55" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Utilizamos solamente los mejores productos naturales</h2><input type="hidden" name="mce_55">
                <div class="col-xs-12 col-sm-6 col-md-6 cap col-centered center-block">
                    <span id="mce_56" class="mce-content-body" contenteditable="true" spellcheck="false"><img class="img-responsive" src="[base_url]img/cap.png" alt=""></span><input type="hidden" name="mce_56">
                </div>
                <p id="mce_57" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Utilizamos un sistema de depuración para electrólisis salina. De este modo no sólo somos respetuosos con el medio ambiente sino que también queremos respetar vuestra piel.</p><input type="hidden" name="mce_57">
                <p id="mce_58" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;"></p><input type="hidden" name="mce_58">
            </div>
        </div>
        <div style="text-align:center;"></div>
    </section>