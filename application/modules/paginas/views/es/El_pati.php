
    <section id="content" class="spa-main">
        <!-- PAGE HEADER -->
        <div class="title-color" style="position: relative;">
            <h2 id="mce_40" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">El pati, BBQ</h2>
            <input type="hidden" name="mce_40">
            <ol class="breadcrumb">
                <li><a href="./index.html" id="mce_41" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Inici</a><input type="hidden" name="mce_41"></li>
                
                <li class="active">El pati</li>
            </ol>
            <input type="hidden" name="mce_43"><input type="hidden" name="mce_44"><input type="hidden" name="mce_45">
        </div>
        <!-- SPA -->
        <div class="spa-block" style="position: relative;">
            <div class="db-image">
                <div class="dbi-inner" style="height:100%; background:url(<?= base_url() ?>img/spa/9.jpg) no-repeat center center">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="spa-right">
                    <h1 id="mce_46" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Un espacio perfecto para tus encuentros</h1>
                    <input type="hidden" name="mce_46">
                    <h6 id="mce_47" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">La casa dispone de un amplio patio donde podréis tomar el sol, comer, descansar....</h6>
                    <input type="hidden" name="mce_47">
                    <p id="mce_48" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">
                        Encontraréis en este espacio un sitio donde reuniros para comer gracias a una barbacoa (con todo el material necesario para utilizarla), una pérgola para amenizar el sol, una mesa y sillas, tumbonas, ....
                    </p>
                    <input type="hidden" name="mce_48">
                    <p id="mce_49" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">
                        
                    </p>
                    <input type="hidden" name="mce_49">
                    <h3 id="mce_50" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Qué tenemos en este espacio</h3>
                    <input type="hidden" name="mce_50">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <ul class="specific">
                                <li>Barbacoa con carbón, encendedor, pinzas, trapos de cocina, recogedor de cenizas....</li>
                                <li>Mesa, sillas y bancos</li>
                                <li>Pérgola per taparse del sol</li>
                                
                            </ul>
                        </div>
                        <!-- 
<div class="col-xs-12 col-sm-6 col-md-6">
                            <ul class="specific">
                                <li>Zona ajardinada</li>
                                <li>fanals per la nit</li>
                                <li>Nevera aprop</li>
                                <li>Lavabo</li>
                                <li>Accés directe a la sala de jocs</li>
                            </ul>
                        </div>
 -->
                    </div>
                    <h3 id="mce_50" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Imágenes del Patio</h3>
                    <div class="row" uk-lightbox>
                        [foreach:galeria_patio]
                            <div class="col-xs-12 col-sm-3" style="margin-top: 29px;"><a href="[foto]"><img src="[foto]" alt="" style="width:100%"></a></div>
                        [/foreach]
                    </div>
                </div>
            </div>
    </section>