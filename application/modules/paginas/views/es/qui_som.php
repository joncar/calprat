<!-- WELCOME -->
<section id="content">
            <section id="content" class="about-main"><!-- PAGE HEADER --><div class="title-color">
			<h2>Sobre Cal Prat</h2>
			<ol class="breadcrumb">
<li><a href="<?= base_url() ?>">Inici</a></li>
				<li class="active"><a href="<?= base_url() ?>p/qui_som.html">Quién somos?</a></li>
			</ol>
</div>
		
		<!-- ABOUT -->
		<div class="about-content">
			<div class="db-image quisom">
				<div class="dbi-inner" style="background:url([base_url]img/About/1.jpg) no-repeat center center">
					<img src="[base_url]img/About/1.jpg" style="width:100%" class="visible-xs">
				</div>
				<div class="dbi-inner" style="background:url([base_url]img/About/2.jpg) no-repeat center center">
					<img src="[base_url]img/About/2.jpg" style="width:100%" class="visible-xs">
				</div>
				<div class="dbi-inner" style="background:url([base_url]img/About/3.jpg) no-repeat center center">
					<img src="[base_url]img/About/3.jpg" style="width:100%" class="visible-xs">
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6">
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6">
				<div class="row">
					<div class="right-about">
						<h1>Cal Prat, casa rural</h1>
						<h6>Turismo rural, relax y desconexión</h6>
						<p>Cal Prat es una antigua casa de payés del siglo XVIII situada en el pequeño municipio de El Soler, dentro del término municipal de Calonge de Segarra en la Alta Anoia, justo a tocar de las comarcas de La Segarra y El Solsonès.</p>
						<p>Una cuidadosa restauración nos ha permitido conservar todos los aposentos y elementos típicos de una antigua casa de labrador como son la bodega, el cup del vino, el establo, el horno de pan, el hogar de fuego, etc.</p>
						<p>Vuestra estancia en Cal Prat os permitirá disfrutar imaginándoos cómo ha sido la vida de nuestro campesinado en los últimos siglos, y podréis fruir del paisaje y la tranquilidad del mundo rural de hoy.</p>
						<p>El Forn y El Celler de Cal Prat, así se denominan nuestros alojamientos por la particularidad que caracteriza a cada uno, tienen una capacidad de hasta 8 y 5 personas respectivamente. También se puede alquilar la casa entera, puesto que los dos alojamientos se pueden comunicar.</p>
						<div class="highlights">
							<h6>Destacados</h6>
							<ul class="specific">
<li>Silencio absoluto en medio de la naturaleza</li>
								<li>Excursiones a tocar de la casa y rutas organizadas</li>
								<li>Material para niños</li>
								<li>Piscina climatizada privada</li>
								<li>Habitaciones privadas con todo detalle</li>
							</ul>
</div>
						<div class="clearfix"></div>
						<div class="img">
							<img src="[base_url]img/About/4.jpg" alt="">
</div>
						<div class="clearfix"></div>
						<h2>No estamos tranquilos hasta verte satisfecho</h2>
						<p>Explícanos qué necesitas, qué buscas, qué te falta y todas las dudas que te surjan para disfrutar al 100% de tu estancia.</p>
						<p>Disponemos de tronas, cunas, camas supletorias y otros materiales que pueden hacerte la estancia más amena.</p>
					</div>
				</div>
			</div>
		</div>
	</section>    </section>