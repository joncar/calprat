<!-- 404 -->
    <section id="content" class="blog">
        <div class="container">
            <div class="error-page">
                <div class="error-bg">
                    <div class="error-img"></div>
                    <div class="col-md-6 no-padding">
                    </div>
                    <div class="col-md-6 no-padding">
                        <div class="error-detial">
                            
                            <div class="col-xs-12 col-sm-12 col-md-12 col-centered center-block">
                                <!-- Row Start -->
                                <div class="row">
                                    <div class="title">
                                        <h1>Sistema de reserva online fuera de servicio temporalmente</h1>                                        
                                        <div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
                                            <span><img class="img-responsive" src="[base_url]img/cap.png" alt=""></span>
                                        </div>
                                    </div>
                                </div>
                                <!-- Row End -->
                            </div>                            
                            <p>                                
                                Disculpa las molestias <br/> Puedes hacer tus reservas en el <a href="mailto:info@calprat.com">email</a> o en el <a href="tel:+34650936400">teléfono</a>
                            </p>
                            <p>
                                Gracias
                            </p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>