<section id="content" class="spa-main"><!-- PAGE HEADER --><div class="title-color" style="position: relative;">
            <h2 id="mce_40" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Sala de jocs</h2><input type="hidden" name="mce_40">
            <ol class="breadcrumb">
                <li><a href="./index.html" id="mce_41" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Inici</a><input type="hidden" name="mce_41"></li>
                <li class="active"><a href="./spa.html" id="mce_42" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Altres Espais</a><input type="hidden" name="mce_42"></li><li class="active">Sala de jocs</li>
            </ol>
            <input type="hidden" name="mce_43"><input type="hidden" name="mce_44"><input type="hidden" name="mce_45"></div>

        <!-- SPA -->
        <div class="spa-block" style="position: relative;">
            <div class="db-image">
                <div class="dbi-inner" style="height:100%; background:url(<?= base_url() ?>img/spa/22.jpg) no-repeat center center">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="spa-right">
                    <h1 id="mce_46" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">L'espai dels més menuts</h1><input type="hidden" name="mce_46">
                    <h6 id="mce_47" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Una sala on divertir-se tot jugant.</h6><input type="hidden" name="mce_47">
                    <p id="mce_48" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">A tocar del pati trobareu l'accés a la sala de jocs, una sala plena de joguines per a diferents edats.</p><input type="hidden" name="mce_48">
                    <p id="mce_49" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Podreu estar tranquils, mentres dineu i fer resopó a taula, els nens podran estar al costat jugant.</p><input type="hidden" name="mce_49">
                    <h3 id="mce_50" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Jocs</h3><input type="hidden" name="mce_50">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <ul class="specific">
                                <li>Futbolí</li>
                                <li>Cotxet, llitet i nines</li>
                                <li>Cuineta i estris per fer el menjar</li>
                                <li>Parking cotxes</li>
                                <li>2 patinets i 3 cascs per els infants</li>
                                <li>3 tricicles</li>
                                <li>2 motos Molto</li>
                                <li>2 jocs de patins</li>
                                <li>Contes</li>
                            </ul>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <ul class="specific">
                                <li>Ossets de peluix</li>
                                <li>Pissarra d'iman amb lletres</li>
                                <li>Jocs d'escacs</li>
                                <li>Jocs de parxís</li>
                                <li>Pales bàdminton</li>
                                <li>Bitlles</li>
                                <li>Party</li>
                            </ul>
                        </div>
                    </div>

                    <h3 id="mce_50" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Imatges de la Sala de Jocs</h3>
                    <div class="row" uk-lightbox>
                        [foreach:galeria_joc]
                            <div class="col-xs-12 col-sm-3" style="margin-top: 29px;"><a href="[foto]"><img src="[foto]" alt="" style="width:100%"></a></div>
                        [/foreach]
                    </div>
                </div>
                
            </div>
        </div>
</section>  