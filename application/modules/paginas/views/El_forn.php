
            <!-- WELCOME -->
<section id="content">
            <section id="content" class="room-gallery"><!-- PAGE HEADER --><div class="title-color" style="position: relative;">
			<div class="container">
				<h2 id="mce_91" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Room Details</h2><input type="hidden" name="mce_91">
				<ol class="breadcrumb">
<li><a href="./index.html" id="mce_92" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Home</a><input type="hidden" name="mce_92"></li>
					<li><a href="./accommodations.html" id="mce_93" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Accomodations</a><input type="hidden" name="mce_93"></li>
					<li class="active">Room Details</li>
				</ol>
</div>
		<input type="hidden" name="mce_94"><input type="hidden" name="mce_95"><input type="hidden" name="mce_96"></div>
		
		<!-- ROOM DETAIL-->
		<div class="container" style="position: relative;">
			<div class="delux-city-view">
				<div class="bloggal-slider">
					<div id="gal-slider" class="flexslider">
						<ul class="slides">
<li><img src="[base_url]img/blog-page/blogpost-gallery/slider-img1.jpg" alt=""></li>
							<li><img src="[base_url]img/blog-page/blogpost-gallery/slider-img1.jpg" alt=""></li>
							<li><img src="[base_url]img/blog-page/blogpost-gallery/slider-img1.jpg" alt=""></li>
							<li><img src="[base_url]img/blog-page/blogpost-gallery/slider-img1.jpg" alt=""></li>
						</ul>
<ul class="flex-direction-nav">
<li><a class="flex-prev mce-content-body" id="mce_97" contenteditable="true" spellcheck="false"><i class="fa fa-angle-left"></i></a><input type="hidden" name="mce_97"></li>
							<li><a class="flex-next mce-content-body" id="mce_98" contenteditable="true" spellcheck="false"><i class="fa fa-angle-right"></i></a><input type="hidden" name="mce_98"></li>
						</ul>
</div>
					<ul class="gal-nav">
<li>
							<div>
								<img src="[base_url]img/blog-page/blogpost-gallery/slider-img2.jpg" alt="">
</div>
						</li>
						<li>
							<div>
								<img src="[base_url]img/blog-page/blogpost-gallery/slider-img3.jpg" alt="">
</div>
						</li>
						<li>
							<div>
								<img src="[base_url]img/blog-page/blogpost-gallery/slider-img4.jpg" alt="">
</div>
						</li>
						<li>
							<div>
								<img src="[base_url]img/blog-page/blogpost-gallery/slider-img5.jpg" alt="">
</div>
						</li>
					</ul>
</div>
				<div class="row">
					<div class="col-xs-12 col-sm-7 col-md-7">
						<h1 id="mce_99" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Deluxe City View Balcony Suite</h1><input type="hidden" name="mce_99">
						<p id="mce_100" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent conubia per inceptos.</p><input type="hidden" name="mce_100">
						<p id="mce_101" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit duis sed odio sit amet nibh vulputate.</p><input type="hidden" name="mce_101">
						<p id="mce_102" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p><input type="hidden" name="mce_102">
					</div>
					<div class="col-xs-12 col-sm-5 col-md-4 col-md-offset-1 pull-right">
						<div class="reservation">
							<h2 id="mce_103" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Make a Reservation</h2><input type="hidden" name="mce_103">
							<div class="rate-from">
								<h5 id="mce_104" class="mce-content-body" contenteditable="true" spellcheck="false">rates from</h5><input type="hidden" name="mce_104">
								<h1 id="mce_105" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;"><span id="mce_106"><sup>$</sup></span><input type="hidden" name="mce_106">120</h1><input type="hidden" name="mce_105">
								<h6 id="mce_107" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">per night</h6><input type="hidden" name="mce_107">
							</div>
							<div class="col-xs-12 col-sm-6 col-md-6 cap col-centered center-block">
								<span id="mce_108" class="mce-content-body" contenteditable="true" spellcheck="false"><img class="img-responsive" src="[base_url]img/cap.png" alt=""></span><input type="hidden" name="mce_108">
							</div>
							<div class="row date-picker">
								<div class="col-xs-12 col-sm-6 col-md-6">
									<div class="form-group">
										<div id="datetimepicker1" class="input-group date">
											<input type="text" placeholder="check-in date" class="form-control"><span class="input-group-addon mce-content-body" id="mce_109" contenteditable="true" spellcheck="false"><i class="fa fa-calendar"></i></span><input type="hidden" name="mce_109">
										</div>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6">
									<div class="form-group">
										<div id="datetimepicker2" class="input-group date">
											<input type="text" placeholder="check-out date" class="form-control"><span class="input-group-addon mce-content-body" id="mce_110" contenteditable="true" spellcheck="false"><i class="fa fa-calendar"></i></span><input type="hidden" name="mce_110">
										</div>
									</div>
								</div>
							</div>
							<div class="row date-picker">
								<div class="col-xs-12 col-sm-6 col-md-6">
									<select id="get_value" tabindex="0"><option value="">Adults</option>
<option value="1">1</option>
<option value="2">2</option>
<option value="3">3</option>
<option value="4">4</option>
<option value="5">5</option></select>
</div>
								<div class="col-xs-12 col-sm-6 col-md-6">
									<select id="get_value2" tabindex="0"><option value="">Children</option>
<option value="1">1</option>
<option value="2">2</option>
<option value="3">3</option>
<option value="4">4</option>
<option value="5">5</option></select>
</div>
							</div>
							<button type="submit" class="btn btn-default btn-availble">check availability</button>
						</div>
					</div>
				</div>
			</div>
			
			<div class="spa-glan-tab">
				<div role="tabpanel">
					<ul role="tablist" class="nav nav-tabs">
<li class="active" role="presentation">
							<a data-toggle="tab" role="tab" href="#overview" id="mce_111" class="mce-content-body" contenteditable="true" spellcheck="false"><i class="fa fa-globe"></i>overview</a><input type="hidden" name="mce_111">
						</li>
						<li role="presentation">
							<a data-toggle="tab" role="tab" href="#amenities" id="mce_112" class="mce-content-body" contenteditable="true" spellcheck="false"><i class="fa fa-trophy"></i>amenities</a><input type="hidden" name="mce_112">
						</li>
						<li role="presentation">
							<a data-toggle="tab" role="tab" href="#available" id="mce_113" class="mce-content-body" contenteditable="true" spellcheck="false"><i class="fa fa-heart"></i>availability</a><input type="hidden" name="mce_113">
						</li>
						<li role="presentation">
							<a data-toggle="tab" role="tab" href="#reviews" id="mce_114" class="mce-content-body" contenteditable="true" spellcheck="false"><i class="fa fa-pagelines"></i>reviews (2)</a><input type="hidden" name="mce_114">
						</li>
					</ul>
<!-- Tabs --><div class="tab-content">
						<div id="overview" class="tab-pane fade in active" role="tabpanel">
							<div class="style-comfort">
								<div class="row">
									<div class="col-xs-12 col-sm-6 col-md-6">
										<img src="[base_url]img/RoomDetail-gallery/1.jpg" alt="">
</div>
									<div class="col-xs-12 col-sm-6 col-md-6">
										<h2 id="mce_115" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">A Stylish Suite for Your Comfort</h2><input type="hidden" name="mce_115">
										<p id="mce_116" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p><input type="hidden" name="mce_116">
										<ul>
<li>
												<span class="des mce-content-body" id="mce_117" contenteditable="true" spellcheck="false" style="position: relative;">beds</span><input type="hidden" name="mce_117">
												<span class="detail mce-content-body" id="mce_118" contenteditable="true" spellcheck="false" style="position: relative;">2 Queen</span><input type="hidden" name="mce_118">
											</li>
											<li>
												<span class="des mce-content-body" id="mce_119" contenteditable="true" spellcheck="false" style="position: relative;">occupancy</span><input type="hidden" name="mce_119">
												<span class="detail mce-content-body" id="mce_120" contenteditable="true" spellcheck="false" style="position: relative;">4 People</span><input type="hidden" name="mce_120">
											</li>
											<li>
												<span class="des mce-content-body" id="mce_121" contenteditable="true" spellcheck="false" style="position: relative;">size</span><input type="hidden" name="mce_121">
												<span class="detail mce-content-body" id="mce_122" contenteditable="true" spellcheck="false" style="position: relative;">79 sqm / 855 sqf</span><input type="hidden" name="mce_122">
											</li>
											<li>
												<span class="des mce-content-body" id="mce_123" contenteditable="true" spellcheck="false" style="position: relative;">view</span><input type="hidden" name="mce_123">
												<span class="detail mce-content-body" id="mce_124" contenteditable="true" spellcheck="false" style="position: relative;">City</span><input type="hidden" name="mce_124">
											</li>
											<li>
												<span class="des mce-content-body" id="mce_125" contenteditable="true" spellcheck="false" style="position: relative;">rates from</span><input type="hidden" name="mce_125">
												<span class="detail mce-content-body" id="mce_126" contenteditable="true" spellcheck="false" style="position: relative;">175 USD</span><input type="hidden" name="mce_126">
											</li>
											<li>
												<span class="des mce-content-body" id="mce_127" contenteditable="true" spellcheck="false" style="position: relative;">view video</span><input type="hidden" name="mce_127">
												<span class="detail mce-content-body" id="mce_128" contenteditable="true" spellcheck="false" style="position: relative;"><a href=