
    <section id="content" class="spa-main"><!-- PAGE HEADER --><div class="title-color" style="position: relative;">
            <h2 id="mce_40" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">La piscine</h2><input type="hidden" name="mce_40">
            <ol class="breadcrumb">
                <li><a href="./index.html" id="mce_41" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Home</a><input type="hidden" name="mce_41"></li>
                <li class="active">La piscine privée</li>
            </ol>
            <input type="hidden" name="mce_43"><input type="hidden" name="mce_44"><input type="hidden" name="mce_45"></div>

        <!-- SPA -->
        <div class="spa-block" style="position: relative;">
            <div class="db-image">
                <div class="dbi-inner" style="height:100%; background:url(<?= base_url() ?>img/spa/1.jpg) no-repeat center center">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="spa-right">
                    <h1 id="mce_46" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Le meilleur moyen de se faire dorloter</h1><input type="hidden" name="mce_46">
                    <h6 id="mce_47" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Profitez d'une expérience de spa à Cal Prat.</h6><input type="hidden" name="mce_47">
                    <p id="mce_48" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Durant votre séjour nous souhaitons que vous puissiez trouver silence, tranquilité, relaxation…</p><input type="hidden" name="mce_48">
                    <p id="mce_49" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Pour cela vous pourrez bénéficier d’une piscine climatisée avec solarium où vous pourrez vous baigner, prendre le soleil, lire… .Elle dispose d’un système de natation à contre courant, un saut d’eau relaxant, illumination, solarium et chauffage au sol.

Nous utilisons un système d’épuration  par électrolyse saline. De cette façon nous sommes, non seulement respectueux avec l’environnement,  mais aussi avec votre peau.</p><input type="hidden" name="mce_49">
                    <h3 id="mce_50" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;"></h3><input type="hidden" name="mce_50">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <ul class="specific">
                                <li>Zone solarium pour prendre le soleil</li>
                                <li>Chaises longues</li>
                                
                            </ul>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <ul class="specific">
                                <li>Sofas autour de la piscine</li>
                                <li>Serviettes de piscine</li>
                                                            </ul>


                        </div>
                    </div>
                    <h3 id="mce_50" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;"></h3>
                    <div class="row" uk-lightbox>
                        [foreach:galeria_piscina]
                            <div class="col-xs-12 col-sm-3" style="margin-top: 29px;"><a href="[foto]"><img src="[foto]" alt="" style="width:100%"></a></div>
                        [/foreach]
                    </div>
                </div>
            </div>
        </div>

        <div class="spa-img-bot" style="position: relative;">
            <div class="spa-natural">
                <h2 id="mce_55" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Nous n’utilisons que les meilleurs produits naturels.</h2><input type="hidden" name="mce_55">
                <div class="col-xs-12 col-sm-6 col-md-6 cap col-centered center-block">
                    <span id="mce_56" class="mce-content-body" contenteditable="true" spellcheck="false"><img class="img-responsive" src="[base_url]img/cap.png" alt=""></span><input type="hidden" name="mce_56">
                </div>
                <p id="mce_57" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Nous utilisons un système d’épuration  par électrolyse saline. De cette façon nous sommes, non seulement respectueux avec l’environnement,  mais aussi avec votre peau.</p><input type="hidden" name="mce_57">
                <p id="mce_58" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;"></p><input type="hidden" name="mce_58">
            </div>
        </div>
        <div style="text-align:center;"></div>
    </section>