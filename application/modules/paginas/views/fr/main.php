<div class="pre-header">
    <ul>
        <li class="ph-tel">
            <div class="dropdown">
              <a style="color: #AF9A5F;" id="dLabel" data-target="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-flag-o fa-2x" style="vertical-align: top; font-size:1.3em"></i> FRA
                <span class="caret"></span>
              </a>

              <ul class="dropdown-menu" aria-labelledby="dLabel">
                <li><a href="<?= base_url('main/traduccion/ca') ?>">CAT</a></li>
                <li><a href="<?= base_url('main/traduccion/es') ?>">ESP</a></li>
                <li><a href="<?= base_url('main/traduccion/en') ?>">ENG</a></li>
                <li><a href="<?= base_url('main/traduccion/fr') ?>">FRA</a></li>
              </ul>
            </div>
        </li>
        <li class="ph-tel"><a href="tel:+34650936400" style="color:inherit;"> <i class="fa fa-mobile fa-2x" style="vertical-align: top;color: #AF9A5F;"></i> <span>+34 650 93 64 00</span></a></li>
        <li class="ph-weather"><img src="[iconotemp]" alt=""/> <span>[temp]  &deg;</span></li>
        <li class="ph-social">
            <div>
                <a href="https://www.facebook.com/Cal-Prat-TurismeRural-143945365955474/" class="fa fa-facebook"></a>
                <a href="https://www.pinterest.com/username" class="fa fa-pinterest"></a>
                <a href="https://www.youtube.com/channel/UCrMyhl_KCzMONdNgmyJJmAA" class="fa fa-youtube"></a>
                <a href="https://www.instagram.com/calprat_turismerural/?hl=es" class="fa fa-instagram"></a>
            </div>
        </li>
        <li class="ph-search">
            <span class="ss-trigger fa fa-search"></span>
            <div class="ss-content">
                <span class="ss-close fa fa-times"></span>
                <div class="ssc-inner">
                    <form action="<?= base_url('paginas/frontend/buscar') ?>">
                        <input type="text" name="q" placeholder="Què busques?">
                        <button type="submit"><i class="fa fa-search"></i></button>
                    </form>
                </div>
            </div>
        </li>
    </ul>
</div>
<div class="top-slider" style="display: none">
    <div id="top-slider">
        <ul class="slides">
            [foreach:banner]
            <li>
                <img src="[foto]" alt="">
                <div class="flex-caption">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 id="mce_91" class="mce-content-body mce-edit-focus" >[titulo]</h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <p id="mce_92">[subtitulo]</p>
                        </div>
                    </div>
                </div>
            </li>
            [/foreach]
        </ul>
        <ul class="flex-direction-nav">
            <li><a class="flex-prev mce-content-body" id="mce_99" ><i class="fa fa-angle-left"></i></a></li>
            <li><a class="flex-next mce-content-body" id="mce_100" ><i class="fa fa-angle-right"></i></a></li>
        </ul>
    </div>
</div>
<!-- WELCOME -->
<section id="content">
    <div class="welcome" style="position: relative;">
        <div class="container">
            <div class="col-xs-12 col-sm-12 col-md-6 col-centered center-block">
                <div class="row">
                    <div class="title">
                        <h6 id="mce_101">Bienvenue à</h6>
                        <h3 id="mce_102">la maison rurale Cal Prat</h3>
                        <div class="col-xs-6 col-sm-4 col-md-4 cap col-centered center-block">
                            <span id="mce_103"><img alt="" src="[base_url]img/cap.png" class="img-responsive"></span>
                        </div>
                        <p id="mce_104">Cal Prat est une ancienne maison de campagne du XVIIIème siècle située au petit hameau d’El Soler, dans le territoire municipal de Calonge de Segarra à l’Alta Anoia, juste à coté de la contrée de la Segarra et El Solsonès.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="left-welcome">
                        <h2 id="mce_105">L’endroit est parfait pour faire une escapade</h2>
                        <p id="mce_106">Votre séjour chez Prat vous permettra d’en profiter tout en vous imaginant comme était la vie de nos paysans durant les derniers siècles et vous pourrez jouir du paysage et la tranquillité du monde rural d’aujourd’hui.</p>
                        <p id="mce_107">Une soigneuse restauration nous a permis de conserver toutes les pièces et éléments typiques d’une ancienne maison de campagne comme sont : le cellier, la cuve du vin, l’étable, le four à pain, la cheminée, etc…</p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="right-welcome">
                        <div id="promovid">
                            <div class="custom-th">
                                <img src="[base_url]img/Homepage_Resort_INDEX/video.png" class="img-responsive" alt="">
                            </div>
                            <div id="thevideo" style="display:none; padding: 0px;">                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ACCOMODATION -->
    <div class="accomodation" style="position: relative;">
        <div class="container">
            <div class="col-xs-12 col-sm-12 col-md-6 col-centered center-block">
                <div class="row">
                    <div class="title">
                        <h6 id="mce_111">Installations</h6>
                        <h3 id="mce_112">Logements et espaces</h3>
                        <div class="col-xs-6 col-sm-4 col-md-4 cap col-centered center-block">
                            <span id="mce_113"><img alt="" src="[base_url]img/cap.png" class="img-responsive"></span>
                        </div>
                        <p id="mce_114">Les logements dénommés Le Four  et Le Cellier ( s’appellent ainsi à cause de leur particularité) ont une capacité de 8 et 5 personnes respectivement. On peut aussi louer la maison dans sa totalité, étant donné que les deux logements communiquent.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-room">
            <ul>
                <li class="col-xs-12 col-sm-6 col-md-3">
                    <div class="mr-inner">
                        <img src="[base_url]img/Homepage_Resort_INDEX/room-img1.jpg" class="img-responsive" alt="">
                        <div class="mr-overlay">
                            <h2 id="mce_115">Ta maison<br>13 personnes</h2>
                            <p id="mce_116">Vous pouvez bénéficier d’une totale intimité en louant toute la maison. Un ample espace pour familles et amis.</p>
                            <a class="room-detail mce-content-body" href="[base_url]habitacion/1-tota-la-casa" id="mce_117" >Plus d'informations<i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </li>
                <li class="col-xs-12 col-sm-6 col-md-3">
                    <div class="mr-inner">
                        <img src="[base_url]img/Homepage_Resort_INDEX/room-img2.jpg" class="img-responsive" alt="">
                        <div class="mr-overlay">
                            <h2 id="mce_118">Le four<br>8 personnes</h2>
                            <p id="mce_119">Le Four avec une capacité pour 8 personnes occupe la façade nord de la maison.<br><br>
                                </p>
                            <a class="room-detail mce-content-body" href="[base_url]habitacion/2-el-form" id="mce_120" >Plus d'informations<i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </li>
                <li class="col-xs-12 col-sm-6 col-md-3">
                    <div class="mr-inner">
                        <img src="[base_url]img/Homepage_Resort_INDEX/room-img3.jpg" class="img-responsive" alt="">
                        <div class="mr-overlay">
                            <h2 id="mce_121">Le cellier<br>5 personnes</h2>
                            <p id="mce_122">Le cellier avec une capacité pour 5 personnes occupe la façade sud de la maison.<br><br>
                                </p>
                            <a class="room-detail mce-content-body" href="[base_url]habitacion/3-el-celler" id="mce_123" >Plus d'informations<i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </li>
                <li class="col-xs-12 col-sm-6 col-md-3">
                    <div class="mr-inner">
                        <img src="[base_url]img/Homepage_Resort_INDEX/room-img4.jpg" class="img-responsive" alt="">
                        <div class="mr-overlay">
                            <h2 id="mce_124">La piscine<br>privée</h2>
                            <p id="mce_125">Durant votre séjour nous souhaitons que vous puissiez trouver silence, tranquilité, relaxation.</p>
                            <a class="room-detail mce-content-body" href="[base_url]p/la_piscina.html" id="mce_126" >Plus d'informations<i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <input name="mce_127" type="hidden"><input name="mce_128" type="hidden"></div>
        <!-- ROOM OFFER -->
        <div class="room-offer" style="position: relative;">
            <div class="container">
                <h3 id="mce_130">TOUTES LES PIÈCES OFFRENT</h3><input name="mce_130" type="hidden">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1">
                        <ul class="specific">
                            <li>Chauffage</li>
                            <li>Chauffage cheminée (bois inclus)</li>
                            <li>Télévision</li>
                            <li>Wifi</li>
                            <li>Draps de lit</li>
                            <li>Serviettes de toilette et serviettes pour la piscine</li>
                            <li>Microondes/réfrigérateur</li>
                            <li>Lave-vaisselle/lave-linge</li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1">
                        <ul class="specific">
                            <li>Cafetière Nespresso /Italienne</li>
                            <li>Berceau/Petite chaise haute pour enfant/barrière de lit</li>
                            <li>Petite pharmacie</li>
                            <li>Jeux de table</li>
                            <li>DVD/Reproducteur CD</li>
                            <li>Produits d’entretien pour la cuisine</li>
                            <li>Vaisselle</li>
                        </ul>
                    </div>
                </div>
            </div>
            <input name="mce_131" type="hidden"><input name="mce_132" type="hidden"><input name="mce_133" type="hidden"></div>
            <div class="home-gal" style="position: relative;">
                <div class="dining-block dual-slider row">
                    <div class="col-md-7 padding-right-none">
                        <div id="gal-slider1" class="flexslider">
                            <ul class="slides">
                                <li><img class="img-responsive" src="[base_url]img/Homepage_Resort_INDEX/home-gal-img1.png" alt=""></li>
                            </ul>
                            <!--<a class="flex-prev1 mce-content-body" id="mce_134" ><i class="fa fa-chevron-left"></i></a><input name="mce_134" type="hidden">
                            <a class="flex-next1 mce-content-body" id="mce_135" ><i class="fa fa-chevron-right"></i></a><input name="mce_135" type="hidden">-->
                        </div>
                    </div>
                    <div class="col-md-5 padding-left-none">
                        <ul class="gal-nav1">
                            <li class="flex-active">
                                <div>
                                    <h2 id="mce_136">Profitez avec les vôtres</h2><input name="mce_136" type="hidden">
                                    <p id="mce_137">Cal Prat se caractérise par la tranquilité de son environnement entouré de nature, idéal pour profiter d’une escapade avec la famille ou les amis.</p><input name="mce_137" type="hidden">
                                    <p id="mce_138">Il ya des endroits intéressant à visiter dans toute la zone qui ne vous laisseront pas indifférents.</p><input name="mce_138" type="hidden">
                                    <a href="[base_url]p/activitat.html" class="btn btn-default mce-content-body" id="mce_139" >Détails</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- SPA -->
                <div class="spa-block dual-slider row">
                    

                    <div class="hidden-xs col-md-5 padding-right-none">
                        <ul class="gal-nav2">
                            <li class="flex-active">
                                <div>
                                    <h2 id="mce_152">Faites-vous plaisir à la piscine</h2><input name="mce_152" type="hidden">
                                    <p id="mce_153"></p><input name="mce_153" type="hidden">
                                    <p id="mce_154">Chez Prat nous sommes spécialisés pour un tourisme rural familial et pou couples, et notre principale préoccupation  est d’offrir à notre clientèles repos et relaxation loin du rythme et bruit de la ville. Dans un environnement naturel très tranquille se trouve notre maison rurale avec le SPA d’usage exclusif, un des trésors de Cal Prat.</p><input name="mce_154" type="hidden">
                                    <a href="[base_url]p/la_piscina.html" class="btn btn-default mce-content-body" id="mce_155" >Détails</a><input name="mce_155" type="hidden">
                                </div>
                            </li>
                        </ul>
                    </div>
                    

                    <div class="col-md-7 padding-left-none">
                        <div id="gal-slider2" class="flexslider">
                            <ul class="slides">
                                <li><img class="img-responsive" src="[base_url]img/Homepage_Resort_INDEX/home-gal-img2.jpg" alt=""></li>                                
                            </ul>
                            <!--<a class="flex-prev2 mce-content-body" ><i class="fa fa-chevron-left"></i></a><input name="mce_168" type="hidden">
                            <a class="flex-next2 mce-content-body" ><i class="fa fa-chevron-right"></i></a><input name="mce_169" type="hidden">-->
                        </div>
                    </div>

                    <div class="visible-xs col-md-5 padding-right-none">
                        <ul class="gal-nav2">
                            <li class="flex-active">
                                <div>
                                    <h2 id="mce_152">Faites-vous plaisir à la piscine</h2><input name="mce_152" type="hidden">
                                    <p id="mce_153"></p><input name="mce_153" type="hidden">
                                    <p id="mce_154">Chez Prat nous sommes spécialisés pour un tourisme rural familial et pou couples, et notre principale préoccupation  est d’offrir à notre clientèles repos et relaxation loin du rythme et bruit de la ville. Dans un environnement naturel très tranquille se trouve notre maison rurale avec le SPA d’usage exclusif, un des trésors de Cal Prat.</p><input name="mce_154" type="hidden">
                                    <a href="[base_url]p/la_piscina.html" class="btn btn-default mce-content-body" id="mce_155" >Détails</a><input name="mce_155" type="hidden">
                                </div>
                            </li>
                        </ul>
                    </div>


                </div>
                <!-- 
<div class="entertain-block dual-slider row">
                    <div class="col-md-7 padding-right-none">
                        <div id="gal-slider3" class="flexslider">
                            <ul class="slides">
                                <li><img class="img-responsive" src="[base_url]img/Homepage_Resort_INDEX/home-gal-img3.png" alt=""></li>
                            </ul>
                            <!~~<a class="flex-prev3 mce-content-body" id="mce_170" ><i class="fa fa-chevron-left"></i></a><input name="mce_170" type="hidden">
                            <a class="flex-next3 mce-content-body" id="mce_171" ><i class="fa fa-chevron-right"></i></a><input name="mce_171" type="hidden">~~>
                        </div>
                    </div>
                    <div class="col-md-5 padding-left-none">
                        <ul class="gal-nav3">
                            <li class="flex-active">
                                <div>
                                    <h2 id="mce_172">Découvrez l’environnement</h2><input name="mce_172" type="hidden">
                                    <p id="mce_173">Chez Prat vous pouvez venir vous reposer, vous promener, faire un tour à bicyclette ou vous baigner à la piscine et déconnecter.</p><input name="mce_173" type="hidden">
                                    <p id="mce_174">Visites guidées, excursions, promenades à cheval, routes à BTT, routes culturelles et vols en montgolfière…</p><input name="mce_174" type="hidden">
                                    <a href="[base_url]p/la_piscina.html" class="btn btn-default mce-content-body" id="mce_175" >Détails</a><input name="mce_175" type="hidden">
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
 -->
                <input name="mce_188" type="hidden"><input name="mce_189" type="hidden"><input name="mce_190" type="hidden"></div>
                <div class="activ-eve" style="position: relative;">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <h3 id="mce_191">Activités</h3><input name="mce_191" type="hidden">
                                <p id="mce_192">Il y a beaucoup à faire dans nos parages. Demandez-nous des informations et nous serons enchantés de vous recommander des activités et des routes à faire. Nous disposons de données de contact et des rabais.</p><input name="mce_192" type="hidden">
                                <ul class="specific">
                                    <li class="col-xs-12 col-sm-6 col-md-6">Visites guidées</li>
                                    <li class="col-xs-12 col-sm-6 col-md-6">Observations astronomiques</li>
                                    <li class="col-xs-12 col-sm-6 col-md-6">Excursions à bicyclette</li>
                                    <li class="col-xs-12 col-sm-6 col-md-6">Activités d’aventure</li>
                                    <li class="col-xs-12 col-sm-6 col-md-6">Promenades à cheval</li>
                                    <li class="col-xs-12 col-sm-6 col-md-6">Geocaching</li>
                                </ul>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <h3 id="mce_193">Evénements</h3><input name="mce_193" type="hidden">
                                <p id="mce_194">Il y a beaucoup à faire dans nos parages. Demandez-nous des informations et nous serons enchantés de vous recommander des activités et des routes à faire. Nous disposons de données de contact et des rabais.</p><input name="mce_194" type="hidden">
                                <ul class="specific">
                                    <li class="col-xs-12 col-sm-6 col-md-6">Festivals de musique</li>
                                    <li class="col-xs-12 col-sm-6 col-md-6">Foires et marchés</li>
                                    <li class="col-xs-12 col-sm-6 col-md-6">Route des châteaux</li>
                                    <li class="col-xs-12 col-sm-6 col-md-6">Randonnées populaires</li>
                                    <li class="col-xs-12 col-sm-6 col-md-6">Pastorets de Calaf (représentation de la naissance de l’enfant Jésus, à Noël)</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="learn-more">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="row">
                                <img class="img-responsive" src="[base_url]img/Homepage_Resort_INDEX/learn-img1.jpg" alt="">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="row">
                                <img class="img-responsive" src="[base_url]img/Homepage_Resort_INDEX/learn-img2.jpg" alt="">
                            </div>
                        </div>
                        <a href="[base_url]agenda" class="btn btn-default mce-content-body" id="mce_195" spellcheck="false">Lire plus</a>
                    </div>
                </div>
                <!-- SERVICES -->
                <div class="activ-eve" style="position: relative;">
                    <div class="container">
                        <div class="col-xs-12 col-sm-12 col-md-6 col-centered center-block">
                            <div class="title" style="padding-bottom: 0px">
                                <h6 id="mce_199">Certification</h6><input name="mce_199" type="hidden">
                                <h3 id="mce_200">Logement avec certification Biosphère</h3><input name="mce_200" type="hidden">
                                <div class="col-xs-6 col-sm-4 col-md-4 cap col-centered center-block">
                                    <span id="mce_201"><img class="img-responsive" src="[base_url]img/cap.png" alt=""></span><input name="mce_201" type="hidden">
                                </div>
                                <p id="mce_202">Cal Prat est un logement qui a obtenu la Certification Biosphère Responsable Tourisme. De cette façon nous misons pour un tourisme responsable et soutenable, compromis avec l’environnement.</p><input name="mce_202" type="hidden">
                            </div>                            
                        </div>
                        
                        <div class="col-xs-12 col-sm-12 col-md-12" style="padding-bottom: 30px">
                            <p style="text-align:center; margin-top:41px"><img src="[base_url]img/bio.jpg"></p>
                            <p id="mce_192" style="text-align:center">
                                La Certification est délivrée par l’Institut de Tourisme Responsable, organisme lié à l’UNESCO et associé a l’Organisation Mondiale de Tourisme et au Global Sustainable Turism Council. Il s’agit d’une Certification pionnière, basée en 17 objectifs de Développement  Soutenable des Nations Unies intégrés à l’Agenda 2030. <br>
                                Le cachet accrédite que les trois marques touristiques  de la zone « Costa Barcelona », « paisatges Barcelona » et « Pirineus Barcelona » promulguées par la Diputació de Barcelona remplissent les conditions du standard « Biosphère Destination » et qui mettent en practique avec succès 6 critères concrets dans leur politique touristique: Gestion soutenable
Développement économique et social, Conservation et amélioration du patrimoine culturel, Conservation environnementale, Qualité et sécurité et Implication du touriste.

                            </p>
                        </div>
                            
                    </div>
                </div>
            </div>
                        <div class="home-img-panel parallax-bg" data-stellar-ratio="2" data-stellar-background-ratio="0.5" style="position: relative;">
                            <div class="panel-img-detail">
                                <h2 id="mce_207">Nous facilitons la planification de votre séjour</h2><input name="mce_207" type="hidden">
                                <div class="col-xs-7 col-sm-7 col-md-7 cap col-centered center-block">
                                    <span id="mce_208"><img alt="" src="[base_url]img/cap.png" class="img-responsive"></span><input name="mce_208" type="hidden">
                                </div>
                                <p id="mce_209">Vous pensez faire une surprise à votre conjoint ou faire se retrouvèrent familial dont vous parlez tant, ou bien célébrer l’anniversaire des enfants ou les noces des parents, ou cette réunion de travail que vous voulez plus détendue … ? Quoi qu’il en soit ,téléphonez-nous et nous vous aiderons à bien le planifier !!!
                                    </p><input name="mce_209" type="hidden">
                                    <a href="[base_url]iniciar-reserva" class="btn btn-default mce-content-body" id="mce_210" >détails</a><input name="mce_210" type="hidden">
                                </div>
                                <input name="mce_211" type="hidden"><input name="mce_212" type="hidden"><input name="mce_213" type="hidden"></div>
                                <div class="main-gallery" style="position: relative;">
                                    <div class="container">
                                        <div class="col-xs-12 col-sm-12 col-md-6 col-centered center-block">
                                            <div class="row">
                                                <div class="title">
                                                    <h3 class="light-title mce-content-body" id="mce_214" >Galerie</h3><input name="mce_214" type="hidden">
                                                    <div class="col-xs-6 col-sm-4 col-md-4 cap col-centered center-block">
                                                        <span id="mce_215"><img alt="" src="[base_url]img/cap.png" class="img-responsive"></span><input name="mce_215" type="hidden">
                                                    </div>
                                                    <p id="mce_216">Connaissez un peu notre maison à travers la galerie d’images. Chaque recoin est spécial et différent. </p><input name="mce_216" type="hidden">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="main-gal-slider">
                                        <div id="home-gallery" class="owl-carousel owl-theme">
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img3.jpg" alt="Owl Image">
                                                <!--<div class="figcaption">
                                                    <h3>Sala d'estar Forn</h3>
                                                    <div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
                                                        <span><img class="img-responsive" src="<?= base_url() ?>img/cap.png" alt=""></span>
                                                    </div>
                                                    <ul>
                                                        <li>
                                                            <span class="des">beds</span>
                                                            <span class="detail">King or 2 Queen</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">occupancy</span>
                                                            <span class="detail">2 People</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">size</span>
                                                            <span class="detail">60 sqm / 650 sqf</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">view</span>
                                                            <span class="detail">Ocean / City</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">rates from</span>
                                                            <span class="detail">215 USD</span>
                                                        </li>
                                                    </ul>
                                                    <div>
                                                        <a href="./roomDetail-gallery.html" class="btn btn-default pull-left">Informació sala</a>
                                                        <a href="./reservation.html" class="btn btn-default pull-right">Reservar</a>
                                                    </div>
                                                </div>-->
                                            </div>
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img2.jpg" alt="Owl Image">
                                                <!--<div class="figcaption">
                                                    <h3>Dormitori Forn 1</h3>
                                                    <div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
                                                        <span><img class="img-responsive" src="<?= base_url() ?>img/cap.png" alt=""></span>
                                                    </div>
                                                    <ul>
                                                        <li>
                                                            <span class="des">Llit doble de matrimoni</span>
                                                            <span class="detail">King or 2 Queen</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">occupancy</span>
                                                            <span class="detail">2 People</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">size</span>
                                                            <span class="detail">60 sqm / 650 sqf</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">view</span>
                                                            <span class="detail">Ocean / City</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">rates from</span>
                                                            <span class="detail">215 USD</span>
                                                        </li>
                                                    </ul>
                                                    <div>
                                                        <a href="./roomDetail-gallery.html" class="btn btn-default pull-left">room details</a>
                                                        <a href="./reservation.html" class="btn btn-default pull-right">book now</a>
                                                    </div>
                                                </div>-->
                                            </div>
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img4.jpg" alt="Owl Image">
                                                <!--<div class="figcaption">
                                                    <h3>Cuina</h3>
                                                    <div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
                                                        <span><img class="img-responsive" src="<?= base_url() ?>img/cap.png" alt=""></span>
                                                    </div>
                                                    <ul>
                                                        <li>
                                                            <span class="des">beds</span>
                                                            <span class="detail">King or 2 Queen</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">occupancy</span>
                                                            <span class="detail">2 People</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">size</span>
                                                            <span class="detail">60 sqm / 650 sqf</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">view</span>
                                                            <span class="detail">Ocean / City</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">rates from</span>
                                                            <span class="detail">215 USD</span>
                                                        </li>
                                                    </ul>
                                                    <div>
                                                        <a href="./roomDetail-gallery.html" class="btn btn-default pull-left">room details</a>
                                                        <a href="./reservation.html" class="btn btn-default pull-right">book now</a>
                                                    </div>
                                                </div>-->
                                            </div>
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img5.jpg" alt="Owl Image">
                                                <!--<div class="figcaption">
                                                    <h3>ocean view balcony room</h3>
                                                    <div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
                                                        <span><img class="img-responsive" src="<?= base_url() ?>img/cap.png" alt=""></span>
                                                    </div>
                                                    <ul>
                                                        <li>
                                                            <span class="des">beds</span>
                                                            <span class="detail">King or 2 Queen</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">occupancy</span>
                                                            <span class="detail">2 People</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">size</span>
                                                            <span class="detail">60 sqm / 650 sqf</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">view</span>
                                                            <span class="detail">Ocean / City</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">rates from</span>
                                                            <span class="detail">215 USD</span>
                                                        </li>
                                                    </ul>
                                                    <div>
                                                        <a href="./roomDetail-gallery.html" class="btn btn-default pull-left">room details</a>
                                                        <a href="./reservation.html" class="btn btn-default pull-right">book now</a>
                                                    </div>
                                                </div>-->
                                            </div>
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img7.jpg" alt="Owl Image">
                                                </div>
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img15.jpg" alt="Owl Image">
                                                </div>
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img16.jpg" alt="Owl Image">
                                            </div>
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img8.jpg" alt="Owl Image">
                                            </div>   
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img9.jpg" alt="Owl Image">
                                            </div>   
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img10.jpg" alt="Owl Image">
                                            </div>   
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img11.jpg" alt="Owl Image">
                                            </div>   
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img12.jpg" alt="Owl Image">
                                            </div>   
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img13.jpg" alt="Owl Image">
                                            </div>   
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img14.jpg" alt="Owl Image">
                                            </div>                                                   
                                        </div>
                                        
                                        <div class="customNavigation">
                                            <a class="prev"><i class="fa fa-angle-left"></i></a>
                                            <a class="next"><i class="fa fa-angle-right"></i></a>
                                        </div>
                                    </div>
                                    <div class="container">
                                        <a href="<?= base_url('p/galeria') ?>" class="btn btn-default mce-content-body" id="mce_289" >Aller à la galerie</a><input name="mce_289" type="hidden">
                                    </div>
                                </div>
                            </div>
                            <!-- TESTIMONIAL -->
                            <div class="testimonial" style="position: relative;">
                                <div class="container">
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-centered center-block">
                                        <div class="row">
                                            <div class="title">
                                                <h6 id="mce_293">temoins</h6><input name="mce_293" type="hidden">
                                                <h3 id="mce_294">Que disent nos clients</h3><input name="mce_294" type="hidden">
                                                <div class="col-xs-6 col-sm-4 col-md-4 cap col-centered center-block">
                                                    <span id="mce_295"><img class="img-responsive" src="[base_url]img/cap.png" alt=""></span><input name="mce_295" type="hidden">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6 col-md-6">
                                            <div class="testi-block">
                                                <div class="row">
                                                    <!--<div class="hidden-xs col-xs-12 col-sm-2 col-md-2">
                                                        <img src="[base_url]img/Homepage_Resort_INDEX/testimonial-img5.png" alt="">
                                                    </div>-->
                                                    <div class="col-xs-12 col-sm-offset-2 col-sm-10 col-md-10">
                                                        <div class="testi-detail">
                                                            <p id="mce_296">"Nous avons cherché une maison tranquille où nous avons passé quelques jours avec notre famille et nous avons adoré Cal Prat. Les enfants pouvaient jouer et profiter du cadre et l’affaire du propriétaire était très gentille et attentionnée."</p><input name="mce_296" type="hidden">
                                                            <h6 id="mce_297">– J. Magaña</h6><input name="mce_297" type="hidden">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6">
                                            <div class="testi-block">
                                                <div class="row">
                                                    <!--<div class="hidden-xs col-xs-12 col-sm-2 col-md-2">
                                                        <img src="[base_url]img/Homepage_Resort_INDEX/testimonial-img6.png" alt="">
                                                    </div>-->
                                                    <div class="col-xs-12 col-sm-offset-2 col-sm-10 col-md-10">
                                                        <div class="testi-detail">
                                                            <p id="mce_298">"Nous avons loué avec le groupe d'amis Cal Prat et c'était génial! Nous pourrions profiter du froid dans une maison rustique pleine de détails. Sûr de répéter!</p><input name="mce_298" type="hidden">
                                                            <h6 id="mce_299">– B. Tusell</h6><input name="mce_299" type="hidden">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="prom-offers">
                                    <div class="dark-gold"></div>
                                    <div class="light-gold"></div>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-5 col-md-5">
                                                <div class="prom-block">
                                                    <h3 id="mce_300">Promotions</h3><input name="mce_300" type="hidden">
                                                    <p id="mce_301">Consultez nos promotions ponctuelles pour jours et fins de semaine concrets durant l’année.</p><input name="mce_301" type="hidden">
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-2 col-md-2">
                                                <img class="img-responsive" src="[base_url]img/Homepage_Resort_INDEX/testimonial-img2.png" alt="">
                                            </div>
                                            <div class="col-xs-12 col-sm-5 col-md-5">
                                                <div class="offer-block">
                                                    <h3 id="mce_302">Offres  spéciales</h3><input name="mce_302" type="hidden">
                                                    <p id="mce_303">Vous êtes nouveaux mariés, groupes nombreux…… contactez Cal Prat et nous vous ferons des prix spéciaux. Nous sommes disposés à vous offrir le meilleur traitement que vous méritez dans des moments spéciaux.</p><input name="mce_303" type="hidden">
                                                </div>
                                            </div>
                                        </div>
                                        <a href="[base_url]ofertas" class="btn btn-default mce-content-body" id="mce_304" >Voir toutes les offres</a><input name="mce_304" type="hidden">
                                    </div>
                                </div>
                                <input name="mce_305" type="hidden"><input name="mce_306" type="hidden"><input name="mce_307" type="hidden"></div>
                                <!-- LOCATION -->
                                <div class="location" style="position: relative;">
                                    <div class="container">
                                        <div class="col-xs-12 col-sm-12 col-md-6 col-centered center-block">
                                            <div class="row">
                                                <div class="title">
                                                    <h6 id="mce_308">LOCALISATION</h6><input name="mce_308" type="hidden">
                                                    <h3 id="mce_309">Où vous nous trouverez
</h3><input name="mce_309" type="hidden">
                                                    <div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
                                                        <span id="mce_310"><img class="img-responsive" src="[base_url]img/cap.png" alt=""></span><input name="mce_310" type="hidden">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-8">
                                                <div class="map">
                                                    <div id="map">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-4">
                                                <ul>
                                                    <li>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4 col-md-6">
                                                                <span class="specific mce-content-body" id="mce_311" >ADRESSE</span><input name="mce_311" type="hidden">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8 col-md-6">
                                                                <p id="mce_312">El Soler, s/n Calonge de Segarra, Barcelona</p><input name="mce_312" type="hidden">
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4 col-md-6">
                                                                <span class="specific mce-content-body" id="mce_313" >Téléphome</span><input name="mce_313" type="hidden">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8 col-md-6">
                                                                <p id="mce_314">+34 650 93 64 00</p><input name="mce_314" type="hidden">
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4 col-md-6">
                                                                <span class="specific mce-content-body" id="mce_315" >email</span><input name="mce_315" type="hidden">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8 col-md-6">
                                                                <a href="mailto:info@fontainbleu.com" id="mce_316"> <input name="mce_316" type="hidden"><a href="mailto:info@calprat.net">info@calprat.net</a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4 col-md-6">
                                                                <span class="specific mce-content-body" id="mce_317" >distances</span><input name="mce_317" type="hidden">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8 col-md-6">
                                                                <p id="mce_318">80km de barcelona <br> 80km de LLeida <br> 100km de Tarragona <br> 150km de Girona</p><input name="mce_318" type="hidden">
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4 col-md-6">
                                                                <span class="specific mce-content-body" id="mce_319" >À côté de</span><input name="mce_319" type="hidden">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8 col-md-6">
                                                                <p id="mce_320">20 Km. d’Igualada<br> 20 km. de Cervera<br> 30 km. de Manresa <br>40 km. de Tàrrega</p><input name="mce_320" type="hidden">
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <input name="mce_321" type="hidden"><input name="mce_322" type="hidden"><input name="mce_323" type="hidden"></div><div style="text-align:center;"></div>
                                </section>