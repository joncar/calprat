
    <section id="content" class="spa-main">
        <!-- PAGE HEADER -->
        <div class="title-color" style="position: relative;">
            <h2 id="mce_40" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">La Cour, BBQ</h2>
            <input type="hidden" name="mce_40">
            <ol class="breadcrumb">
                <li><a href="./index.html" id="mce_41" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Home</a><input type="hidden" name="mce_41"></li>
                
                <li class="active">La Cour</li>
            </ol>
            <input type="hidden" name="mce_43"><input type="hidden" name="mce_44"><input type="hidden" name="mce_45">
        </div>
        <!-- SPA -->
        <div class="spa-block" style="position: relative;">
            <div class="db-image">
                <div class="dbi-inner" style="height:100%; background:url(<?= base_url() ?>img/spa/9.jpg) no-repeat center center">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="spa-right">
                    <h1 id="mce_46" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Un espace parfait pour vous retrouver</h1>
                    <input type="hidden" name="mce_46">
                    <h6 id="mce_47" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">La maison dispose d’une grande cour où vous pourrez prendre le soleil, dîner, vous reposer…</h6>
                    <input type="hidden" name="mce_47">
                    <p id="mce_48" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">
                        Vous trouverez dans cet espace un endroit où vous réunir pour dîner grâce à un barbecue (avec tout le matériel nécessaire pour son utilisation), une pergola pour tamiser le soleil, une table, des chaises ainsi que des chaises longues…
                    </p>
                    <input type="hidden" name="mce_48">
                    <p id="mce_49" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">
                        
                    </p>
                    <input type="hidden" name="mce_49">
                    <h3 id="mce_50" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Que trouver dans cet espace</h3>
                    <input type="hidden" name="mce_50">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <ul class="specific">
                                <li>Barbecue avec charbon, allumage, pinces, torchons de cuisine, matériel pour ramasser les cendres…</li>
                                <li>Table, chaises et bancs</li>
                                <li>Pergola pour se protéger du soleil</li>
                                
                            </ul>
                        </div>
                        <!-- 
<div class="col-xs-12 col-sm-6 col-md-6">
                            <ul class="specific">
                                <li>Zona ajardinada</li>
                                <li>fanals per la nit</li>
                                <li>Nevera aprop</li>
                                <li>Lavabo</li>
                                <li>Accés directe a la sala de jocs</li>
                            </ul>
                        </div>
 -->
                    </div>
                    <h3 id="mce_50" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Photos de la cour</h3>
                    <div class="row" uk-lightbox>
                        [foreach:galeria_patio]
                            <div class="col-xs-12 col-sm-3" style="margin-top: 29px;"><a href="[foto]"><img src="[foto]" alt="" style="width:100%"></a></div>
                        [/foreach]
                    </div>
                </div>
            </div>
    </section>