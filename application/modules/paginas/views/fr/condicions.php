<section id="content" class="business-main">
	
	<!-- PAGE HEADER -->
	<div class="title-color">
		<h2>Política de reserva i cancel·lacions</h2>
		<ol class="breadcrumb">
			<li><a href="<?= site_url() ?>">Inici</a></li>
			<li class="active"><a href="#">Política de reserva i cancel·lacions</a></li>
		</ol>
	</div>
	
	<!-- MAIN CONTENT -->
	<div class="container">
		<div class="col-xs-12 col-sm-9 col-md-9 col-centered center-block">
			<div class="row">
				<div class="title">
					<h6>&nbsp;</h6>
					<h1>Política de reserva i cancel·lacions</h1>
					<div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
						<span><img class="img-responsive" src="<?= base_url() ?>img/cap.png" alt=""></span>
					</div>
					<p>Cal Prat es preocupa per la seguretat de les dades que els nostres clients ens proporciones i per aixì segueix la nova legislació de protecció de dades.</p>
				</div>
			</div>
		</div>
	</div>
	<div class="business-detail" style="margin-bottom:39px">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
                    <h6 style="font-weight: 700;"> PAGA I SENYAL PER PART DE LES PERSONES USUÀRIES</h6>
					<p>Les persones titulars dels establiments d'allotjament poden exigir a les persones usuàries que efectuïn una reserva, un avançament del preu que s'entén a compte de l'import resultant dels serveis prestats.<br/>
					En aquest sentit per a formalitzar la reserva (mínim dues nits) al nostre establiment, Cal Prat solicitarà un ingrés del 25€ de l'import total indicant el vostre nom, D.N.I. i dia d'entrada.<br/>
                    El pagament de la resta s'efectuarà el mateix dia de sortida en efectiu o per transferència bancària.<br/></p>
					<br><br>
					<h6 style="font-weight: 700;">CANCEL·LACIÓ DE LA RESERVA</h6>
					<p>1. L'establiment d'allotjament turístic està obligat a informar a la persona usuària, abans de la formalització del contracte, sobre les clàusules de cancel·lació.<br/>
					2. L'anulació de la reserva dintre dels 15 dies anteriors a la data d'ocupació, donarà lloc a la pèrdia de la quantitat lliurada a compte.<br/>
					a) Reserva per 2 o menys dies, el 50% del preu total de l'estada.<br/>
					b) Reserva per més de 2 dies fins a 7 dies, el 35% del preu total d'estada.<br/>
					c) Reserva per més de 7 dies, el 25% del preu total d'estada.<br/>
					Les anteriors penalitzacions no són d'aplicació quan la cancel·lació es produeix per causa de força major, degudament acreditada.<br/>
					3. La persona usuària té dret a cancel·lar la reserva confirmada, sense cap penalització, sempre que es faci abans dels 15 dies anteriors a la data d'arribada, llevat pacte contrari.    <br><br></p>
					
					<h6 style="font-weight: 700;">RESCABALAMENT PER RENÚNCIA DE L'ESTADA</h6>
					<p>La persona titular de l'establiment d'allotjament turístic està obligada a informar la persona o persones usuàries, abans de la realització del contracte, sobre la normativa aplicable en cas de renúncia de l'estada.<br/>
					Quan el client o clienta d'un establiment d'allotjament abandoni la unitat reservada abans de la data fins la qual la tenia reservada, la persona titular de l'establiment no retornarà cap import de la reserva efectuada.<br/>
					<br><br></p>
					
				</div>
			</div>
			
		</div>
	</div>
</section>