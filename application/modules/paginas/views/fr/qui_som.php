<!-- WELCOME -->
<section id="content">
            <section id="content" class="about-main"><!-- PAGE HEADER --><div class="title-color">
			<h2>Cal Prat</h2>
			<ol class="breadcrumb">
<li><a href="<?= base_url() ?>">Home</a></li>
				<li class="active"><a href="<?= base_url() ?>p/qui_som.html">Qui sommes-nous?</a></li>
			</ol>
</div>
		
		<!-- ABOUT -->
		<div class="about-content">
			<div class="db-image quisom">
				<div class="dbi-inner" style="background:url([base_url]img/About/1.jpg) no-repeat center center">
					<img src="[base_url]img/About/1.jpg" style="width:100%" class="visible-xs">
				</div>
				<div class="dbi-inner" style="background:url([base_url]img/About/2.jpg) no-repeat center center">
					<img src="[base_url]img/About/2.jpg" style="width:100%" class="visible-xs">
				</div>
				<div class="dbi-inner" style="background:url([base_url]img/About/3.jpg) no-repeat center center">
					<img src="[base_url]img/About/3.jpg" style="width:100%" class="visible-xs">
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6">
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6">
				<div class="row">
					<div class="right-about">
						<h1>Cal Prat, casa rural de campagne</h1>
						<h6>Tourisme rural, relaxation et déconnexion</h6>
						<p>Cal Prat est une ancienne maison de campagne du XVIIIème siècle située au petit hameau d’El Soler, dans le territoire municipal de Calonge de Segarra à l’Alta Anoia, juste à coté de la contrée de la Segarra et El Solsonès.</p>
						<p>Une soigneuse restauration nous a permis de conserver toutes les pièces et éléments typiques d’une ancienne maison de campagne comme sont : le cellier, la cuve du vin, l’étable, le four à pain, la cheminée, etc…</p>
						<p>Votre séjour chez Prat vous permettra d’en profiter tout en vous imaginant comme était la vie de nos paysans durant les derniers siècles et vous pourrez jouir du paysage et la tranquillité du monde rural d’aujourd’hui.</p>
						<p>Les logements dénommés Le Four  et Le Cellier ( s’appellent ainsi à cause de leur particularité) ont une capacité de 8 et 5 personnes respectivement. On peut aussi louer la maison dans sa totalité, étant donné que les deux logements communiquent.</p>
						<div class="highlights">
							<h6>Soulignés</h6>
							<ul class="specific">
<li>Silence absolu au milieu de la nature</li>
								<li>Excursions à côté de la maison et routes organisées</li>
								<li>Matériel pour enfants</li>
								<li>Piscine climatisé privée</li>
								<li>Chambres privées complètement équipées</li>
							</ul>
</div>
						<div class="clearfix"></div>
						<div class="img">
							<img src="[base_url]img/About/4.jpg" alt="">
</div>
						<div class="clearfix"></div>
						<h2>Nous ne sommes tranquilles que lorsque nous nous voyons satisfaits</h2>
						<p>Expliquez-nous ce que vous nécessitez, ce que vous cherchez, ce qui vous manque et tous les doutes qui surgissent à fin de jouir à cent pour cent de votre séjour.</p>
						<p>Nous disposons de petites chaises hautes, berceaux, lits supplémentaires et autres matériels qui peuvent vous faciliter le séjour.</p>
					</div>
				</div>
			</div>
		</div>
	</section>    </section>