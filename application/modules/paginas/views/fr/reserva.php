
            
            
            <!-- WELCOME -->
<section id="content">
            <section id="content"><!-- PAGE HEADER -->
            	<div class="title-color">
			<div class="container">
				<h2 id="mce_48" class="mce-content-body">Réserve</h2>
				<ol class="breadcrumb">
<li><a href="<?= base_url('') ?>" id="mce_49" class="mce-content-body">Home</a></li>
					<li><a href="#" id="mce_50" class="mce-content-body">Logements</a></li>
					<li class="active">Réserve</li>
				</ol>
</div>
		</div>
		
		<!-- RESERVATION CONTENT -->
		<div class="container">
			<form id="room-information-form" method="post" action="<?= base_url('reservar/checkout') ?>">
			<div class="reservation-img">
				<!--<img class="img-responsive" src="<?= base_url() ?>img/reservation/1.jpg" alt="">-->
			</div>
			[msg_response]
			<div class="reser-tab">
				<div class="tabbable tabs-left">
					<div class="tl-nav col-xs-12 col-sm-12 col-md-4">
					<div class="easy-step">
						<img class="img-responsive" src="<?= base_url() ?>img/reservation/4step-img1.png" alt=""><h3 id="mce_54" class="mce-content-body">ÉTAPES PRINCIPALES</h3>
					</div>

                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#a" data-toggle="tab">
                                    <div class="number"><span>1</span></div>
                                    Sélectionnez les données
                                </a>
                            </li>
                            <li>
                                <a href="#b" data-toggle="tab">
                                    <div class="number"><span>2</span></div>
                                    Sélectionnez les logements
                                </a>
                            </li>
                            <li>
                                <a href="#c" data-toggle="tab">
                                    <div class="number"><span>3</span></div>
                                    Données de réservation
                                </a>
                            </li>
                            <li>
                                <a href="#d" data-toggle="tab">
                                    <div class="number"><span>4</span></div>
                                    Examiner et envoyer
                                </a>
                            </li>
                        </ul>
                        <p style="margin-top:25px;">* Pour les vacances, consultez Cal Prat <a href="mailto:info@calprat.net">INFO</a></p>
</div>
					<div class="tab-content col-sm-12 col-md-8">
						<div class="tab-pane active" id="a">
							<div class="row" id="booking-date-range-inline">
								<div class="col-xs-12 col-sm-6 col-md-6">
									<h6 id="mce_63" class="mce-content-body">DATE D'ENTRÉE</h6>
									<div class="form-group">
										<div class="input-group date">
											<input type="text" readonly="" [desde] placeholder="[startlabel]" name="desde" class="form-control">
											<span class="input-group-addon mce-content-body" id="mce_64">
												<span class="fa fa-calendar mce-content-body" id="mce_65">
													<br>
												</span>
											</span>
										</div>
									</div>
									<div class="main-cal">
										<div id="datetimepicker1-inline" class="check-in" name="startCalendar" value=''>
										</div>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6">
									<h6 id="mce_101" class="mce-content-body">DATE DE SORTIE</h6>
									<div class="form-group">
										<div class="input-group date">
											<input type="text" readonly="" [hasta] placeholder="[endlabel]" class="form-control" name="hasta">
											<span class="input-group-addon mce-content-body" id="mce_102">
												<span class="fa fa-calendar mce-content-body" id="mce_103">
													<br>
												</span>
											</span>
										</div>
									</div>
									<div class="main-cal">
										<div id="datetimepicker2-inline" class="check-out" name="endCalendar" value=''>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="b">
							<div class="row detail-type">
								<div class="col-xs-12 col-sm-12 col-md-7" id="habitacionContent">
									<h6 id="mce_139" class="mce-content-body">Logements</h6>
									[habitaciones]
								</div>
								<div class="col-xs-12 col-sm-12 col-md-5">
									
										<div class="form-group nrooms">
											<h6 id="mce_140" class="mce-content-body">Montant</h6>
											<input type="text" class="form-control" placeholder="1" value="1" readonly="">
</div>
										<div class="form-group rbudget">
											<h6 id="mce_141" class="mce-content-body">Prix</h6>
											<label>EUR</label>										
											<input type="text" name="total" id="precio" class="form-control" placeholder="" value="0" readonly="">
</div>
									
								</div>
							</div>
							<div class="row adult detail-type">
								<div class="col-xs-12 col-sm-12 col-md-6">
									<div class="row">
										
											<div class="form-group col-md-6">
												<h6 id="mce_142" class="mce-content-body">Núm adultes</h6>
												[adultos]
</div>
											<div class="form-group col-md-6">
												<!--<h6 id="mce_143" class="mce-content-body">Núm de nens</h6>-->
												[nens]
</div>
										
									</div>
								</div>
							</div>
							<div class="row detail-type desired">
								<div class="col-xs-12 col-sm-12 col-md-12">
									<label>Extras:</label>
									<label class="radio-inline">
									<input type="checkbox" name="extras" id="inlineRadio1" value="bresol"> Berceau
									</label> 
									<label class="radio-inline">
									<input type="checkbox" name="extras" id="inlineRadio2" value="supletori"> Petite chaise haute pour enfant
									</label>
								</div>
							</div>
							<div class="row detail-type desired">
								<div class="col-xs-11 col-sm-11 col-md-11">
									<label>Avez-vous été à Cal Prat avant?</label>
									<label class="radio-inline">
									<input type="radio" name="historia" id="inlineRadio5" value="SI">Ouais
									</label> 
									<label class="radio-inline">
									<input type="radio" name="historia" id="inlineRadio6" value="NO">Non
									</label>
								</div>
							</div>
							<div class="row detail-type">
								<div class="col-xs-11 col-sm-11 col-md-11">
									<label>Avez-vous besoin de quelque chose d'autre?</label>
									<textarea class="form-control" name="requerimiento" rows="3" placeholder="Avez-vous besoin de quelque chose d'autre?"></textarea>
</div>
							</div>
						</div>
						<div class="tab-pane" id="c">
							<div class="row detail-type2">
								<div class="col-xs-12 col-sm-4 col-md-5">
									<h6 id="mce_146" class="mce-content-body">Nom</h6>
									<input type="text" name="nombre" value="" class="form-control" placeholder="">
</div>
								<div class="col-xs-12 col-sm-6 col-md-5">
									<h6 id="mce_147" class="mce-content-body">Nom de famille</h6>
									<input type="text" name="apellido" class="form-control" placeholder="">
</div>
								<div class="col-xs-12 col-sm-2 col-md-2">
									<h6 id="mce_148" class="mce-content-body">titre</h6>
									<select id="get_value3" name="titulo">
										<option value="Sr">Monsieur</option>
										<option value="Sra">Madame</option>
										<option value="Tutor">Tuteur</option>
									</select>
</div>
							</div>
							<div class="row detail-type2">
								<div class="col-xs-12 col-sm-4 col-md-5">
									<h6 id="mce_149" class="mce-content-body">email</h6>
									<input type="email" name="email" class="form-control" placeholder="">
</div>
								<div class="col-xs-12 col-sm-8 col-md-7">
									<h6 id="mce_150" class="mce-content-body">Adresse</h6>
									<input type="text" name="direccion" class="form-control" placeholder="">
</div>
							</div>
							<div class="row detail-type2">
								<div class="col-xs-12 col-sm-4 col-md-5">
									<h6 id="mce_151" class="mce-content-body">ville</h6>
									<input type="text" name="ciudad" class="form-control" placeholder="">
</div>
								<div class="col-xs-12 col-sm-8 col-md-7">
									<div class="row">
										<div class="col-xs-12 col-sm-4 col-md-4">
											<h6 id="mce_152" class="mce-content-body">Province</h6>
											<input type="text" name="provincia" class="form-control" placeholder="">
</div>
										<div class="col-xs-12 col-sm-4 col-md-4">
											<h6 id="mce_153" class="mce-content-body">CP</h6>
											<input type="text" name="cp" class="form-control" placeholder="">
</div>
										<div class="col-xs-12 col-sm-4 col-md-4">
											<h6 id="mce_154" class="mce-content-body">Pays</h6>
											<input type="text" name="pais" class="form-control" placeholder="">
</div>
									</div>
								</div>
							</div>
							<div class="row detail-type2">
								<div class="col-xs-12 col-sm-4 col-md-5">
									<h6 id="mce_155" class="mce-content-body">téléphone</h6>
									<input type="text" name="telefono" class="form-control" placeholder="">
</div>
								<div class="col-xs-12 col-sm-6 col-md-5">
									<h6 id="mce_156" class="mce-content-body">whatsapp</h6>
									<input type="text" name="whatsapp" class="form-control" placeholder="">
</div>
							</div>
						</div>
						<div class="tab-pane" id="d">
							<ul>
<li>
									<span class="des mce-content-body" id="mce_157">Nom et nom de famille:</span>
									<span class="detail mce-content-body" id="nombre-label">no especificat</span>
								</li>
								<li>
									<span class="des mce-content-body" id="mce_159">email :</span>
									<span class="detail mce-content-body" id="email-label">non spécifié</span>
								</li>
								<li>
									<span class="des mce-content-body" id="mce_161">adresse:</span>
									<span class="detail mce-content-body" id="direccion-label">non spécifié</span>
								</li>
								<li>
									<span class="des mce-content-body" id="mce_163">téléphone:</span>
									<span class="detail mce-content-body" id="telefono-label">non spécifié</span>
								</li>
								<li>
									<span class="des mce-content-body" id="mce_165">Whatsapp:</span>
									<span class="detail mce-content-body" id="whatsapp-label">non spécifié</span>
								</li>
								<li>
									<span class="des mce-content-body" id="mce_167">Logement:</span>
									<span class="detail mce-content-body" id="habitacion-label">non spécifié</span>
								</li>
								<li>
									<span class="des mce-content-body" id="mce_169">Quantité:</span>
									<span class="detail mce-content-body" id="mce_170">1</span>
								</li>
								<li>
									<span class="des mce-content-body" id="mce_171">adultes / enfants:</span>
									<span class="detail mce-content-body" id="personas-label">no especificat</span>
								</li>
								<li>
									<span class="des mce-content-body" id="mce_173">Entrée / sortie:</span>
									<span class="detail mce-content-body" id="fecha-label">non spécifié</span>
								</li>
								<li>
									<span class="des mce-content-body" id="mce_175">Extra:</span>
									<span class="detail mce-content-body" id="extras-label">non spécifié</span>
								</li>
								<li>
									<span class="des mce-content-body" id="mce_177">Avez-vous été avant:</span>
									<span class="detail mce-content-body" id="historia-label">non spécifié</span>
								</li>
								<li>
									<span class="des mce-content-body" id="mce_181">Autre chose?:</span>
									<span class="detail mce-content-body" id="observacion-label">non spécifié</span>
								</li>

								<li>
									<span class="des mce-content-body" id="mce_181">Prix:</span>
									<span class="detail mce-content-body" id="precioLabel">non spécifié</span>
								</li>

								<li>
									<span class="des mce-content-body" id="mce_181">Abonnement:</span>
									<span class="detail mce-content-body" id="abonoLabel">non spécifié</span>
								</li>
							</ul>
							<input type="hidden" name="abono" id="abono" value="0">
							<input type="hidden" id="idioma" name="idioma" value="<?= $_SESSION['lang'] ?>">
							<input type="hidden" name="dias" id="dias" value="[dias]" class="duration">
							[oferta]
							<label for=""><input id="condiciones" type="checkbox" name="condiciones" value="1"> J'ai lu et je les accepte <a href="<?= base_url() ?>p/condicions" target="_blank">Termes et conditions.* </a></label><br/>
							<label for=""><input type="checkbox" name="not" value="1"> Je ne veux pas recevoir d'informations de CalPrat.</label><br/>
							<label for=""><input id="politicas" type="checkbox" name="politicas" value="1"> J'ai lu et je les accepte <a href="<?= base_url() ?>p/avis-legal" target="_blank">politique de confidentialité* </a></label><br/>

							<button class="btn btn-default btn-availble" type="submit">envoyer</button>
						</div>
					</div>
				</div>
			</div>
		</form>
		</div>
	</section>
</section>