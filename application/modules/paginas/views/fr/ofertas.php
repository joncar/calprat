
		<div class="special-offer">
			<div class="container">
				<div class="col-xs-12 col-sm-8 col-md-8 col-centered center-block">
					<div class="row">
						<div class="title">
							<h6>special offers</h6>
							<h1>Exclusive Offers for Our Guests</h1>
							<div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
								<span><img class="img-responsive" src="images/cap.png" alt=""></span>
							</div>
						</div>
					</div>
				</div>
				<div aria-multiselectable="true" role="tablist" id="accordion" class="panel-group">

					[foreach:ofertas]

					<div class="panel panel-default">
						<div id="heading[id]" role="tab" class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion" href="#collapse[id]" aria-expanded="true" aria-controls="collapse[id]">
								[habitacion_nombre]
								</a>
								<span class="viewdetail1">view details +</span>
							</h4>
						</div>

						<div id="collapse[id]" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading[id]">
							<div class="panel-body">
								<div class="row">
									<div class="col-xs-12 col-sm-7 col-md-7">
										<img class="img-responsive" src="[fondo]" alt="">
									</div>
									<div class="col-xs-12 col-sm-5 col-md-5">
										<p>[detalle]</p>
										<a href="#" class="btn btn-default">Reservar</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					[/foreach]

				</div>
			</div>
		</div>