<section id="content" class="business-main">
	
		<!-- PAGE HEADER -->
		<div class="title-color">
			<h2>Avís legal</h2>
			<ol class="breadcrumb">
				<li><a href="<?= site_url() ?>">Inici</a></li>
				<li class="active"><a href="#">Avís legal</a></li>				
			</ol>
		</div>
		
		<!-- MAIN CONTENT -->
		<div class="container">
			<div class="col-xs-12 col-sm-9 col-md-9 col-centered center-block">
				<div class="row">
					<div class="title">
						<h6>Avís legal</h6>
						<h1>Protegim les teves dades</h1>
						<div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
							<span><img class="img-responsive" src="<?= base_url() ?>img/cap.png" alt=""></span>
						</div>
						<p>Cal Prat es preocupa per la seguretat de les dades que els nostres clients ens proporciones i per aixì segueix la nova legislació de protecció de dades.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="business-detail" style="margin-bottom:39px">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<h6>Qui és el responsable del tractament de les seves dades personals?</h6> <br>
<p>M. Alba Escolà Montaner és el responsable del tractament de les dades personals que vostè ens proporciona i es responsabilitza de les referides dades personals d'acord amb la normativa aplicable en matèria de protecció de dades.
<br>M. Alba Escolà Montaner  amb domicili social a carrer El Soler, s/n, Calonge de Segarra, España; amb CIF número 79276286-R  i inscrita en el Registre de Fundacions privades de la Generalitat de Catalunya amb el numero PCC-000384 i PCC-000385.
<br>E-Mail de contacte: info@calprat.net
</p>
<h6>On emmagatzemem les seves dades?</h6> <br>
<p>Les dades que recopilem sobre vostè s'emmagatzemen dins de l'Espai Econòmic Europeu («EEE»). Qualsevol transferència de les seves dades personals serà realitzada de conformitat amb les lleis aplicables.
</p>
<h6>¿A qui comuniquem les seves dades?</h6> <br>
<p>Les seves dades poden ser compartides per l'empresa de Cal Prat. Mai passem, venem ni intercanviem les seves dades personals amb terceres parts. Les dades que s'enviïn a tercers, s'utilitzaran únicament per oferir-li a vostè els nostres serveis. Li detallarem les categories de tercers que accedeixen a les seves dades per a cada activitat de tractament específica.
</p>
<h6>¿Quina és la base legal per al tractament de les seves dades personals? </h6> <br>
<p>En cada tractament específic de dades personals recopilades sobre vostè, li informarem si la comunicació de dades personals és un requisit legal o contractual, o un requisit necessari per subscriure un contracte, i si està obligat a facilitar les dades personals, així com de les possibles conseqüències de no facilitar tals dades.
</p>
<h6>¿Quins són els seus drets? </h6> <br>
<h6>Dret d’accés: </h6> <br>
<p>Qualsevol persona té dret a obtenir confirmació sobre si Cal Prat estem tractant dades personals que els concerneixin, o no. Pot contactar a info@calprat.net que li remetrà les dades personals que tractem sobre vostè per email.
</p>
<h6>Dret de portabilitat: </h6> <br>
<p>Sempre que CalPrat processi les seves dades personals a través de mitjans automatitzats sobre la base del seu consentiment o a un acord, vostè té dret a obtenir una còpia de les seves dades en un format estructurat, d'ús comú i lectura mecànica transferida al seu nom o a un tercer. En ella s'inclouran únicament les dades personals que vostè ens hagi facilitat.
</p>
<h6>Dret de rectificació:  </h6> <br>
<p>Vostè té dret a sol·licitar la rectificació de les seves dades personals si són inexactes, incloent el dret a completar dades que figurin incomplets.
</p>
<h6>Dret de supressió:  </h6> <br>
<p>Vostè té dret a obtenir sense dilació indeguda la supressió de qualsevol dada personal seva tractada per CalPrat a qualsevol moment, excepte en les següents situacions: <br> *té un deute pendent amb Cal Prat, independentment del mètode de pagament
<br>* se sospita o està confirmat que ha utilitzat incorrectament els nostres serveis en els últims quatre anys
<br>* ha contractat algun servei pel que conservarem les seves dades personals en relació amb la transacció per normativa comptable.

</p>
<h6>Dret d’oposició al tractament de dades en base a l’interés legítim: </h6> <br>
<p>Vostè té dret a oposar-se al tractament de les seves dades personals sobre la base de l'interès legítim de Cal Prat. Cal Prat no seguirà tractant les dades personals tret que puguem acreditar motius legítims imperiosos per al tractament que prevalguin sobre els seus interessos, drets i llibertats, o bé per a la formulació, l'exercici o la defensa de reclamacions.
</p>
</p>
<h6>Dret d’oposició al marketing directe: </h6> <br>
<p>Vostè té dret a oposar-se al màrqueting directe, incloent l'elaboració de perfils realitzada per a aquest màrqueting directe.<br>
Pot desvincular-se del màrqueting directe en qualsevol moment de les següents maneres: *seguint les indicacions facilitades a cada correu de màrqueting

</p>
</p>
<h6>Dret a presentar una reclamació davant d’una autoritat de control: </h6> <br>
<p>Si vostè considera que Cal Prat tracta les seves dades d'una manera incorrecta, pot contactar amb nosaltres. També té dret a presentar una queixa davant l'autoritat de protecció de dades competent.
</p>
<h6>Dret a limitació en el tractament:  </h6> <br>
<p>Vostè té dret a sol·licitar que Cal Prat limiti el tractament de les seves dades personals en les següents circumstàncies:
<br>* si vostè s'oposa al tractament de les seves dades sobre la base de l'interès legítim de Cal Prat. Cal Prat haurà de limitar qualsevol tractament d'aquestes dades a l'espera de la verificació de l'interès legítim.
<br>* si vostè reclama que les seves dades personals són incorrectes, Cal Prat ha de limitar qualsevol tractament d'aquestes dades fins que es verifiqui la precisió dels mateixos.
<br>* si el tractament és il·legal, vostè podrà oposar-se al fet que s'eliminin les dades personals i, en el seu lloc, sol·licitar la limitació del seu ús.
<br>* si Cal Prat ja no necessita les dades personals, però vostè els necessita per a la formulació, l'exercici o la defensa de reclamacions.

</p>
<h6>Exercici de drets:  </h6> <br>
<p>Ens prenem molt de debò la protecció de dades i, per tant, comptem amb personal de servei al client dedicat que s'ocupa de les seves sol·licituds en relació amb els drets abans esmentats. Sempre pot posar-se en contacte amb ells a: info@calprat.net

</p>
						</div>					
				</div>
				
			</div>
		</div>
		
	</section>