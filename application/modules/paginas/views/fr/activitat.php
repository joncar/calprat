
    <section id="content" class="gallery-main">
    
        <div class="title-color">
        <h2>
Activités à Cal Prat</h2>
        <ol class="breadcrumb">
    <li><a href="<?= base_url() ?>">Inici</a></li>
    <li><a href="#">Activités et excursions</a></li>
<!--            <li><a href="--><?//= base_url() ?><!--">Activitats i Excursions</a></li>-->
            <!--                <li class="active"><a>Activities &amp; Excursions</a></li>-->
        </ol>
    </div>

    <!-- ACTIVITIES CONTENT -->
    <div class="activitie-block">
        <div class="container">
            <div class="col-xs-12 col-sm-12 col-md-8 col-centered center-block">
                <div class="row">
                    <div class="title">
                        <h6>VOUS NE SAVEZ QUE FAIRE ?
</h6>
                        <h1>Activités et excursions</h1>
                        <div class="col-xs-6 col-sm-4 col-md-4 cap col-centered center-block">
                            <span><img class="img-responsive" src="[base_url]img/cap.png" alt=""></span>
                        </div>
                        <p>Voici un petit recueil de nombreuses activités que vous pourrez faire chez Prat et alentours. Il y a mille lieux, mille excursions qui vous enchanteront!</p>
                    </div>
                </div>
            </div>

            <div class="" style=" padding: 0px 0 0 0;">

                




                





            </div>
        </div>
        <div class="gallery-block">
            <div class="main-room">
                <ul>
                    <li class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                        <div class="img">
                            <img src="<?= base_url() ?>theme/images/Gallery/1.jpg" class="img-responsive" alt="">
                            <div class="mr-head">
                                <h3>PROFITEZ DE ENVIRONEMENT</h3>
                                <div class="col-md-6 cap col-centered center-block">
                                    <span><img class="img-responsive" src="<?= base_url() ?>theme/images/cap.png" alt=""></span>
                                </div>
                                <p>Chez Prat à part de vous reposer et jouir du silence il y a beaucoup de choses à faire.</p>
                                <p>Routes, excursions, visites…</p>
                            </div>
                        </div>
                    </li>
                    [foreach:agenda]
                        <li class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <div class="mr-inner">
                                <img src="[foto]" class="img-responsive" alt="">
                                <div class="mr-overlay">
                                    <h2>[titulo]</h2>
                                    
                                    <p>[explicacion]</p>
                                    <a class="room-detail" href="[enlace]" target="_new">Voir</a>
                                </div>
                            </div>
                        </li>
                    [/foreach]
                </ul>
            </div>
        </div>
    </section>