<!-- WELCOME -->
<section id="content" class="activitie-main"><!-- PAGE HEADER --><div class="title-color">
<h2>Agenda</h2>
<ol class="breadcrumb">
	<li><a href="<?= base_url() ?>">Home</a></li>
	<li><a href="#">Agenda</a></li>
	<!--				<li class="active"><a>Activities &amp; Excursions</a></li>-->
</ol>
</div>

<!-- ACTIVITIES CONTENT -->
<div class="activitie-block">
<div class="container">
	<div class="col-xs-12 col-sm-12 col-md-8 col-centered center-block">
		<div class="row">
			<div class="title">
				<h6>VOUS NE SAVEZ QUE FAIRE?</h6>
				<h1>Agenda</h1>
				<div class="col-xs-6 col-sm-4 col-md-4 cap col-centered center-block">
					<span><img class="img-responsive" src="[base_url]img/cap.png" alt=""></span>
				</div>
				<p>Voici un petit recueil de nombreuses activités que vous pourrez faire chez Prat et alentours. Il y a mille lieux, mille excursions qui vous enchanteront!</p>
			</div>
		</div>
	</div>
	[foreach:eventos]
	<div class="activities">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-6 no-padding">
				<a href="[web]">
					<div class="img">
						<img class="img-responsive" src="[foto_grande]" alt="">
						<span class="btn btn-default">détails</span>
					</div>
				</a>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6 no-padding">
				<div class="right-activ">
					<h2>[titulo]</h2>
					
					<p>[descripcion]</p>
					
					<div class="include">
						<h4>Où cela se produit?</h4>
						[tags]
					</div>
					<p class="botonazul" style="text-align: right">
						<a href="[web]" class="btn btn-default">Aller sur le web</a>
					</p>
					<div class="activ-botpan">
						<div class="active-left">
							<ul>
								<!-- 
<li><a href="<?= base_url() ?>"><i class="fa fa-cutlery"></i></a></li>
								<li><a href="<?= base_url() ?>"><i class="fa fa-glass"></i></a></li>
								<li><a href="<?= base_url() ?>"><i class="fa fa-wheelchair"></i></a></li>
 -->
							</ul>
						</div>
						<div class="active-right">
							<!-- <img src="[base_url]img/activities/6.png" alt=""> -->
						</div>
					</div>
				</div>
				<div class="visitor">
					<span class="calender"><i class="fa fa-calendar-o"></i>[fecha]</span>
					<!-- 
<span class="comment"><i class="fa fa-clock-o"></i>[hora]</span>
					<span class="views"><i class="fa fa-bookmark"></i>[precio]</span>
 -->
				</div>
			</div>
		</div>
	</div>
	[/foreach]
	
	
	
	<div class="activitie-cal">
		<div class="col-xs-12 col-sm-12 col-md-8 col-centered center-block">
			<div class="row">
				<div class="title">
					<h6>CALENDRIER D’ACTIVITÉS</h6>
					<h1>Prochainement...</h1>
					<div class="col-xs-6 col-sm-4 col-md-4 cap col-centered center-block">
						<span><img alt="" src="[base_url]img/cap.png" class="img-responsive"></span>
					</div>
					<p>Vous trouverez ici les évènements qui se célébreront ces prochains jours aux environs et à proximité de chez Prat</p>
				</div>
			</div>
		</div>
		
	</div>

	<div class="gallery-block">
            <div class="main-room">
                <ul>
                    <li class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                        <div class="img">
                            <img src="<?= base_url() ?>theme/images/Gallery/1.jpg" class="img-responsive" alt="">
                            <div class="mr-head" style="text-align: center">
                                <h3>Calendrier<br>d'activités</h3>
                                <div class="col-md-6 cap col-centered center-block">
                                    <span><img class="img-responsive" src="<?= base_url() ?>theme/images/cap.png" alt=""></span>
                                </div>
                                <p>Profitez-les...</p>
                                <p></p>
                            </div>
                        </div>
                    </li>
                    [foreach:activitats]
                        <li class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <div class="mr-inner">
                                <img src="[foto]" class="img-responsive" alt="" style="">
                                <div class="mr-overlay">
                                    <h2>[titulo]</h2>
                                    <p>[fecha]</p>
                                    <p class="botonazul">
                                    	<a href="[web]" class="btn btn-default">Aller sur le web</a>
                                    </p>
                                </div>
                            </div>
                        </li>
                    [/foreach]
                </ul>
            </div>
        </div>
</div>
</div>
</section>