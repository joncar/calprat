<section id="content" class="spa-main"><!-- PAGE HEADER --><div class="title-color" style="position: relative;">
            <h2 id="mce_40" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Salle de jeux</h2><input type="hidden" name="mce_40">
            <ol class="breadcrumb">
                <li><a href="./index.html" id="mce_41" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Home</a><input type="hidden" name="mce_41"></li>
                <li class="active">Salle de jeux</li>
            </ol>
            <input type="hidden" name="mce_43"><input type="hidden" name="mce_44"><input type="hidden" name="mce_45"></div>

        <!-- SPA -->
        <div class="spa-block" style="position: relative;">
            <div class="db-image">
                <div class="dbi-inner" style="height:100%; background:url(<?= base_url() ?>img/spa/22.jpg) no-repeat center center">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="spa-right">
                    <h1 id="mce_46" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">L’espace des plus petits.</h1><input type="hidden" name="mce_46">
                    <h6 id="mce_47" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Une salle où l’on peut se divertir tout en jouant.</h6><input type="hidden" name="mce_47">
                    <p id="mce_48" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">A coté de la cour vous trouverez l’accès à la salle de jeux, une salle pleine de jouets pour les différents âges.</p><input type="hidden" name="mce_48">
                    <p id="mce_49" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Vous pouvez être tranquilles, pendant que vous déjeunez , les enfants pourront jouer tout en étant près de vous.</p><input type="hidden" name="mce_49">
                    <h3 id="mce_50" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Jeux</h3><input type="hidden" name="mce_50">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <ul class="specific">
                                <li>Baby-foot</li>
                                <li>Landau, petit lit et poupées</li>
                                <li>Petite cuisine et outils pour faire le repas</li>
                                <li>Garage de voitures</li>
                                <li>Deux trottinettes et trois casques pour enfants</li>
                                <li>3 tricycles</li>
                                <li>2 motos Molto</li>
                                <li>2 jeux de patins</li>
                                <li>Contes</li>
                            </ul>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <ul class="specific">
                                <li>Ours de peluche</li>
                                <li>Tableaux aimantés avec des lettres</li>
                                <li>Jeux d’échecs</li>
                                <li>Jeux de petits chevaux</li>
                                <li>Raquettes badminton</li>
                                <li>Billes</li>
                            </ul>
                        </div>
                    </div>

                    <h3 id="mce_50" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Photos de la Salle de Jeux</h3>
                    <div class="row" uk-lightbox>
                        [foreach:galeria_joc]
                            <div class="col-xs-12 col-sm-3" style="margin-top: 29px;"><a href="[foto]"><img src="[foto]" alt="" style="width:100%"></a></div>
                        [/foreach]
                    </div>
                </div>
                
            </div>
        </div>
</section>  