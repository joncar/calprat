<!-- WELCOME -->
<section id="content" class="about-main"><!-- PAGE HEADER -->
<div class="title-color">
    <h2>Allotjaments</h2>
    <ol class="breadcrumb">
        <li><a href="<?= base_url() ?>">Inici</a></li>
        <li><a href="#">Allotjaments</a></li>
    </ol>
</div>
<!-- ROOM INFO -->
<div class="container room-col room-three">
    <div class="col-xs-12 col-sm-10 col-md-6 col-centered center-block">
        <div class="row">
            <div class="title">
                <h6 id="mce_54" class="mce-content-body">Allotjaments</h6>
                <h1 id="mce_55" class="mce-content-body">Espais amb molt de confort</h1>
                <div class="col-xs-12 col-sm-4 col-md-4 cap col-centered center-block">
                    <span id="mce_56" class="mce-content-body"><img class="img-responsive" src="<?= base_url() ?>img/cap.png" alt=""></span>
                </div>
                <p id="mce_57" class="mce-content-body">Descobreix els racons de Cal Prat. Racons on podràs passar grans moments, grans caps de setmana i estades en família i amics.</p>
            </div>
        </div>
    </div>
    <div class="three-col">
        <div id="rooms-mason" class="isotope rm-mason-3col">
            <div>
                [foreach:list]
                <div class="rm-item item-room">
                    <div class="item">
                        <img alt="Owl Image" src="[foto]">
                        <div class="figcaption">
                            <h3 id="mce_58" class="mce-content-body">[habitacion_nombre]</h3>
                            <div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
                                <span id="mce_59" class="mce-content-body">
                                    <img alt="" src="<?= base_url() ?>img/cap.png" class="img-responsive">
                                </span>
                            </div>
                            <ul>
                                <li>
                                    <span class="des mce-content-body" id="mce_60">llits</span>
                                    <span class="detail mce-content-body" id="mce_61">[camas]</span>
                                </li>
                                <li>
                                    <span class="des mce-content-body" id="mce_62">ocupació</span>
                                    <span class="detail mce-content-body" id="mce_63">[max_personas]</span>
                                </li>
                                <li>
                                    <span class="des mce-content-body" id="mce_68">preu des de</span>
                                    <span class="detail mce-content-body" id="mce_69">[precio_desde] €</span>
                                </li>
                                <li>
                                    <span class="des mce-content-body" id="mce_70">video</span>
                                    <span class="detail mce-content-body" id="mce_71">
                                        <a href="https://vimeo.com/25598691" class="prettyPhoto mce-content-body" id="mce_72">
                                            <i class="fa fa-video-camera"></i>
                                        </a>
                                    </span>
                                </li>
                            </ul>
                            <div class="dual-btns">
                                <a href="[link]" class="btn btn-default pull-left mce-content-body" id="mce_73">detalls</a>
                                <a href="<?= base_url() ?>iniciar-reserva" class="btn btn-default pull-right mce-content-body" id="mce_74">reseva ara</a>
                            </div>
                        </div>
                    </div>
                    <div class="single-detail">
                        <a href="[link]"><span class="plus mce-content-body" id="mce_75">+</span>
                    <h3 id="mce_76" class="mce-content-body">[habitacion_nombre]</h3></a>
                </div>
            </div>
            [/foreach]
        </div>
    </div>
</div>
</div>
</section>