
    <section id="content" class="spa-main">
        <!-- PAGE HEADER -->
        <div class="title-color" style="position: relative;">
            <h2 id="mce_40" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">El pati, BBQ</h2>
            <input type="hidden" name="mce_40">
            <ol class="breadcrumb">
                <li><a href="./index.html" id="mce_41" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Inici</a><input type="hidden" name="mce_41"></li>
                <li class="active"><a href="./spa.html" id="mce_42" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">El pati, BBQ</a><input type="hidden" name="mce_42"></li>
                <li class="active">El pati</li>
            </ol>
            <input type="hidden" name="mce_43"><input type="hidden" name="mce_44"><input type="hidden" name="mce_45">
        </div>
        <!-- SPA -->
        <div class="spa-block" style="position: relative;">
            <div class="db-image">
                <div class="dbi-inner" style="height:100%; background:url(<?= base_url() ?>img/spa/9.jpg) no-repeat center center">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="spa-right">
                    <h1 id="mce_46" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Un espai perfecte per a les teves trobades</h1>
                    <input type="hidden" name="mce_46">
                    <h6 id="mce_47" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">La casa disposa d'un ampli pati on podreu prendre el sol, dinar, descansar....</h6>
                    <input type="hidden" name="mce_47">
                    <p id="mce_48" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">
                        Trobareu en aquest espai un lloc on reunir-vos per dinar gràcies a una barbacoa (amb tot el material necessari per utilitzar-la), una pèrgola per amenitzar el sol, una taula i cadires, tumbones, ....
                    </p>
                    <input type="hidden" name="mce_48">
                    <p id="mce_49" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">
                        
                    </p>
                    <input type="hidden" name="mce_49">
                    <h3 id="mce_50" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Què tenim en aquest espai</h3>
                    <input type="hidden" name="mce_50">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <ul class="specific">
                                <li>Barbacoa amb carbó, encenedor, pinces, draps de cuina, recollidor de cendres....</li>
                                <li>Taula, cadires i bancs</li>
                                <li>Pèrgola per tapar del sol</li>
                                
                            </ul>
                        </div>
                        <!-- 
<div class="col-xs-12 col-sm-6 col-md-6">
                            <ul class="specific">
                                <li>Zona ajardinada</li>
                                <li>fanals per la nit</li>
                                <li>Nevera aprop</li>
                                <li>Lavabo</li>
                                <li>Accés directe a la sala de jocs</li>
                            </ul>
                        </div>
 -->
                    </div>
                    <h3 id="mce_50" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Imatges del Pati</h3>
                    <div class="row" uk-lightbox>
                        [foreach:galeria_patio]
                            <div class="col-xs-12 col-sm-3" style="margin-top: 29px;"><a href="[foto]"><img src="[foto]" alt="" style="width:100%"></a></div>
                        [/foreach]
                    </div>
                </div>
            </div>
    </section>