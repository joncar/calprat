<div class="top-slider" style="display: none">
    <div id="top-slider">
        <ul class="slides">
            [foreach:banner]
            <li>
                <img src="[foto]" alt="">
                <div class="flex-caption">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 id="mce_91" class="mce-content-body mce-edit-focus" >[titulo]</h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <p id="mce_92">[subtitulo]</p>
                        </div>
                    </div>
                </div>
            </li>
            [/foreach]
        </ul>
        <ul class="flex-direction-nav">
            <li><a class="flex-prev mce-content-body" id="mce_99" ><i class="fa fa-angle-left"></i></a></li>
            <li><a class="flex-next mce-content-body" id="mce_100" ><i class="fa fa-angle-right"></i></a></li>
        </ul>
    </div>
</div>
<!-- WELCOME -->
<section id="content">
    <div class="welcome" style="position: relative;">
        <div class="container">
            <div class="col-xs-12 col-sm-12 col-md-6 col-centered center-block">
                <div class="row">
                    <div class="title">
                        <h6 id="mce_101">Benvinguts a</h6>
                        <h3 id="mce_102">la casa rural Cal Prat</h3>
                        <div class="col-xs-6 col-sm-4 col-md-4 cap col-centered center-block">
                            <span id="mce_103"><img alt="" src="[base_url]img/cap.png" class="img-responsive"></span>
                        </div>
                        <p id="mce_104">Cal Prat és una antiga casa de pagès del segle XVIII situada al petit nucli d’El Soler, dins el terme municipal de Calonge de Segarra a l’Alta Anoia, just a tocar de les comarques de La Segarra i El Solsonès</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="left-welcome">
                        <h2 id="mce_105">El lloc perfecte per fer una escapada</h2>
                        <p id="mce_106">La vostra estada a Cal Prat us permetrà gaudir-ne tot imaginant-vos com ha estat la vida de la nostra pagesia en els darrers segles, i podreu fruir del paisatge i la tranquil·litat del món rural d’avui.</p>
                        <p id="mce_107">Una acurada restauració ens ha permès  conservar totes les estances i elements típics d’una antiga casa de pagès com són el celler, el cup del vi, l’estable, el forn de pa, la llar de foc, etc</p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="right-welcome">
                        <div id="promovid">
                            <div class="custom-th">
                                <img src="[base_url]img/Homepage_Resort_INDEX/video.png" class="img-responsive" alt="">
                            </div>
                            <div id="thevideo" style="display:none; padding: 0px;">                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ACCOMODATION -->
    <div class="accomodation" style="position: relative;">
        <div class="container">
            <div class="col-xs-12 col-sm-12 col-md-6 col-centered center-block">
                <div class="row">
                    <div class="title">
                        <h6 id="mce_111">Instal.lacions</h6>
                        <h3 id="mce_112">Allotjaments i espais</h3>
                        <div class="col-xs-6 col-sm-4 col-md-4 cap col-centered center-block">
                            <span id="mce_113"><img alt="" src="[base_url]img/cap.png" class="img-responsive"></span>
                        </div>
                        <p id="mce_114">El Forn i El Celler de Cal Prat, així s’anomenen els nostres allotjaments per la particularitat que caracteritza a cadascun, tenen una capacitat de fins a 8 i 5 persones respectivament. També es pot llogar la casa sencera, ja que els dos allotjaments es poden comunicar.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-room">
            <ul>
                <li class="col-xs-12 col-sm-6 col-md-3">
                    <div class="mr-inner">
                        <img src="[base_url]img/Homepage_Resort_INDEX/room-img1.jpg" class="img-responsive" alt="">
                        <div class="mr-overlay">
                            <h2 id="mce_115">Casa teva<br>13 persones</h2>
                            <p id="mce_116">Podeu gaudir de total privacitat llogant la casa sencera. <br> Un espai ampli per famílies i colles d'amics.</p>
                            <a class="room-detail mce-content-body" href="[base_url]habitacion/1-tota-la-casa" id="mce_117" >Més informació<i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </li>
                <li class="col-xs-12 col-sm-6 col-md-3">
                    <div class="mr-inner">
                        <img src="[base_url]img/Homepage_Resort_INDEX/room-img2.jpg" class="img-responsive" alt="">
                        <div class="mr-overlay">
                            <h2 id="mce_118">El forn<br>8 persones</h2>
                            <p id="mce_119">El Forn amb capacitat per a 8 persones, ocupa la cara nord de la casa.<br><br>
                                </p>
                            <a class="room-detail mce-content-body" href="[base_url]habitacion/2-el-form" id="mce_120" >Més informació<i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </li>
                <li class="col-xs-12 col-sm-6 col-md-3">
                    <div class="mr-inner">
                        <img src="[base_url]img/Homepage_Resort_INDEX/room-img3.jpg" class="img-responsive" alt="">
                        <div class="mr-overlay">
                            <h2 id="mce_121">El celler<br>5 persones</h2>
                            <p id="mce_122">El Celler, amb capacitat per a 5 persones, ocupa la cara sud de la casa.<br><br>
                                
                                </p>
                            <a class="room-detail mce-content-body" href="[base_url]habitacion/3-el-celler" id="mce_123" >Més informació<i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </li>
                <li class="col-xs-12 col-sm-6 col-md-3">
                    <div class="mr-inner">
                        <img src="[base_url]img/Homepage_Resort_INDEX/room-img4.jpg" class="img-responsive" alt="">
                        <div class="mr-overlay">
                            <h2 id="mce_124">La piscina<br>privada</h2>
                            <p id="mce_125">Durant  la vostra estada, volem que pugueu  trobar allò que hi busqueu: silenci, tranquil·litat, relaxament…<br><br>
                                </p>
                            <a class="room-detail mce-content-body" href="[base_url]p/la_piscina.html" id="mce_126" >Més informació<i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <input name="mce_127" type="hidden"><input name="mce_128" type="hidden"></div>
        <!-- ROOM OFFER -->
        <div class="room-offer" style="position: relative;">
            <div class="container">
                <h3 id="mce_130">TOTES LES ESTANCES OFEREIXEN</h3><input name="mce_130" type="hidden">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1">
                        <ul class="specific">
                            <li>Calefacció</li>
                            <li>Llar de foc (llenya inclosa)</li>
                            <li>Televisió</li>
                            <li>Wifi</li>
                            <li>Llençols i roba de llit</li>
                            <li>Tovalloles i tovalloles per a la piscina</li>
                            <li>Microones/ Nevera</li>
                            <li>Rentaplats / Rentadora</li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1">
                        <ul class="specific">
                            <li>Cafetera Nespresso / Italiana</li>
                            <li>Bressol / Trona / Barana llit 90 cm</li>
                            <li>Farmaciola</li>
                            <li>Jocs de taula</li>
                            <li>DVD / Reproductor CD</li>
                            <li>Material per a la neteja de cuina</li>
                            <li>Parament per la taula</li>
                        </ul>
                    </div>
                </div>
            </div>
            <input name="mce_131" type="hidden"><input name="mce_132" type="hidden"><input name="mce_133" type="hidden"></div>
            <div class="home-gal" style="position: relative;">
                <div class="dining-block dual-slider row">
                    <div class="col-md-7 padding-right-none">
                        <div id="gal-slider1" class="flexslider">
                            <ul class="slides">
                                <li><img class="img-responsive" src="[base_url]img/Homepage_Resort_INDEX/home-gal-img1.png" alt=""></li>
                            </ul>
                            <!--<a class="flex-prev1 mce-content-body" id="mce_134" ><i class="fa fa-chevron-left"></i></a><input name="mce_134" type="hidden">
                            <a class="flex-next1 mce-content-body" id="mce_135" ><i class="fa fa-chevron-right"></i></a><input name="mce_135" type="hidden">-->
                        </div>
                    </div>
                    <div class="col-md-5 padding-left-none">
                        <ul class="gal-nav1">
                            <li class="flex-active">
                                <div>
                                    <h2 id="mce_136">Gaudeix amb els teus</h2><input name="mce_136" type="hidden">
                                    <p id="mce_137">Cal Prat es caracteritza per la tranquil·litat del seu entorn, envoltada de naturalesa, ideal per gaudir d'una escapada amb la família o amics.</p><input name="mce_137" type="hidden">
                                    <p id="mce_138">No obstant això, hi ha molts llocs interessants per visitar en tota la zona, i que segur no us deixaran indiferents.</p><input name="mce_138" type="hidden">
                                    <a href="[base_url]p/activitat.html" class="btn btn-default mce-content-body" id="mce_139" >Detalls</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- SPA -->
                <div class="spa-block dual-slider row">
                    

                    <div class="hidden-xs col-md-5 padding-right-none">
                        <ul class="gal-nav2">
                            <li class="flex-active">
                                <div>
                                    <h2 id="mce_152">Mimeu-vos a la piscina</h2><input name="mce_152" type="hidden">
                                    <p id="mce_153"></p><input name="mce_153" type="hidden">
                                    <p id="mce_154">A Cal Prat estem especialitzats en un Turisme Rural familiar i per parelles, i la nostra principal preocupació es la d'oferir als nostres clients descans i relaxació lluny del ritme i soroll de la ciutat. En un entorn natural molt tranquil es troba la nostre Casa Rural amb el Spa d’ús exclusiu, un dels tresors de Cal Prat.</p><input name="mce_154" type="hidden">
                                    <a href="[base_url]p/la_piscina.html" class="btn btn-default mce-content-body" id="mce_155" >Detalls</a><input name="mce_155" type="hidden">
                                </div>
                            </li>
                        </ul>
                    </div>
                    

                    <div class="col-md-7 padding-left-none">
                        <div id="gal-slider2" class="flexslider">
                            <ul class="slides">
                                <li><img class="img-responsive" src="[base_url]img/Homepage_Resort_INDEX/home-gal-img2.jpg" alt=""></li>                                
                            </ul>
                            <!--<a class="flex-prev2 mce-content-body" ><i class="fa fa-chevron-left"></i></a><input name="mce_168" type="hidden">
                            <a class="flex-next2 mce-content-body" ><i class="fa fa-chevron-right"></i></a><input name="mce_169" type="hidden">-->
                        </div>
                    </div>

                    <div class="visible-xs col-md-5 padding-right-none">
                        <ul class="gal-nav2">
                            <li class="flex-active">
                                <div>
                                    <h2 id="mce_152">Mimeu-vos a la piscina</h2><input name="mce_152" type="hidden">
                                    <p id="mce_153"></p><input name="mce_153" type="hidden">
                                    <p id="mce_154">A Cal Prat estem especialitzats en un Turisme Rural familiar i per parelles, i la nostra principal preocupació es la d'oferir als nostres clients descans i relaxació lluny del ritme i soroll de la ciutat. En un entorn natural molt tranquil es troba la nostre Casa Rural amb el Spa d’ús exclusiu, un dels tresors de Cal Prat.</p><input name="mce_154" type="hidden">
                                    <a href="[base_url]p/la_piscina.html" class="btn btn-default mce-content-body" id="mce_155" >Detalls</a><input name="mce_155" type="hidden">
                                </div>
                            </li>
                        </ul>
                    </div>


                </div>
                <!-- 
<div class="entertain-block dual-slider row">
                    <div class="col-md-7 padding-right-none">
                        <div id="gal-slider3" class="flexslider">
                            <ul class="slides">
                                <li><img class="img-responsive" src="[base_url]img/Homepage_Resort_INDEX/home-gal-img3.png" alt=""></li>
                            </ul>
                            <!~~<a class="flex-prev3 mce-content-body" id="mce_170" ><i class="fa fa-chevron-left"></i></a><input name="mce_170" type="hidden">
                            <a class="flex-next3 mce-content-body" id="mce_171" ><i class="fa fa-chevron-right"></i></a><input name="mce_171" type="hidden">~~>
                        </div>
                    </div>
                    <div class="col-md-5 padding-left-none">
                        <ul class="gal-nav3">
                            <li class="flex-active">
                                <div>
                                    <h2 id="mce_172">Decobreix l'entorn</h2><input name="mce_172" type="hidden">
                                    <p id="mce_173">A Cal Prat hi podeu venir a descansar, passejar, fer un tomb amb bicicleta pels voltants, o bé banyar-vos a la piscina i desconnectar.</p><input name="mce_173" type="hidden">
                                    <p id="mce_174">Visites guidades, excursions, passejos a cavall, rutes en BTT, rutes culturals, vols en globus,...</p><input name="mce_174" type="hidden">
                                    <a href="[base_url]p/la_piscina.html" class="btn btn-default mce-content-body" id="mce_175" >Detalls</a><input name="mce_175" type="hidden">
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
 -->
                <input name="mce_188" type="hidden"><input name="mce_189" type="hidden"><input name="mce_190" type="hidden"></div>
                <div class="activ-eve" style="position: relative;">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <h3 id="mce_191">Activitats</h3><input name="mce_191" type="hidden">
                                <p id="mce_192">Al nostre entorn hi ha molt a fer. Demana'ns informació i estarem encantats de recomanar-te activitats i rutes a fer. Disposem de dades de contacte i descomptes.</p><input name="mce_192" type="hidden">
                                <ul class="specific">
                                    <li class="col-xs-12 col-sm-6 col-md-6">Visites guiades</li>
                                    <li class="col-xs-12 col-sm-6 col-md-6">Observacions astronòmiques</li>
                                    <li class="col-xs-12 col-sm-6 col-md-6">Excursions amb bicicleta</li>
                                    <li class="col-xs-12 col-sm-6 col-md-6">Activitats d'aventura</li>
                                    <li class="col-xs-12 col-sm-6 col-md-6">Passejades amb cavall</li>
                                    <li class="col-xs-12 col-sm-6 col-md-6">Geocaching</li>
                                </ul>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <h3 id="mce_193">Esdeveniments</h3><input name="mce_193" type="hidden">
                                <p id="mce_194">Al nostre entorn hi ha molt a fer. Demana'ns informació i estarem encantats de recomanar-te esdeveniments. Disposem de dades de contacte i descomptes.</p><input name="mce_194" type="hidden">
                                <ul class="specific">
                                    <li class="col-xs-12 col-sm-6 col-md-6">Festivals de música</li>
                                    <li class="col-xs-12 col-sm-6 col-md-6">Fires i mercats</li>
                                    <li class="col-xs-12 col-sm-6 col-md-6">Ruta de castells</li>
                                    <li class="col-xs-12 col-sm-6 col-md-6">Caminades populars</li>
                                    <li class="col-xs-12 col-sm-6 col-md-6">Pastorets de Calaf</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="learn-more">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="row">
                                <img class="img-responsive" src="[base_url]img/Homepage_Resort_INDEX/learn-img1.jpg" alt="">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="row">
                                <img class="img-responsive" src="[base_url]img/Homepage_Resort_INDEX/learn-img2.jpg" alt="">
                            </div>
                        </div>
                        <a href="[base_url]agenda" class="btn btn-default mce-content-body" id="mce_195" spellcheck="false">Llegir més</a>
                    </div>
                </div>
                <!-- SERVICES -->
                <div class="activ-eve" style="position: relative;">
                    <div class="container">
                        <div class="col-xs-12 col-sm-12 col-md-6 col-centered center-block">
                            <div class="title" style="padding-bottom: 0px">
                                <h6 id="mce_199">Certificació</h6><input name="mce_199" type="hidden">
                                <h3 id="mce_200">Allotjament amb certificació Biosphere</h3><input name="mce_200" type="hidden">
                                <div class="col-xs-6 col-sm-4 col-md-4 cap col-centered center-block">
                                    <span id="mce_201"><img class="img-responsive" src="[base_url]img/cap.png" alt=""></span><input name="mce_201" type="hidden">
                                </div>
                                <p id="mce_202">Cal Prat és un allotjament que ha obtingut la certificació Biosphere Responsible Tourism. D’aquesta manera apostem per un turisme responsable i sostenible, compromès amb el medi ambient. </p><input name="mce_202" type="hidden">
                            </div>                            
                        </div>
                        
                        <div class="col-xs-12 col-sm-12 col-md-12" style="padding-bottom: 30px">
                            <p style="text-align:center; margin-top:41px"><img src="[base_url]img/bio.jpg"></p>
                            <p id="mce_192" style="text-align:center">
                                La certificació l’atorga l’Institut de Turisme Responsable, un organisme vinculat a la UNESCO i soci de l’Organització Mundial del Turisme i del Global Sustainable Tourism Council. Es tracta d’una certificació pionera, basada en els 17 objectius de Desenvolupament Sostenible de Nacions Unides integrats en l’Agenda 2030. <br>
                                El segell acredita que les tres marques turístiques de la demarcació –“Costa Barcelona”, “Paisatges Barcelona” i “Pirineus Barcelona”, impulsades per la Diputació de Barcelona- compleixen els requisits de l’estàndard “Biosphere Destination” i que apliquen amb èxit 6 criteris concrets en les seves polítiques turístiques: Gestió sostenible, Desenvolupament econòmic i social, Conservació i millora del patrimoni cultural, Conservació Ambiental, Qualitat i seguretat, i Implicació del visitant.
                            </p>
                        </div>
                            
                    </div>
                </div>
            </div>
                        <div class="home-img-panel parallax-bg" data-stellar-ratio="2" data-stellar-background-ratio="0.5" style="position: relative;">
                            <div class="panel-img-detail">
                                <h2 id="mce_207">Ho fem fàcil per planificar la vostra estada</h2><input name="mce_207" type="hidden">
                                <div class="col-xs-7 col-sm-7 col-md-7 cap col-centered center-block">
                                    <span id="mce_208"><img alt="" src="[base_url]img/cap.png" class="img-responsive"></span><input name="mce_208" type="hidden">
                                </div>
                                <p id="mce_209">Estàs pensant en fer una sorpresa a la teva parella, o fer aquella trobada familiar que tant en parles, o bé celebrar l’aniversari dels nens o les noces dels pares, o aquella reunió de feina que vols que sigui més distesa...? Sigui el que sigui truca’ns i t’ajudarem a planificar-ho bé !!
                                    </p><input name="mce_209" type="hidden">
                                    <a href="[base_url]iniciar-reserva" class="btn btn-default mce-content-body" id="mce_210" >detalls</a><input name="mce_210" type="hidden">
                                </div>
                                <input name="mce_211" type="hidden"><input name="mce_212" type="hidden"><input name="mce_213" type="hidden"></div>
                                <div class="main-gallery" style="position: relative;">
                                    <div class="container">
                                        <div class="col-xs-12 col-sm-12 col-md-6 col-centered center-block">
                                            <div class="row">
                                                <div class="title">
                                                    <h3 class="light-title mce-content-body" id="mce_214" >Galeria</h3><input name="mce_214" type="hidden">
                                                    <div class="col-xs-6 col-sm-4 col-md-4 cap col-centered center-block">
                                                        <span id="mce_215"><img alt="" src="[base_url]img/cap.png" class="img-responsive"></span><input name="mce_215" type="hidden">
                                                    </div>
                                                    <p id="mce_216">Coneix una mica casa nostra a través de la galeria d'imatges. Cada racó és especial i diferent.</p><input name="mce_216" type="hidden">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="main-gal-slider">
                                        <div id="home-gallery" class="owl-carousel owl-theme">
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img3.jpg" alt="Owl Image">
                                                <!--<div class="figcaption">
                                                    <h3>Sala d'estar Forn</h3>
                                                    <div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
                                                        <span><img class="img-responsive" src="<?= base_url() ?>img/cap.png" alt=""></span>
                                                    </div>
                                                    <ul>
                                                        <li>
                                                            <span class="des">beds</span>
                                                            <span class="detail">King or 2 Queen</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">occupancy</span>
                                                            <span class="detail">2 People</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">size</span>
                                                            <span class="detail">60 sqm / 650 sqf</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">view</span>
                                                            <span class="detail">Ocean / City</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">rates from</span>
                                                            <span class="detail">215 USD</span>
                                                        </li>
                                                    </ul>
                                                    <div>
                                                        <a href="./roomDetail-gallery.html" class="btn btn-default pull-left">Informació sala</a>
                                                        <a href="./reservation.html" class="btn btn-default pull-right">Reservar</a>
                                                    </div>
                                                </div>-->
                                            </div>
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img2.jpg" alt="Owl Image">
                                                <!--<div class="figcaption">
                                                    <h3>Dormitori Forn 1</h3>
                                                    <div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
                                                        <span><img class="img-responsive" src="<?= base_url() ?>img/cap.png" alt=""></span>
                                                    </div>
                                                    <ul>
                                                        <li>
                                                            <span class="des">Llit doble de matrimoni</span>
                                                            <span class="detail">King or 2 Queen</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">occupancy</span>
                                                            <span class="detail">2 People</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">size</span>
                                                            <span class="detail">60 sqm / 650 sqf</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">view</span>
                                                            <span class="detail">Ocean / City</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">rates from</span>
                                                            <span class="detail">215 USD</span>
                                                        </li>
                                                    </ul>
                                                    <div>
                                                        <a href="./roomDetail-gallery.html" class="btn btn-default pull-left">room details</a>
                                                        <a href="./reservation.html" class="btn btn-default pull-right">book now</a>
                                                    </div>
                                                </div>-->
                                            </div>
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img4.jpg" alt="Owl Image">
                                                <!--<div class="figcaption">
                                                    <h3>Cuina</h3>
                                                    <div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
                                                        <span><img class="img-responsive" src="<?= base_url() ?>img/cap.png" alt=""></span>
                                                    </div>
                                                    <ul>
                                                        <li>
                                                            <span class="des">beds</span>
                                                            <span class="detail">King or 2 Queen</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">occupancy</span>
                                                            <span class="detail">2 People</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">size</span>
                                                            <span class="detail">60 sqm / 650 sqf</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">view</span>
                                                            <span class="detail">Ocean / City</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">rates from</span>
                                                            <span class="detail">215 USD</span>
                                                        </li>
                                                    </ul>
                                                    <div>
                                                        <a href="./roomDetail-gallery.html" class="btn btn-default pull-left">room details</a>
                                                        <a href="./reservation.html" class="btn btn-default pull-right">book now</a>
                                                    </div>
                                                </div>-->
                                            </div>
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img5.jpg" alt="Owl Image">
                                                <!--<div class="figcaption">
                                                    <h3>ocean view balcony room</h3>
                                                    <div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
                                                        <span><img class="img-responsive" src="<?= base_url() ?>img/cap.png" alt=""></span>
                                                    </div>
                                                    <ul>
                                                        <li>
                                                            <span class="des">beds</span>
                                                            <span class="detail">King or 2 Queen</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">occupancy</span>
                                                            <span class="detail">2 People</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">size</span>
                                                            <span class="detail">60 sqm / 650 sqf</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">view</span>
                                                            <span class="detail">Ocean / City</span>
                                                        </li>
                                                        <li>
                                                            <span class="des">rates from</span>
                                                            <span class="detail">215 USD</span>
                                                        </li>
                                                    </ul>
                                                    <div>
                                                        <a href="./roomDetail-gallery.html" class="btn btn-default pull-left">room details</a>
                                                        <a href="./reservation.html" class="btn btn-default pull-right">book now</a>
                                                    </div>
                                                </div>-->
                                            </div>
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img7.jpg" alt="Owl Image">
                                                </div>
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img15.jpg" alt="Owl Image">
                                                </div>
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img16.jpg" alt="Owl Image">
                                            </div>
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img8.jpg" alt="Owl Image">
                                            </div>   
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img9.jpg" alt="Owl Image">
                                            </div>   
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img10.jpg" alt="Owl Image">
                                            </div>   
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img11.jpg" alt="Owl Image">
                                            </div>   
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img12.jpg" alt="Owl Image">
                                            </div>   
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img13.jpg" alt="Owl Image">
                                            </div>   
                                            <div class="item">
                                                <img src="<?= base_url() ?>img/Homepage_Resort_INDEX/slider-img14.jpg" alt="Owl Image">
                                            </div>                                                   
                                        </div>
                                        
                                        <div class="customNavigation">
                                            <a class="prev"><i class="fa fa-angle-left"></i></a>
                                            <a class="next"><i class="fa fa-angle-right"></i></a>
                                        </div>
                                    </div>
                                    <div class="container">
                                        <a href="<?= base_url('p/galeria') ?>" class="btn btn-default mce-content-body" id="mce_289" >anar a la galeria</a><input name="mce_289" type="hidden">
                                    </div>
                                </div>
                            </div>
                            <!-- TESTIMONIAL -->
                            <div class="testimonial" style="position: relative;">
                                <div class="container">
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-centered center-block">
                                        <div class="row">
                                            <div class="title">
                                                <h6 id="mce_293">testimonis</h6><input name="mce_293" type="hidden">
                                                <h3 id="mce_294">Què diuen els nostres clients</h3><input name="mce_294" type="hidden">
                                                <div class="col-xs-6 col-sm-4 col-md-4 cap col-centered center-block">
                                                    <span id="mce_295"><img class="img-responsive" src="[base_url]img/cap.png" alt=""></span><input name="mce_295" type="hidden">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6 col-md-6">
                                            <div class="testi-block">
                                                <div class="row">
                                                    <!--<div class="hidden-xs col-xs-12 col-sm-2 col-md-2">
                                                        <img src="[base_url]img/Homepage_Resort_INDEX/testimonial-img5.png" alt="">
                                                    </div>-->
                                                    <div class="col-xs-12 col-sm-offset-2 col-sm-10 col-md-10">
                                                        <div class="testi-detail">
                                                            <p id="mce_296">"Buscavem una casa tranquila on passar uns dies en família i ens va encantar Cal Prat. Els nens podien jugar i gaudir de l'entorn i el tracte per part de la propietària va ser molt amable i atent"</p><input name="mce_296" type="hidden">
                                                            <h6 id="mce_297">– J. Magaña</h6><input name="mce_297" type="hidden">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6">
                                            <div class="testi-block">
                                                <div class="row">
                                                    <!--<div class="hidden-xs col-xs-12 col-sm-2 col-md-2">
                                                        <img src="[base_url]img/Homepage_Resort_INDEX/testimonial-img6.png" alt="">
                                                    </div>-->
                                                    <div class="col-xs-12 col-sm-offset-2 col-sm-10 col-md-10">
                                                        <div class="testi-detail">
                                                            <p id="mce_298">"Vam llogar amb la colla d'amics Cal Prat i va ser genial! Vam poder gaudir del fred en una casa rústica i plena de detalls. Segur que repetim!</p><input name="mce_298" type="hidden">
                                                            <h6 id="mce_299">– B. Tusell</h6><input name="mce_299" type="hidden">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="prom-offers">
                                    <div class="dark-gold"></div>
                                    <div class="light-gold"></div>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-5 col-md-5">
                                                <div class="prom-block">
                                                    <h3 id="mce_300">Promocions</h3><input name="mce_300" type="hidden">
                                                    <p id="mce_301">Consulteu les nostres promocions puntuals per a dies concrets i caps de setmana concrets durant l'any.</p><input name="mce_301" type="hidden">
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-2 col-md-2">
                                                <img class="img-responsive" src="[base_url]img/Homepage_Resort_INDEX/testimonial-img2.png" alt="">
                                            </div>
                                            <div class="col-xs-12 col-sm-5 col-md-5">
                                                <div class="offer-block">
                                                    <h3 id="mce_302">Ofertes Especials</h3><input name="mce_302" type="hidden">
                                                    <p id="mce_303">Sou nuvis, sou grups nombrosos...contacteu amb Cal Prat i podem fer preus especials. Estem sempre disposats a oferir-te el millor tracte que us mereixeu en moments especials.</p><input name="mce_303" type="hidden">
                                                </div>
                                            </div>
                                        </div>
                                        <a href="[base_url]ofertas" class="btn btn-default mce-content-body" id="mce_304" >Veure totes les ofertes</a><input name="mce_304" type="hidden">
                                    </div>
                                </div>
                                <input name="mce_305" type="hidden"><input name="mce_306" type="hidden"><input name="mce_307" type="hidden"></div>
                                <!-- LOCATION -->
                                <div class="location" style="position: relative;">
                                    <div class="container">
                                        <div class="col-xs-12 col-sm-12 col-md-6 col-centered center-block">
                                            <div class="row">
                                                <div class="title">
                                                    <h6 id="mce_308">LOCALITZACIÓ</h6><input name="mce_308" type="hidden">
                                                    <h3 id="mce_309">ON ENS TROBAREU</h3><input name="mce_309" type="hidden">
                                                    <div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
                                                        <span id="mce_310"><img class="img-responsive" src="[base_url]img/cap.png" alt=""></span><input name="mce_310" type="hidden">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-8">
                                                <div class="map">
                                                    <div id="map">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-4">
                                                <ul>
                                                    <li>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4 col-md-6">
                                                                <span class="specific mce-content-body" id="mce_311" >ADREÇA</span><input name="mce_311" type="hidden">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8 col-md-6">
                                                                <p id="mce_312">El Soler, s/n Calonge de Segarra, Barcelona</p><input name="mce_312" type="hidden">
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4 col-md-6">
                                                                <span class="specific mce-content-body" id="mce_313" >telèfon</span><input name="mce_313" type="hidden">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8 col-md-6">
                                                                <p id="mce_314">+34 650 93 64 00</p><input name="mce_314" type="hidden">
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4 col-md-6">
                                                                <span class="specific mce-content-body" id="mce_315" >email</span><input name="mce_315" type="hidden">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8 col-md-6">
                                                                <a href="mailto:info@fontainbleu.com" id="mce_316"> <input name="mce_316" type="hidden"><a href="mailto:info@calprat.net">info@calprat.net</a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4 col-md-6">
                                                                <span class="specific mce-content-body" id="mce_317" >distàncies</span><input name="mce_317" type="hidden">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8 col-md-6">
                                                                <p id="mce_318">80km de barcelona <br> 80km de LLeida <br> 100km de Tarragona <br> 150km de Girona</p><input name="mce_318" type="hidden">
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4 col-md-6">
                                                                <span class="specific mce-content-body" id="mce_319" >A prop de</span><input name="mce_319" type="hidden">
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8 col-md-6">
                                                                <p id="mce_320">20 Km. d’Igualada<br> 20 km. de Cervera<br> 30 km. de Manresa <br>40 km. de Tàrrega</p><input name="mce_320" type="hidden">
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <input name="mce_321" type="hidden"><input name="mce_322" type="hidden"><input name="mce_323" type="hidden"></div><div style="text-align:center;"></div>
                                </section>