<!-- WELCOME -->
<section id="content">
	<section id="content" class="room-gallery"><!-- PAGE HEADER --><div class="title-color">
	<div class="container">
		<h2 id="mce_91" class="mce-content-body mce-edit-focus">Els nostres preus</h2>
		<ol class="breadcrumb">
			<li><a href="<?= base_url() ?>" id="mce_92" class="mce-content-body">Inici</a></li>
			
			<li class="active">Preus</li>
		</ol>
	</div>
</div>
<!-- ROOM DETAIL-->
<div class="container">
	<div class="blog">		
		<div class="row">
			<div class="main-grid main-blog-grid">
					<div class="col-xs-12 col-sm-12 col-md-12">
						<div class="row">
							<div class="blog-grid">
								<ul>

									<?php foreach($this->db->get_where('habitaciones')->result() as $h): ?>
										<li class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
											<img class="img-responsive" src="<?= base_url('img/habitaciones/'.$h->foto) ?>" alt="">
											<div class="blog-detail">
												<h3 style="height:38px"><?= $h->habitacion_nombre ?></h3>
												<div class="col-xs-6 col-sm-6 col-md-6 cap col-centered center-block">
													<span><img class="img-responsive" src="[base_url]img/cap.png" alt=""></span>
												</div>
												
													<table class="table table-bordered tablepreus tb-table ">
														<thead>
															<tr>
																<th colspan="2">Hivern</th>																
															</tr>
														</thead>
														<tbody>
															<tr>
																<td style="text-align:left">Cap de Setmana</td>
																<td class="price" style="padding:0 20px; text-align:right"><?= str_replace(',00','',number_format(($h->precio_fin_tbaja*2),2,',','.')); ?>€</td>
															</tr>
															<tr>
																<td style="text-align:left">Dia</td>
																<td class="price" style="padding:0 20px; text-align:right"><?= str_replace(',00','',number_format($h->precio_dia_tbaja,2,',','.')); ?>€</td>
															</tr>
															<tr>
																<td style="text-align:left">Tota la Setmana</td>
																<td class="price" style="padding:0 20px; text-align:right"><?= str_replace(',00','',number_format($h->precio_sem_tbaja,2,',','.')); ?>€</td>
															</tr>															
														</tbody>
														<thead>
															<tr>
																<th colspan="2">Estiu</th>																
															</tr>
														</thead>
														<tbody>
															<tr>
																<td style="text-align:left">Cap de Setmana</td>
																<td class="price" style="padding:0 20px; text-align:right"><?= str_replace(',00','',number_format(($h->precio_fin_talta*2),2,',','.')); ?>€</td>
															</tr>
															<tr>
																<td style="text-align:left">Dia</td>
																<td class="price" style="padding:0 20px; text-align:right"><?= str_replace(',00','',number_format($h->precio_dia_talta,2,',','.')); ?>€</td>
															</tr>
															<tr>
																<td style="text-align:left">Tota la Setmana</td>
																<td class="price" style="padding:0 20px; text-align:right"><?= str_replace(',00','',number_format($h->precio_sem_talta,2,',','.')); ?>€</td>
															</tr>															
														</tbody>
													</table>
													
												<div>
													<a href="<?= base_url('iniciar-reserva') ?>" class="btn btn-default btn-preus">Reservar</a>													
												</div>
											</div>
											<div class="visitor">
												<span class="calender">
													<b style="font-weight:bold;">Hivern</b> 01/10 - 30/06
												</span>												
												<span class="views">
													<b style="font-weight:bold;">Estiu</b> 01/07 - 30/09
												</span>
											</div>
										</li>
									<?php endforeach ?>
								</ul>
							</div>
							<span class="views">
													<b style="font-weight:bold;">*Nadal, Cap d'Any i Setmana Santa:</b> Només disponible lloguer de tota la casa.
													<br><b style="font-weight:bold;">*Per ponts:</b> consultar
												</span>
						</div>
					</div>
				</div>
		</div>
	</div>
</div>

</section>