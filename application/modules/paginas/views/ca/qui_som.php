<!-- WELCOME -->
<section id="content">
            <section id="content" class="about-main"><!-- PAGE HEADER --><div class="title-color">
			<h2>Sobre Cal Prat</h2>
			<ol class="breadcrumb">
<li><a href="<?= base_url() ?>">Inici</a></li>
				<li class="active"><a href="<?= base_url() ?>p/qui_som.html">Qui som?</a></li>
			</ol>
</div>
		
		<!-- ABOUT -->
		<div class="about-content">
			<div class="db-image quisom">
				<div class="dbi-inner" style="background:url([base_url]img/About/1.jpg) no-repeat center center">
					<img src="[base_url]img/About/1.jpg" style="width:100%" class="visible-xs">
				</div>
				<div class="dbi-inner" style="background:url([base_url]img/About/2.jpg) no-repeat center center">
					<img src="[base_url]img/About/2.jpg" style="width:100%" class="visible-xs">
				</div>
				<div class="dbi-inner" style="background:url([base_url]img/About/3.jpg) no-repeat center center">
					<img src="[base_url]img/About/3.jpg" style="width:100%" class="visible-xs">
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6">
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6">
				<div class="row">
					<div class="right-about">
						<h1>Cal Prat, casa rural de pagès</h1>
						<h6>Turisme rural, relax i desconnexió</h6>
						<p>Cal Prat és una antiga casa de pagès del segle XVIII situada al petit nucli d’El Soler, dins el terme municipal de Calonge de Segarra a l’Alta Anoia, just a tocar de les comarques de La Segarra i El Solsonès.</p>
						<p>Una acurada restauració ens ha permès  conservar totes les estances i elements típics d’una antiga casa de pagès com són el celler, el cup del vi, l’estable, el forn de pa, la llar de foc, etc.</p>
						<p>La vostra estada a Cal Prat us permetrà gaudir-ne tot imaginant-vos com ha estat la vida de la nostra pagesia en els darrers segles, i podreu fruir del paisatge i la tranquil·litat del món rural d’avui.</p>
						<p>El Forn i El Celler de Cal Prat, així s’anomenen els nostres allotjaments per la particularitat que caracteritza a cadascun, tenen una capacitat de fins a 8 i 5 persones respectivament. També es pot llogar la casa sencera, ja que els dos allotjaments es poden comunicar.</p>
						<div class="highlights">
							<h6>Destacats</h6>
							<ul class="specific">
<li>Silenci absolut enmig de plena naturalesa</li>
								<li>Excursions a tocar de la casa i rutes organitzades</li>
								<li>Material per a infants</li>
								<li>Piscina climatitzada privada</li>
								<li>Habitacions privades amb tot detall</li>
							</ul>
</div>
						<div class="clearfix"></div>
						<div class="img">
							<img src="[base_url]img/About/4.jpg" alt="">
</div>
						<div class="clearfix"></div>
						<h2>No estem tranquils fins a veure't satisfet</h2>
						<p>Explica'ns què necessites, què busques, què et falta i tots els dubtes que et sorgeixin per tal de gaudir al 100% de la teva estada.</p>
						<p>Disposem de trones, bressols, llits supletoris i altres materials que poden fer-te l'estada més amena.</p>
					</div>
				</div>
			</div>
		</div>
	</section>    </section>