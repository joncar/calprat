
            <section id="content" class="gallery-main">
	
		<!-- PAGE HEADER -->
		<div class="title-color">
			<h2 id="mce_48" class="mce-content-body">Galeria</h2>
			<ol class="breadcrumb">
                <li><a href="<?= base_urL() ?>" id="mce_92" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Inici</a></li>
                <li><a href="#" id="mce_93" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Galeria</a></li>
<!--                            <li class="active">Video Post</li>-->
            </ol>
		</div>
		
		<!-- GALLERY CONTENT -->
		<div class="container">
			<div class="col-xs-12 col-sm-10 col-md-6 col-centered center-block">
				<div class="row">
					<div class="title">
						<h6 id="mce_56" class="mce-content-body">Una imatge val més que mil paraules</h6>
						<h1 id="mce_57" class="mce-content-body">Cal Prat en imatges</h1>
						<div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
							<span id="mce_58" class="mce-content-body"><img class="img-responsive" src="[base_url]img/cap.png" alt=""></span>
						</div>
						<p id="mce_59" class="mce-content-body mce-edit-focus">Coneix cada racó de la casa a través d'imatges. No hem escatimat en detalls, tot perquè estigui al teu gust.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="gallery-block">
			<div class="main-room" uk-lightbox>
				<ul>
					<li class="hidden-xs col-xs-12 col-sm-6 col-md-3 col-lg-3">
						<div class="img">
							<img src="[base_url]img/Gallery/1.jpg" class="img-responsive" alt="">
							<div class="mr-head">
								<h3 id="mce_63" class="mce-content-body">Mira'ns amb bons ulls</h3>
								<div class="col-md-6 cap col-centered center-block">
									<span id="mce_64" class="mce-content-body"><img class="img-responsive" src="[base_url]img/cap.png" alt=""></span>
								</div>
								<p id="mce_65" class="mce-content-body">Coneix Cal Prat a partir d'un petit recull d'imatges. <br> Descobreix cada racó, cada habitació, cada sala....</p>
<!--								<p>Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent.</p>-->
<!--							</div>-->
						</div>
					</div></li>

					[foreach:galeria]
					<li class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
						<a href="[foto]">
							<div class="mr-inner">
								<img src="[foto]" class="img-responsive" alt="">
								<div class="mr-overlay">
									<h2 id="mce_66" class="mce-content-body">[titulo]<br></h2>
									<p id="mce_67" class="mce-content-body">[descripcion]</p>								
								</div>
							</div>
						</a>
					</li>
					[/foreach]

				</ul>
			</div>
		</div>
		<!--<div class="find-room">
			<div class="container">
				<h2 id="mce_72" class="mce-content-body">Disponibilitat</h2>
				<div class="form row">
					<div class="form-group col-xs-12 col-sm-6 col-md-2 col-lg-3">
						<label>data arribada</label>
						<div id="datetimepicker1" class="input-group date">
							<input placeholder="" class="form-control" type="text">
							<span class="input-group-addon mce-content-body" id="mce_73"><i class="fa fa-calendar"></i></span>
						</div>
					</div>
					<div class="form-group col-xs-12 col-sm-6 col-md-2 col-lg-3">
						<label>data sortida</label>
						<div id="datetimepicker2" class="input-group date">
							<input placeholder="" class="form-control" type="text">
							<span class="input-group-addon mce-content-body" id="mce_74"><i class="fa fa-calendar"></i></span>
						</div>
					</div>
					<div class="form-group col-xs-12 col-sm-6 col-md-3 col-lg-2">
						<label>Tipus d'allotjaments</label>
						<input placeholder="" class="form-control" type="text">
					</div>
					<div class="form-group col-xs-12 col-sm-6 col-md-2 col-lg-2">
						<label>Núm.de persones </label>
						<input placeholder="" class="form-control" type="text">
					</div>
					<div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-2">
						<button class="btn btn-default btn-availble" type="submit">Buscar Ara</button>
					</div>
				</div>
			</div>
		</div>--><div></div>
	</section>    