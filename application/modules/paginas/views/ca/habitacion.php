<!-- WELCOME -->
<section id="content">
	<section id="content" class="room-gallery"><!-- PAGE HEADER --><div class="title-color">
	<div class="container">
		<h2 id="mce_91" class="mce-content-body mce-edit-focus">[habitacion_nombre]</h2>
		<ol class="breadcrumb">
			<li><a href="<?= base_url() ?>" id="mce_92" class="mce-content-body">Inici</a></li>
			
			<li class="active">[habitacion_nombre]</li>
		</ol>
	</div>
</div>
<!-- ROOM DETAIL-->
<div class="container">
	<div class="delux-city-view">
		<div class="bloggal-slider">
			<div id="gal-slider" class="flexslider">
				<ul class="slides">
					[slider]
				</ul>
				<ul class="flex-direction-nav">
					<li><a class="flex-prev mce-content-body" id="mce_97"><i class="fa fa-angle-left"></i></a></li>
					<li><a class="flex-next mce-content-body" id="mce_98"><i class="fa fa-angle-right"></i></a></li>
				</ul>
			</div>
			<ul class="gal-nav">
				[previews]
			</ul>
		</div>
		<div style="background: transparent; text-align: right;padding-top: 0;margin-top: -30px;margin-bottom: 0px;" class="visible-xs reservation">
			<a href="javascript:goFotos()" class="btn btn-default btn-availble" style="margin-top: 0px;width: 100%;">Més fotos</a>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-7 col-md-7">
				<h1 id="mce_99" class="mce-content-body mce-edit-focus">[habitacion_nombre]</h1>
				
				
				[descripcion]

				<div class="hidden-xs reservation" style="background: transparent; text-align: left">
					<a href="javascript:goFotos()" class="btn btn-default btn-availble">Més fotos</a>
				</div>
			</div>
			<div class="col-xs-12 col-sm-5 col-md-4 col-md-offset-1 pull-right">
				<div class="reservation">
					<form method="get" action="<?= base_url('iniciar-reserva') ?>">
						<h2 id="mce_103" class="mce-content-body">Fer la reserva</h2>
						<div class="rate-from">
							<h5 id="mce_104" class="mce-content-body">des de</h5>
							<h1 id="mce_105" class="mce-content-body"><span id="mce_106"><sup></sup></span>[precio_desde]</h1><h2>€/nit</h2>
							<h6 id="mce_107" class="mce-content-body">*Consultar condicions</h6>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6 cap col-centered center-block">
							<span id="mce_108" class="mce-content-body"><img class="img-responsive" src="[base_url]img/cap.png" alt=""></span>
						</div>
						<div class="row input-daterange">
							<div class="col-xs-12 col-sm-6 col-md-6">
								<div class="form-group">
									<div id="datetimepicker1" class="input-group date">
										<input placeholder="DATA ENTRADA" name="desde" class="form-control" type="text" readonly="">
										<span class="input-group-addon mce-content-body" id="mce_109">
											<i class="fa fa-calendar"></i>
										</span>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-6">
								<div class="form-group">
									<div id="datetimepicker2" class="input-group date">
										<input placeholder="DATA SORTIDA" name="hasta" class="form-control" type="text" readonly="">
										<span class="input-group-addon mce-content-body" id="mce_110">
											<i class="fa fa-calendar"></i>
										</span>
									</div>
								</div>
							</div>
						</div>
						<div class="row date-picker">
							<div class="col-xs-12 col-sm-6 col-md-6">
								<select id="get_value" name="adultos" tabindex="0"><option value="">Adults</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
							</select>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6">
							<select id="get_value2" name="infantes" tabindex="0"><option value="">Nens</option>
							<option value="0">0</option>
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
						</select>
					</div>
				</div>
				[habitacion_id]
				<button type="submit" class="btn btn-default btn-availble">Veure disponibilitat</button>
			</form>
		</div>
	</div>
</div>
</div>
<div class="spa-glan-tab">
<div role="tabpanel">
	<ul role="tablist" class="nav nav-tabs">
		<li class="active" role="presentation">
			<a data-toggle="tab" role="tab" href="#overview" id="mce_111" class="mce-content-body">
				<i class="fa fa-globe"></i>Descripció
			</a>
		</li>
		
		<li role="presentation">
			<a data-toggle="tab" role="tab" href="#available" id="mce_113" class="mce-content-body">
				<i class="fa fa-heart"></i>Disponibilitat
			</a>
		</li>
		<li role="presentation">
			<a data-toggle="tab" role="tab" href="#reviews" id="mce_114" class="mce-content-body"><i class="fa fa-pagelines"></i>Opinions</a>
		</li>
	</ul>
	<!-- Tabs --><div class="tab-content">
	<div id="overview" class="tab-pane fade in active" role="tabpanel">
		<div class="style-comfort">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6">
					<img src="[foto]" alt="">
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6">
					<ul>
						<li>
							<span class="des mce-content-body" id="mce_117">Llits</span>
							<span class="detail mce-content-body" id="mce_118">[camas]</span>
						</li>
						<li>
							<span class="des mce-content-body" id="mce_119">Capacitat</span>
							<span class="detail mce-content-body" id="mce_120">[max_personas]</span>
						</li>
						<li>
							<span class="des mce-content-body" id="mce_125">Preu des de</span>
							<span class="detail mce-content-body" id="mce_126">[precio_desde]€</span>
						</li>
						<li>
							<span class="des mce-content-body" id="mce_127">Veure galeria</span>
							<span class="detail mce-content-body" id="mce_128"><a href="[base_url]p/galeria"><i class="fa fa-photo"></i></a></span>
						</li>
						<li>
							<span class="des mce-content-body" id="mce_127">Veure vídeo</span>
							<span class="detail mce-content-body" id="mce_128"><a href="https://www.youtube.com/watch?v=QH_IXokfyBA" class="prettyPhoto" id="mce_129"><i class="fa fa-video-camera"></i></a></span>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	
	<div id="available" class="tab-pane fade" role="tabpanel">
		<div class="availability">
			<p id="mce_133" class="mce-content-body">LA RESERVA ÉS FERMA EN FER UN INGRÉS DEL 25% DE L’IMPORT TOTAL, INDICANT EL VOSTRE NOM, D.N.I. I DIA D’ENTRADA.
<br>EL PAGAMENT DE LA RESTA S’EFECTUARÀ EL MATEIX DIA DE SORTIDA EN EFECTIU.
<br>L’ANUL·LACIÓ DE LA RESERVA DINTRE DELS 15 DIES ANTERIORS A LA DATA D’OCUPACIÓ, DONARÀ LLOC A LA PÈRDUA DE LA QUANTITAT LLIURADA A COMPTE.
</p>
			<div id="ab-carousel" class="owl-carousel owl-theme">
				<div class="item">
					<div class="main-cal">
						<div id="datetimepicker11-inline">
						</div>
					</div>
				</div>
				<div class="item">
					<div class="main-cal">
						<div id="datetimepicker12-inline">
						</div>
					</div>
				</div>
				<div class="item">
					<div class="main-cal">
						<div id="datetimepicker13-inline">
						</div>
					</div>
				</div>
				<div class="item">
					<div class="main-cal">
						<div id="datetimepicker14-inline">
						</div>
					</div>
				</div>
				<div class="item">
					<div class="main-cal">
						<div id="datetimepicker15-inline">
						</div>
					</div>
				</div>
				<div class="item">
					<div class="main-cal">
						<div id="datetimepicker16-inline">
						</div>
					</div>
				</div>
				<div class="item">
					<div class="main-cal">
						<div id="datetimepicker17-inline">
						</div>
					</div>
				</div>
				<div class="item">
					<div class="main-cal">
						<div id="datetimepicker18-inline">
						</div>
					</div>
				</div>
				<div class="item">
					<div class="main-cal">
						<div id="datetimepicker19-inline">
						</div>
					</div>
				</div>
				<div class="item">
					<div class="main-cal">
						<div id="datetimepicker110-inline">
						</div>
					</div>
				</div>
				<div class="item">
					<div class="main-cal">
						<div id="datetimepicker111-inline">
						</div>
					</div>
				</div>
				<div class="item">
					<div class="main-cal">
						<div id="datetimepicker112-inline">
						</div>
					</div>
				</div>
			</div>
			<div class="customNavigation">
				<a class="prev mce-content-body" id="mce_344"><i class="fa fa-angle-left"></i></a>
				<a class="next mce-content-body" id="mce_345"><i class="fa fa-angle-right"></i></a>
			</div>
		</div>
	</div>
	<div id="reviews" class="tab-pane fade" role="tabpanel">
		<div class="main-review">
			<div class="review-block">
				<div class="title">
					<h4 id="mce_348" class="mce-content-body">Xavi López</h4>
					<ul>
						<li><a id="mce_349" class="mce-content-body"><i class="fa fa-star"></i></a></li>
						<li><a id="mce_350" class="mce-content-body"><i class="fa fa-star"></i></a></li>
						<li><a id="mce_351" class="mce-content-body"><i class="fa fa-star"></i></a></li>
						<li><a id="mce_352" class="mce-content-body"><i class="fa fa-star"></i></a></li>
						<li class="active"><a id="mce_353" class="mce-content-body"><i class="fa fa-star-o"></i></a></li>
					</ul>
					<span class="days mce-content-body" id="mce_354">2 d'Abril 2018</span>
				</div>
				<h3 id="mce_355" class="mce-content-body">Vam passar uns dies de luxe!</h3>
				<p id="mce_356" class="mce-content-body">Hemos estado este fin de semana. Familia monoparental con dos niñas, lo primero que quería agradecer el trato de la propietaria, por su amabilidad, por su accesibilidad a la hora de la entrada y salida de la casa y por guiarnos y orientarnos con el entorno. La casa más cómoda imposible, limpieza extraordinaria, todas las comodidades necesarias en los dormitorios, un gusto exquisito en la decoración de toda la casa, y además como hacía calor pudimos disfrutar de la piscina, repetiremos la experiencia. Un saludo</p>
				<img src="[base_url]img/Homepage_Resort_INDEX/testimonial-img1.png" alt="">
			</div>
			<div class="review-block">
				<div class="title">
					<h4 id="mce_357" class="mce-content-body">Marina, Élisa, Ana Romain, Victoria et Bruno</h4>
					<ul>
						<li><a id="mce_358" class="mce-content-body"><i class="fa fa-star"></i></a></li>
						<li><a id="mce_359" class="mce-content-body"><i class="fa fa-star"></i></a></li>
						<li><a id="mce_360" class="mce-content-body"><i class="fa fa-star"></i></a></li>
						<li><a id="mce_361" class="mce-content-body"><i class="fa fa-star"></i></a></li>
						<li class="active"><a id="mce_362" class="mce-content-body"><i class="fa fa-star-o"></i></a></li>
					</ul>
					<span class="days mce-content-body" id="mce_363">5 Maig de 2018</span>
				</div>
				<h3 id="mce_364" class="mce-content-body">C’est une merveille</h3>
				<p id="mce_365" class="mce-content-body">Que dire de votre maison? A part que c’est une merveille, en plus de l’avoir en que pour nous. Nous nous sommes sentis comme chez nous. L’accueil d’Alba est formidable, vous êtes une famille très à l’écoute. Nous avons passé un séjour agréable, découvert la région et testé l’accueil des catalans épatant. En espérant venir vous  revoir bientôt dans cette charmante maison, où rien ne manque. Nous ne manquerons pas de parler de vous en France. Nous vous embrassons fort!!  
</p>
				<img src="[base_url]img/Homepage_Resort_INDEX/testimonial-img1.png" alt="">
			</div>
		</div>
	</div>
</div>
</div>
</div>
</div>
[habitacion_oferta]


<div id="galeria" class="special-offer" style="background: transparent;">
	<div class="container">
		<div class="col-xs-12 col-sm-8 col-md-8 col-centered center-block">
			<div class="row">
				<div class="title">
					<h6>Galeria</h6>
					<h1>Fotos</h1>
					<div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
						<span><img class="img-responsive" src="<?= base_url() ?>img/cap.png" alt=""></span>
					</div>
				</div>
			</div>
		</div>
		[fotos22]
	</div>
</div>
</section>