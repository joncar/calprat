
    <section id="content" class="spa-main"><!-- PAGE HEADER --><div class="title-color" style="position: relative;">
            <h2 id="mce_40" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">La piscina</h2><input type="hidden" name="mce_40">
            <ol class="breadcrumb">
                <li><a href="./index.html" id="mce_41" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Inici</a><input type="hidden" name="mce_41"></li>
                <li class="active"><a href="./spa.html" id="mce_42" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Altres Espais</a><input type="hidden" name="mce_42"></li><li class="active">La piscina</li>
            </ol>
            <input type="hidden" name="mce_43"><input type="hidden" name="mce_44"><input type="hidden" name="mce_45"></div>

        <!-- SPA -->
        <div class="spa-block" style="position: relative;">
            <div class="db-image">
                <div class="dbi-inner" style="height:100%; background:url(<?= base_url() ?>img/spa/1.jpg) no-repeat center center">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="spa-right">
                    <h1 id="mce_46" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">La millor manera de mimar-se</h1><input type="hidden" name="mce_46">
                    <h6 id="mce_47" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Gaudeix d'una experència spa a Cal Prat.</h6><input type="hidden" name="mce_47">
                    <p id="mce_48" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Durant  la vostra estada , volem que pugueu  trobar allò que hi busqueu: silenci, tranquil·litat, relaxament…</p><input type="hidden" name="mce_48">
                    <p id="mce_49" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Per això podreu gaudir d’una piscina climatitzada amb solàrium, on lògicament us hi podreu banyar, prendre el sol, llegir un llibre…Aquesta disposa d’un sistema de natació contracorrent, saltant d’aigua relaxant, lluminari, solàrium i  calefacció per terra radiant.

Utilitzem un sistema de depuració per electròlisi salina. D’aquesta manera no només som respectuosos amb el medi ambient sinó que també volem respectar la vostra pell.</p><input type="hidden" name="mce_49">
                    <h3 id="mce_50" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;"></h3><input type="hidden" name="mce_50">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <ul class="specific">
                                <li>Zona solàrium per a prendre el sol</li>
                                <li>Tumbones</li>
                                
                            </ul>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <ul class="specific">
                                <li>Sofàs a l'interior de la piscina</li>
                                <li>Tovalloles de piscina</li>
                                                            </ul>


                        </div>
                    </div>
                    <h3 id="mce_50" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;"></h3>
                    <div class="row" uk-lightbox>
                        [foreach:galeria_piscina]
                            <div class="col-xs-12 col-sm-3" style="margin-top: 29px;"><a href="[foto]"><img src="[foto]" alt="" style="width:100%"></a></div>
                        [/foreach]
                    </div>
                </div>
            </div>
        </div>

        <div class="spa-img-bot" style="position: relative;">
            <div class="spa-natural">
                <h2 id="mce_55" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Utilitzem només els millors productes naturals</h2><input type="hidden" name="mce_55">
                <div class="col-xs-12 col-sm-6 col-md-6 cap col-centered center-block">
                    <span id="mce_56" class="mce-content-body" contenteditable="true" spellcheck="false"><img class="img-responsive" src="[base_url]img/cap.png" alt=""></span><input type="hidden" name="mce_56">
                </div>
                <p id="mce_57" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;">Utilitzem un sistema de depuració per electròlisi salina. D’aquesta manera no només som respectuosos amb el medi ambient sinó que també volem respectar la vostra pell.</p><input type="hidden" name="mce_57">
                <p id="mce_58" class="mce-content-body" contenteditable="true" spellcheck="false" style="position: relative;"></p><input type="hidden" name="mce_58">
            </div>
        </div>
        <div style="text-align:center;"></div>
    </section>