<!-- WELCOME -->
<section id="content">
    <section id="content" class="contact-main">
        <!-- PAGE HEADER -->
        <div class="title-color">
            <div class="container">
                <h2 id="mce_91">Contacta amb nosaltres</h2>
                <input name="mce_91" type="hidden"><input name="mce_91" type="hidden"><input name="mce_91" type="hidden">
                <ol class="breadcrumb">
                    <li><a href="./index.html" id="mce_92">Inici</a><input name="mce_92" type="hidden"><input name="mce_92" type="hidden"><input name="mce_92" type="hidden"></li>
                    <li><a href="./accommodations.html" id="mce_93">Contacte</a><input name="mce_93" type="hidden"><input name="mce_93" type="hidden"><input name="mce_93" type="hidden"></li>
                    <!--					<li class="active">Contacte</li>-->
                </ol>
            </div>
            <input name="mce_94" type="hidden"><input name="mce_95" type="hidden"><input name="mce_96" type="hidden"><input name="mce_48" type="hidden"><input name="mce_49" type="hidden"><input name="mce_50" type="hidden">
        </div>
        <!-- CONTACT INFO -->
        <div class="main-contact">
            <div class="container">
                <div class="detail-cont">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-7 col-lg-7 left-detail">
                            <div class="direct-inq">
                                <form method="post" action="<?= base_url('paginas/frontend/contacto') ?>">
                                [response]
                                <h3 id="mce_97">Formulari de Contacte</h3>
                                <input name="mce_97" type="hidden"><input name="mce_97" type="hidden">
                                <div class="row detail-type">
                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                        <h6 id="mce_98">Nom *</h6>                                        
                                        <input name="nombre" id="senderfirstName" class="form-control"  type="text">
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                        <h6 id="mce_99">Cognoms *</h6>                                        
                                        <input name="apellido" id="senderlastName" class="form-control"  type="text">
                                    </div>
                                </div>
                                <div class="row detail-type">
                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                        <h6 id="mce_100">email *</h6>                                        
                                        <input name="email" id="senderEmail2" class="form-control"  type="email">
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                        <h6 id="mce_101">Telèfon</h6>                                        
                                        <input name="telefono" id="phone" class="form-control" type="text">
                                    </div>
                                </div>
                                <div class="row detail-type">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <h6 id="mce_102">Tema</h6>                                        
                                        <input name="tema" id="subject2" class="form-control"  type="text">
                                    </div>
                                </div>
                                <div class="row detail-type">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <h6 id="mce_103">Missatge</h6>                                        
                                        <textarea name="message" id="message2" rows="3"></textarea>
                                        <span class="req mce-content-body" id="mce_104" spellcheck="false">* obligatori</span>
                                    </div>
                                </div>
                                <div class="row detail-type">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <h6>
                                            <input type="checkbox" name="politicas" value="1"> He llegit i accepto la <a href="<?= base_url('p/avis-legal') ?>" target="_new" style="color:#464646; text-decoration: underline !important;">política de privacitat* </a>
                                        </h6>
                                    </div>
                                </div>
                                <div class="row detail-type">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <div class="g-recaptcha" data-sitekey="6LdSNGAUAAAAAPFcFDPrON0NKwHLnN0GeL3ilX8s"></div>
                                    </div>
                                </div>
                                <div class="row detail-type">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <button class="btn btn-default" type="submit">ENVIAR</button>
                                    </div>
                                </div>   
                                </form>                            
                            </div>
                            <div class="map-direction">
                                <h3 id="mce_109" class="mce-content-body mce-edit-focus" spellcheck="false">Mapa / Ubicació</h3>
                                <input name="mce_109" type="hidden">
                                <div class="map">
                                    <div id="map">
                                    </div>
                                </div>
                                <ul>
                                    <li>
                                        <h4 id="mce_110">Des de Barcelona</h4>
                                        <input name="mce_110" type="hidden"><input name="mce_110" type="hidden">
                                        <p id="mce_111">
                                           Agafar la B-10 desde Via Laietana, Passeig Colom i Passeig Josep Carner durant 13 km. Després agafar la A-2 cap a la C-1412a i agafar la sortida 545 direcció Jorba/Ponts/Andorra. Seguir durant 12,4 km per aquesta via fins a trobar una rotonda, seguir recte. A la següent rotonda també recte i a la tercera rotonda agafar la tercera sortida. Un cop seguim uns 200 metres trobarem una altres sortida i agafar la primera sortida, tot seguit girar a l'esquerra i trobareu la indicació de Cal Prat.
                                        </p>
                                        <input name="mce_111" type="hidden"><input name="mce_111" type="hidden">
                                    </li>
                                    <li>
                                        <h4 id="mce_112">Des de Lleida o Tarragona</h4>
                                        <input name="mce_112" type="hidden"><input name="mce_112" type="hidden">
                                        <p id="mce_113">
                                            Agafar la A-2 i C-25 cap a C-1412a. Agafa la sortida 103 cap a Calaf/Igualada des de C-25. A la rotonda, agafa la cinquena sortida en direcció C-1412a. A la rotonda, agafa la primera sortida i continua per C-1412a. Gira a l'esquerra en 3'4km. Durant 1,5 km mantenirse a la dreta. I en 350 m gira a l'esquerra.
                                        </p>
                                        <input name="mce_113" type="hidden"><input name="mce_113" type="hidden">
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5 right-detail">

                            <div class="blog">
                                <div class="main-sidebar">
                                    <form method="get" action="<?= base_url('iniciar-reserva') ?>">
                                    <div class="reservation">
                                        <h2 id="mce_114">Fer una reserva</h2>                                        
                                        <div class="rate-from">
                                            <h5 id="mce_115">des de</h5>                                            
                                            <h1 id="mce_116"><span id="mce_117"><sup>€</sup></span>120</h1>                                            
                                            <h6 id="mce_118">per nit</h6>                                            
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6 cap col-centered center-block">
                                            <span id="mce_119"><img class="img-responsive" src="[base_url]img/cap.png" alt=""></span>
                                        </div>
                                        <div class="row input-daterange">
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <div id="datetimepicker1" class="input-group date">
                                            <input placeholder="DATA D\'ARRIBADA" name="desde" class="form-control" type="text" readonly="">
                                            <span class="input-group-addon mce-content-body" id="mce_109">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <div id="datetimepicker2" class="input-group date">
                                            <input placeholder="DATA SORTIDA" name="hasta" class="form-control" type="text" readonly="">
                                            <span class="input-group-addon mce-content-body" id="mce_110">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row date-picker">
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <select id="get_value" name="adultos" tabindex="0">
                                        <option value="">Adults</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select>
</div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <select id="get_value2" name="infantes" tabindex="0">
                                        <option value="">Nens</option>
                                        <option value="0">0</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select>
</div>
</div>
                                        <button type="submit" class="btn btn-default btn-availble">Veure disponibilitat</button>
                                    </div></form>
                                    <div class="sidebar-contact">
                                        <div class="detail">
                                            <h3 class="title mce-content-body" id="mce_124" spellcheck="false">Com contactar-nos?</h3>
                                            <input name="mce_124" type="hidden">
                                            <ul>
                                                <li>
                                                    <div class="img">
                                                        <i class="fa fa-phone"></i>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                                        <div class="row">
                                                            <span class="category mce-content-body" id="mce_125" spellcheck="false">telèfon</span><input name="mce_125" type="hidden"><input name="mce_125" type="hidden">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                                        <div class="row">
                                                            <p id="mce_126">
                                                                +34 650 93 64 00
                                                            </p>
                                                            
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="img">
                                                        <i class="fa fa-bell"></i>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                                        <div class="row">
                                                            <span class="category mce-content-body" id="mce_127" spellcheck="false">whats app</span><input name="mce_127" type="hidden"><input name="mce_127" type="hidden">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                                        <div class="row">
                                                            <p id="mce_128">
                                                                +34 650 93 64 00
                                                            </p>
                                                            
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="img">
                                                        <i class="fa fa-envelope"></i>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                                        <div class="row">
                                                            <span class="category mce-content-body" id="mce_129" spellcheck="false">email</span><input name="mce_129" type="hidden"><input name="mce_129" type="hidden">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                                        <div class="row">
                                                            <a href="mailto:info@fontainbleu.com" id="mce_130">info@calprat.net</a><input name="mce_130" type="hidden"><input name="mce_130" type="hidden">
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="img">
                                                        <i class="fa fa-phone"></i>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                                        <div class="row">
                                                            <span class="category mce-content-body" id="mce_131" spellcheck="false">adreça</span><input name="mce_131" type="hidden"><input name="mce_131" type="hidden">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                                        <div class="row">
                                                            <p id="mce_132">
                                                                El Soler, s/n <br>
                                                                CALONGE DE SEGARRA <br>
                                                                Barcelona

                                                            </p>
                                                            
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="subscribe" id="subscribir">
                                        <div class="title-block">
                                            <h3 class="title">Inscriu-te al butlletí
                                            </h3>
                                        </div>
                                        <div class="sub-detail">
                                            <p>Et mantindrem informat de les nostres ofertes i novetats. No serem pesats, només volem que estiguis a la última</p>
                                            [msg2]
                                            <form class="form-inline" method="post" action="<?= base_url('paginas/frontend/subscribir') ?>">
                                                <div class="form-group">
                                                    <input type="email" name="email" placeholder="escriu el teu email" id="exampleInputEmail2" class="form-control">
                                                </div>
                                                <button class="btn btn-default" type="submit">enviar</button>
                                            </form>
                                            <span class="promise">Ens comprometem a no enviar correu brossa.</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input name="mce_136" type="hidden"><input name="mce_137" type="hidden"><input name="mce_138" type="hidden"><input name="mce_51" type="hidden"><input name="mce_52" type="hidden"><input name="mce_53" type="hidden">
        </div>
        <div style="text-align: center; position: relative;">
            <input name="mce_54" type="hidden"><input name="mce_55" type="hidden"><input name="mce_56" type="hidden">
        </div>
        <div style="text-align:center;">
        </div>
    </section></section>