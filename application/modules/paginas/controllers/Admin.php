<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        } 
        
        function loadView($view = ''){
            if(is_string($view)){
                $output = $this->load->view($view,array(),TRUE);
                $view = array('view'=>'panel','crud'=>'user','output'=>$output);
            }
            parent::loadView($view);
        }
        
        function paginas($action = '',$id = ''){
            switch($action){
                case 'add':
                    $this->loadView('cms/add');
                break;
                case 'insert':
                    $this->form_validation->set_rules('nombre','Nombre','required');                    
                    if($this->form_validation->run()){
                        $content = '';
                        if(!empty($_POST['template'])){                            
                            $template = new DOMDocument();
                            @$template->loadHTML(file_get_contents('theme/'.$_POST['template']));                            
                            $xpath = new DOMXPath($template);
                            $xpath_resultset =  $xpath->query("//section[@id='content']");
                            $content = $template->saveHTML($xpath_resultset->item(0));
                            $content = str_replace('src="images/','src="'.base_url().'/img/',$content);
                            $content = str_replace('src="img/','src="'.base_url().'/img/',$content);                            
                        }
                        $content = $this->load->view('cms/empty',array('content'=>$content),TRUE);
                        file_put_contents('application/modules/paginas/views/'.$_POST['nombre'].'.php',$content);
                        header("Location:".base_url('paginas/frontend/editor/'.str_replace('.php','',$_POST['nombre'])));
                        exit;
                    }else{
                        header("Location:".base_url('paginas/admin/paginas/add?msj='.urlencode('Debe llenar los datos faltantes')));
                        exit;
                    }
                break;
                case 'edit':
                    if(!empty($_POST['data']) && !empty($id)){
                        $name = $_POST['name'];
                        file_put_contents('application/modules/paginas/views/'.$name.'.php',$_POST['data']);   
                    }
                break;
                case 'file_upload': 
                    $size = getimagesize($_FILES['image']['tmp_name']);
                    $extension = $_FILES['image']['type'];
                    $extension = explode('/',$extension);
                    $extension = count($extension>1)?$extension[1]:$extension[0];
                    $name = $id.'-'.date("dmHis").'.'.$extension;
                    if(move_uploaded_file($_FILES['image']['tmp_name'],'images/'.$name)){
                        echo json_encode(array('success'=>true,'name'=>$name,'size'=>array($size[0],$size[1])));
                    }else{
                        echo json_encode(array('success'=>false,'name'=>$name));
                    }
                break;
                case 'delete':
                    unlink('application/modules/paginas/views/'.$id);
                    redirect(base_url('paginas/admin/paginas'));
                break;
                default:
                    if(empty($action)){
                        $pages = scandir('application/modules/paginas/views');
                        $this->loadView(array('view'=>'panel','crud'=>'user','output'=>$this->load->view('cms/list',array('files'=>$pages),TRUE)));
                    }
                break;
                    
            }            
        }        

        function categoria_galeria($x = '',$y = ''){
            $crud = $this->crud_function('','');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function galeria($x = '',$y = ''){
            $crud = $this->crud_function('','');
            $crud->set_field_upload('foto','img/galeria');
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function ftp(){
            $this->loadView('cms/elfinder');
        }

        function verImg($connector = 0){
            if($connector==0){
                $this->load->view('cms/_elfinder_img');
            }else{
                require_once APPPATH.'libraries/elfinder/connector.minimal_img.php';                
            }
        }

        function verFtp($connector = 0){
            if($connector==0){
                $this->load->view('cms/_elfinder');
            }else{
                require_once APPPATH.'libraries/elfinder/connector.minimal.php';                
            }
        }

        function banner(){            
            $crud = $this->crud_function('','');
            $crud->set_field_upload('foto','img/Homepage_Resort_INDEX/');
            $crud->display_as('foto','Foto (2700x1173)');
            $crud = $crud->render();
            $this->loadView($crud);

        }

        function actividades(){
            $this->as['actividades'] = 'agenda';
            $crud = $this->crud_function('','');
            $crud->set_field_upload('foto','img/agenda');
            $crud = $crud->render();
            $this->loadView($crud);

        }


    }
?>
