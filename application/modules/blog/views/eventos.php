<?php 
	$view = $this->load->view($this->theme.'agenda',array(),TRUE,'paginas');
	$this->db->limit(2);
	$this->db->where('idioma',$_SESSION['lang']);
	$this->db->order_by('fecha','ASC');
	$eventos = $this->db->get_where('eventos');	
	if($eventos->num_rows()>0){
		foreach($eventos->result() as $n=>$v){
			$eventos->row($n)->foto = base_url('img/eventos/'.$v->foto);
			$eventos->row($n)->descripcion_corta = strip_tags($v->descripcion_corta);
			$eventos->row($n)->foto_grande = base_url('img/eventos/'.$v->foto_grande);

			$eventos->row($n)->tags = '<ul>';
			foreach(explode(',',$v->incluye) as $i){
				$eventos->row($n)->tags.= '<li>'.$i.'</i>';
			}
			$eventos->row($n)->tags.= '</ul>';
			$eventos->row($n)->fecha= date("d/m/Y",strtotime($v->fecha));
		}


		
	$this->db->limit(0,2);
	$this->db->order_by('fecha','ASC');
	$this->db->where('idioma',$_SESSION['lang']);
	$activitats = $this->db->get_where('eventos');	
	foreach($activitats->result() as $n=>$v){
		$activitats->row($n)->foto = base_url('img/eventos/'.$v->foto);
		$activitats->row($n)->descripcion_corta = strip_tags($v->descripcion_corta);
		$activitats->row($n)->foto_grande = base_url('img/eventos/'.$v->foto_grande);

		$activitats->row($n)->tags = '<ul>';
		foreach(explode(',',$v->incluye) as $i){
			$activitats->row($n)->tags.= '<li>'.$i.'</i>';			
		}
		$activitats->row($n)->descripcion_corta = substr($v->descripcion_corta,0,100);
		$activitats->row($n)->tags.= '</ul>';

		$activitats->row($n)->fecha= date("d/m/Y",strtotime($v->fecha));

	}

		
		$view = $this->querys->fillFields($view,array('eventos'=>$eventos,'activitats'=>$activitats));
		$view = str_replace('[base_url]',base_url(),$view);
	}

	echo $view;
?>