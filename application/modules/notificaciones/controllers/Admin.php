<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function notificaciones(){
            $crud = $this->crud_function('','');  
            $crud->set_clone();
            $crud->add_action('Enviar boletin','',base_url('notificaciones/admin/sendNewsletter').'/');
            $this->loadView($crud->render());
        }

        function mensajes(){
            $crud = $this->crud_function('','');  
            $crud->set_clone();            
            $this->loadView($crud->render());
        }

        function subscritos(){
            $crud = $this->crud_function('','');              
            $this->loadView($crud->render());
        }        

        function importador(){
            $crud = $this->crud_function('','');
            $crud->set_field_upload('fichero','files/excel'); 
            $crud->add_action('Importador','',base_url('notificaciones/admin/importar').'/');            
            $this->loadView($crud->render());
        }

        function importar($id){
            if(is_numeric($id)){
                $fichero = $this->db->get_where('importador',array('id'=>$id));
                if($fichero->num_rows()>0){
                    $fichero = $fichero->row();
                    require_once(APPPATH.'libraries/PHPExcel.php');
                    require_once(APPPATH.'libraries/PHPExcel/Reader/Excel2007.php');                    
                    $objReader = new PHPExcel_Reader_Excel2007();                    
                    $objPHPExcel = $objReader->load('files/excel/'.$fichero->fichero);
                    $objPHPExcel->setActiveSheetIndex(0);
                    $datos = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
                    if($fichero->reemplazo){
                        $this->db->delete('subscritos',array('id >'=>0));
                    }
                    foreach($datos as $d){
                        $columna = $fichero->columna;

                        if(!empty($d[$columna])){                            
                            $this->db->insert('subscritos',array('email'=>$d[$columna]));
                        }
                    }
                    redirect('notificaciones/admin/importador/success');
                }
            }
        }

        function sendNewsletter($id){
            if(is_numeric($id)){
                $boletin = $this->db->get_where('notificaciones',array('id'=>$id));
                if($boletin->num_rows()>0){
                    $enviados = '';
                    $content = file_get_contents('files/boletin1/content.html');                    
                    $content = str_replace('[base_url]',base_url(),$content);
                    $content = str_replace('[id]',$id,$content);
                    foreach($this->db->get_where('subscritos')->result() as $s){
                        $cont = str_replace('[email]',$s->email,$content);                        
                        correo($s->email,$boletin->row()->titulo,$cont);
                        $enviados.= 'Correo enviado a <b>'.$s->email.'</b><br/>';
                    }
                }
                echo $enviados;
                echo $content;
            }
        }
    }
?>
