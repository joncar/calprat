<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Inventarios extends Panel{
        function __construct() {
            parent::__construct();
        }
        function inventario(){
            $crud = $this->crud_function('','');
            $crud = $crud->render();
            $this->loadView($crud);
        }                
    }
?>
