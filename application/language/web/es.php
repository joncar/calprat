<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$lang = array(
    'adreça'=>'dirección',
    'telèfon'=>'teléfono',
    'Activitats'=>'Actividades',
    'Notícies'=>'Noticias',
    'Galeria'=>'Galería',
    'últimes notícies'=>'últimas noticias',
    'Informa\'t'=>'infórmate',
    'Subscriu-te al butlletí'=>'Subscríbete al boletín',
    'EL TEU EMAIL'=>'tu email',
    'escriu el teu email'=>'escribe tu email',
    'xarxes socials'=>'redes sociales',
    'què t\'ha semblat?'=>'qué te ha parecido?',
    'Siusplau deixa\'ns el teu comentari'=>'Porfavor déjanos tu comentario',
    'Contacta\'ns'=>'contáctanos',
    'Nom'=>'NOMBRE',
    'Telèfon'=>'teléfono',
    'Avís Legal'=>'Aviso Legal',
    'Missatge'=>'mensaje',
    'He llegit i accepto la'=>'He leído y acepto la',
    'Inici'=>'Inicio',
    'INICI'=>'INICIO',
    'PUJAR'=>'SUBIR',
    'CONTACTE'=>'CONTACTO',
    'Qui som?'=>'Quien somos?',
    'Allotjaments'=>'Alojamientos',
    'Espais'=>'Espacios',
    'Contacte'=>'Contacto',
    'El pati'=>'El Patio',
    'Sala de jocs'=>'Sala de juegos',
    'Reserva Ara'=>'Reserva ahora',
    'Què són les Cookies?'=>'Qué son las cookies?',
    'Accepto les Cookies'=>'Acepto las cookies',
    'On es fa?'=>'Donde se celebra?',
    'Utilitzem cookies pròpies i de tercers per millorar els nostres serveis. Si continua navegant, considerem que accepta el seu ús.'=>'
Utilizamos cookies propias y de terceros para mejorar nuestros servicios. Si continúa navegante, consideramos que acepta su uso.',
);
