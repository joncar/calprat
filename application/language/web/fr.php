<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$lang = array(
    'adreça'=>'adresse',
    'telèfon'=>'Téléphone',
    'Activitats'=>'Activités',
    'Notícies'=>'Informations',
    'Galeria'=>'Galerie',
    'últimes notícies'=>'dernières nouvelles',
    'Informa\'t'=>'découvrir',
    'Subscriu-te al butlletí'=>'Abonnez-vous à la newsletter',
    'EL TEU EMAIL'=>'email',
    'xarxes socials'=>'réseaux sociaux',
    'què t\'ha semblat?'=>'qu\'en pensez vous?',
    'Siusplau deixa\'ns el teu comentari'=>'S\'il vous plaît laissez-nous votre commentaire',
    'Contacta\'ns'=>'contactez nous',
    'Nom'=>'nom',
    'Preus'=>'prix',
    'Què fer'=>'QUE FAIRE',
    'política de privacitat'=>'politique privée',
    'Avís Legal'=>'Avis juridique',
    'Missatge'=>'message',
    'He llegit i accepto la'=>'J\'ai lu et accepte le',
    'Inici'=>'Home',
    'Qui som?'=>'Qui sommes-nous?',
    'Allotjaments'=>'Logements',
    'Espais'=>'Espaces',
    'Contacte'=>'Contact',
    'El pati'=>'La Cour',
    'Sala de jocs'=>'Salle de jeux',
    'La piscina'=>'La piscine',
    'Reserva Ara'=>'Réserver',
    'On es fa?'=>'Donde se celebra?',
    'Utilitzem cookies pròpies i de tercers per millorar els nostres serveis. Si continua navegant, considerem que accepta el seu ús.'=>'Utilizamos cookies propias y de terceros para mejorar nuestros servicios. Si continúa navegante, consideramos que acepta su uso.',
);
