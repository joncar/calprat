<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$lang = array(
     'adreça'=>'address',
    'telèfon'=>'phone',
    'Activitats'=>'Activities',
    'Notícies'=>'News',
    'Cognoms'=>'Surname',
    'Tema'=>'Subject',
    'política de privacitat'=>'political of privacy',
    'La piscina'=>'The pool',
    'Galeria'=>'Gallery',
    'últimes notícies'=>'last news',
    'Informa\'t'=>'inform',
    'Subscriu-te al butlletí'=>'Subscribe newsletter',
    'EL TEU EMAIL'=>'your email',
    'escriu el teu email'=>'write your email',
    'xarxes socials'=>'social network',
    'què t\'ha semblat?'=>'What do you think?',
    'Siusplau deixa\'ns el teu comentari'=>'Please leave you comment',
    'Contacta\'ns'=>'contact us',
    'Nom'=>'Name',
    'Enviar'=>'Send',
    'Telèfon'=>'phone',
    'Avís Legal'=>'Legal advice',
    'Missatge'=>'message',
    'He llegit i accepto la'=>'I read and accept',
    'Inici'=>'Home',
    'INICI'=>'HOME',
    'PUJAR'=>'GO UP',
    'CONTACTE'=>'CONTACT',
    'RESERVAR'=>'BOOK',
    'Qui som?'=>'About us',
    'Allotjaments'=>'Accommodations',
    'Espais'=>'Spaces',
    'Contacte'=>'Contact',
    'Agenda'=>'Events',
    'La Piscina'=>'Pool',
    'El pati'=>'The terrace',
    'Sala de jocs'=>'Room of games',
    'Reserva Ara'=>'Book now',
    'Què són les Cookies?'=>'Qué son las cookies?',
    'Accepto les Cookies'=>'Acepto las cookies',
    'On es fa?'=>'Donde se celebra?',
    'Utilitzem cookies pròpies i de tercers per millorar els nostres serveis. Si continua navegant, considerem que accepta el seu ús.'=>'
Utilizamos cookies propias y de terceros para mejorar nuestros servicios. Si continúa navegante, consideramos que acepta su uso.',
);
