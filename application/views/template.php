<!DOCTYPE html>
<!--[if IE 8]>          <html class="ie ie8"> <![endif]-->
<!--[if IE 9]>          <html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->  <html> <!--<![endif]-->
<head>


    <meta charset="utf-8">
    <title><?= empty($title) ? 'Calprat' : $title ?></title>
    <meta name="keywords" content="<?= empty($keywords) ?'': $keywords ?>" />
    <meta name="description" content="<?= empty($keywords) ?'': $description ?>" />     
    <link rel="icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>    
    <link rel="shortcut icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">    

    <!-- Google Webfont -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,500,400italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Vollkorn:400,400italic,700,700italic' rel='stylesheet' type='text/css'>

    <!-- CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>css/template/bootstrap.css">
    <link rel="stylesheet" href="<?= base_url() ?>js/template/uikit/css/uikit.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>css/template/fonts/fonts.css">
    <link rel="stylesheet" href="<?= base_url() ?>css/template/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="<?= base_url() ?>js/template/isotope/isotope.css">
    <link rel="stylesheet" href="<?= base_url() ?>js/template/flexslider/flexslider.css">
    <link rel="stylesheet" href="<?= base_url() ?>js/template/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" href="<?= base_url() ?>js/template/owl-carousel/owl.theme.css">
    <link rel="stylesheet" href="<?= base_url() ?>css/template/sidebar-calendar/css/style.css">
    <link rel="stylesheet" href="<?= base_url() ?>js/template/selectric-selectbox/selectric.css">
    <link rel="stylesheet" href="<?= base_url() ?>js/template/datetimepicker/bootstrap-datetimepicker.css">
    <link rel="stylesheet" href="<?= base_url() ?>js/template/superfish/css/megafish.css" media="screen">
    <link rel="stylesheet" href="<?= base_url() ?>js/template/superfish/css/superfish.css" media="screen">
    <link rel="stylesheet" href="<?= base_url() ?>js/template/stepper/jquery.fs.stepper.css">
    <link rel="stylesheet" href="<?= base_url() ?>css/template/prettyphoto.css">
    <link rel="stylesheet" href="<?= base_url() ?>css/template/bootstrap.datepicker.css">
    <link href="<?= base_url() ?>js/stocookie/stoCookie.css" rel="stylesheet">
    <link rel="stylesheet" href="<?= base_url() ?>css/template/style.css?v=1.1">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src='https://www.google.com/recaptcha/api.js?hl=ca'></script>
</head>
<body class="pages">

<div class="main-preloader">
    <div class="ball-scale">
        <div></div>
    </div>
</div>

<div id="page-top"></div>
<div id="notice">
    <?= l('notice') ?>
</div>
<!-- STICKY MENU -->
<div class="sticky-menu">
    <ul>
        <li data-toggle="tooltip" data-placement="right" title="INICI"><a href="<?= base_url() ?>"><i class="fa fa-home"></i></a></li>
        <li data-toggle="tooltip" data-placement="right" title="PUJAR"><a class="fa fa-arrow-up" href="#page-top"></a></li>
        <li data-toggle="tooltip" data-placement="right" title="RESERVAR"><a href="<?= base_url('iniciar-reserva') ?>" class="fixed-bn"><i class="fa fa-calendar-o"></i></a></li>
        <li id="sm-share">
            <a><i class="fa fa-share-alt"></i></a>
            <span class="sm-share">
            <a href="http://www.facebook.com/sharer.php?u=<?= base_url() ?>&amp;t=Calprat" class="fa fa-facebook"></a>
            <a href="http://twitter.com/home/?status=<?= base_url() ?>" class="fa fa-twitter"></a>
            <a href="https://plus.google.com/share?url=<?= base_url() ?>" class="fa fa-google-plus"></a>
            <a href="http://www.linkedin.com/shareArticle?mini=true&amp;title=Calprat&amp;url=<?= base_url() ?>" class="fa fa-linkedin"></a>
            </span>
        </li>        
        <li data-toggle="tooltip" data-placement="right" title="CONTACTE"><a href="<?= base_url('p/contacte') ?>" class="page-scroll"><i class="fa fa-envelope-o"></i></a></li>
    </ul>
</div>

<div id="wrapper">
    <!-- HEADER -->
    <?= $this->load->view('includes/template/header') ?>
    <!-- SLIDER -->
    <?php $this->load->view($view); ?>
    <?php $this->load->view('includes/template/footer'); ?>
</div>
<?php if($this->router->fetch_method()!='editor'): ?>
<!-- BOOK NOW POPUP -->
<div class="bn-reserve">
    <div class="bw-cal">
        <select id="sel_property">
            <option value="">Select Property</option>
            <option value="">Individual Villa</option>
            <option value="">Deluxe Room</option>
            <option value="">VIP Apartments</option>
        </select>
        <form>
            <div class="arrive-date">
                <h5>Arriving</h5>
                <i class="fa fa-calendar"></i>
                <span class="ad-month">April<br>2015</span>
                <input type="number" value="25" min="1" max="30" step="1" />
            </div>
            <div class="arrive-date">
                <h5>Departing</h5>
                <i class="fa fa-calendar"></i>
                <span class="ad-month">April<br>2015</span>
                <input type="number" value="30" min="1" max="30" step="1" />
            </div>
            <div class="arrive-date">
                <h5>Rooms</h5>
                <input type="number" value="1" min="1" max="300" step="1" />
            </div>
            <div class="people-count">
                <h5>Guests</h5>
                <div class="arrive-date">
                    <span class="ad-month">Adults</span>
                    <input type="number" value="2" min="1" max="300" step="1" />
                </div>
                <div class="arrive-date">
                    <span class="ad-month">Children</span>
                    <input type="number" value="0" min="1" max="300" step="1" />
                </div>
            </div>
        </form>
        <button type="submit" class="btn btn-default btn-availble">check availability</button>
    </div>
    <div class="bn-close fa fa-times"></div>
</div>
<div class="bn-overlay"></div>

<!-- Javascript -->
<script>
    var URL = '<?= base_url() ?>';
    var player;
</script>
<a href="https://wa.me/34650936400?text=Hola,+vull+contactar+amb+vosaltres!" style="font-size:18px;padding:18px 18px;text-decoration:none;background-color:#189D0E;color:white;text-shadow:none;position:fixed;bottom:0px;right:0px;z-index: 10000"> <i class="fa fa-whatsapp fa-2x"></i> Contacta'ns</a>
<script src="<?= base_url() ?>js/template/jquery.min.js"></script>
<script src="<?= base_url() ?>js/template/bootstrap.min.js"></script>
<script src="<?= base_url() ?>js/template/bootstrap.datepicker.js"></script>
<script src="<?= base_url() ?>js/template/jquery.easing.min.js"></script>
<script src="<?= base_url() ?>js/template/superfish/js/hoverIntent.js"></script>
<script src="<?= base_url() ?>js/template/superfish/js/superfish.js"></script>
<script src="<?= base_url() ?>js/template/flexslider/jquery.flexslider.js"></script>
<script src="<?= base_url() ?>js/template/owl-carousel/owl.carousel.min.js"></script>
<script src="<?= base_url() ?>js/template/selectric-selectbox/jquery.selectric.js"></script>
<script src="<?= base_url() ?>js/template/datetimepicker/moment.min.js"></script>
<script src="<?= base_url() ?>js/template/datetimepicker/bootstrap-datetimepicker.js"></script>
<script src="<?= base_url() ?>js/template/isotope/isotope.pkgd.js"></script>
<script src="<?= base_url() ?>js/template/stepper/jquery.fs.stepper.min.js"></script>
<script src="<?= base_url() ?>js/template/jquery.prettyphoto.js"></script>
<script src="<?= base_url() ?>js/template/tweecool.js"></script>
<script id="scriptMain" src="<?= base_url() ?>js/template/main.js"></script>
<script src="<?= base_url() ?>js/template/contact.js"></script>
<script src="https://maps.google.com/maps/api/js?key=AIzaSyDyPEFmiS1aSSx_fpoB5US78NBVI2pmRjc&libraries=places"></script>
<script src="<?= base_url() ?>js/template/gmap/gmaps.js"></script>
<script src="<?= base_url() ?>js/template/gmap/main.js"></script>
<script src="<?= base_url() ?>js/template/uikit/js/uikit.min.js"></script>
<script>
  $(document).on('ready',function(){
    $(".footerbtn").on('click',function(){
        $("#recaptchaDiv").show();
        $(this).css('margin-top','36px');
        $(this).attr('type','submit');
    });
  });

  function contacto(form){
    formData = new FormData(form);
    $.ajax({
        url: '<?= base_url() ?>paginas/frontend/contacto',
        data: formData,
        context: document.body,
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST',
        success:function(data){
            $("#mensajeFooter").html(data);
        }
    });
    return false;
  }



    var tag = document.createElement('script');
    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementById('scriptMain');
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    function onYouTubeIframeAPIReady() { 
        player = new YT.Player('thevideo', {
          height: '323',
          width: '555',
          videoId: 'QH_IXokfyBA'          
        });
      }
</script>
<?php endif ?>
</body>
</html> 