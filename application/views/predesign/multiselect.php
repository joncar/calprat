<link rel="stylesheet" type="text/css" href="<?= base_url().'assets/grocery_crud/css/ui/simple/jquery-ui-1.10.1.custom.min.css' ?>">
<link rel="stylesheet" type="text/css" href="<?= base_url().'assets/grocery_crud/css/jquery_plugins/ui.multiselect.css' ?>">
<script src="<?=  base_url().'assets/grocery_crud/js/jquery_plugins/ui/jquery-ui-1.10.3.custom.min.js' ?>"></script>
<script src="<?=  base_url().'assets/grocery_crud/js/jquery_plugins/ui.multiselect.min.js' ?>"></script>
<script src="<?=  base_url().'assets/grocery_crud/js/jquery_plugins/config/jquery.multiselect.js' ?>"></script>
<script>
    $(document).ready(function(){
        $(".fieldmultiselect").multiselect();
    })
</script>