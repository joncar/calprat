<!-- 404 -->
<section id="content" class="blog">
	<div class="container">
		<div class="error-page">
			<div class="error-bg">
				<div class="error-img"></div>
				<div class="col-md-6 no-padding">
				</div>
				<div class="col-md-6 no-padding" style="background: #333333; color:white;">
					<div class="error-detial">
						
						<div class="col-xs-12 col-sm-12 col-md-12 col-centered center-block">
							<!-- Row Start -->
							<div class="row">
								<div class="title">
									<h1 style="color:white">Eliminar la subscripció al butlletí</h1>
									<div class="col-xs-4 col-sm-4 col-md-4 cap col-centered center-block">
										<span><img class="img-responsive" src="http://hipo.tv/calprat/img/cap.png" alt=""></span>
									</div>
								</div>
							</div>
							<!-- Row End -->
						</div>
						
							<?php if(!empty($success)): ?>
							<div class="alert alert-success">								
								S'ha donat de baixa satisfactòriament, ja no li arribaran els correus informatius al seu email
							</div>
							<?php else: ?>
							<?php if(isset($success)): ?>
							<div class="alert alert-danger">
								El correu que desitja donar de baixa no està registrat
							</div>
							<?php endif ?>
							<p style="color:white; padding-bottom: 0px;">
								Si vols donar-se de baixa de les notificacions electròniques, omple el formulari al teu email
							</p>
							<form onsubmit="return confirm('Està vostè segur que vol donar de baixa el seu correu?')" method="post">
								<div class="reservation" style="border:0px;">
									<div class="">
										<div class="col-xs-12">
											<div class="form-group">
												<div class="input-group date" id="datetimepicker1" style="width:100%">
													<input style="width:100%" class="form-control" placeholder="Email" type="text">													
												</div>
											</div>
											<button class="btn btn-default btn-availble" type="submit">Donar de baixa</button>
										</div>										
									</div>
								</form>
								
								<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>