<!-- JS Include Section -->
<script src="<?= base_url() ?>js/template/jquery-3.1.0.min.js"></script>
<script src="<?= base_url() ?>js/template/helper.js"></script>
<script src="<?= base_url() ?>js/template/owl.carousel.min.js"></script>
<script src="<?= base_url() ?>js/template/select2.min.js"></script>
<script src="<?= base_url() ?>js/template/imagesloaded.pkgd.min.js"></script>
<script src="<?= base_url() ?>js/template/isotope.pkgd.min.js"></script>
<script src="<?= base_url() ?>js/template/jquery.magnific-popup.min.js"></script>
<script src="https://maps.google.com/maps/api/js?key=AIzaSyDyPEFmiS1aSSx_fpoB5US78NBVI2pmRjc&libraries=places"></script>
<script src="<?= base_url() ?>js/template/template.js"></script>
<script src='https://www.google.com/recaptcha/api.js?hl=ca'></script>
<script>    
    $(document).ready(function(){      
       if(typeof($("#full-payment").val())!=='undefined' && typeof($("#deposit").val())!=='undefined'){
           $("#full-payment, #deposit").on("click",function(){
               $("#abono").val($(this).val());
           });
       }
    });
</script>
<script>
function subscribir(){
  $.post('<?= base_url('paginas/frontend/subscribir') ?>',{email:$("#emailSub").val()},function(data){
      $("#subsmessage").html(data);
      $("#subsmessage").show();
  });
  return false;
}
</script>
