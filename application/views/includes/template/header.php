<div class="pre-header <?= $this->router->fetch_method()=='index'?'':'ph-hide' ?>">
        <ul>
            <li class="ph-tel">
            <div class="dropdown">
              <a style="color: #AF9A5F;" id="dLabel" data-target="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-flag-o fa-2x" style="vertical-align: top; font-size:1.3em"></i> <?= $_SESSION['langLabel'] ?>
                <span class="caret"></span>
              </a>

              <ul class="dropdown-menu" aria-labelledby="dLabel">
                <li><a href="<?= base_url('main/traduccion/ca') ?>">CAT</a></li>
                <li><a href="<?= base_url('main/traduccion/es') ?>">ESP</a></li>
                <li><a href="<?= base_url('main/traduccion/en') ?>">ENG</a></li>
                <li><a href="<?= base_url('main/traduccion/fr') ?>">FRA</a></li>
              </ul>
            </div>
        </li>
            <li class="ph-tel"><a href="tel:+34650936400" style="color:inherit;"><img src="<?= base_url() ?>img/phone.png" alt=""/> <span>+34 650 93 64 00</span></a></li>
            <li class="ph-weather"><img src="[iconotemp]" alt="" /> <span>[temp] &deg;</span></li>
            <li class="ph-social">
                <div>                    
                    <a href="https://www.facebook.com/Cal-Prat-TurismeRural-143945365955474/" class="fa fa-facebook"></a>
                    <a href="https://www.pinterest.com/username" class="fa fa-pinterest"></a>
                    <a href="https://www.youtube.com/channel/UCrMyhl_KCzMONdNgmyJJmAA" class="fa fa-youtube"></a>
                    <a href="https://www.instagram.com/calprat_turismerural/?hl=es" class="fa fa-instagram"></a>
                </div>
            </li>
            <li class="ph-search">
                <span class="ss-trigger fa fa-search"></span>
                <div class="ss-content">
                    <span class="ss-close fa fa-times"></span>
                    <div class="ssc-inner">
                        <form action="<?= base_url('paginas/frontend/buscar') ?>">
                            <input type="text" name="q" placeholder="Buscar">
                            <button type="submit"><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                </div>
            </li>
        </ul>
        <div class="biosphere"><img src="<?= base_url() ?>img/biosphere.svg"></div>
    </div>
<!-- HEADER -->
    <header class="header">
        <div class="biosphere visible-sm" style="padding: 9px 20px;">
            <img src="<?= base_url() ?>img/biosphere.svg">
        </div>
        <div class="logo">
            <a href="<?= base_url() ?>">
                <img src="<?= base_url() ?>img/logo.png" class="img-responsive" alt=""/>                
            </a>
        </div>
        <div class="dropdown idiomamenu" style="">
          <a style="color: #fff;" id="dLabel" data-target="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
            <?= $_SESSION['langLabel'] ?>
            <span class="caret"></span>
          </a>

          <ul class="dropdown-menu" aria-labelledby="dLabel">
            <li><a href="<?= base_url('main/traduccion/ca') ?>">CAT</a></li>
            <li><a href="<?= base_url('main/traduccion/es') ?>">ESP</a></li>
            <li><a href="<?= base_url('main/traduccion/en') ?>">ENG</a></li>
            <li><a href="<?= base_url('main/traduccion/fr') ?>">FRA</a></li>
          </ul>
        </div>
        <div class="menu-trigger <?= $this->router->fetch_class()!='main'?'mt-hide':'' ?>"><i class="fa fa-bars"></i></div>
        <nav class="navbar navbar-default <?= $this->router->fetch_class()!='main'?'nav-active':'' ?>">
            <div class="navheader">
                <div class="pull-right">
                    <div class="head-search">
                        <i class="ss-trigger fa fa-search"></i>
                        <div class="ss-content">
                            <span class="ss-close fa fa-times"></span>
                            <div class="ssc-inner">
                                <form action="<?= base_url('paginas/frontend/buscar') ?>">
                                    <input type="text" name="q" placeholder="Què busques?">
                                    <button type="submit"><i class="fa fa-search"></i></button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="book-nw">
                        <div class="bn-trigger">Reserva Ara</div>
                    </div>
                    <div class="menu-close"><i class=" fa fa-times"></i></div>
                </div>
                <!-- Navmenu -->
                <div class="menu-wrap pull-right">
                    <div id="mobnav-btn"><i class="fa fa-bars"></i></div>
                    <ul class="sf-menu">
                        <li>
                            <a href="<?= base_url() ?>main.html">Inici</a>
                        </li>
                        <li>
                            <a href="<?= base_url() ?>p/qui_som.html">Qui som?</a>                            
                        </li>
                        <li>
                            <a href="<?= base_url() ?>habitacions">Allotjaments</a>
                            <div class="mobnav-subarrow"><i class="fa fa-plus"></i></div>
                            <ul>
                                <?php foreach($this->db->get_where('habitaciones')->result() as $h): ?>
                                <li><a href="<?= base_url() ?>habitacion/<?= toUrl($h->id.'-'.$h->habitacion_nombre) ?>"><?= $h->habitacion_nombre ?></a></li>
                                <?php endforeach ?>                                
                            </ul>
                        </li>
                        <li>
                            <a>Espais</a>
                            <div class="mobnav-subarrow"><i class="fa fa-plus"></i></div>
                            <ul>
                                <li><a href="<?= base_url() ?>p/la_piscina.html">La piscina</a></li>
                                <li><a href="<?= base_url() ?>p/El_pati.html">El pati</a></li>
                                <li><a href="<?= base_url() ?>p/sala-de-jocs.html">Sala de jocs</a></li>                                
                            </ul>
                        </li>
                        <li>
                            <a href="#">Què fer</a>
                            <div class="mobnav-subarrow"><i class="fa fa-plus"></i></div>
                            <ul>                                
                                <!-- <li><a href="<?= base_url('agenda') ?>">Agenda</a></li>  -->   
                                <li><a href="<?= base_url() ?>p/activitat.html">Activitats</a></li>
                            </ul>
                        </li>
                        <li><a href="<?= base_url() ?>p/galeria.html">Galeria</a></li>
                        <li><a href="<?= base_url() ?>precios">Preus</a></li>
                        <li><a href="<?= base_url('blog') ?>">Notícies</a></li>
                        <li><a href="<?= base_url() ?>p/contacte.html">Contacte</a></li> 
                    </ul>
                </div>
            </div>
        </nav>
    </header>