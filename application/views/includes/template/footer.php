<!-- FOOTER -->
<footer id="footer">
    <div class="container">
        <div class="footer">
            <div class="row">
                <div class="col-xs-12 col-sm-3 col-md-3">
                    <div class="f-block1">
                        <a href="<?= base_url() ?>">
                            <img class="img-responsive" src="<?= base_url() ?>img/logo.svg" alt="">
                        </a>
                        <ul>
                            <li>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-4">
                                        <span class="specific">adreça</span>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-8">
                                        <p> El Soler, s/n <br>CALONGE DE SEGARRA <br>Barcelona</p>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-4">
                                        <span class="specific">Telèfon</span>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-8">
                                        <p><a href="tel:+34650936400">+34 650 93 64 00</a></p>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <!-- <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-4">
                                        <span class="specific">fax</span>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-8">
                                        <p>(415) 899-3457</p>
                                    </div>
                                </div>-->
                            </li>
                            <li>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-4">
                                        <span class="specific">email</span>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-8">
                                        <a href="mailto:info@calprat.net">info@calprat.net</a>
                                    </div>
                                </div>
                            </li>                        
                    </ul>
                    <img src="<?= base_url() ?>img/segell.png" style="margin-bottom: 38px; margin-top: 18px"alt="">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3">
                    <div class="f-block2">
                        <div class="links">
                            <h4 class="title">web links</h4>
                            <ul>
                                <li><a href="<?= site_url('agenda') ?>">Agenda</a></li>
                                <li><a href="<?= site_url('p/el_pati') ?>">El pati</a></li>
                                <li><a href="<?= site_url('p/la_piscina') ?>">La Piscina</a></li>
                                <li><a href="<?= site_url('p/activitat') ?>">Activitats</a></li>
                                <li><a href="<?= site_url('blog') ?>">Notícies</a></li>
                                <li><a href="<?= site_url('p/galeria') ?>">Galeria</a></li>
                            </ul>
                        </div>
                        <div class="links">
                            <h4 class="title">últimes notícies</h4>
                            <ul>
                                <?php 
                                    $this->db->limit(2);
                                    $this->db->order_by('id','DESC');
                                    foreach($this->db->get_where('blog',array('idioma'=>$_SESSION['lang']))->result() as $b): ?>
                                    <li style="text-overflow: ellipsis;overflow: hidden; margin: 0 0 20px 0;">
                                        <a href="<?= site_url('blog/'.toUrl($b->id.'-'.$b->titulo)) ?>">
                                            <img src="<?= base_url('img/blog/'.$b->foto) ?>" style="width:60px; vertical-align: top; margin-right: 5px;">
                                            <div style="width:111px; display: inline-block;margin-top: 10px;"><?= $b->titulo ?></div>
                                        </a>
                                    </li>
                                <?php endforeach ?>                                    
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3">
                    <div class="f-block3">
                        <form action="<?= base_url('paginas/frontend/subscribir') ?>" method="post" onsubmit="if($('#recaptchaDivBolleti').css('display')=='none'){$('#recaptchaDivBolleti').css('display','block'); return false;}">
                            <div class="date">
                                <h4 class="title">Informa't</h4>
                                <p>Subscriu-te al butlletí</p>
                                
                                <div class="input-group">
                                        <input type="email" placeholder="EL TEU EMAIL" class="form-control footer-nl" name="email">
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn btn-default"><i class="fa fa-envelope"></i></button>
                                        </span>
                                    
                                </div>
                                <div id="recaptchaDivBolleti" class="form-group" style="display: none">
                                    <div class="g-recaptcha"  data-theme="dark" data-sitekey="6LdSNGAUAAAAAPFcFDPrON0NKwHLnN0GeL3ilX8s" data-callback='showCaptcha'></div>
                                </div>
                                
                            </div>
                        </form>
                        <div class="social">
                            <h4 class="title">xarxes socials</h4>
                            <ul>                                
                                <li><a href="https://www.facebook.com/Cal-Prat-TurismeRural-143945365955474/"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="https://www.pinterest.com/username"><i class="fa fa-pinterest"></i></a></li>
                                <li><a href="https://www.instagram.com/calprat_turismerural/?hl=es"><i class="fa fa-instagram"></i></a></li>                                
                                <li><a href="http://www.youtube.com/watch?v=QH_IXokfyBA"><i class="fa fa-youtube"></i></a></li>
                            </ul>
                        </div>
                        <div class="enjoy">
                            <h4 class="title">què t'ha semblat?</h4>
                            <p>Siusplau deixa'ns el teu comentari
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3">
                    <div class="f-block4">
                        <h4 class="title">Contacta'ns</h4>
                        <div class="form">
                            <div id="mensajeFooter"></div>
                            <form id="footercontacto" onsubmit="return contacto(this)" action="<?= base_url('paginas/frontend/contacto') ?>" method="post">
                                <div class="form-group">
                                    <input name="nombre" id="senderName" type="text" class="form-control" placeholder="Nom" required="required" />
                                </div>
                                <div class="form-group">
                                    <input type="email" name="email" id="senderEmail" class="form-control" placeholder="Email " required="required" />
                                </div>
                                <div class="form-group">
                                    <input type="text" name="telefono" id="senderEmail" class="form-control" placeholder="Telèfon " required="required" />
                                </div>
                                <div class="form-group">
                                    <textarea name="message" id="message" class="form-control" rows="3" placeholder="Missatge"></textarea>
                                </div>
                                <div class="form-group">
                                    <p><input type="checkbox" name="politicas" value="1"> He llegit i accepto la <a href="<?= base_url('p/avis-legal') ?>" target="_new">política de privacitat* </a></p>
                                </div>
                                <div id="recaptchaDiv" class="form-group" style="display: none">
                                    <div class="g-recaptcha"  data-theme="dark" data-sitekey="6LdSNGAUAAAAAPFcFDPrON0NKwHLnN0GeL3ilX8s" data-callback='showCaptcha'></div>
                                </div>
                                <input type="hidden" name="ajax" value="1">
                                <button type="button" class="footerbtn btn btn-default">Enviar</button>
                            </form>
                            <div id="sendingMessage" class="statusMessage">
                                <p>S'està enviant el missatge. Espereu ...</p>
                            </div>
                            <div id="successMessage" class="successmessage">
                                <p>Thanks for sending your message! We'll get back to you shortly.</p>
                            </div>
                            <div id="failureMessage" class="errormessage">
                                <p>Gràcies per enviar el teu missatge! Us tornarem a contactar en breu.</p>
                            </div>
                            <div id="incompleteMessage" class="statusMessage">
                                <p>Completeu tots els camps del formulari abans d'enviar.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 cap col-centered center-block">
            <span><img class="img-responsive" src="<?= base_url() ?>img/cap.png" alt=""></span>
        </div>
        <div class="footer-bot">
            <ul>
                <li><a href="<?= base_url('p/avis-legal') ?>">Avís Legal</a></li>
                <li><a href="<?= base_url('p/condicions') ?>">Condicions de Reserva</a></li>
            </ul>
            <p>&copy; Cal Prat  2018. Tots els drets reservats.</p>
        </div>
    </div>
</footer>